from mesh import *
from quad import *
from basis import *
from functionspace import *
#import copy
import instant
#import os
from codegenerator import *
#import petsc4py
import time
from common import *
from mpi4py import MPI
import numpy
#from field import *

def two_form_preallocate_opt(mesh,matrix,space1,space2,s1i,s2i):
	
	#nrows = space1.get_localndofs(s1i)
	#dnnzarr = np.zeros((nrows),dtype=np.int32)
	#onnzarr = np.zeros((nrows),dtype=np.int32)
	elem1 = space1.get_elem(s1i)
	elem2 = space2.get_elem(s2i)

	space1_ranges = space1.get_xy(s1i)
	space2_ranges = np.array(space2.get_xy(s2i),dtype=np.int32)
	nx1s = space1.get_local_nxny(s1i)
	#nx2s = space2.get_nxny(s2i)
	
	swidth = space2.get_da(s2i).getStencilWidth()
	xyzmaxs_space2 = space2.get_nxny(s2i)

	#adjust space2 ranges to account for domain boundaries
	#space2_ranges[0] = list(space2_ranges[0])
	#if space2_ranges[0][0] == 0:
	#	space2_ranges[0][0] = space2_ranges[0][0] - swidth
	#if space2_ranges[0][1] == xyzmaxs_space2[0]:
#		space2_ranges[0][1] = space2_ranges[0][1] + swidth 
	#print space2_ranges
	
	#if mesh.ndims >= 2:
		#if space2_ranges[1][0] == 0:
			#space2_ranges[1][0] = space2_ranges[1][0] - swidth 
		#if space2_ranges[1][1] == xyzmaxs_space2[1]:
			#space2_ranges[1][1] = space2_ranges[1][1] + swidth 
		
	#if mesh.ndims == 3:
		#if space2_ranges[2][0] == 0:
			#space2_ranges[2][0] = space2_ranges[2][0] - swidth
		#if space2_ranges[2][1] == xyzmaxs_space2[2]:
			#space2_ranges[2][1] = space2_ranges[2][1] + swidth 
			
	#CAN I OPTIMIZE THE USE OF IMOD FOR NON-PERIODIC BOUNDARIES- IE IMOD(X,N) = X
	#MAYBE, BUT IT PROBABLY REQUIRE INTERACTION OFFSET BLOCKS FOR CG/DG ELEMENTS
	#SO MAYBE NOT WORTH IT...
	
	#clip values to a range
	py_clip = lambda x, l, u: l if x < l else u if x > u else x

	#compute i bounds
	nxcell = mesh.nxs[0] 
	dnnz_x = np.zeros((nx1s[0]),dtype=np.int32) 
	nnz_x = np.zeros((nx1s[0]),dtype=np.int32) 
	icells = elem1.get_sub_elem(0).get_interaction_cells(nxcell,mesh.bcs[0])
	leftmostcells = icells[:,0]
	rightmostcells = icells[:,1]
	#print leftmostcells
	#print rightmostcells
	#print 'ranges',space2_ranges[0][0],space2_ranges[0][1],space2_ranges[0][1]-1
	#print  np.array(space2.get_xy(s2i),dtype=np.int32),swidth
	for i in xrange(space1_ranges[0][0],space1_ranges[0][1]):
		#ioffsets = elem1.get_sub_elem(0).get_interaction_offsets(i)[0] #get interaction offsets for space1- these are the CELLS that this basis function is non-zero in
		#leftmostcell = ioffsets[0] + i
		#rightmostcell = ioffsets[-1] + i
		#print i
		leftmostcell = leftmostcells[i]
		rightmostcell = rightmostcells[i]
		leftmost_offsets = elem2.get_sub_elem(0).get_offsets(i_to_global(leftmostcell,nxcell))
		leftmost_offsetmult = elem2.get_sub_elem(0).get_offsetmult(i_to_global(leftmostcell,nxcell))
		rightmost_offsets = elem2.get_sub_elem(0).get_offsets(i_to_global(rightmostcell,nxcell))
		rightmost_offsetmult = elem2.get_sub_elem(0).get_offsetmult(i_to_global(rightmostcell,nxcell))
		leftbound = leftmost_offsets[0] + leftmostcell * leftmost_offsetmult[0]
		rightbound = rightmost_offsets[-1] + rightmostcell * rightmost_offsetmult[-1]
		#print i,leftbound,rightbound,leftmostcell,rightmostcell
		nnz_x[i-space1_ranges[0][0]] = rightbound - leftbound + 1 #this is the total size
		dnnz_x[i-space1_ranges[0][0]] = py_clip(rightbound,space2_ranges[0][0],space2_ranges[0][1]-1) - py_clip(leftbound,space2_ranges[0][0],space2_ranges[0][1]-1) + 1
		#print i,rightbound,py_clip(rightbound,space2_ranges[0][0],space2_ranges[0][1]-1)
		#print i,leftbound,py_clip(leftbound,space2_ranges[0][0],space2_ranges[0][1]-1)
		#print i,nnz_x[i-space1_ranges[0][0]],dnnz_x[i-space1_ranges[0][0]]
		
	#print nnz_x.shape
	#print nnz_x
	#compute j bounds	
	if mesh.ndims >= 2:
		nxcell = mesh.nxs[1]
		dnnz_y = np.zeros((nx1s[1]),dtype=np.int32)
		nnz_y = np.zeros((nx1s[1]),dtype=np.int32)
		icells = elem1.get_sub_elem(1).get_interaction_cells(nxcell,mesh.bcs[1])
		leftmostcells = icells[:,0]
		rightmostcells = icells[:,1]
		for i in xrange(space1_ranges[1][0],space1_ranges[1][1]):
			leftmostcell = leftmostcells[i]
			rightmostcell = rightmostcells[i]
			leftmost_offsets = elem2.get_sub_elem(1).get_offsets(i_to_global(leftmostcell,nxcell))
			leftmost_offsetmult = elem2.get_sub_elem(1).get_offsetmult(i_to_global(leftmostcell,nxcell))
			rightmost_offsets = elem2.get_sub_elem(1).get_offsets(i_to_global(rightmostcell,nxcell))
			rightmost_offsetmult = elem2.get_sub_elem(1).get_offsetmult(i_to_global(rightmostcell,nxcell))
			leftbound = leftmost_offsets[0] + leftmostcell * leftmost_offsetmult[0]
			rightbound = rightmost_offsets[-1] + rightmostcell * rightmost_offsetmult[-1]
			nnz_y[i-space1_ranges[1][0]] = rightbound - leftbound + 1 #this is the total size
			dnnz_y[i-space1_ranges[1][0]] = py_clip(rightbound,space2_ranges[1][0],space2_ranges[1][1]-1) - py_clip(leftbound,space2_ranges[1][0],space2_ranges[1][1]-1) + 1
	#print nnz_y.shape
	#print nnz_y
	
	#compute k bounds
	if mesh.ndims == 3:
		nxcell =  mesh.nxs[2]
		dnnz_z = np.zeros((nx1s[2]),dtype=np.int32)
		nnz_z = np.zeros((nx1s[2]),dtype=np.int32)
		icells = elem1.get_sub_elem(2).get_interaction_cells(nxcell,mesh.bcs[2])
		leftmostcells = icells[:,0]
		rightmostcells = icells[:,1]
		for i in xrange(space1_ranges[2][0],space1_ranges[2][1]):
			leftmostcell = leftmostcells[i]
			rightmostcell = rightmostcells[i]
			leftmost_offsets = elem2.get_sub_elem(2).get_offsets(i_to_global(leftmostcell,nxcell))
			leftmost_offsetmult = elem2.get_sub_elem(2).get_offsetmult(i_to_global(leftmostcell,nxcell))
			rightmost_offsets = elem2.get_sub_elem(2).get_offsets(i_to_global(rightmostcell,nxcell))
			rightmost_offsetmult = elem2.get_sub_elem(2).get_offsetmult(i_to_global(rightmostcell,nxcell))
			leftbound = leftmost_offsets[0] + leftmostcell * leftmost_offsetmult[0]
			rightbound = rightmost_offsets[-1] + rightmostcell * rightmost_offsetmult[-1]
			nnz_z[i-space1_ranges[2][0]] = rightbound - leftbound + 1 #this is the total size
			dnnz_z[i-space1_ranges[2][0]] = py_clip(rightbound,space2_ranges[2][0],space2_ranges[2][1]-1) - py_clip(leftbound,space2_ranges[2][0],space2_ranges[2][1]-1) + 1

	#fix periodic boundaries when there is only 1 processor in that direction
	nprocs = mesh.cell_da.getProcSizes()
	#for bc,nproc in zip(mesh.bcs,nprocs):
	if (mesh.bcs[0] == 'periodic') and (nprocs[0] == 1):
		dnnz_x = nnz_x
	if mesh.ndims >=2:
		if (mesh.bcs[1] == 'periodic') and (nprocs[1] == 1):
			dnnz_y = nnz_y
	if mesh.ndims == 3:
		if (mesh.bcs[2] == 'periodic') and (nprocs[2] == 1):
			dnnz_z = nnz_z

	#print 'x',nnz_x
	#print 'y',nnz_y
	#print 'z',nnz_z
	
	if mesh.ndims == 1:
		dnnzarr = dnnz_x
		nnzarr = nnz_x
	if mesh.ndims == 2:
		dnnzarr = reduce(np.multiply, np.ix_(dnnz_y,dnnz_x)) #np.ravel(np.outer(nnz_x,nnz_y))
		nnzarr = reduce(np.multiply, np.ix_(nnz_y,nnz_x)) #np.ravel(np.outer(nnz_x,nnz_y))
	if mesh.ndims == 3:
		dnnzarr = reduce(np.multiply, np.ix_(dnnz_z,dnnz_y,dnnz_x))
		nnzarr = reduce(np.multiply, np.ix_(nnz_z,nnz_y,nnz_x))
	onnzarr = nnzarr - dnnzarr

		#see http://stackoverflow.com/questions/17138393/numpy-outer-product-of-n-vectors
	
	#print 'pre',s1i,s2i
	#print nnzarr.shape
	#rank= PETSc.COMM_WORLD.getRank()
	#print 'dnnz_x',rank,dnnz_x
	#print 'nnz_x',rank,nnz_x
	#print 'dnnz_y',dnnz_y
	#print 'nnz_y',nnz_y
	
	#print 'dnnz',rank,dnnzarr
	#print 'onnz',rank,onnzarr
	#print 'nnz',rank,nnzarr


	#if (PETSc.COMM_WORLD.getSize() == 1):
	#	matrix.setPreallocationNNZ((nnzarr,nnzarr))
	#else:
	if not matrix == None:
		matrix.setPreallocationNNZ((dnnzarr,onnzarr))
	else:
		return dnnzarr,onnzarr

def create_coefficient_args_list(coefficientlist):
	argslist = []
	for item in coefficientlist:
		argslist.append(np.ravel(item.data))
	return argslist
	
def create_geometry_args_list(geometry,geometrylist):
	argslist = []
	for item in geometrylist:
		if item == 'J':
			argslist.append(np.ravel(geometry.J))
		if item == 'JT':
			argslist.append(np.ravel(geometry.JT))
		if item == 'Jinv':
			argslist.append(np.ravel(geometry.Jinv))
		if item == 'JTinv':
			argslist.append(np.ravel(geometry.JTinv))
		if item == 'detJ':
			argslist.append(np.ravel(geometry.detJ))
		if item == 'detJinv':
			argslist.append(np.ravel(geometry.detJinv))
		if item == 'coords':
			argslist.append(np.ravel(geometry.quadcoords))
		if item == 'JTrotJ':
			argslist.append(np.ravel(geometry.JTrotJ))
		if item == 'JinvrotJTinv':
			argslist.append(np.ravel(geometry.JinvrotJTinv))
		if item == 'rotJ':
			argslist.append(np.ravel(geometry.rotJ))
		if item == 'rotJTinv':
			argslist.append(np.ravel(geometry.rotJTinv))
		if item == 'JTJ':
			argslist.append(np.ravel(geometry.JTJ))
		if item == 'JTJdetJinv':
			argslist.append(np.ravel(geometry.JTJdetJinv))
		if item == 'JTrotJdetJinv':
			argslist.append(np.ravel(geometry.JTrotJdetJinv))
		if item == 'JinvJTinv':
			argslist.append(np.ravel(geometry.JinvJTinv))
		if item == 'JinvJTinvdetJ':
			argslist.append(np.ravel(geometry.JinvJTinvdetJ))
	return argslist
	
#Quadrature MUST match the one used to create geometric quantities, otherwise bad things will happen
#ADD FACET INTEGRALS TO THIS EVENTUALLY
def OperatorAction(form,functionals,quadrature,x,y,integral_type='volume'):
	with PETSc.Log.Stage(form.name + '_mvmult'):
		#compile functional IFF form not already compiled
		#also extract coefficient and geometry args lists
		with PETSc.Log.Event('compile'):						
			for t in xrange(form.target.nspaces):
				for s in xrange(form.source.nspaces):
					if (not functionals[t][s] == None) and (not functionals[t][s].compiled):
						functionals[t][s].geometry_args_list = create_geometry_args_list(form.mesh.geometry,functionals[t][s].geometrylist)
						functionals[t][s].coefficient_args_list = create_coefficient_args_list(functionals[t][s].coefficientlist)
						functionals[t][s].mult_functions = [None for _ in xrange(form.target.get_space(t).nelems)]
						for k in xrange(form.target.get_space(t).nelems):
							functionals[t][s].ci1 = k
							functionals[t][s].mult_functions[k] = [None for _ in xrange(form.source.get_space(s).nelems)]
							for l in xrange(form.source.get_space(s).nelems):
								functionals[t][s].ci2 = l
								mult_routine,geometry_array_list,coefficient_array_list = generate_assembly_routine(form.mesh,form.target.get_space(t),form.source.get_space(s),quadrature,functionals[t][s])
								mult_routine = mult_routine.encode('ascii','ignore')
								functionals[t][s].mult_functions[k][l] = instant.build_module( code=mult_routine,
								  include_dirs=include_dirs,
								  library_dirs=library_dirs,
								  libraries=libraries,
								  arrays = geometry_array_list + coefficient_array_list,
								  init_code = '    import_array();',
								  cppargs=['-O3',],
								  swig_include_dirs=swig_include_dirs).assemble
						functionals[t][s].compiled = True

		#extract x+y, scatter into xlvecs
		with PETSc.Log.Event('extract'):
			##extract x
			#xcompindicess = []
			#xsubs = []
			#i = 0
			#for t in xrange(form.target.nspaces):
				#tspace = form.target.get_space(t)
				#for k in xrange(tspace.nelems):
					#xcompindices = tspace.get_compindices(k)
					#xsub = x.getSubVector(xcompindices)
					#xscatter = tspace.get_scatter(k)
					#xscatter.scatter(xsub, form.xlvecs[i]) #addv=None, mode=None)
					#xsubs.append(xsub)
					#xcompindicess.append(xcompindices)
					#i = i + 1
			
			##extract y
			##THIS COULD MOSTLY (EXCEPT ylvec.zero part) MOVE TO RESTORE AREA
			#y.zeroEntries()
			#ycompindicess = []
			#ysubs = []
			#j = 0
			#for s in xrange(form.source.nspaces):
				#sspace = form.source.get_space(s)
				#for l in xrange(sspace.nelems):		
					#form.ylvecs[j].zeroEntries()
					#ycompindices = sspace.get_compindices(l)
					#ysub = y.getSubVector(ycompindices)
					#ysubs.append(ysub)
					#ycompindicess.append(ycompindices)
					#j = j + 1
					
			#extract x
			xcompindicess = []
			xsubs = []
			j = 0
			for s in xrange(form.source.nspaces):
				sspace = form.source.get_space(s)
				for l in xrange(sspace.nelems):
					xcompindices = sspace.get_compindices(l)
					xsub = x.getSubVector(xcompindices)
					xscatter = sspace.get_scatter(l)
					xscatter.scatter(xsub, form.xlvecs[j]) #addv=None, mode=None)
					xsubs.append(xsub)
					xcompindicess.append(xcompindices)
					j = j + 1
			
			#extract y
			#THIS COULD MOSTLY (EXCEPT ylvec.zero part) MOVE TO RESTORE AREA
			y.zeroEntries()
			ycompindicess = []
			ysubs = []
			i = 0
			for t in xrange(form.target.nspaces):
				tspace = form.target.get_space(t)
				for k in xrange(tspace.nelems):		
					form.ylvecs[i].zeroEntries()
					ycompindices = tspace.get_compindices(k)
					ysub = y.getSubVector(ycompindices)
					ysubs.append(ysub)
					ycompindicess.append(ycompindices)
					i = i + 1
			
			#extract fields
			for t in xrange(form.target.nspaces):
				for s in xrange(form.source.nspaces):
					fieldargs_list = []
					tempvecs = []
					if (not functionals[t][s] == None):
						for k in xrange(len(functionals[t][s].fields)):
							field = functionals[t][s].fields[k]
							si = functionals[t][s].fsi[k]
							off = field.space.get_space_offset(si)
							for ci in xrange(field.space.get_space(si).nelems):
								lvec = field.lvectors[ci+off]
								scatter = field.space.get_space(si).get_scatter(ci)
								compindices = field.space.get_space(si).get_compindices(ci)
								tempfield = field.vector.getSubVector(compindices)
								tempvecs.append(tempfield)
								scatter.scatter(tempfield, lvec) #addv=None, mode=None)
								fieldargs_list.append(field.space.get_space(si).get_da(ci)) 
								fieldargs_list.append(lvec)
						functionals[t][s].field_args_list = fieldargs_list
						functionals[t][s].field_tempvecs = tempvecs
					
		#Do mults
		with PETSc.Log.Event('mult'):
			i = 0
			for t in xrange(form.target.nspaces):
				tspace = form.target.get_space(t)
				for k in xrange(tspace.nelems):
					j = 0
					for s in xrange(form.source.nspaces):
						sspace = form.source.get_space(s)
						for l in xrange(sspace.nelems):
							if (not functionals[t][s] == None):		
								#functionals[t][s].mult_functions[k][l](form.mesh.cell_da,sspace.get_da(l),form.ylvecs[j],tspace.get_da(k),form.xlvecs[i],*(functionals[t][s].field_args_list + functionals[t][s].geometry_args_list + functionals[t][s].coefficient_args_list))
								functionals[t][s].mult_functions[k][l](form.mesh.cell_da,tspace.get_da(k),form.ylvecs[i],sspace.get_da(l),form.xlvecs[j],*(functionals[t][s].field_args_list + functionals[t][s].geometry_args_list + functionals[t][s].coefficient_args_list))
							j = j+1
					i = i +1
					
		#restore x/y, scatter into ysubs
		with PETSc.Log.Event('restore'):

			##restore x
			#i = 0
			#for t in xrange(form.target.nspaces):
				#tspace = form.target.get_space(t)
				#for k in xrange(tspace.nelems):
					#x.restoreSubVector(xcompindicess[i],xsubs[i])
					#i = i + 1
			##THIS CAN BE MERGED WITH STUFF FROM EXTRACT
			##IE GET SUBECTOR, SCATTER INTO IT
			##SHOULD BE POSSIBLE TO SCATTER DIRECTLY INTO Y ITSELF THOUGH...
			##NEED TO THINK ABOUT INDICES HERE..
			##restore y
			#j = 0
			#for s in xrange(form.source.nspaces):
				#sspace = form.source.get_space(s)
				#for l in xrange(sspace.nelems):
					#yscatter = sspace.get_scatter(l)
					#yscatter.scatter(form.ylvecs[j],ysubs[j],addv=PETSc.InsertMode.ADD_VALUES,mode=PETSc.ScatterMode.SCATTER_REVERSE) #addv=None, mode=None)	
					#j = j + 1
			#j = 0
			#for s in xrange(form.source.nspaces):
				#sspace = form.source.get_space(s)
				#for l in xrange(sspace.nelems):		
					#y.restoreSubVector(ycompindicess[j],ysubs[j])
					#j = j + 1
					
			#restore x
			j = 0
			for s in xrange(form.source.nspaces):
				sspace = form.source.get_space(s)
				for l in xrange(sspace.nelems):
					x.restoreSubVector(xcompindicess[j],xsubs[j])
					j = j + 1
			#THIS CAN BE MERGED WITH STUFF FROM EXTRACT
			#IE GET SUBECTOR, SCATTER INTO IT
			#SHOULD BE POSSIBLE TO SCATTER DIRECTLY INTO Y ITSELF THOUGH...
			#NEED TO THINK ABOUT INDICES HERE..
			#restore y
			i = 0
			for t in xrange(form.target.nspaces):
				tspace = form.target.get_space(t)
				for k in xrange(tspace.nelems):
					yscatter = tspace.get_scatter(k)
					yscatter.scatter(form.ylvecs[i],ysubs[i],addv=PETSc.InsertMode.ADD_VALUES,mode=PETSc.ScatterMode.SCATTER_REVERSE) #addv=None, mode=None)	
					i = i + 1
			i = 0
			for t in xrange(form.target.nspaces):
				tspace = form.target.get_space(t)
				for k in xrange(tspace.nelems):		
					y.restoreSubVector(ycompindicess[i],ysubs[i])
					i = i + 1
					
			#restore fields
			for t in xrange(form.target.nspaces):
				for s in xrange(form.source.nspaces):
					if (not functionals[t][s] == None):
						l = 0
						for k in xrange(len(functionals[t][s].fields)):
							field = functionals[t][s].fields[k]
							si = functionals[t][s].fsi[k]
							for ci in xrange(field.space.get_space(si).nelems):
								compindices = field.space.get_space(si).get_compindices(ci)
								field.vector.restoreSubVector(compindices,functionals[t][s].field_tempvecs[l])
								l = l + 1
										
#Quadrature MUST match the one used to create geometric quantities, otherwise bad things will happen
#ADD FACET INTEGRALS TO THIS EVENTUALLY
def AssembleForm(form,functional,quadrature,si1=0,si2=0,integral_type='volume',use_getsubmatrix=True,zeroform=False):
	with PETSc.Log.Stage(form.name + '_assemble'):
		if form.dimension == 0:
			nt = 1
			ns = 1
			tspace = None
			sspace = None
			toff = 0
			soff = 0
			if form.layered:
				functional.layered = True
				functional.layered_direc = form.layered_direc
		if form.dimension == 1:
			tspace = form.target.get_space(si1)
			toff = form.target.get_space_offset(si1)
			nt = tspace.nelems
			sspace = None
			ns = 1
			soff = 0
		if form.dimension == 2:	
			tspace = form.target.get_space(si1)
			toff = form.target.get_space_offset(si1)
			nt = tspace.nelems
			sspace = form.source.get_space(si2)
			soff = form.source.get_space_offset(si2)
			ns = sspace.nelems

		#compile functional IFF form not already compiled
		#also extract coefficient and geometry args lists
		with PETSc.Log.Event('compile'):
			if not functional.compiled:
				functional.geometry_args_list = create_geometry_args_list(form.mesh.geometry,functional.geometrylist)
				functional.coefficient_args_list = create_coefficient_args_list(functional.coefficientlist)

				functional.assemble_functions = [None for _ in xrange(nt)]
				if form.dimension == 1:
					functional.templvecs = [None for _ in xrange(nt)]
					for i in xrange(nt):
						functional.templvecs[i] = tspace.get_da(i).createLocalVector() 
						
				for i in xrange(nt):
					functional.ci1 = i
					functional.assemble_functions[i] = [None for _ in xrange(ns)]
					for j in xrange(ns):
						functional.ci2 = j
						assembly_routine,geometry_array_list,coefficient_array_list = generate_assembly_routine(form.mesh,tspace,sspace,quadrature,functional)
						assembly_routine = assembly_routine.encode('ascii','ignore')
						functional.assemble_functions[i][j] = instant.build_module( code=assembly_routine,
						  include_dirs=include_dirs,
						  library_dirs=library_dirs,
						  libraries=libraries,
						  arrays = geometry_array_list + coefficient_array_list,
						  init_code = '    import_array();',
						  cppargs=['-O3',],
						  swig_include_dirs=swig_include_dirs).assemble
				functional.compiled = True
                                            
		#zero the matrix IFF mode is insert
		with PETSc.Log.Event('zero'):
			if zeroform and form.dimension == 1:
				for i in xrange(nt):
					indices = tspace.get_compindices(i)
					temp = form.vector.getSubVector(indices)
					temp.setLGMap(tspace.get_lgmap(i))
					temp.set(0.0)
					form.vector.restoreSubVector(indices,temp)         
			if zeroform and form.dimension == 2:
				for i in xrange(nt):
					isrow = tspace.get_compindices(i)
					for j in xrange(ns):
						iscol = sspace.get_compindices(j) 
						submat = form.A.getSubMatrix(isrow,iscol)
						submat.zeroEntries()				
			if not use_getsubmatrix: #this should be called ONLY for the very first MATRIX assembly
				for i in xrange(nt):
					for j in xrange(ns):
						form.matrices[i+toff][j+soff].zeroEntries()

							
		#extract vectors and fields
		with PETSc.Log.Event('extract'):
			#extract fields
			fieldargs_list = []
			tempvecs = []
			for k in xrange(len(functional.fields)):
				field = functional.fields[k]
				si = functional.fsi[k]
				off = field.space.get_space_offset(si)
				for ci in xrange(field.space.get_space(si).nelems):
					lvec = field.lvectors[ci+off]
					scatter = field.space.get_space(si).get_scatter(ci)
					compindices = field.space.get_space(si).get_compindices(ci)
					tempfield = field.vector.getSubVector(compindices)
					tempvecs.append(tempfield)
					scatter.scatter(tempfield, lvec,addv=PETSc.InsertMode.INSERT_VALUES) #addv=None, mode=None)
					fieldargs_list.append(field.space.get_space(si).get_da(ci)) 
					fieldargs_list.append(lvec)
		
		#assemble
		with PETSc.Log.Event('assemble'):
			#0-forms
			#MOVE THE RELEVANT EXTRACT/RESTORE STUFF OUT OF HERE
			#ADD MODE STUFF HERE- IE INSERT VERSUS ADD
			if form.dimension == 0:
				if form.layered:
					lvalue = np.zeros((form.mesh.nxs[functional.layered_direc]))
					for kindex in range(form.mesh.nxs[functional.layered_direc]):
						lvalue[kindex] = functional.assemble_functions[0][0](form.mesh.cell_da,kindex,*(fieldargs_list + functional.geometry_args_list + functional.coefficient_args_list))
				else:
					lvalue = functional.assemble_functions[0][0](form.mesh.cell_da,*(fieldargs_list + functional.geometry_args_list + functional.coefficient_args_list))
				if form.layered:
					if PETSc.COMM_WORLD.Get_size() == 1:
						form.value = lvalue
					else:
						#print lvalue,self.value
						tbuf = np.zeros((mesh.ndims))
						mpicomm = PETSc.COMM_WORLD.tompi4py()
						mpicomm.Allreduce([lvalue,MPI.DOUBLE],[tbuf,MPI.DOUBLE],op=MPI.SUM) #this defaults to sum, so we are good
						#print self.value,type(self.value),self.value.shape
						form.value = np.copy(tbuf)
				else:
					if PETSc.COMM_WORLD.Get_size() == 1:
						form.value = lvalue
					else:
						#print lvalue,self.value
						tbuf = np.array(0,'d')
						mpicomm = PETSc.COMM_WORLD.tompi4py()
						mpicomm.Allreduce([np.array(lvalue,'d'),MPI.DOUBLE],[tbuf,MPI.DOUBLE],op=MPI.SUM) #this defaults to sum, so we are good
						#print self.value,type(self.value),self.value.shape
						form.value = np.copy(tbuf)
			
				#if show_result:
					#if PETSc.COMM_WORLD.Get_rank() == 0 :
						#print 'scalar',form.value
						
			#1-forms
			#MOVE THE RELEVANT EXTRACT/RESTORE STUFF OUT OF HERE

			if form.dimension == 1:
				#print 'one form ',form.name
				for i in xrange(nt):
					functional.templvecs[i].zeroEntries()
					functional.assemble_functions[i][0](form.mesh.cell_da,tspace.get_da(i), functional.templvecs[i],*(fieldargs_list + functional.geometry_args_list + functional.coefficient_args_list))
					#SHOULD BE ABLE TO SCATTER DIRECTLY INTO GLOBAL VECTOR? NEED TO THINK ABOUT INDICES HERE...
					indices = tspace.get_compindices(i)
					temp = form.vector.getSubVector(indices)
					temp.setLGMap(tspace.get_lgmap(i))
					scatter = tspace.get_scatter(i)
					scatter.scatter(functional.templvecs[i],temp,addv=PETSc.InsertMode.ADD_VALUES,mode=PETSc.ScatterMode.SCATTER_REVERSE) #addv=None, mode=None)	
					form.vector.restoreSubVector(indices,temp)         
				#if show_result:
					#if PETSc.COMM_WORLD.Get_rank() == 0 :
						#print 'vector',form.name
					#form.vector.view()
			
			#2-forms
			#FIX THIS SO IT PROPERLY USES LOCAL INDICES
			#AND THEN GET LOCAL SUB MATRIX
			#also allows us to eliminate use_getsubmatrix
			if form.dimension == 2:
				if use_getsubmatrix:
					for i in xrange(nt):
						#isrow = tspace.get_indices(i)
						isrow = tspace.get_compindices(i)
						for j in xrange(ns):
							#iscol = sspace.get_indices(j)
							iscol = sspace.get_compindices(j) 
							#submat = form.A.getLocalSubMatrix(isrow,iscol)
							submat = form.A.getSubMatrix(isrow,iscol)
							functional.assemble_functions[i][j](form.mesh.cell_da,tspace.get_da(i),sspace.get_da(j), submat,*(fieldargs_list + functional.geometry_args_list + functional.coefficient_args_list))
							#if show_result:
								#if PETSc.COMM_WORLD.Get_rank() == 0 :
									#print 'matrix',i+toff,j+soff,form.names[i+toff][j+soff]
								#submat.view()
							#WE DON'T APPEAR TO NEED THIS.....NOT SURE EXACTLY WHY?
							#The function below doesn't exist anyways...
							#form.A.restoreLocalSubMatrix(isrow,iscol,submat) #should this be restoreSubMatrix?
							#form.A.restoreSubMatrix(isrow,iscol,submat) #should this be restoreSubMatrix?
				else:
					for i in xrange(nt):
						for j in xrange(ns):
							functional.assemble_functions[i][j](form.mesh.cell_da,tspace.get_da(i),sspace.get_da(j), form.matrices[i+toff][j+soff],*(fieldargs_list + functional.geometry_args_list + functional.coefficient_args_list))
							#if show_result:
								#if PETSc.COMM_WORLD.Get_rank() == 0 :
									#print 'matrix',i+toff,j+soff,form.names[i+toff][j+soff]
								#form.matrices[i+toff][j+soff].view()
						
		#restore fields and vectors
		with PETSc.Log.Event('restore'):
			#restore fields
			l = 0
			for k in xrange(len(functional.fields)):
				field = functional.fields[k]
				si = functional.fsi[k]
				for ci in xrange(field.space.get_space(si).nelems):
					compindices = field.space.get_space(si).get_compindices(ci)
					field.vector.restoreSubVector(compindices,tempvecs[l])
					l = l + 1

		#set the assembled flag
		#form.assembled = True
	

#Quadrature MUST match the one used to create geometric quantities, otherwise bad things will happen
def EvaluateCoefficient(coefficient,functional,quadrature):
	with PETSc.Log.Stage(coefficient.name + '_assemble'):

		#compile functional IFF form not already compiled
		#also extract coefficient and geometry args lists
		with PETSc.Log.Event('compile'):
			if not functional.compiled:
				functional.geometry_args_list = create_geometry_args_list(coefficient.mesh.geometry,functional.geometrylist)
				functional.coefficient_args_list = create_coefficient_args_list(functional.coefficientlist)

				assembly_routine,geometry_array_list,coefficient_array_list = generate_assembly_routine(coefficient.mesh,None,None,quadrature,functional)
				assembly_routine = assembly_routine.encode('ascii','ignore')
				functional.evaluate_function = instant.build_module( code=assembly_routine,
						  include_dirs=include_dirs,
						  library_dirs=library_dirs,
						  libraries=libraries,
						  arrays = geometry_array_list + coefficient_array_list,
						  init_code = '    import_array();',
						  cppargs=['-O3',],
						  swig_include_dirs=swig_include_dirs).evaluate
				functional.compiled = True
                                            
		#extract fields
		with PETSc.Log.Event('extract'):
			fieldargs_list = []
			tempvecs = []
			for k in xrange(len(functional.fields)):
				field = functional.fields[k]
				si = functional.fsi[k]
				off = field.space.get_space_offset(si)
				for ci in xrange(field.space.get_space(si).nelems):
					lvec = field.lvectors[ci+off]
					scatter = field.space.get_space(si).get_scatter(ci)
					compindices = field.space.get_space(si).get_compindices(ci)
					tempfield = field.vector.getSubVector(compindices)
					tempvecs.append(tempfield)
					scatter.scatter(tempfield, lvec) #addv=None, mode=None)
					fieldargs_list.append(field.space.get_space(si).get_da(ci)) 
					fieldargs_list.append(lvec)
		
		#evaluate
		with PETSc.Log.Event('evaluate'):
			functional.evaluate_function(coefficient.mesh.cell_da,*(fieldargs_list + functional.geometry_args_list + functional.coefficient_args_list + [np.ravel(functional.valscoeff.data),]))
			
		#restore fields
		with PETSc.Log.Event('restore'):
			l = 0
			for k in xrange(len(functional.fields)):
				field = functional.fields[k]
				si = functional.fsi[k]
				for ci in xrange(field.space.get_space(si).nelems):
					compindices = field.space.get_space(si).get_compindices(ci)
					field.vector.restoreSubVector(compindices,tempvecs[l])
					l = l + 1
