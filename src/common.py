import sys, petsc4py
#initialize petsc
petsc4py.init(sys.argv)
from petsc4py import PETSc
import numpy
import os

#for given a set of global indices (i,j,k) and sizes in various direction, returns the natural numbering
def i_to_global(i,nx):
	return i%nx
def ij_to_global(i,nx,j,ny):
	return (i%nx) + ((j%ny)*nx)

include_dirs = [os.path.join(os.getenv('PETSC_DIR'), 'include'),
                petsc4py.get_include(),numpy.get_include(),
                # May need to add extras here depending on your environment
                '/usr/lib/openmpi/include', '/usr/lib/openmpi/include/openmpi',
                ]
                
swig_include_dirs = [petsc4py.get_include()]
library_dirs = [os.path.join(os.getenv('PETSC_DIR'),'lib')] # os.getenv('PETSC_ARCH')
libraries = ['petsc']
