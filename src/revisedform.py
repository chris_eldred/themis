from mesh import *
from quad import *
from basis import *
from functionspace import *
#import copy
import instant
#import os
from codegenerator import *
#import petsc4py
import time
from common import *
from mpi4py import MPI
import numpy
from assemble import *
	
class ZeroForm():
	def __init__(self,mesh,name,layered=False,layered_direc=2):
		self.name = name
		self.value = np.array(0,'d')
		self.mesh = mesh
		self.assembled = False
		self.dimension = 0
		self.layered = layered
		self.layered_direc = layered_direc
		if self.layered:
			self.value = np.zeros((self.mesh.nxs[layered_direc]))

	def destroy(self):
		pass #nothing to do here, actually
		
	def output(self):
		pass #possibly start writing this to a file, useful for tracking things like energy in time-stepping simulations
	
class OneForm():
	def __init__(self,mesh,names,target):
		with PETSc.Log.Stage(names[0] + '_init'):

			self.names = names[1]
			self.name = names[0]
			self.target = target
			self.mesh = mesh
			self.assembled = False
			self.dimension = 1
			
			#create vector
			with PETSc.Log.Event('create'):
				globalsize = 0
				localsize = 0
				for t in xrange(self.target.nspaces):
					for k in xrange(self.target.get_space(t).nelems):
						globalsize = globalsize + self.target.get_space(t).get_ndofs(k)
						localsize = localsize + self.target.get_space(t).get_localndofs(k)
				
				self.vector = PETSc.Vec().create(comm=PETSc.COMM_WORLD)
				self.vector.setSizes((localsize,globalsize))
				self.vector.setUp()
				self.vector.set(0.0)
				 
	def output(self,view,ts=None):
		k = 0
		for t in xrange(self.target.nspaces):
			for i in xrange(self.target.get_space(t).nelems):
				if not (ts == None):
					tname = self.names[k] + str(ts)
				else:
					tname = self.names[k]
				indices = self.target.get_space(t).get_compindices(i)
				natvec = self.target.get_space(t).get_da(i).createNaturalVec()
				temp = self.vector.getSubVector(indices)
				self.target.get_space(t).get_da(i).globalToNatural(temp,natvec)
				vector_output(natvec,tname,view)
				self.vector.restoreSubVector(indices,temp)
				natvec.destroy()
				k = k+1
		vector_output(self.vector,self.name,view)
		
	def destroy(self):
		self.vector.destroy()
		
class TwoForm():
	def __init__(self,mesh,names,target,source,mattype='block',multfuncs=None,multquad=None):
		with PETSc.Log.Stage(names[0] + '_init'):

			self.names = names[1]
			self.name = names[0]
			self.target = target
			self.source = source
			self.mesh = mesh
			#self.assembled = False
			self.dimension = 2
			self.mattype = mattype
			self.multfuncs = multfuncs
			self.multquadrature = multquad
			self.bcs = None
			
			#create matrices
			with PETSc.Log.Event('create'):
				if mattype == 'block':
					self.target_size = target.nelems
					self.source_size = source.nelems
					self.matrices = [None for _ in xrange(self.target_size)]
					i = 0
					for t in xrange(target.nspaces):
						tspace = target.get_space(t)
						for k in xrange(tspace.nelems):
							self.matrices[i] = [None for _ in xrange(self.source_size)]
							j = 0
							for s in xrange(source.nspaces):
								sspace = source.get_space(s)
								for l in xrange(sspace.nelems):						
									m = tspace.get_localndofs(k)
									n = sspace.get_localndofs(l)
									matrix = PETSc.Mat()
									matrix.create(PETSc.COMM_WORLD)
									matrix.setSizes(((m, None),(n,None)))
									matrix.setType('aij')
									rowmap = tspace.get_lgmap(k)
									colmap = sspace.get_lgmap(l)
									matrix.setLGMap(rowmap, cmap=colmap)
									self.matrices[i][j] = matrix
									j = j + 1
							i = i + 1
				if mattype == 'monolithic':
					mlist = []
					nlist = []
					for t in xrange(target.nspaces):
						tspace = target.get_space(t)
						for k in xrange(tspace.nelems):
							m = tspace.get_localndofs(k)
							mlist.append(m)
					for s in xrange(source.nspaces):
						sspace = source.get_space(s)
						for l in xrange(sspace.nelems):						
							n = sspace.get_localndofs(l)
							nlist.append(n)
					
					M = np.sum(np.array(mlist,dtype=np.int32))
					N = np.sum(np.array(nlist,dtype=np.int32))
					self.A = PETSc.Mat()
					self.A.create(PETSc.COMM_WORLD)
					self.A.setSizes(((M, None),(N,None)))
					self.A.setType('aij')
					self.A.setOption(PETSc.Mat.Option.IGNORE_ZERO_ENTRIES, False)
					#NEED TO ADD A COMPOSED LOCAL TO GLOBAL MAP HERE!!!!
				if mattype == 'shell':
					mlist = []
					nlist = []
					for t in xrange(target.nspaces):
						tspace = target.get_space(t)
						for k in xrange(tspace.nelems):
							m = tspace.get_localndofs(k)
							mlist.append(m)
					for s in xrange(source.nspaces):
						sspace = source.get_space(s)
						for l in xrange(sspace.nelems):						
							n = sspace.get_localndofs(l)
							nlist.append(n)
					
					M = np.sum(np.array(mlist,dtype=np.int32))
					N = np.sum(np.array(nlist,dtype=np.int32))
					self.A = PETSc.Mat()
					self.A.create(PETSc.COMM_WORLD)
					self.A.setSizes(((M, None),(N,None)))
					self.A.setType('python')
					self.A.setPythonContext(self)
					self.A.setUp()
					
					#create local vectors- one for member in target and source space
					self.xlvecs= []
					for t in xrange(self.target.nspaces):
						for k in xrange(self.target.get_space(t).nelems):
							self.xlvecs.append(self.target.get_space(t).get_da(k).createLocalVector())
					self.ylvecs= []
					for s in xrange(self.source.nspaces):
						for l in xrange(self.source.get_space(s).nelems):
							self.ylvecs.append(self.source.get_space(s).get_da(l).createLocalVector())
					
			#preallocate matrices
			with PETSc.Log.Event('prealloc'):
				if mattype == 'block':
					i = 0
					for t in xrange(self.target.nspaces):
						tspace = self.target.get_space(t)
						for k in xrange(tspace.nelems):
							j = 0
							for s in xrange(self.source.nspaces):
								sspace = self.source.get_space(s)
								for l in xrange(sspace.nelems):	
									two_form_preallocate_opt(self.mesh,self.matrices[i][j],tspace,sspace,k,l)
									self.matrices[i][j].setOption(PETSc.Mat.Option.IGNORE_ZERO_ENTRIES, False)
									self.matrices[i][j].setUp()
									j = j+1
							i = i +1

				if mattype == 'monolithic':
					mlist.insert(0,0)
					mlist_adj = np.cumsum(mlist)
					dnnzarr = np.zeros(M,dtype=np.int32)
					onnzarr = np.zeros(M,dtype=np.int32)
					i = 0
					for t in xrange(self.target.nspaces):
						tspace = self.target.get_space(t)
						for k in xrange(tspace.nelems):
							for s in xrange(self.source.nspaces):
								sspace = self.source.get_space(s)
								for l in xrange(sspace.nelems):
									dnnz,onnz = two_form_preallocate_opt(self.mesh,None,tspace,sspace,k,l)
									dnnz = np.ravel(dnnz)
									onnz = np.ravel(onnz)
									dnnzarr[mlist_adj[i]:mlist_adj[i+1]] = dnnzarr[mlist_adj[i]:mlist_adj[i+1]] + dnnz
									onnzarr[mlist_adj[i]:mlist_adj[i+1]] = onnzarr[mlist_adj[i]:mlist_adj[i+1]] + onnz
							i = i + 1
					self.A.setPreallocationNNZ((dnnzarr,onnzarr))
					self.A.setUp()
					
			#do an empty assembly
			if mattype == 'block':
				for t in xrange(self.target.nspaces):
					for s in xrange(self.source.nspaces):
						zerofunctional = Functional('standard.functional','zero',2) #need a new functional for each subspace since offsets/etc. might change!
						AssembleForm(self,zerofunctional,TensorProductQuadrature([EmptyQuad,EmptyQuad,EmptyQuad],[1,1,1]),t,s,use_getsubmatrix=False)
			#THIS PART IS BROKEN....
			#really issue is with get sub matrix- can't do this becuase matrix is not assembled
			#should really be sub local sub matrix
			#then we can merge monolithic/block stuff here also!
			#if mattype == 'monolithic':
			#	for t in xrange(self.target.nspaces):
			#		for s in xrange(self.source.nspaces):
			#			zerofunctional = Functional('standard.functional','zeromat',2) #need a new functional for each subspace since offsets/etc. might change!
			#			AssembleForm(self,zerofunctional,TensorProductQuadrature([EmptyQuad,EmptyQuad,EmptyQuad],[1,1,1]),t,s,use_getsubmatrix=True)
						
			#set various matrix options
			if mattype == 'block':
				i = 0
				for t in xrange(self.target.nspaces):
					tspace = self.target.get_space(t)
					for k in xrange(tspace.nelems):
						j = 0
						for s in xrange(self.source.nspaces):
							sspace = self.source.get_space(s)
							for l in xrange(sspace.nelems):	
								
								#this catches bugs in pre-allocation by locking the non-zero structure
								self.matrices[i][j].setOption(PETSc.Mat.Option.NEW_NONZERO_LOCATION_ERR, True) 
								self.matrices[i][j].setOption(PETSc.Mat.Option.UNUSED_NONZERO_LOCATION_ERR, True)

								#This is a possible future optimization ala PyOP2- trades some additional computation for less communication
								#Would double stencil/halo width
								#Maybe usable for FEEC, less useful for MGD
								#self.matrices[i][j].setOption(PETSc.Mat.Option.NO_OFF_PROC_ENTRIES, True)

								#These are for zeroRows- the first keeps the non-zero structure when zeroing rows, the 2nd tells PETSc that the process only zeros owned rows
								#Don't set them for now...
								self.matrices[i][j].setOption(PETSc.Mat.Option.KEEP_NONZERO_PATTERN, True) 
								#self.matrices[i][j].setOption(PETSc.Mat.Option.NO_OFF_PROC_ZERO_ROWS, True)
								j = j+1
						i = i +1
			if mattype == 'monolithic':
				#this catches bugs in pre-allocation by locking the non-zero structure
				self.A.setOption(PETSc.Mat.Option.NEW_NONZERO_LOCATION_ERR, True) 
				self.A.setOption(PETSc.Mat.Option.UNUSED_NONZERO_LOCATION_ERR, True)    

				#This is a possible future optimization ala PyOP2- trades some additional computation for less communication
				#Would double stencil/halo width
				#Maybe usable for FEEC, less useful for MGD
				#self.A.setOption(PETSc.Mat.Option.NO_OFF_PROC_ENTRIES, True)

				#These are for zeroRows- the first keeps the non-zero structure when zeroing rows, the 2nd tells PETSc that the process only zeros owned rows
				#Don't set them for now...
				self.A.setOption(PETSc.Mat.Option.KEEP_NONZERO_PATTERN, True) 
				#self.A.setOption(PETSc.Mat.Option.NO_OFF_PROC_ZERO_ROWS, True)		
						
			#create self.A for block matrices
			if mattype == 'block':
				if (len(self.matrices) == 1) and (len(self.matrices[0]) == 1):
					self.A = self.matrices[0][0]
				else:
					self.A = PETSc.Mat().createNest(self.matrices,comm=PETSc.COMM_WORLD) 
				self.A.setUp()
			
	def destroy(self):
		if self.mattype == 'block':
			for matrix in [val for sublist in self.matrices for val in sublist]:
				matrix.destroy()
		self.A.destroy()
	
	def output(self):
		if self.mattype == 'block':
			i = 0
			for t in xrange(self.target.nspaces):
				tspace = self.target.get_space(t)
				for k in xrange(tspace.nelems):
					j = 0
					for s in xrange(self.source.nspaces):
						sspace = self.source.get_space(s)
						for l in xrange(sspace.nelems):		
							matrix_output(self.matrices[i][j],self.names[i][j])
							j = j+1
					i = i +1
		matrix_output(self.A,self.name)
	
	def mult(self,A,x,y):
		if not self.bcs == None:
			for bc in self.bcs:
				bc.apply_rhs(x)
		OperatorAction(self,self.multfuncs,self.multquadrature,x,y)
		if not self.bcs == None:
			for bc in self.bcs:
				bc.apply_rhs(y)
