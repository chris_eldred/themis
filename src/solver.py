from output import *
from functionspace import *
		
class LinearSystem():
	def __init__(self,form,solver,precon,fieldsplit=False,directpackage='petsc',bcs=None,atol=None,divtol=None,rtol=1e-10,max_it=None):
			
		self.form = form
		self.solnspace = form.target
		#ADD A CHECK HERE THAT FORM.TARGET AND FORM.SOURCE ARE THE SAME SPACE
		#OTHERWISE THIS IS NOT A SQUARE MATRIX/LINEAR SYSTEM!
		self.bcs = bcs
		
		if not self.bcs == None:
			if form.mattype == 'shell':
				form.bcs = bcs
			else:
				for bc in self.bcs:
					bc.apply_matrix(form)
					
		self.ksp = PETSc.KSP()
		self.ksp.create(PETSc.COMM_WORLD)
		self.ksp.setType(solver)
		pc = self.ksp.getPC()
		pc.setType(precon)
		
		if solver == 'preonly':
			pc.setFactorSolverPackage(directpackage)

		#THIS SHOULD BE CLEANED UP AND MADE MORE GENERAL...
		#SHOULD INTERFACE WITH COMMAND LINE ARGUMENTS AND HOW PETSC DOES STUFF INTERNALLY!
		#SET FROM OPTIONS?
		if fieldsplit and (not (precon == 'none')):
			#print 'fieldsplit'
			pc.setType('fieldsplit') # and jacobi
			#
			for t in xrange(self.solnspace.nspaces):
				for i in xrange(self.solnspace.get_space(t).nelems):
					indices = self.solnspace.get_space(t).get_compindices(i)
					name = str(t) + '-' + str(i)
					pc.setFieldSplitIS((name,indices))
				pc.setFieldSplitType(PETSc.PC.CompositeType.MULTIPLICATIVE) #MULTIPLICATIVE ADDITIVE SCHUR
				#THIS SHOULD BE SETTABLE AT COMMAND LINE OR IN ARGUMENTS
				for ksp in pc.getFieldSplitSubKSP():
					ksp.setType(solver)
					ksp.getPC().setType(precon)
	
		
		#ADD NULL SPACE STUFF- HOW DO WE INCORPORATE THIS?
		#nspace = PETSc.NullSpace().create(constant=True)
		#self.A.setNullSpace(nspace)
		
		self.ksp.setInitialGuessNonzero(True)
		
		# finish setting up system
		self.ksp.setOperators(self.form.A)
		self.ksp.setFromOptions()
		self.ksp.setTolerances(atol=atol,divtol=divtol,rtol=rtol,max_it=max_it)
		self.ksp.setUp()
		
	def output(self):
		matrix_output(self.form.A,self.form.name)

	def solve(self,rhs,soln,kspinfo=False):
		#ADD A CHECK THAT SOLN CORRESPONDS TO SOLN SPACE
		#ADD A CHECK THAT RHS CORRESPONDS TO FORM SPACES
		
		if not self.bcs == None:
			for bc in self.bcs:
				bc.apply_rhs(rhs.vector)
			
		self.ksp.solve(rhs.vector,soln.vector)
		
		rank = PETSc.COMM_WORLD.Get_rank() 
		if kspinfo and (rank == 0):
			print 'norm',self.ksp.norm
			print 'iteration number',self.ksp.getIterationNumber()
			print 'converged reason',self.ksp.getConvergedReason()
			
	def destroy(self):
		self.ksp.destroy()

class EssentialBC():
	def __init__(self,mesh,field,direc,bval,si,ci):
		self.field = field
		self.ci = ci
		self.si = si
		self.direc = direc
		self.bval = bval
		self.mesh = mesh
		self.space = self.field.space.get_space(si)
		self._determine_rows()
	
	#THIS WORKS, BUT IT WONT SCALE TO LARGE GRID SIZES OR PROCESS COUNTS
	#SINCE WE COMPUTE THE ROWS ON EACH PROCESS (AND ALSO DO THE NON-SCALABLE
	#NATURAL TO GLOBAL AO TRANSFORM)
	
	#SINCE WE EVENTUALLY WANT TO DO SYMMETRIC ESSENTIAL BOUNDARIES IE DO THEM
	#IN ASSEMBLY, THIS IS FINE
	def _determine_rows(self):
		#these are in GLOBAL ordering, but each process generates only the boundary cells that it owns
		#they are generated in NATURAL ordering, and then transformed into GLOBAL ordering
		#using the AO provided by the relevant DMDA
		
		global_to_natural = self.space.get_da(self.ci).getAO() #this turns global to natural
		
		#CAN THIS BE MERGED INTO A DIMENSION AGNOSTIC ROUTINE?
		#IS THERE A PETSC ROUTINE THAT DOES THIS AUTOMATICALLY FOR DMDAS?
		#HARD TO IMAGINE THERE IS NOT...
		if self.mesh.ndims == 1:
			nx = self.space.get_nxny(self.ci)[0]
			leftbound = 0
			rightbound = nx-1
			self.rows = np.array([leftbound,rightbound],dtype=np.int32)
		if self.mesh.ndims == 2:
			nx,ny = self.space.get_nxny(self.ci)
			if self.direc == 'x': 
				self.rows = np.zeros((2*ny),dtype=np.int32)
				j1d = np.arange(0,ny,dtype=np.int32) 
				i1d = np.array([0,nx-1],dtype=np.int32)
			if self.direc == 'y': 
				self.rows = np.zeros((2*nx),dtype=np.int32)
				i1d = np.arange(0,nx,dtype=np.int32) 
				j1d = np.array([0,ny-1],dtype=np.int32)
			i,j = np.meshgrid(i1d,j1d,indexing='ij')
			i = np.ravel(i)
			j = np.ravel(j)
			self.rows[:] = i + nx*j
			#print self.rows
		if self.mesh.ndims == 3: 
			nx,ny,nz = self.space.get_nxny(self.ci)
			if self.direc == 'x': 
				self.rows = np.zeros((2*ny*nz),dtype=np.int32)
				j1d = np.arange(0,ny,dtype=np.int32) 
				k1d = np.arange(0,nz,dtype=np.int32)
				i1d = np.array([0,nx-1],dtype=np.int32)
			if self.direc == 'y': 
				self.rows = np.zeros((2*nx*nz),dtype=np.int32)
				i1d = np.arange(0,nx,dtype=np.int32) 
				k1d = np.arange(0,nz,dtype=np.int32)
				j1d = np.array([0,ny-1],dtype=np.int32)
			if self.direc == 'z': 
				self.rows = np.zeros((2*nx*ny),dtype=np.int32)
				j1d = np.arange(0,ny,dtype=np.int32) 
				i1d = np.arange(0,nx,dtype=np.int32)
				k1d = np.array([0,nz-1],dtype=np.int32)
			i,j,k = np.meshgrid(i1d,j1d,k1d,indexing='ij')
			i = np.ravel(i)
			j = np.ravel(j)
			k = np.ravel(k)
			self.rows[:] = i + nx*(j + k*ny)
		self.rows = global_to_natural.petsc2app(self.rows) #this takes natural indices to global indices
		self.bvals = np.ones(self.rows.shape) * self.bval
		self.indices = self.space.get_compindices(self.ci)
		
	#this will work for monolithic and block
	def apply_matrix(self,form):
		#print 'applying matrix'
		isrow = form.target.get_space(self.si).get_compindices(self.ci)
		off = self.field.space.get_space_offset(self.si)
		i = 0
		for s in xrange(form.source.nspaces):
			sspace = form.source.get_space(s)
			for l in xrange(sspace.nelems):	
				iscol = sspace.get_compindices(l)
				submatrix = form.A.getSubMatrix(isrow,iscol)
				if (i == off+self.ci):
					submatrix.zeroRows(self.rows,1.0)
				else:
					submatrix.zeroRows(self.rows,0.0)
				i = i+1		
						
	def apply_rhs(self,vector):
		#print 'applying rhs'
		temp = vector.getSubVector(self.indices)
		temp.setValues(self.rows,self.bvals,addv=PETSc.InsertMode.INSERT_VALUES)
		vector.restoreSubVector(self.indices,temp)		
