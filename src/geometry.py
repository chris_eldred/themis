import numpy as np
from quad import *
import sympy

class Geometry():
	def __init__(self):
		pass
	def create_plotting_coordinates(self):
		pass
	def create_quad_coordinates(self):
		pass
	def create_transforms(self): #this will create coordinate transforms
		pass
	def create_jacobians(self): #this will create Jacobians of coordinate transforms
		pass
	def destroy(self):
		pass

class UniformVariableBox(Geometry):
	def __init__(self,ref_cell,quadrature,*args):
		self.dxs = args
		self.dims = len(args)
		self.ref_cell = ref_cell
		self.phys_cell = []
		for dx in self.dxs:
			self.phys_cell.append([-dx/2.,dx/2.])
		self.quad = quadrature
		self.uniform = False

	
	def create_plotting_quad_coordinates(self,dm,nxs,bcs,quadrature):

		local_nxs = []
		if self.dims >= 1:
			local_nxs.append(dm.getRanges()[0][1] - dm.getRanges()[0][0])
		if self.dims >= 2:
			local_nxs.append(dm.getRanges()[1][1] - dm.getRanges()[1][0])
		if self.dims >= 3:
			local_nxs.append(dm.getRanges()[2][1] - dm.getRanges()[2][0])
		if self.dims == 1:
			local_nxs.append(1) #k
			local_nxs.append(1) #j
		if self.dims == 2:
			local_nxs.append(1) #k
		local_nxs = list(reversed(local_nxs))
		
		coordsvec = dm.getCoordinates()
		coordsarr = coordsvec.getArray().copy()
		
		newshape = list(local_nxs)
		newshape.append(len(nxs))
		coordsarr = np.reshape(coordsarr,(np.prod(np.array(local_nxs,dtype=np.int32)),len(nxs)),order='C')
		coordsarr = np.reshape(coordsarr,newshape,order='C')
				
		quadcoords_cell = quadrature.pts[:,:,:,:len(nxs)]
		quadcoords = np.zeros(quadcoords_cell.shape)
		for i in range(len(nxs)):
			quadcoords[:,:,:,i] = self.alpha[0,0,0,0,0,0,i]*quadcoords_cell[:,:,:,i] + self.beta[0,0,0,0,0,0,i]
		
		coordsarr = np.expand_dims(coordsarr,3)
		coordsarr = np.expand_dims(coordsarr,3)
		coordsarr = np.expand_dims(coordsarr,3)

		return quadcoords + coordsarr
		
	def create_quad_coordinates(self,dm,nxs,bcs):
		xmin = 0.0
		xmax = 0.0
		ymin = 0.0
		ymax = 0.0
		zmin = 0.0
		zmax = 0.0
		if self.dims >= 1:
			xmin = 0.5*self.dxs[0]
			if bcs[0] == 'periodic': #this is needed due to strange behaviour of DMDA setUniformCoordinates
				xmax = nxs[0]*self.dxs[0] + 0.5*self.dxs[0]
			else:
				xmax = nxs[0]*self.dxs[0] - 0.5*self.dxs[0]
		if self.dims >= 2:
			ymin = 0.5*self.dxs[1]
			if bcs[1] == 'periodic': #this is needed due to strange behaviour of DMDA setUniformCoordinates
				ymax = nxs[1]*self.dxs[1] + 0.5*self.dxs[1]
			else:
				ymax = nxs[1]*self.dxs[1] - 0.5*self.dxs[1]
		if self.dims >= 3:
			zmin = 0.5*self.dxs[2]
			if bcs[2] == 'periodic': #this is needed due to strange behaviour of DMDA setUniformCoordinates
				zmax = nxs[2]*self.dxs[2] + 0.5*self.dxs[2]
			else:
				zmax = nxs[2]*self.dxs[2] - 0.5*self.dxs[2]	
		
		local_nxs = []
		if self.dims >= 1:
			local_nxs.append(dm.getRanges()[0][1] - dm.getRanges()[0][0])
		if self.dims >= 2:
			local_nxs.append(dm.getRanges()[1][1] - dm.getRanges()[1][0])
		if self.dims >= 3:
			local_nxs.append(dm.getRanges()[2][1] - dm.getRanges()[2][0])
		if self.dims == 1:
			local_nxs.append(1) #k
			local_nxs.append(1) #j
		if self.dims == 2:
			local_nxs.append(1) #k
		local_nxs = list(reversed(local_nxs))
		
		dm.setUniformCoordinates(xmin=xmin, xmax=xmax,ymin=ymin, ymax=ymax, zmin=zmin, zmax=zmax)

		coordsvec = dm.getCoordinates()
		coordsarr = coordsvec.getArray().copy()
		
		newshape = list(local_nxs)
		newshape.append(len(nxs))
		coordsarr = np.reshape(coordsarr,(np.prod(np.array(local_nxs,dtype=np.int32)),len(nxs)),order='C')
		coordsarr = np.reshape(coordsarr,newshape,order='C')
				
		quadcoords_cell = self.quad.pts[:,:,:,:len(nxs)]
		quadcoords = np.zeros(quadcoords_cell.shape)
		for i in range(len(nxs)):
			quadcoords[:,:,:,i] = self.alpha[0,0,0,0,0,0,i]*quadcoords_cell[:,:,:,i] + self.beta[0,0,0,0,0,0,i]
		
		coordsarr = np.expand_dims(coordsarr,3)
		coordsarr = np.expand_dims(coordsarr,3)
		coordsarr = np.expand_dims(coordsarr,3)

		self.quadcoords = quadcoords + coordsarr

	
	#our transform goes from reference space to physical space
	def create_transforms(self,dm,*nxs): #this will create coordinate transforms
		#xphys = alpha * xref + beta, where alpha=dx/refx
		self.alpha = []
		self.beta = []
		for i in xrange(self.dims):
			a,b = self.ref_cell[i]
			c,d = self.phys_cell[i]
			self.alpha.append((d-c)/(b-a))
			self.beta.append((d+c) - self.alpha[i]*(b+a)/2.)
		alpha = np.array(self.alpha)
		beta = np.array(self.beta)
			
		local_nxs = []
		if self.dims >= 1:
			local_nxs.append(dm.getRanges()[0][1] - dm.getRanges()[0][0])
		if self.dims >= 2:
			local_nxs.append(dm.getRanges()[1][1] - dm.getRanges()[1][0])
		if self.dims >= 3:
			local_nxs.append(dm.getRanges()[2][1] - dm.getRanges()[2][0])
		if self.dims == 1:
			local_nxs.append(1) #k
			local_nxs.append(1) #j
		if self.dims == 2:
			local_nxs.append(1) #k
		local_nxs = list(reversed(local_nxs))
		
		#alpha and beta have shape [nx][ny][nz][qx][qy][qz][ndim]
		newshape = list(local_nxs)
		newshape.append(self.quad.wts.shape[0])
		newshape.append(self.quad.wts.shape[1])
		newshape.append(self.quad.wts.shape[2])
		newshape.append(1)
		self.alpha = np.tile(alpha,newshape)
		self.beta = np.tile(beta,newshape)
	
	#our transform goes from reference space to physical space
	#ie from [-1,1] boxes to [-dx/2,dx/2] boxes
	def create_jacobians(self,dm,*nxs): #this will create Jacobians of coordinate transforms
		#these are just diagonal matrices with diagonal values given by alpha or its inverse
		alpha = self.alpha[0,0,0,0,0,0,:]
		self.J = np.diag(alpha)
		self.JT = np.diag(alpha)
		self.Jinv = np.diag(1./alpha)
		self.JTinv = np.diag(1./alpha)
		#determinanats are the trace of the matrix for a diagonal matrix
		self.detJ = np.prod(alpha)
		self.detJinv = np.prod(1./alpha)
		
		
		#useful combinations
		self.JTJ = np.dot(self.JT,self.J)
		self.JinvJTinv =  np.dot(self.Jinv,self.JTinv)
		if self.dims == 3:
			rot = np.array([[0,-1,0],[1,0,0],[0,0,1]])
		if self.dims == 2:
			rot = np.array([[0,-1],[1,0]])
		if self.dims == 1:
			rot = np.array([[1],])
		self.JTrotJ = np.dot(self.JT,np.dot(rot,self.J))
		self.JinvrotJTinv = np.dot(self.Jinv,np.dot(rot,self.JTinv))
		self.rotJ = np.dot(rot,self.J)
		self.rotJTinv = np.dot(rot,self.JTinv)

		local_nxs = []
		if self.dims >= 1:
			local_nxs.append(dm.getRanges()[0][1] - dm.getRanges()[0][0])
		if self.dims >= 2:
			local_nxs.append(dm.getRanges()[1][1] - dm.getRanges()[1][0])
		if self.dims >= 3:
			local_nxs.append(dm.getRanges()[2][1] - dm.getRanges()[2][0])
		if self.dims == 1:
			local_nxs.append(1) #k
			local_nxs.append(1) #j
		if self.dims == 2:
			local_nxs.append(1) #k
		local_nxs = list(reversed(local_nxs))
			
		newshape = list(local_nxs)
		newshape.append(self.quad.wts.shape[0])
		newshape.append(self.quad.wts.shape[1])
		newshape.append(self.quad.wts.shape[2])
		self.detJ = np.tile(self.detJ,newshape)
		self.detJinv = np.tile(self.detJinv,newshape)

		newshape = list(local_nxs)
		newshape.append(self.quad.wts.shape[0])
		newshape.append(self.quad.wts.shape[1])
		newshape.append(self.quad.wts.shape[2])
		newshape.append(1)
		newshape.append(1)
		self.J = np.tile(self.J,newshape)
		self.JT = np.tile(self.JT,newshape)
		self.Jinv = np.tile(self.Jinv,newshape)
		self.JTinv = np.tile(self.JTinv,newshape)
		self.JTJ = np.tile(self.JTJ,newshape)
		self.JinvJTinv = np.tile(self.JinvJTinv,newshape)
		self.JTrotJ = np.tile(self.JTrotJ,newshape)
		self.JinvrotJTinv = np.tile(self.JinvrotJTinv,newshape)
		self.rotJ = np.tile(self.rotJ,newshape)
		self.rotJTinv = np.tile(self.rotJTinv,newshape)

		#J has shape [nx][ny][nz][qx][qy][qz][ndim][ndim]
		#detJ has shape [nx][ny][nz][qx][qy][qz][ndim][ndim]
		
		self.JTJdetJinv = self.JTJ * np.expand_dims(np.expand_dims(self.detJinv,axis=-1),axis=-1)
		self.JTrotJdetJinv = self.JTrotJ * np.expand_dims(np.expand_dims(self.detJinv,axis=-1),axis=-1)
		self.JinvJTinvdetJ  = self.JinvJTinv * np.expand_dims(np.expand_dims(self.detJ,axis=-1),axis=-1)
		
class DistortedPeriodic2DBox(Geometry):
	def __init__(self,ref_cell,a,quadrature,*args):
		self.dxs = args
		self.dims = len(args)
		self.ref_cell = ref_cell
		self.comp_cell = []
		for dx in self.dxs:
			self.comp_cell.append([-dx/2.,dx/2.])
		self.quad = quadrature
		self.a = a
		self.use_J = True
		self.adjusted = False
		self.toy = False
		self.other = False
		self.other2 = True
		#other is a fully distorted grid
		#other2 is a rectangular distorted grid
		#Valid options are Toy=True, use_J = True or False; AND Toy=False, use_J = True, adjusted = False
	#Build as cartesian -> transformed
	def create_plotting_quad_coordinates(self,dm,nxs,bcs,quadrature):

		local_nxs = []
		if self.dims >= 1:
			local_nxs.append(dm.getRanges()[0][1] - dm.getRanges()[0][0])
		if self.dims >= 2:
			local_nxs.append(dm.getRanges()[1][1] - dm.getRanges()[1][0])
		if self.dims >= 3:
			local_nxs.append(dm.getRanges()[2][1] - dm.getRanges()[2][0])
		if self.dims == 1:
			local_nxs.append(1) #k
			local_nxs.append(1) #j
		if self.dims == 2:
			local_nxs.append(1) #k
		local_nxs = list(reversed(local_nxs))
		
		coordsvec = dm.getCoordinates()
		coordsarr = coordsvec.getArray().copy()
		
		newshape = list(local_nxs)
		newshape.append(len(nxs))
		coordsarr = np.reshape(coordsarr,(np.prod(np.array(local_nxs,dtype=np.int32)),len(nxs)),order='C')
		coordsarr = np.reshape(coordsarr,newshape,order='C')
				
		quadcoords_cell = quadrature.pts[:,:,:,:len(nxs)]
		quadcoords = np.zeros(quadcoords_cell.shape)
		for i in range(len(nxs)):
			quadcoords[:,:,:,i] = self.alpha[i]*quadcoords_cell[:,:,:,i] + self.beta[i]
		
		coordsarr = np.expand_dims(coordsarr,3)
		coordsarr = np.expand_dims(coordsarr,3)
		coordsarr = np.expand_dims(coordsarr,3)
		
		cartesian = quadcoords + coordsarr
		x = cartesian[:,:,:,:,:,:,0]
		y = cartesian[:,:,:,:,:,:,1]
		physical = np.zeros(cartesian.shape)
		
		if self.toy:
			physical[:,:,:,:,:,:,0] = x + self.a * 1./2. * np.sin(y/self.a)
			physical[:,:,:,:,:,:,1] = y + self.a * np.sin(x/self.a)
		if self.other:
			physical[:,:,:,:,:,:,0] = x + self.a * 0.2 * np.sin(x/self.a) * np.sin(y/self.a)
			physical[:,:,:,:,:,:,1] = y + self.a * 0.3 * np.sin(y/self.a) * np.sin(x/self.a)
		if self.other2:
			physical[:,:,:,:,:,:,0] = x + self.a * 0.2 * np.sin(x/self.a)
			physical[:,:,:,:,:,:,1] = y + self.a * 0.3 * np.sin(y/self.a)
		
		return cartesian,physical
		
	#Build as ref -> cartesian -> transformed
	def create_quad_coordinates(self,dm,nxs,bcs):

		xmin = 0.0
		xmax = 0.0
		ymin = 0.0
		ymax = 0.0
		zmin = 0.0
		zmax = 0.0
		nxs_sum = 1
		if self.dims >= 1:
			xmin = 0.5*self.dxs[0]
			if bcs[0] == 'periodic': #this is needed due to strange behaviour of DMDA setUniformCoordinates
				xmax = nxs[0]*self.dxs[0] + 0.5*self.dxs[0]
			else:
				xmax = nxs[0]*self.dxs[0] - 0.5*self.dxs[0]
			nxs_sum = nxs_sum * (dm.getRanges()[0][1] - dm.getRanges()[0][0])
		if self.dims >= 2:
			ymin = 0.5*self.dxs[1]
			if bcs[1] == 'periodic': #this is needed due to strange behaviour of DMDA setUniformCoordinates
				ymax = nxs[1]*self.dxs[1] + 0.5*self.dxs[1]
			else:
				ymax = nxs[1]*self.dxs[1] - 0.5*self.dxs[1]
			nxs_sum = nxs_sum * (dm.getRanges()[1][1] - dm.getRanges()[1][0])
		if self.dims >= 3:
			zmin = 0.5*self.dxs[2]
			if bcs[2] == 'periodic': #this is needed due to strange behaviour of DMDA setUniformCoordinates
				zmax = nxs[2]*self.dxs[2] + 0.5*self.dxs[2]
			else:
				zmax = nxs[2]*self.dxs[2] - 0.5*self.dxs[2]	
			nxs_sum = nxs_sum * (dm.getRanges()[2][1] - dm.getRanges()[2][0])

		xmin = xmin - np.pi * self.a
		xmax = xmax - np.pi * self.a
		ymin = ymin - np.pi * self.a
		ymax = ymax - np.pi * self.a
		
		dm.setUniformCoordinates(xmin=xmin, xmax=xmax,ymin=ymin, ymax=ymax, zmin=zmin, zmax=zmax)

		local_nxs = []
		if self.dims >= 1:
			local_nxs.append(dm.getRanges()[0][1] - dm.getRanges()[0][0])
		if self.dims >= 2:
			local_nxs.append(dm.getRanges()[1][1] - dm.getRanges()[1][0])
		if self.dims >= 3:
			local_nxs.append(dm.getRanges()[2][1] - dm.getRanges()[2][0])
		if self.dims == 1:
			local_nxs.append(1) #k
			local_nxs.append(1) #j
		if self.dims == 2:
			local_nxs.append(1) #k
		local_nxs = list(reversed(local_nxs))
		
		coordsvec = dm.getCoordinates()
		coordsarr = coordsvec.getArray().copy()
		
		newshape = list(local_nxs)
		newshape.append(len(nxs))
		coordsarr = np.reshape(coordsarr,(np.prod(np.array(local_nxs,dtype=np.int32)),len(nxs)),order='C')
		coordsarr = np.reshape(coordsarr,newshape,order='C')
				
		quadcoords_cell = self.quad.pts[:,:,:,:len(nxs)]
		quadcoords = np.zeros(quadcoords_cell.shape)
		for i in range(len(nxs)):
			quadcoords[:,:,:,i] = self.alpha[i]*quadcoords_cell[:,:,:,i] + self.beta[i]
		
		coordsarr = np.expand_dims(coordsarr,3)
		coordsarr = np.expand_dims(coordsarr,3)
		coordsarr = np.expand_dims(coordsarr,3)

		quadcoords = quadcoords + coordsarr
		
		#zeta and eta are coordinates in cartesian space
		self.zeta = quadcoords[:,:,:,:,:,:,0]
		self.eta = quadcoords[:,:,:,:,:,:,1]
		
		#x and y and quadcoords are coordinates in transformed space
		if self.toy:
			x = self.zeta + self.a * 1./2. * np.sin(self.eta/self.a) 
			y = self.eta + self.a * np.sin(self.zeta/self.a)
		if self.other:
			x = self.zeta + self.a * 0.2 * np.sin(self.zeta/self.a)  * np.sin(self.eta/self.a)
			y = self.eta + self.a * 0.3 * np.sin(self.eta/self.a)  * np.sin(self.zeta/self.a)
		if self.other2:
			x = self.zeta + self.a * 0.2 * np.sin(self.zeta/self.a)
			y = self.eta + self.a * 0.3 * np.sin(self.eta/self.a)
		self.quadcoords = np.concatenate((np.expand_dims(x,axis=-1),np.expand_dims(y,axis=-1)),axis=-1)
			
	#Just need these to help create the Jacobians, and they are constant
	#These are JUST the ref -> cartesian transforms
	#Ie from [-1,1] to [-dx/2,dx/2]
	def create_transforms(self,dm,*nxs): #this will create coordinate transforms
		#xcomp = alpha * xref + beta, where alpha=dx/refx (=dx/2. for our case)
		#so actully beta = 0
		self.alpha = []
		self.beta = []
		for i in xrange(self.dims):
			a,b = self.ref_cell[i]
			c,d = self.comp_cell[i]
			self.alpha.append((d-c)/(b-a))
			self.beta.append((d+c) - self.alpha[i]*(b+a)/2.)
		self.alpha = np.array(self.alpha)
		self.beta = np.array(self.beta)
	
	#This is the composition of a transform from cartesian (ie a set of [-dx/2,dx/2] cells) to transformed space and the transform from ref to cartesian
	#Let  x = f(zeta,eta), y = g(zeta,eta) be the transforms from cartesian to transformed and zeta = alpha0 * u, eta = alpha1 * v be the transforms from ref to cartesian
	#where x,y are coordinates in transformed ie physical space, zeta,eta are coordinates in cartesian space and u,v are coordinates in reference space
	#Then J(0,0) = J00, etc. are given by
	#J00 = dx/du = df/dzeta dzeta/du + df/deta deta/du = df/dzeta dzeta/du
	#J01 = dx/dv = df/dzeta dzeta/dv + df/deta deta/dv = df/deta deta/dv
	#J10 = dy/du = dg/dzeta dzeta/du + dg/deta deta/du = dg/dzeta dzeta/du
	#J11 = dy/dv = dg/dzeta dzeta/dv + dg/deta deta/dv = dg/deta deta/dv
	
	#For Toy:
	#Since df/dzeta = 1, df/deta = 0.5 * np.cos(self.eta/self.a), dg/deta = 1 and dg/dzeta = np.cos(self.zeta/self.a)
	#and dzeta/du = alpha0, dzeta/dv= 0, deta/du=0 and deta/dv = alpha1 this gives
	#J00 = alpha0
	#J01 = 0.5 * np.cos(self.eta/self.a) * alpha1
	#J10 = np.cos(self.zeta/self.a) * alpha0
	#J11 = alpha1
	
	#SHOULD THIS ACTUALLY BE JINV WITH THE RECIPROCAL OF ALPHAS?
	#I DONT THINK SO....
	#NEED TO CAREFULLY CHECK MIKE TOY STUFF AGAIN
	
	def create_jacobians(self,dm,*nxs): #this will create Jacobians of coordinate transforms

		Jshape = list(self.quadcoords.shape) 
		Jshape.append(2)
		
		self.J = np.zeros(Jshape)
		self.JT = np.zeros(Jshape)
		self.Jinv = np.zeros(Jshape)
		self.JTinv = np.zeros(Jshape)
		#print Jshape
		
		if self.use_J:
			if self.toy:
				J00 = self.alpha[0]
				J01 = 0.5 * np.cos(self.eta/self.a) * self.alpha[1]
				J10 = np.cos(self.zeta/self.a) * self.alpha[0]
				J11 = self.alpha[1]
			if self.other:
				J00 = (1. + 0.2 * np.cos(self.zeta/self.a) * np.sin(self.eta/self.a))  * self.alpha[0]
				J01 =  0.2 * np.sin(self.zeta/self.a) * np.cos(self.eta/self.a) * self.alpha[1]
				J10 =  0.3 * np.sin(self.eta/self.a) * np.cos(self.zeta/self.a) * self.alpha[0]
				J11 = (1. + 0.3 * np.cos(self.eta/self.a) * np.sin(self.zeta/self.a)) * self.alpha[1]
			if self.other2:
				J00 = (1. + 0.2 * np.cos(self.zeta/self.a))  * self.alpha[0]
				J01 =  0.0
				J10 =  0.0
				J11 = (1. + 0.3 * np.cos(self.eta/self.a)) * self.alpha[1]
				
			self.detJ = J00 * J11 - J01 * J10
			self.detJinv = 1.0/self.detJ
			
			self.J[:,:,:,:,:,:,0,0] = J00
			self.J[:,:,:,:,:,:,0,1] = J01
			self.J[:,:,:,:,:,:,1,0] = J10
			self.J[:,:,:,:,:,:,1,1] = J11

			self.JT[:,:,:,:,:,:,0,0] = J00
			self.JT[:,:,:,:,:,:,0,1] = J10
			self.JT[:,:,:,:,:,:,1,0] = J01
			self.JT[:,:,:,:,:,:,1,1] = J11

			self.Jinv[:,:,:,:,:,:,0,0] = J11 / self.detJ
			self.Jinv[:,:,:,:,:,:,0,1] = -J01 / self.detJ
			self.Jinv[:,:,:,:,:,:,1,0] = -J10 / self.detJ
			self.Jinv[:,:,:,:,:,:,1,1] = J00 / self.detJ

			self.JTinv[:,:,:,:,:,:,0,0] = J11 / self.detJ
			self.JTinv[:,:,:,:,:,:,0,1] = -J10 / self.detJ
			self.JTinv[:,:,:,:,:,:,1,0] = -J01 / self.detJ
			self.JTinv[:,:,:,:,:,:,1,1] = J00 / self.detJ

		if not self.use_J:
			if self.toy:
				J00 = 1. / self.alpha[0]
				J01 = 0.5 * np.cos(self.eta/self.a) / self.alpha[1]
				J10 = np.cos(self.zeta/self.a) / self.alpha[0]
				J11 = 1. / self.alpha[1]		

			self.detJinv = J00 * J11 - J01 * J10
			self.detJ = 1.0/self.detJinv
			
			#Likely that this is correct, since H1 2D works (sort of, even order stuff is not really converging...)
			self.Jinv[:,:,:,:,:,:,0,0] = J00
			self.Jinv[:,:,:,:,:,:,0,1] = J01
			self.Jinv[:,:,:,:,:,:,1,0] = J10
			self.Jinv[:,:,:,:,:,:,1,1] = J11

			self.JTinv[:,:,:,:,:,:,0,0] = J00
			self.JTinv[:,:,:,:,:,:,0,1] = J10
			self.JTinv[:,:,:,:,:,:,1,0] = J01
			self.JTinv[:,:,:,:,:,:,1,1] = J11

			#Not sure this stuff is correct- Hdiv 2D is failing!
			#but it is converging quickly in the iterative solver, which suggests that it is actually working, just with the wrong boundary conditions or rhs?
			self.J[:,:,:,:,:,:,0,0] = J11 / self.detJinv
			self.J[:,:,:,:,:,:,0,1] = -J01 / self.detJinv
			self.J[:,:,:,:,:,:,1,0] = -J10 / self.detJinv
			self.J[:,:,:,:,:,:,1,1] = J00 / self.detJinv

			self.JT[:,:,:,:,:,:,0,0] = J11 / self.detJinv
			self.JT[:,:,:,:,:,:,0,1] = -J10 / self.detJinv
			self.JT[:,:,:,:,:,:,1,0] = -J01 / self.detJinv
			self.JT[:,:,:,:,:,:,1,1] = J00 / self.detJinv
			
		if self.adjusted:

			self.Jadj = np.zeros(Jshape)
			self.JTadj = np.zeros(Jshape)
			self.Jinvadj = np.zeros(Jshape)
			self.JTinvadj = np.zeros(Jshape)

			if self.toy:
				J00 = 1.
				J01 = 0.5 * np.cos(self.eta/self.a) 
				J10 = np.cos(self.zeta/self.a) 
				J11 = 1.
							
			if not self.use_J:

				self.detJinvadj = J00 * J11 - J01 * J10
				self.detJadj = 1.0/self.detJinvadj
				
				#Likely that this is correct, since H1 2D works (sort of, even order stuff is not really converging...)
				self.Jinvadj[:,:,:,:,:,:,0,0] = J00
				self.Jinvadj[:,:,:,:,:,:,0,1] = J01
				self.Jinvadj[:,:,:,:,:,:,1,0] = J10
				self.Jinvadj[:,:,:,:,:,:,1,1] = J11

				self.JTinvadj[:,:,:,:,:,:,0,0] = J00
				self.JTinvadj[:,:,:,:,:,:,0,1] = J10
				self.JTinvadj[:,:,:,:,:,:,1,0] = J01
				self.JTinvadj[:,:,:,:,:,:,1,1] = J11

				#Not sure this stuff is correct- Hdiv 2D is failing!
				#but it is converging quickly in the iterative solver, which suggests that it is actually working, just with the wrong boundary conditions or rhs?
				self.Jadj[:,:,:,:,:,:,0,0] = J11 / self.detJinvadj
				self.Jadj[:,:,:,:,:,:,0,1] = -J01 / self.detJinvadj
				self.Jadj[:,:,:,:,:,:,1,0] = -J10 / self.detJinvadj
				self.Jadj[:,:,:,:,:,:,1,1] = J00 / self.detJinvadj

				self.JTadj[:,:,:,:,:,:,0,0] = J11 / self.detJinvadj
				self.JTadj[:,:,:,:,:,:,0,1] = -J10 / self.detJinvadj
				self.JTadj[:,:,:,:,:,:,1,0] = -J01 / self.detJinvadj
				self.JTadj[:,:,:,:,:,:,1,1] = J00 / self.detJinvadj
				
			if self.use_J:

				self.detJadj = J00 * J11 - J01 * J10
				self.detJinvadj = 1.0/self.detJadj
				
				self.Jadj[:,:,:,:,:,:,0,0] = J00
				self.Jadj[:,:,:,:,:,:,0,1] = J01
				self.Jadj[:,:,:,:,:,:,1,0] = J10
				self.Jadj[:,:,:,:,:,:,1,1] = J11

				self.JTadj[:,:,:,:,:,:,0,0] = J00
				self.JTadj[:,:,:,:,:,:,0,1] = J10
				self.JTadj[:,:,:,:,:,:,1,0] = J01
				self.JTadj[:,:,:,:,:,:,1,1] = J11

				self.Jinvadj[:,:,:,:,:,:,0,0] = J11 / self.detJadj
				self.Jinvadj[:,:,:,:,:,:,0,1] = -J01 / self.detJadj
				self.Jinvadj[:,:,:,:,:,:,1,0] = -J10 / self.detJadj
				self.Jinvadj[:,:,:,:,:,:,1,1] = J00 / self.detJadj

				self.JTinvadj[:,:,:,:,:,:,0,0] = J11 / self.detJadj
				self.JTinvadj[:,:,:,:,:,:,0,1] = -J10 / self.detJadj
				self.JTinvadj[:,:,:,:,:,:,1,0] = -J01 / self.detJadj
				self.JTinvadj[:,:,:,:,:,:,1,1] = J00 / self.detJadj
			self.JTJadj = np.einsum('...kl,...lm->...km', self.JTadj,self.Jadj)
			self.JinvJTinvadj =  np.einsum('...kl,...lm->...km', self.Jinvadj,self.JTinvadj)
		 
		#useful combinations
		#THESE WORK FOR ANY DIMENSION!!!
		self.JTJ = np.einsum('...kl,...lm->...km', self.JT,self.J)
		self.JinvJTinv =  np.einsum('...kl,...lm->...km', self.Jinv,self.JTinv)
		
		if self.dims == 3:
			rot = np.array([[0,-1,0],[1,0,0],[0,0,1]])
		if self.dims == 2:
			rot = np.array([[0,-1],[1,0]])
		if self.dims == 1:
			rot = np.array([[1],])
			
		#MIGHT BE POSSIBLE HERE TO DO EINSUM('KL,...LM->...KM')
		#ie don't bother tiling rot
		#CHECK THIS!
		local_nxs = []
		if self.dims >= 1:
			local_nxs.append(dm.getRanges()[0][1] - dm.getRanges()[0][0])
		if self.dims >= 2:
			local_nxs.append(dm.getRanges()[1][1] - dm.getRanges()[1][0])
		if self.dims >= 3:
			local_nxs.append(dm.getRanges()[2][1] - dm.getRanges()[2][0])
		if self.dims == 1:
			local_nxs.append(1) #k
			local_nxs.append(1) #j
		if self.dims == 2:
			local_nxs.append(1) #k
		local_nxs = list(reversed(local_nxs))
			
		newshape = list(local_nxs)
		newshape.append(self.quad.wts.shape[0])
		newshape.append(self.quad.wts.shape[1])
		newshape.append(self.quad.wts.shape[2])
		newshape.append(1)
		newshape.append(1)
		self.rot = np.tile(rot,newshape)

		self.rotJ =  np.einsum('...kl,...lm->...km', rot,self.J)
		self.rotJTinv =  np.einsum('...kl,...lm->...km', rot,self.JTinv)

		self.JTrotJ =  np.einsum('...kl,...lm->...km', self.JT,self.rotJ)
		self.JinvrotJTinv =  np.einsum('...kl,...lm->...km', self.Jinv,self.rotJTinv)

class UniformBoxSymbolic(Geometry):
	#IMPLICIT ASSUMPTION HERE THAT REFERENCE CELL IS [-1,1]^N
	def __init__(self,ref_cell,*args):
		self.dxs = args
		self.dims = len(args)

	def create_quad_coordinates(self,dm,nxs,bcs):
		
		if self.dims >= 1:
			self.xcoords = sympy.Matrix(np.expand_dims(np.arange(nxs[0]),axis=1)) * self.dxs[0] + sympy.ones(nxs[0],1) * self.dxs[0]/2
		if self.dims >= 2:
			self.ycoords = sympy.Matrix(np.expand_dims(np.arange(nxs[1]),axis=1)) * self.dxs[1] + sympy.ones(nxs[1],1) * self.dxs[1]/2
		if self.dims == 3:
			self.zcoords = sympy.Matrix(np.expand_dims(np.arange(nxs[2]),axis=1)) * self.dxs[2] + sympy.ones(nxs[2],1) * self.dxs[2]/2
			
	def create_transforms(self,*nxs): #this will create coordinate transforms
		#xphys = alpha * xref + beta, where alpha=dx/refx
		self.alpha = []
		self.beta = []
		self.alphainv = []
		for i in xrange(self.dims):
			self.alpha.append(sympy.Rational(1,2)*self.dxs[i])
			self.beta.append(0)
			self.alphainv.append(2/self.dxs[i])
		
	def create_jacobians(self,*nxs): #this will create Jacobians of coordinate transforms
		#these are just diagonal matrices with diagonal values given by alpha or its inverse
		self.J = sympy.diag(*self.alpha)
		self.JT = sympy.diag(*self.alpha)
		self.Jinv = sympy.diag(*self.alphainv)
		self.JTinv = sympy.diag(*self.alphainv)
		#determinanats are the trace of the matrix for a diagonal matrix
		self.detJ = self.J.det()
		self.detJinv = self.Jinv.det()
		
		#useful combinations
		self.JTJ = self.JT * self.J
		self.JinvJTinv = self.Jinv * self.JTinv
		if self.dims == 3:
			rot = sympy.Matrix([[0,-1,0],[1,0,0],[0,0,1]])
		if self.dims == 2:
			rot = sympy.Matrix([[0,-1],[1,0]])
		if self.dims == 1:
			rot = sympy.Matrix([[1],])
		self.JTrotJ = self.JT * rot * self.J
		self.JinvrotJTinv = self.Jinv * rot * self.JTinv
		self.rotJ = rot * self.J
		self.rotJTinv = rot * self.JTinv
