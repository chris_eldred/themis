import numpy as np
from math import sqrt
from lagrange import *
from galerkindifferences import *

class Quadrature:
	def __init__(self):
		pass
	def npoints(self):
		return self.n
	def get_points_and_weights(self):
		return self.pts,self.wts
	def get_dimension(self):
		return self.dimension

class EmptyQuad(Quadrature):
	def __init__(self,n):
		self.n = 1
		self.dimension = 1
		self.ref_cell = [-1.,1.]
		self.pts = np.array([[0.,],],dtype=np.float64)
		self.wts = np.array([1.,],dtype=np.float64)

class GaussLobattoLegendre1D(Quadrature):
	def __init__(self,n):
		self.n = n
		self.dimension = 1
		self.ref_cell = [-1.,1.]

		pts = np.array(gauss_lobatto(n),dtype=np.float64) 
		self.wts = np.zeros((n))
		if n>=3:
			x = sympy.var('x')
			p = sympy.polys.orthopolys.legendre_poly(n-1,x) #legendre_poly(n)
			interior_pts = pts[1:-1]
			p = sympy.polys.orthopolys.legendre_poly(n-1,x) #legendre_poly(n)
			pfunc = sympy.lambdify(x, p, "numpy")
			pnum = pfunc(interior_pts)
			self.wts[1:-1] = 2./((n*(n-1.))*np.square(pnum))
		self.wts[0] = 2./(n*(n-1.))
		self.wts[-1] = 2./(n*(n-1.))
		self.pts = np.expand_dims(pts,axis=1)

class GaussChebyshev1st1D(Quadrature):
	def __init__(self,n):
		self.n = n
		self.dimension = 1
		self.ref_cell = [-1.,1.]

		indices = np.arange(1,n+1)
		pts = np.cos((2.*indices -1.)/(2.*n)*np.pi)
		self.wts = np.ones(n) * np.pi/n
		self.pts = np.expand_dims(pts,axis=1)
		
class GaussChebyshev2nd1D(Quadrature):
	def __init__(self,n):
		self.n = n
		self.dimension = 1
		self.ref_cell = [-1.,1.]

		indices = np.arange(1,n+1)
		pts = np.cos(indices/(n+1.)*np.pi)
		self.wts = np.pi/(n+1.) * np.square(np.sin(indices/(n+1.)*np.pi))
		self.pts = np.expand_dims(pts,axis=1)

class GaussLobattoChebyshev1st1D(Quadrature):
	def __init__(self,n):
		self.n = n
		self.dimension = 1
		self.ref_cell = [-1.,1.]

		indices = np.arange(n)
		pts = -1.*np.cos(np.pi * indices / (n-1))
		self.wts = np.ones(n) * np.pi/(n-1)
		self.wts[0] = np.pi/(2.*(n-1))
		self.wts[-1] = np.pi/(2.*(n-1))
		self.pts = np.expand_dims(pts,axis=1)
		
class GaussLobattoChebyshev2nd1D(Quadrature):
	def __init__(self,n):
		self.n = n
		self.dimension = 1
		self.ref_cell = [-1.,1.]

class NewtonCotesOpen1D(Quadrature):
	def __init__(self,n):
		self.n = n
		self.dimension = 1
		self.ref_cell = [-1.,1.]

		pts = np.linspace(-1.,1.,n+2)
		pts = pts[1:-1]
		self.wts = np.zeros((n))
		x = sympy.var('x')
		for i in range(n):
			li = lagrange_poly_support(i,pts,x)
			self.wts[i] = sympy.integrate(li,(x,-1.,1.))
		self.pts = np.expand_dims(pts,axis=1)
		
class NewtonCotesClosed1D(Quadrature):
	def __init__(self,n):
		self.n = n
		self.dimension = 1
		self.ref_cell = [-1.,1.]

		pts = np.linspace(-1.,1.,n)

		self.wts = np.zeros((n))
		x = sympy.var('x')
		for i in range(n):
			li = lagrange_poly_support(i,pts,x)
			self.wts[i] = sympy.integrate(li,(x,-1.,1.))
		self.pts = np.expand_dims(pts,axis=1)

class GaussLegendre1D(Quadrature):
	def __init__(self,n):
		self.n = n
		self.dimension = 1
		self.ref_cell = [-1.,1.]
		
		pts = np.array(gauss_legendre(n),dtype=np.float64)
		x = sympy.var('x')
		p = sympy.polys.orthopolys.legendre_poly(n,x) #legendre_poly(n)
		self.wts = np.zeros((n))
		pd = sympy.diff(p,x)
		pdfunc = sympy.lambdify(x, pd, "numpy")
		diffp = pdfunc(pts)
		self.wts = 2./((1.-np.square(pts))*np.square(diffp))
		self.pts = np.expand_dims(pts,axis=1)
			
class TensorProductQuadrature(Quadrature):
	def __init__(self,quadlist,nlist):
		if len(quadlist) == 1:
			quadlist.append(EmptyQuad)
			quadlist.append(EmptyQuad)
			nlist.append(1)
			nlist.append(1)
		if len(quadlist) == 2:
			quadlist.append(EmptyQuad)
			nlist.append(1)
		self.dimension = len(quadlist)
		self.pts_list = []
		self.wts_list = []
		nquad = []
		for quad,n in zip(quadlist,nlist):
			quad = quad(n)
			pts,wts = quad.get_points_and_weights()
			self.pts_list.append(pts[:,0]) 
			self.wts_list.append(wts)
			nquad.append(wts.shape[0])
		
		pts = np.zeros((nquad + [len(quadlist),]))
		wts = np.zeros((nquad))
		for nx in range(nquad[0]):
			for ny in range(nquad[1]):
				for nz in range(nquad[2]):
					wts[nx][ny][nz]	= self.wts_list[0][nx] * self.wts_list[1][ny] * self.wts_list[2][nz] 			
					pts[nx][ny][nz][0] = self.pts_list[0][nx]
					pts[nx][ny][nz][1] = self.pts_list[1][ny]
					pts[nx][ny][nz][2] = self.pts_list[2][nz]
		self.wts = wts
		self.pts = pts
		
#######################################

def get_quadrature(quadchoice):	
	if quadchoice == 'gauss':
		quad = GaussLegendre1D
	if quadchoice == 'gauss-lobatto':
		quad = GaussLobattoLegendre1D
	if quadchoice == 'gauss-chebyshev1st':
		quad = GaussChebyshev1st1D
	if quadchoice == 'gauss-lobatto-chebyshev1st':
		quad = GaussLobattoChebyshev1st1D
	if quadchoice == 'gauss-chebyshev2nd':
		quad = GaussChebyshev2nd1D
	#if quadchoice == 'gauss-lobatto-chebyshev2nd':
	#	quad = GaussLobattoChebyshev2nd1D
	if quadchoice == 'newton-cotes-open':
		quad = NewtonCotesOpen1D
	if quadchoice == 'newton-cotes-closed':
		quad = NewtonCotesClosed1D
	return quad
	
if __name__ == "__main__":
	GaussLobattoChebyshev1st1D(2)
	GaussLobattoChebyshev1st1D(3)
	GaussLobattoChebyshev1st1D(4)
	GaussLobattoChebyshev1st1D(5)
