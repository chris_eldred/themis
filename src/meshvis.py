import numpy as np
#import scipy as sp
import matplotlib.pyplot as plt
from math import *
from matplotlib import cm
import matplotlib.collections as mcoll
import matplotlib.colors as mcolors
#from matplotlib.backends import pylab_setup

#new_figure_manager, draw_if_interactive, _show = pylab_setup()

def draw_edges(AxesInstance, polygons, edgecolor, linestyle, linewidths, **kwargs):

	if not AxesInstance._hold: AxesInstance.cla()
	
	xmin = np.min(polygons[:,:,0])
	xmax = np.max(polygons[:,:,0])
	ymin = np.min(polygons[:,:,1])
	ymax = np.max(polygons[:,:,1])
	
	AxesInstance._process_unit_info(xdata=polygons[:,0],ydata=polygons[:,1],kwargs=kwargs)
	
	collection = mcoll.PolyCollection(
		polygons,
		linewidths = linewidths,
		transOffset = AxesInstance.transData,
		)
		
	collection.set_facecolor('none')
	collection.set_edgecolors(edgecolor)
	collection.set_linestyle(linestyle)		
	collection.update(kwargs)

	corners = ((xmin, ymin), (xmax, ymax))
	AxesInstance.update_datalim( corners)
	AxesInstance.autoscale_view(tight=True)

	# add the collection last
	AxesInstance.add_collection(collection)
	return collection

def drawedges(polygons, edgecolor='k', linestyle='solid', linewidths=None, hold=None, **kwargs):
    ax = plt.gca()
    # allow callers to override the hold state by passing hold=True|False
    washold = ax.ishold()

    if hold is not None:
        ax.hold(hold)
    try:
        ret = draw_edges(ax, polygons, edgecolor, linestyle, linewidths, **kwargs)
    finally:
        ax.hold(washold)
    plt.sci(ret)
    return ret
   
