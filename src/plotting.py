from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
#from matplotlib.ticker import LinearLocator, FormatStrFormatter
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.collections import RegularPolyCollection
from meshvis import *
import matplotlib

def meshplot_1d(mesh):
	xvert = mesh.vertex_coords
	xcell = mesh.cell_coords
	ivert = mesh.vertex_i
	icell = mesh.cell_i
	natvert = mesh.vertex_nat
	natcell = mesh.cell_nat
	globvert = mesh.vertex_glob
	globcell = mesh.cell_glob
	rankcell = mesh.cell_rank
	rankvertex = mesh.vertex_rank
	
	symbols = ['x','o']
	xlist = [xvert,xcell]
	ilist = [ivert,icell]
	natlist = [natvert,natcell]
	globlist = [globvert,globcell]
	ranklist = [rankvertex,rankcell]

	# define the colormap
	cmap = plt.cm.jet
	# extract all colors from the .jet map
	cmaplist = [cmap(i) for i in range(cmap.N)]
	# force the first color entry to be grey
	#cmaplist[0] = (.5,.5,.5,1.0)
	# create the new map
	cmap = cmap.from_list('Custom cmap', cmaplist, cmap.N)
	# define the bins and normalize
	bounds = np.linspace(0,np.amax(rankcell)+1,np.amax(rankcell)+2)
	norm = matplotlib.colors.BoundaryNorm(bounds, cmap.N)

	plt.figure(figsize=(18, 6), dpi=100)
	for x,i,r,symb in zip(xlist,ilist,ranklist,symbols):
		y = np.zeros(x.shape)
		plt.xlim(x[0]-1,x[-1]+1)
		for l in xrange(i.shape[0]):
			plt.text(x[l],0.,str(i[l]))
		plt.scatter(x,y,marker=symb,s=65,c=r,cmap=cmap, norm=norm)
	plt.colorbar()
	plt.vlines(xvert,-0.5,0.5)
	plt.xlabel('x')
	plt.ylabel('y')
	plt.savefig('mesh.i.png',bbox_inches='tight')

	plt.figure(figsize=(18, 6), dpi=100)
	for x,i,r,symb in zip(xlist,natlist,ranklist,symbols):
		y = np.zeros(x.shape)
		plt.xlim(x[0]-1,x[-1]+1)
		for l in xrange(i.shape[0]):
			plt.text(x[l],0.,str(i[l]))
		plt.scatter(x,y,marker=symb,s=65,c=r,cmap=cmap, norm=norm)
	plt.colorbar()
	plt.vlines(xvert,-0.5,0.5)
	plt.xlabel('x')
	plt.ylabel('y')
	plt.savefig('mesh.nat.png',bbox_inches='tight')
	
	plt.figure(figsize=(18, 6), dpi=100)
	for x,i,r,symb in zip(xlist,globlist,ranklist,symbols):
		y = np.zeros(x.shape)
		plt.xlim(x[0]-1,x[-1]+1)
		for l in xrange(i.shape[0]):
			plt.text(x[l],0.,str(i[l]))
		plt.scatter(x,y,marker=symb,s=65,c=r,cmap=cmap, norm=norm)
	plt.colorbar()
	plt.vlines(xvert,-0.5,0.5)
	plt.xlabel('x')
	plt.ylabel('y')
	plt.savefig('mesh.global.png',bbox_inches='tight')
	
def get_polygons(xy,dx):
	polygons = np.zeros((xy.shape[0]*xy.shape[1],4,2))
	#i,j
	polygons[:,0,0] = np.ravel(xy[:,:,0] - 0.5*dx)
	polygons[:,0,1] = np.ravel(xy[:,:,1] - 0.5*dx)
	#i+1,j
	polygons[:,1,0] = np.ravel(xy[:,:,0] + 0.5*dx)
	polygons[:,1,1] = np.ravel(xy[:,:,1] - 0.5*dx)
	#i+1,j+1
	polygons[:,2,0] = np.ravel(xy[:,:,0] + 0.5*dx)
	polygons[:,2,1] = np.ravel(xy[:,:,1] + 0.5*dx)
	#i,j+1
	polygons[:,3,0] = np.ravel(xy[:,:,0] - 0.5*dx)
	polygons[:,3,1] = np.ravel(xy[:,:,1] + 0.5*dx)
	return polygons

def meshplot_2d(mesh):
	xyvert = mesh.vertex_coords
	xycell = mesh.cell_coords
	xyuedge = mesh.uedge_coords
	xyvedge = mesh.vedge_coords
	ivert = mesh.vertex_i
	icell = mesh.cell_i
	iuedge = mesh.uedge_i
	ivedge = mesh.vedge_i
	jvert = mesh.vertex_j
	jcell = mesh.cell_j
	juedge = mesh.uedge_j
	jvedge = mesh.vedge_j
	natvert = mesh.vertex_nat
	natcell = mesh.cell_nat
	natuedge = mesh.uedge_nat
	natvedge = mesh.vedge_nat
	globvert = mesh.vertex_glob
	globcell = mesh.cell_glob
	globuedge = mesh.uedge_glob
	globvedge = mesh.vedge_glob
	rankcell = mesh.cell_rank
	rankvertex = mesh.vertex_rank
	rankuedge = mesh.uedge_rank
	rankvedge = mesh.vedge_rank

	symbols = ['x','o','_','|']
	xlist = [xyvert,xycell,xyuedge,xyvedge]
	ilist = [ivert,icell,iuedge,ivedge]
	jlist = [jvert,jcell,juedge,jvedge]
	natlist = [natvert,natcell,natuedge,natvedge]
	globlist = [globvert,globcell,globuedge,globvedge]
	ranklist = [rankvertex,rankcell,rankuedge,rankvedge]
	
	
	# define the colormap
	cmap = plt.cm.jet
	# extract all colors from the .jet map
	cmaplist = [cmap(i) for i in range(cmap.N)]
	# force the first color entry to be grey
	#cmaplist[0] = (.5,.5,.5,1.0)
	# create the new map
	cmap = cmap.from_list('Custom cmap', cmaplist, cmap.N)
	# define the bins and normalize
	bounds = np.linspace(0,np.amax(rankcell)+1,np.amax(rankcell)+2)
	norm = matplotlib.colors.BoundaryNorm(bounds, cmap.N)
	
	plt.figure(figsize=(18, 10), dpi=100)
	for xy,i,j,r,symb in zip(xlist,ilist,jlist,ranklist,symbols):
		x = np.ravel(xy[:,:,0])
		y = np.ravel(xy[:,:,1])
		i = np.ravel(i)
		j = np.ravel(j)
		r = np.ravel(r)
		plt.xlim(x[0]-1,x[-1]+1)
		plt.ylim(y[0]-1,y[-1]+1)
		for l in xrange(i.shape[0]):
			plt.text(x[l],y[l],'('+str(i[l])+','+str(j[l])+')')
		plt.scatter(x,y,marker=symb,s=65,c=r,cmap=cmap, norm=norm)
	plt.colorbar()
	polygons = get_polygons(xycell,1.0) #dx
	drawedges(polygons)
	plt.xlabel('x')
	plt.ylabel('y')
	plt.savefig('mesh.ij.png',bbox_inches='tight')
	

	plt.figure(figsize=(18, 10), dpi=100)
	for xy,i,r,symb in zip(xlist,natlist,ranklist,symbols):
		x = np.ravel(xy[:,:,0])
		y = np.ravel(xy[:,:,1])
		i = np.ravel(i)
		j = np.ravel(j)
		r = np.ravel(r)
		plt.xlim(x[0]-1,x[-1]+1)
		plt.ylim(y[0]-1,y[-1]+1)
		for l in xrange(i.shape[0]):
			plt.text(x[l],y[l],str(i[l]))
		plt.scatter(x,y,marker=symb,s=65,c=r,cmap=cmap, norm=norm)
	plt.colorbar()
	polygons = get_polygons(xycell,1.0) #dx
	drawedges(polygons)
	plt.xlabel('x')
	plt.ylabel('y')
	plt.savefig('mesh.nat.png',bbox_inches='tight')

	plt.figure(figsize=(18, 10), dpi=100)
	for xy,i,r,symb in zip(xlist,globlist,ranklist,symbols):
		x = np.ravel(xy[:,:,0])
		y = np.ravel(xy[:,:,1])
		i = np.ravel(i)
		j = np.ravel(j)
		r = np.ravel(r)
		plt.xlim(x[0]-1,x[-1]+1)
		plt.ylim(y[0]-1,y[-1]+1)
		for l in xrange(i.shape[0]):
			plt.text(x[l],y[l],str(i[l]))
		plt.scatter(x,y,marker=symb,s=65,c=r,cmap=cmap, norm=norm)
	plt.colorbar()
	polygons = get_polygons(xycell,1.0) #dx
	drawedges(polygons)
	plt.xlabel('x')
	plt.ylabel('y')
	plt.savefig('mesh.global.png',bbox_inches='tight')
	
#offsets = np.random.rand(20,2)
#facecolors = [cm.jet(x) for x in np.random.rand(20)]
#black = (0,0,0,1)

#collection = RegularPolyCollection(
    #numsides=5, # a pentagon
    #rotation=0, sizes=(50,),
    #facecolors = facecolors,
    #edgecolors = (black,),
    #linewidths = (1,),
    #offsets = offsets,
    #transOffset = ax.transData,
    #)

def plot_1d(pde):
	x = pde.da.createNaturalVec()
	pde.da.globalToNatural(pde.u, x)
	draw = pde.petsc.Viewer.DRAW(pde.u.comm)
	pde.optdb['draw_pause'] = 3
	draw(x)

def vectorplot_1d(x,u,name):
	plt.figure()
	plt.quiver(x,0.,u,0.)
	plt.savefig(name + '.png')
	
def vectorplot_2d(xyu,xyv,u,v,name):
	xu = xyu[:,:,0]
	yu = xyu[:,:,1]
	xv = xyv[:,:,0]
	yv = xyv[:,:,1]
	plt.figure()
	plt.quiver(xu,yu,u,0.0)
	plt.quiver(xv,yv,0.0,v)
	plt.savefig(name + '.png')

def spatialplot_1d_multi(x,us,unames,name):
	plt.figure(figsize=(11, 9))
	for u,uname in zip(us,unames):
		#plt.scatter(x,u)
		plt.plot(x,u,label=uname)
	plt.legend(loc=6)
	plt.xlabel('time')
	plt.ylabel('energy')
	plt.savefig(name + '.png',bbox_inches='tight')
	
def spatialplot_1d(x,u,name):
	plt.figure(figsize=(11, 9))
	plt.plot(x,u)
	#plt.scatter(x,u)
	plt.savefig(name + '.png',bbox_inches='tight')

def spatialplot_2d(x,y,u,name):
	#contour plot
	plt.figure()
	if not (np.sum(np.abs(u - np.average(u))) == 0.0):
		try:
			plt.tricontour(x,y,u)
			plt.tricontourf(x,y,u)
			plt.colorbar()
		except ValueError:
			pass
	plt.savefig(name + '.contour.png')
	plt.clf()

	#plt.figure()
	#if not (np.sum(np.abs(u - np.average(u))) == 0.0):
		#try:
			#plt.tripcolor(x,y,u)
			#plt.colorbar()
		#except ValueError:
			#pass
	#plt.savefig(name + '.pcolor.png')
	#plt.clf()
	
	#3D plot
	#fig = plt.figure()
	#ax = fig.gca(projection='3d')
	#surf = ax.plot_trisurf(x, y, u, cmap=cm.jet, linewidth=0.2)
	#plt.savefig(name + '.surf.png')
def spatialplot_3d_slices(x,y,u,name1,name2,nz):
	#contour plot
	for k in range(nz):
		plt.figure()
		#if not (np.sum(np.abs(u - np.average(u))) == 0.0):
		xadj = np.ravel(x[k,:,:,:,:,:])
		yadj = np.ravel(y[k,:,:,:,:,:])
		uadj = np.ravel(u[k,:,:,:,:,:])
		try:
			plt.tricontour(xadj,yadj,uadj)
			plt.tricontourf(xadj,yadj,uadj)
			plt.colorbar()
		except ValueError:
			pass
		plt.savefig(name1 + '.' + str(k) + '.' + name2 + '.contour.png')
		plt.clf()

#I THINK THIS IS BROKEN?
def spatialplot_3d_slices_square(x,y,u,name1,name2,nz):
	#contour plot
	for k in range(nz):
		plt.figure()
		#if not (np.sum(np.abs(u - np.average(u))) == 0.0):
		try:
			plt.contour(x[k,:,:],y[k,:,:],u[k,:,:])
			plt.contourf(x[k,:,:],y[k,:,:],u[k,:,:])
			plt.colorbar()
		except ValueError:
			pass
		plt.savefig(name1 + '.' + str(k) + '.' + name2 + '.contour.png')
		plt.clf()
	
def spatialplot_2d_square(x,y,u,name):
	x = xy[:,:,0]
	y = xy[:,:,1]
	
	#contour plot
	plt.figure()
	if not (np.sum(np.abs(u - np.average(u))) == 0.0):
		plt.contour(x,y,u)
		plt.contourf(x,y,u)
		plt.colorbar()
	plt.savefig(name + '.contour.png')
	plt.clf()
	
	#3D plot
	#fig = plt.figure()
	#ax = fig.gca(projection='3d')
	#surf = ax.plot_surface(x, y, u, cmap=cm.jet, linewidth=0.2,rstride=1,cstride=1)
	#plt.savefig(name + '.surf.png')
	
def convergence_plot(resolution,errors,names,pltname):
	plt.figure(figsize=(11, 9))
	a1  = errors[0,0] / (resolution[0]**(1)) #this lets the theoretical covergence rate lines move down, which is useful
	firstorder = a1 * np.power(resolution,1)
	secondorder = a1 * np.power(resolution,2)
	thirdorder = a1 * np.power(resolution,3)
	fourthorder = a1 * np.power(resolution,4)
	fifthhorder = a1 * np.power(resolution,5)
	sixthhorder = a1 * np.power(resolution,6)
	plt.loglog(resolution,firstorder,linestyle='dashed')#,color='k')
	plt.loglog(resolution,secondorder,linestyle='dashed')#,color='k')
	plt.loglog(resolution,thirdorder,linestyle='dashed')#,color='k')
	plt.loglog(resolution,fourthorder,linestyle='dashed')#,color='k')
	plt.loglog(resolution,fifthhorder,linestyle='dashed')#,color='k')
	plt.loglog(resolution,sixthhorder,linestyle='dashed')#,color='k')
	for i in range(errors.shape[0]):
		plt.loglog(resolution,errors[i,:],label=names[i])
		plt.scatter(resolution,errors[i,:])
	plt.gca().invert_xaxis()
	plt.legend(loc=3)
	plt.xlabel('Grid Resolution')
	plt.ylabel('L2 Error')
	plt.savefig(pltname + '.png',bbox_inches='tight')
	
def convergence_plot_ndofs(ndofs,errors,names,pltname):
	plt.figure(figsize=(11, 9))
	#a1  = errors[0,0] / (resolution[0]**(1)) #this lets the theoretical covergence rate lines move down, which is useful
	#firstorder = a1 * np.power(resolution,1)
	#secondorder = a1 * np.power(resolution,2)
	#thirdorder = a1 * np.power(resolution,3)
	#fourthorder = a1 * np.power(resolution,4)
	#plt.loglog(resolution,firstorder,linestyle='dashed')#,color='k')
	#plt.loglog(resolution,secondorder,linestyle='dashed')#,color='k')
	#plt.loglog(resolution,thirdorder,linestyle='dashed')#,color='k')
	#plt.loglog(resolution,fourthorder,linestyle='dashed')#,color='k')
	for i in range(errors.shape[0]):
		plt.loglog(ndofs[i,:],errors[i,:],label=names[i])
		plt.scatter(ndofs[i,:],errors[i,:])
	#plt.gca().invert_xaxis()
	plt.legend(loc=3)
	plt.xlabel('Degrees of Freedom')
	plt.ylabel('L2 Error')
	plt.savefig(pltname + '.png',bbox_inches='tight')
