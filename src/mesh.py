import numpy as np
from common import *
from output import *

class Mesh():
	def __init__(self,dims,geometry,nxs,bcs):
		
		self.nxs = nxs
		self.bcs = bcs
		self.geometry = geometry
		self.ndims = dims
		
		self.blist = []
		for bc in bcs:
			if bc == 'periodic':
				self.blist.append(PETSc.DM.BoundaryType.PERIODIC)
			else:
				self.blist.append(PETSc.DM.BoundaryType.GHOSTED)
	
		# generate mesh
		#eventually add in proc_sizes=[] here, basically to play around with distribution of DMDA across processors
		self.cell_da = PETSc.DMDA().create(dim=self.ndims,dof=1,sizes=self.nxs, boundary_type = self.blist, stencil_type= PETSc.DMDA.StencilType.BOX, stencil_width=1)
		
		self.geometry.create_transforms(self.cell_da,*self.nxs)
		self.geometry.create_quad_coordinates(self.cell_da,self.nxs,self.bcs)
		self.geometry.create_jacobians(self.cell_da,*self.nxs)
				
	def get_plotting_quad_coords(self,quadrature):
		return self.geometry.create_plotting_quad_coordinates(self.cell_da,self.nxs,self.bcs,quadrature)
		
	def get_nxny(self):
		return self.nxs

	def get_local_nxny(self):
		localnxs = []
		xys = self.get_xy()
		for xy in xys:
			localnxs.append(xy[1]-xy[0])
		return localnxs
		
	def get_xy(self):
		return self.cell_da.getRanges()
		
	def output(self,view):
		da_output(self.cell_da,'mesh.da')
		
	def destroy(self):
		self.cell_da.destroy()
