import jinja2
from basis import *
from geometry import *
from quad import *
from functionspace import *
from numpy import *

def a_to_cinit_string(x):
	np.set_printoptions(threshold = np.prod(x.shape))
	sx = np.array2string(x,separator=',',precision=100)
	sx = sx.replace('\n', '')
	sx = sx.replace('[','{')
	sx = sx.replace(']','}')
	return sx
	
class Functional():
	def __init__(self,functionalfile,functionalname,formdim,ci1=0,ci2=0,fields=None,fsi=None,parameters=None,geometrylist=None,
	assemblytype='standard',coefficientlist=None,evaluate=False,valscoeff=None): #,tensorcoeff=False):

		self.functionalfile = functionalfile #this is the file that the functional is defined in
		self.functionalname = functionalname #name of the function ie if functionalname = XXX
		self.formdim = formdim #0, 1 or 2 forms supported for now
		self.ci1 = ci1
		self.ci2 = ci2
		self.fields = fields
		if self.fields == None:
			self.fields = []
		self.fsi = fsi
		self.parameters = parameters
		self.geometrylist = geometrylist #GeometryList(geometry_list)
		if self.geometrylist == None:
			self.geometrylist = []
		self.coefficientlist = coefficientlist #GeometryList(geometry_list)
		if self.coefficientlist == None:
			self.coefficientlist = []
		self.assemblytype=assemblytype
		self.layered = False
		self.layered_direc = 2
		self.compiled = False
		
		self.evaluate = evaluate
		self.valscoeff = valscoeff
			
def generate_assembly_routine(mesh,space1,space2,quadrature,functional):		
	#load templates
	templateLoader = jinja2.FileSystemLoader( searchpath=["../../src","../src",""] )
	
	#create environment
	templateEnv = jinja2.Environment( loader=templateLoader, trim_blocks=True)
	templateVars = {}

	#Load quadrature
	x_list = quadrature.pts_list
	adjwts_list = quadrature.wts_list
	wts = quadrature.wts
	adjwts = np.expand_dims(np.expand_dims(np.expand_dims(wts,axis=0),axis=0),axis=0)

	#Load element specific information- basis, derivs, offsets
	if mesh.ndims == 1:
		bindices = [0,0,0]
	if mesh.ndims == 2:
		bindices = [0,0,0]
	if mesh.ndims == 3:
		bindices = [0,0,0]
		
	#get the basis functions
	#get basis, derivs and offsets

	if not (space1 == None):
		space1size = space1.nelems
		basis_funcs1_list_x = []
		basis_funcs1_list_y = []
		basis_funcs1_list_z = []
		weighted_basis_funcs1_list_x = []
		weighted_basis_funcs1_list_y = []
		weighted_basis_funcs1_list_z = []
		basis_derivs1_list_x = []
		basis_derivs1_list_y = []
		basis_derivs1_list_z = []
		weighted_basis_derivs1_list_x = []
		weighted_basis_derivs1_list_y = []
		weighted_basis_derivs1_list_z = []
		elem1_list = []
		offsets1_list_x = []
		offset_mult1_list_x = []
		offsets1_list_y = []
		offset_mult1_list_y = []
		offsets1_list_z = []
		offset_mult1_list_z = []
		basis_funcs1_list = []
		weighted_basis_funcs1_list = []
		basis_derivs1_list = []
		weighted_basis_derivs1_list = []
		offsets1_list = []
		offset_mult1_list = []

		for k in xrange(space1size):
			elem1 = space1.get_elem(k)
			elem1_list.append(elem1)

			basis_funcs1_list.append(elem1.eval_basis_functions(*(x_list + bindices)))
			weighted_basis_funcs1_list.append(elem1.eval_basis_functions(*(x_list + bindices)) * adjwts)
			basis_derivs1_list.append(elem1.eval_basis_derivatives(*(x_list + bindices)))
			weighted_basis_derivs1_list.append(elem1.eval_basis_derivatives(*(x_list + bindices)) * np.expand_dims(adjwts,axis=-1))
			offsets1_list.append(elem1.get_offsets(*bindices))
			offset_mult1_list.append(elem1.get_offsetmult(*bindices))
			
			basis_funcs1_list_x.append(elem1.get_sub_elem(0).eval_basis_functions(x_list[0],0))
			weighted_basis_funcs1_list_x.append(elem1.get_sub_elem(0).eval_basis_functions(x_list[0],0) * np.expand_dims(adjwts_list[0],axis=0))
			basis_derivs1_list_x.append(elem1.get_sub_elem(0).eval_basis_derivatives(x_list[0],0))
			weighted_basis_derivs1_list_x.append(elem1.get_sub_elem(0).eval_basis_derivatives(x_list[0],0) * np.expand_dims(adjwts_list[0],axis=0))
			offsets1_list_x.append(elem1.get_sub_elem(0).get_offsets(0)) 
			offset_mult1_list_x.append(elem1.get_sub_elem(0).get_offsetmult(0)) 

			basis_funcs1_list_y.append(elem1.get_sub_elem(1).eval_basis_functions(x_list[1],0))
			weighted_basis_funcs1_list_y.append(elem1.get_sub_elem(1).eval_basis_functions(x_list[1],0) * np.expand_dims(adjwts_list[1],axis=0))
			basis_derivs1_list_y.append(elem1.get_sub_elem(1).eval_basis_derivatives(x_list[1],0))
			weighted_basis_derivs1_list_y.append(elem1.get_sub_elem(1).eval_basis_derivatives(x_list[1],0) * np.expand_dims(adjwts_list[1],axis=0))
			offsets1_list_y.append(elem1.get_sub_elem(1).get_offsets(0)) 
			offset_mult1_list_y.append(elem1.get_sub_elem(1).get_offsetmult(0)) 

			basis_funcs1_list_z.append(elem1.get_sub_elem(2).eval_basis_functions(x_list[2],0))
			weighted_basis_funcs1_list_z.append(elem1.get_sub_elem(2).eval_basis_functions(x_list[2],0) * np.expand_dims(adjwts_list[2],axis=0))
			basis_derivs1_list_z.append(elem1.get_sub_elem(2).eval_basis_derivatives(x_list[2],0))
			weighted_basis_derivs1_list_z.append(elem1.get_sub_elem(2).eval_basis_derivatives(x_list[2],0) * np.expand_dims(adjwts_list[2],axis=0))
			offsets1_list_z.append(elem1.get_sub_elem(2).get_offsets(0)) 
			offset_mult1_list_z.append(elem1.get_sub_elem(2).get_offsetmult(0)) 
	
		offset_mult1_x = offset_mult1_list_x[functional.ci1]
		offsets1_x = offsets1_list_x[functional.ci1]
		basis1_x = basis_funcs1_list_x[functional.ci1]
		weighted_basis1_x = weighted_basis_funcs1_list_x[functional.ci1]
		derivs1_x = basis_derivs1_list_x[functional.ci1]
		weighted_derivs1_x = weighted_basis_derivs1_list_x[functional.ci1]

		offset_mult1_y = offset_mult1_list_y[functional.ci1]
		offsets1_y = offsets1_list_y[functional.ci1]
		basis1_y = basis_funcs1_list_y[functional.ci1]
		weighted_basis1_y = weighted_basis_funcs1_list_y[functional.ci1]
		derivs1_y = basis_derivs1_list_y[functional.ci1]
		weighted_derivs1_y = weighted_basis_derivs1_list_y[functional.ci1]

		offset_mult1_z = offset_mult1_list_z[functional.ci1]
		offsets1_z = offsets1_list_z[functional.ci1]
		basis1_z = basis_funcs1_list_z[functional.ci1]
		weighted_basis1_z = weighted_basis_funcs1_list_z[functional.ci1]
		derivs1_z = basis_derivs1_list_z[functional.ci1]
		weighted_derivs1_z = weighted_basis_derivs1_list_z[functional.ci1]

		offset_mult1 = offset_mult1_list[functional.ci1]
		offsets1 = offsets1_list[functional.ci1]
		basis1 = basis_funcs1_list[functional.ci1]
		weighted_basis1 = weighted_basis_funcs1_list[functional.ci1]
		derivs1 = basis_derivs1_list[functional.ci1]
		weighted_derivs1 = weighted_basis_derivs1_list[functional.ci1]
		
	if not (space2 == None):
		space2size = space2.nelems
		basis_funcs2_list_x = []
		basis_funcs2_list_y = []
		basis_funcs2_list_z = []
		weighted_basis_funcs2_list_x = []
		weighted_basis_funcs2_list_y = []
		weighted_basis_funcs2_list_z = []
		basis_derivs2_list_x = []
		basis_derivs2_list_y = []
		basis_derivs2_list_z = []
		weighted_basis_derivs2_list_x = []
		weighted_basis_derivs2_list_y = []
		weighted_basis_derivs2_list_z = []
		elem2_list = []
		offsets2_list_x = []
		offset_mult2_list_x = []
		offsets2_list_y = []
		offset_mult2_list_y = []
		offsets2_list_z = []
		offset_mult2_list_z = []
		basis_funcs2_list = []
		weighted_basis_funcs2_list = []
		basis_derivs2_list = []
		weighted_basis_derivs2_list = []
		offsets2_list = []
		elem2_list = []
		offset_mult2_list = []
		for k in xrange(space2size):
			elem2 = space2.get_elem(k)
			elem2_list.append(elem2)

			basis_funcs2_list.append(elem2.eval_basis_functions(*(x_list + bindices)))
			weighted_basis_funcs2_list.append(elem2.eval_basis_functions(*(x_list + bindices)) * adjwts)
			basis_derivs2_list.append(elem2.eval_basis_derivatives(*(x_list + bindices)))
			weighted_basis_derivs2_list.append(elem2.eval_basis_derivatives(*(x_list + bindices)) * np.expand_dims(adjwts,axis=-1))
			offsets2_list.append(elem2.get_offsets(*bindices))
			offset_mult2_list.append(elem2.get_offsetmult(*bindices))
			
			basis_funcs2_list_x.append(elem2.get_sub_elem(0).eval_basis_functions(x_list[0],0))
			weighted_basis_funcs2_list_x.append(elem2.get_sub_elem(0).eval_basis_functions(x_list[0],0) * np.expand_dims(adjwts_list[0],axis=0))
			basis_derivs2_list_x.append(elem2.get_sub_elem(0).eval_basis_derivatives(x_list[0],0))
			weighted_basis_derivs2_list_x.append(elem2.get_sub_elem(0).eval_basis_derivatives(x_list[0],0) * np.expand_dims(adjwts_list[0],axis=0))
			offsets2_list_x.append(elem2.get_sub_elem(0).get_offsets(0)) 
			offset_mult2_list_x.append(elem2.get_sub_elem(0).get_offsetmult(0)) 

			basis_funcs2_list_y.append(elem2.get_sub_elem(1).eval_basis_functions(x_list[1],0))
			weighted_basis_funcs2_list_y.append(elem2.get_sub_elem(1).eval_basis_functions(x_list[1],0) * np.expand_dims(adjwts_list[1],axis=0))
			basis_derivs2_list_y.append(elem2.get_sub_elem(1).eval_basis_derivatives(x_list[1],0))
			weighted_basis_derivs2_list_y.append(elem2.get_sub_elem(1).eval_basis_derivatives(x_list[1],0) * np.expand_dims(adjwts_list[1],axis=0))
			offsets2_list_y.append(elem2.get_sub_elem(1).get_offsets(0)) 
			offset_mult2_list_y.append(elem2.get_sub_elem(1).get_offsetmult(0)) 

			basis_funcs2_list_z.append(elem2.get_sub_elem(2).eval_basis_functions(x_list[2],0))
			weighted_basis_funcs2_list_z.append(elem2.get_sub_elem(2).eval_basis_functions(x_list[2],0) * np.expand_dims(adjwts_list[2],axis=0))
			basis_derivs2_list_z.append(elem2.get_sub_elem(2).eval_basis_derivatives(x_list[2],0))
			weighted_basis_derivs2_list_z.append(elem2.get_sub_elem(2).eval_basis_derivatives(x_list[2],0) * np.expand_dims(adjwts_list[2],axis=0))
			offsets2_list_z.append(elem2.get_sub_elem(2).get_offsets(0)) 
			offset_mult2_list_z.append(elem2.get_sub_elem(2).get_offsetmult(0)) 
		
		offset_mult2_x = offset_mult2_list_x[functional.ci2]
		offsets2_x = offsets2_list_x[functional.ci2]
		basis2_x = basis_funcs2_list_x[functional.ci2]
		weighted_basis2_x = weighted_basis_funcs2_list_x[functional.ci2]
		derivs2_x = basis_derivs2_list_x[functional.ci2]
		weighted_derivs2_x = weighted_basis_derivs2_list_x[functional.ci2]

		offset_mult2_y = offset_mult2_list_y[functional.ci2]
		offsets2_y = offsets2_list_y[functional.ci2]
		basis2_y = basis_funcs2_list_y[functional.ci2]
		weighted_basis2_y = weighted_basis_funcs2_list_y[functional.ci2]
		derivs2_y = basis_derivs2_list_y[functional.ci2]
		weighted_derivs2_y = weighted_basis_derivs2_list_y[functional.ci2]

			
		offset_mult2_z = offset_mult2_list_z[functional.ci2]
		offsets2_z = offsets2_list_z[functional.ci2]
		basis2_z = basis_funcs2_list_z[functional.ci2]
		weighted_basis2_z = weighted_basis_funcs2_list_z[functional.ci2]
		derivs2_z = basis_derivs2_list_z[functional.ci2]
		weighted_derivs2_z = weighted_basis_derivs2_list_z[functional.ci2]
		
		offset_mult2 = offset_mult2_list[functional.ci2]
		offsets2 = offsets2_list[functional.ci2]
		basis2 = basis_funcs2_list[functional.ci2]
		weighted_basis2 = weighted_basis_funcs2_list[functional.ci2]
		derivs2 = basis_derivs2_list[functional.ci2]
		weighted_derivs2 = weighted_basis_derivs2_list[functional.ci2]

	#load fields
	#functional contains a list of si's for each field- ie nsi = nfields
	#to do a mixed space field, pass it in for EACH component of the mixed
	#space that you want- ie hdiv/l2 mixed space field is passed in as
	#h,u with si = 0,1

	fields = functional.fields
	addl_args_string = ''
	for i in xrange(len(fields)):
		field = fields[i]
		si = functional.fsi[i]
		field.nbasis_x = []
		field.nbasis_y = []
		field.nbasis_z = []
		field.basis_str = []
		field.basis_x_str = []
		field.basis_y_str = []
		field.basis_z_str = []
		field.derivs_str = []
		field.derivs_x_str = []
		field.derivs_y_str = []
		field.derivs_z_str = []
		field.offsets_str = []
		field.offsets_x_str = []
		field.offsets_y_str = []
		field.offsets_z_str = []
		field.offset_mult_str = []
		field.offset_mult_x_str = []
		field.offset_mult_y_str = []
		field.offset_mult_z_str = []
		for ci in xrange(field.space.get_space(si).nelems):
			basis = field.space.get_space(si).get_elem(ci).eval_basis_functions(*(x_list + bindices))
			derivs = field.space.get_space(si).get_elem(ci).eval_basis_derivatives(*(x_list + bindices))
			offsets = field.space.get_space(si).get_elem(ci).get_offsets(*bindices)
			offset_mult = field.space.get_space(si).get_elem(ci).get_offsetmult(*bindices)

			basis_x = field.space.get_space(si).get_elem(ci).get_sub_elem(0).eval_basis_functions(x_list[0],0)
			basis_y = field.space.get_space(si).get_elem(ci).get_sub_elem(1).eval_basis_functions(x_list[1],0)
			basis_z = field.space.get_space(si).get_elem(ci).get_sub_elem(2).eval_basis_functions(x_list[2],0)

			derivs_x = field.space.get_space(si).get_elem(ci).get_sub_elem(0).eval_basis_derivatives(x_list[0],0)
			derivs_y = field.space.get_space(si).get_elem(ci).get_sub_elem(1).eval_basis_derivatives(x_list[1],0)
			derivs_z = field.space.get_space(si).get_elem(ci).get_sub_elem(2).eval_basis_derivatives(x_list[2],0)

			offsets_x = field.space.get_space(si).get_elem(ci).get_sub_elem(0).get_offsets(0)
			offsets_y = field.space.get_space(si).get_elem(ci).get_sub_elem(1).get_offsets(0)
			offsets_z = field.space.get_space(si).get_elem(ci).get_sub_elem(2).get_offsets(0)
			
			offset_mult_x = field.space.get_space(si).get_elem(ci).get_sub_elem(0).get_offsetmult(0)
			offset_mult_y = field.space.get_space(si).get_elem(ci).get_sub_elem(1).get_offsetmult(0)
			offset_mult_z = field.space.get_space(si).get_elem(ci).get_sub_elem(2).get_offsetmult(0)
						
			field.nbasis_x.append(basis_x.shape[0])
			field.nbasis_y.append(basis_y.shape[0])
			field.nbasis_z.append(basis_z.shape[0])
			
			field.basis_str.append(a_to_cinit_string(basis))
			field.basis_x_str.append(a_to_cinit_string(basis_x))
			field.basis_y_str.append(a_to_cinit_string(basis_y))
			field.basis_z_str.append(a_to_cinit_string(basis_z))
			
			field.derivs_str.append(a_to_cinit_string(derivs))
			field.derivs_x_str.append(a_to_cinit_string(derivs_x))
			field.derivs_y_str.append(a_to_cinit_string(derivs_y))
			field.derivs_z_str.append(a_to_cinit_string(derivs_z))
			
			field.offsets_str.append(a_to_cinit_string(offsets))
			field.offsets_x_str.append(a_to_cinit_string(offsets_x))
			field.offsets_y_str.append(a_to_cinit_string(offsets_y))
			field.offsets_z_str.append(a_to_cinit_string(offsets_z))
			
			field.offset_mult_str.append(a_to_cinit_string(offset_mult))
			field.offset_mult_x_str.append(a_to_cinit_string(offset_mult_x))
			field.offset_mult_y_str.append(a_to_cinit_string(offset_mult_y))
			field.offset_mult_z_str.append(a_to_cinit_string(offset_mult_z))
		field.ncomp = field.space.get_space(si).nelems

		for ci in xrange(field.space.get_space(si).nelems):
			dmname = 'DM fda'+str(i)+'_'+str(ci)
			vecname = 'Vec field'+str(i)+'_'+str(ci)
			addl_args_string = addl_args_string + ', ' + dmname
			addl_args_string = addl_args_string + ', ' + vecname
		
	templateVars['fields'] = fields
	templateVars['fieldargs'] = addl_args_string
		
	#Compute geometry arguments
	geometry_args_string = ''
	geometry_array_list = []
	for item in functional.geometrylist:	
		geometry_args_string = geometry_args_string + ', ' + 'int lenx_' + item
		geometry_args_string = geometry_args_string + ', ' + 'double * ' + item	
		geometry_array_list.append(['lenx_'+item,item])
	templateVars['geometryargs'] = geometry_args_string
	templateVars['geometrylist'] = functional.geometrylist

	#Compute coefficient arguments
	coefficient_args_string = ''
	coefficient_array_list = []
	for item in functional.coefficientlist:	
		coefficient_args_string = coefficient_args_string + ', ' + 'int lenx_' + item.name
		coefficient_args_string = coefficient_args_string + ', ' + 'double * ' + item.name	
		coefficient_array_list.append(['lenx_'+item.name,item.name])
	templateVars['coefficientargs'] = coefficient_args_string
	templateVars['coefficientlist'] = functional.coefficientlist
	
	#This is just a Coefficient, but we are putting data into it!
	if functional.evaluate:
		vals_args_string = ''
		vals_args_string = vals_args_string + ',' + 'int lenx_vals'
		vals_args_string = vals_args_string + ',' + 'double * vals'
		coefficient_array_list.append(['lenx_vals','vals'])
		templateVars['valsargs'] = vals_args_string
		templateVars['valstype'] = functional.valscoeff.ctype

	# Specific the input variables for the template
	templateVars['formdim'] = functional.formdim
	templateVars['functionalfile']  = functional.functionalfile
	templateVars['functionalname']  = functional.functionalname
	templateVars['assemblytype']  = functional.assemblytype

	templateVars['ndim'] = mesh.ndims

	#quadrature stuff
	templateVars['npts_x'] = adjwts_list[0].shape[0]
	templateVars['wts_x'] = a_to_cinit_string(adjwts_list[0]) 
	templateVars['npts_y'] = adjwts_list[1].shape[0] 
	templateVars['wts_y'] = a_to_cinit_string(adjwts_list[1]) 
	templateVars['npts_z'] = adjwts_list[2].shape[0]
	templateVars['wts_z'] = a_to_cinit_string(adjwts_list[2])
	templateVars['wts'] = a_to_cinit_string(wts)

	#basis/derivs/etc.
	if not (space1 == None):
		templateVars['ci1'] = functional.ci1

		templateVars['basis1'] = a_to_cinit_string(basis1)
		templateVars['weighted_basis1'] = a_to_cinit_string(weighted_basis1)
		templateVars['derivs1'] = a_to_cinit_string(derivs1)
		templateVars['weighted_derivs1'] = a_to_cinit_string(weighted_derivs1)
		templateVars['offsets1'] = a_to_cinit_string(offsets1)
		templateVars['offset_mult1'] = a_to_cinit_string(offset_mult1)
		
		templateVars['offsets1_x'] = a_to_cinit_string(offsets1_x)
		templateVars['offset_mult1_x'] = a_to_cinit_string(offset_mult1_x)
		templateVars['nbasis1_x'] = basis1_x.shape[0] 
		templateVars['basis1_x'] = a_to_cinit_string(basis1_x) 
		templateVars['weighted_basis1_x'] = a_to_cinit_string(weighted_basis1_x) 
		templateVars['derivs1_x'] = a_to_cinit_string(derivs1_x)
		templateVars['weighted_derivs1_x'] = a_to_cinit_string(weighted_derivs1_x)

		templateVars['offsets1_y'] = a_to_cinit_string(offsets1_y)
		templateVars['offset_mult1_y'] = a_to_cinit_string(offset_mult1_y)
		templateVars['nbasis1_y'] = basis1_y.shape[0] 
		templateVars['basis1_y'] = a_to_cinit_string(basis1_y)
		templateVars['weighted_basis1_y'] = a_to_cinit_string(weighted_basis1_y)
		templateVars['derivs1_y'] = a_to_cinit_string(derivs1_y) 
		templateVars['weighted_derivs1_y'] = a_to_cinit_string(weighted_derivs1_y) 

		templateVars['offsets1_z'] = a_to_cinit_string(offsets1_z)
		templateVars['offset_mult1_z'] = a_to_cinit_string(offset_mult1_z)
		templateVars['nbasis1_z'] = basis1_z.shape[0]
		templateVars['basis1_z'] = a_to_cinit_string(basis1_z) 
		templateVars['weighted_basis1_z'] = a_to_cinit_string(weighted_basis1_z) 
		templateVars['derivs1_z'] = a_to_cinit_string(derivs1_z)
		templateVars['weighted_derivs1_z'] = a_to_cinit_string(weighted_derivs1_z)

		
	if not (space2 == None):
		templateVars['ci2'] = functional.ci2
	
		templateVars['basis2'] = a_to_cinit_string(basis2)
		templateVars['weighted_basis2'] = a_to_cinit_string(weighted_basis2)
		templateVars['derivs2'] = a_to_cinit_string(derivs2)
		templateVars['weighted_derivs2'] = a_to_cinit_string(weighted_derivs2)
		templateVars['offsets2'] = a_to_cinit_string(offsets2)
		templateVars['offset_mult2'] = a_to_cinit_string(offset_mult2)
		
		templateVars['offsets2_x'] = a_to_cinit_string(offsets2_x)
		templateVars['offset_mult2_x'] = a_to_cinit_string(offset_mult2_x)
		templateVars['nbasis2_x'] = basis2_x.shape[0] 
		templateVars['basis2_x'] = a_to_cinit_string(basis2_x) 
		templateVars['weighted_basis2_x'] = a_to_cinit_string(weighted_basis2_x) 
		templateVars['derivs2_x'] = a_to_cinit_string(derivs2_x)
		templateVars['weighted_derivs2_x'] = a_to_cinit_string(weighted_derivs2_x)

		templateVars['offsets2_y'] = a_to_cinit_string(offsets2_y)
		templateVars['offset_mult2_y'] = a_to_cinit_string(offset_mult2_y)
		templateVars['nbasis2_y'] = basis2_y.shape[0] 
		templateVars['basis2_y'] = a_to_cinit_string(basis2_y) 
		templateVars['weighted_basis2_y'] = a_to_cinit_string(weighted_basis2_y) 
		templateVars['derivs2_y'] = a_to_cinit_string(derivs2_y) 
		templateVars['weighted_derivs2_y'] = a_to_cinit_string(weighted_derivs2_y) 

		templateVars['offsets2_z'] = a_to_cinit_string(offsets2_z)
		templateVars['offset_mult2_z'] = a_to_cinit_string(offset_mult2_z)
		templateVars['nbasis2_z'] = basis2_z.shape[0] 
		templateVars['basis2_z'] = a_to_cinit_string(basis2_z) 
		templateVars['weighted_basis2_z'] = a_to_cinit_string(weighted_basis2_z) 
		templateVars['derivs2_z'] = a_to_cinit_string(derivs2_z) 
		templateVars['weighted_derivs2_z'] = a_to_cinit_string(weighted_derivs2_z) 

	#parameters
	if not (functional.parameters == None):
		vecparams = []
		tensorparams = []
		for parameter,val in functional.parameters:
			if type(val) == float or type(val) == int:
				templateVars[parameter] = val
			elif len(val.shape) == 1:
				length = val.shape[0]
				val = a_to_cinit_string(val)
				vecparams.append((parameter,length,val))
			elif len(val.shape) == 2:
				length1 = val.shape[0]
				length2 = val.shape[1]
				val = a_to_cinit_string(val)
				tensorparams.append((parameter,length1,length2,val))
		templateVars['vecparams'] = vecparams
		templateVars['tensorparams'] = tensorparams
	
	#if functional.assemblytype == 'duality-BLAS-tensor-product' or functional.assemblytype == 'duality-BLAS-tensor-product-matrixfree' \
	#or functional.assemblytype == 'duality-BLAS' or functional.assemblytype == 'duality-BLAS-matrixfree':
		#templateVars['use_blas'] = 1
	#else:
		#templateVars['use_blas'] = 0

	if functional.layered == True:
		templateVars['layered'] = 1
		templateVars['layered_direc'] = functional.layered_direc
	else:
		templateVars['layered'] = 0
	if functional.evaluate:
		templateVars['evaluate'] = 1
	else:
		templateVars['evaluate'] = 0
	if functional.assemblytype == 'tensor-product-matrixfree':
		templateVars['matrixfree'] = 1
	else:
		templateVars['matrixfree'] = 0
		
	#read the template
	if functional.assemblytype == 'standard' or functional.evaluate:
		template = templateEnv.get_template('standard.template')
	if functional.assemblytype == 'tensor-product' or functional.assemblytype == 'tensor-product-matrixfree':
		template = templateEnv.get_template('tensorproduct.template')
	
	#OLD TEMPLATE TYPES
	#if functional.assemblytype == 'tensor-product-matrixfree':
	#	template = templateEnv.get_template('tensorproduct-matrixfree.template')	
	#if functional.evaluate:
	#	template = templateEnv.get_template('evaluate.template')
	#if functional.assemblytype == 'duality-tensor-product' or functional.assemblytype == 'duality-BLAS-tensor-product':
	#	template = templateEnv.get_template('duality-tensorproduct.template')
	#if functional.assemblytype == 'duality-tensor-product-matrixfree' or functional.assemblytype == 'duality-BLAS-tensor-product-matrixfree':
	#	template = templateEnv.get_template('duality-tensorproduct-matrixfree.template')
	#if functional.assemblytype == 'normal-matrixfree':
	#	template = templateEnv.get_template( 'standard-matrixfree.template')
	#if functional.assemblytype == 'duality' or functional.assemblytype == 'duality-BLAS':
	#	template = templateEnv.get_template('duality.template')
	#if functional.assemblytype == 'duality-matrixfree' or functional.assemblytype == 'duality-BLAS-matrixfree':
	#	template = templateEnv.get_template('duality-matrixfree.template')

	#if functional.assemblytype == 'duality-tempfree-tensor-product':
	#	template = templateEnv.get_template('tensorproduct.template')
	#if functional.assemblytype == 'duality-tempfree-tensor-product-matrixfree':
	#	template = templateEnv.get_template('tensorproduct-matrixfree.template')

	#if functional.assemblytype == 'test':
	#	template = templateEnv.get_template('test.template')

	# Process template to produce source code
	outputText = template.render( templateVars )

	return outputText,geometry_array_list,coefficient_array_list
		

