import numpy as np
from common import *

def matrix_output(matrix,name):
	ViewASCII = PETSc.Viewer()
	ViewASCII.createASCII(name + '.out', mode=PETSc.Viewer.Mode.WRITE,comm= PETSc.COMM_WORLD)
	ViewASCII.view(obj=matrix)  
	ViewASCII.destroy()
	
def is_output(IS,name):
	#print name
	ViewASCII = PETSc.Viewer()
	ViewASCII.createASCII(name + '.out', mode=PETSc.Viewer.Mode.WRITE,comm= PETSc.COMM_WORLD)
	ViewASCII.view(obj=IS)  
	ViewASCII.destroy()
	
def da_output(da,name):
	#print name
	ViewASCII = PETSc.Viewer()
	ViewASCII.createASCII(name + '.out', mode=PETSc.Viewer.Mode.WRITE,comm= PETSc.COMM_WORLD)
	ViewASCII.view(obj=da)  
	ViewASCII.destroy()

def scatter_output(scatter,name):
	ViewASCII = PETSc.Viewer()
	ViewASCII.createASCII(name + '.out', mode=PETSc.Viewer.Mode.WRITE,comm= PETSc.COMM_WORLD)
	ViewASCII.view(obj=scatter)  
	ViewASCII.destroy()

def lgmap_output(lgmap,name):
	ViewASCII = PETSc.Viewer()
	ViewASCII.createASCII(name + '.out', mode=PETSc.Viewer.Mode.WRITE,comm= PETSc.COMM_WORLD)
	ViewASCII.view(obj=lgmap)  
	ViewASCII.destroy()
	
#def output_mesh(pde):
	#ViewHDF5 = pde.petsc.Viewer()  
	#ViewHDF5.createHDF5(pde.outdir + 'mesh.' + pde.simname + '.h5', mode=pde.petsc.Viewer.Mode.WRITE,comm= pde.petsc.COMM_WORLD)

	##output coordinates
	#vector_output(pde.cell_coords,'cell_coords',ViewHDF5) 
	#vector_output(pde.vertex_coords,'vertex_coords',ViewHDF5)
	#if pde.ndims == 2:
		#vector_output(pde.uedge_coords,'uedge_coords',ViewHDF5) 
		#vector_output(pde.vedge_coords,'vedge_coords',ViewHDF5) 
	
	#dalist = [pde.cell_da,pde.vertex_da]
	#danamelist = ['cell','vertex']
	#if pde.ndims == 2:
		#dalist.append(pde.uedge_da)
		#dalist.append(pde.vedge_da)
		#danamelist.append('uedge')
		#danamelist.append('vedge')
		
	#for da,daname in zip(dalist,danamelist):
		##create i,j,nat and global vecs
		#ivec = da.createGlobalVector()
		#natvec = da.createGlobalVector()
		#globalvec = da.createGlobalVector()
		#rankvec = da.createGlobalVector()
		
		##get mapping
		#global_to_natural = da.getAO()
		
		#if pde.ndims == 2:
			#jvec = da.createGlobalVector()

		##fill iarray, jarray, natarray and globalarray
		#if pde.ndims == 1:
			#nx = da.getSizes()[0]
			##print nx
			##print da.getRanges()
			#imin,imax = da.getRanges()[0]
			#print pde.petsc.COMM_WORLD.getRank(),'i',imin,imax,nx
			#iarray = np.array(xrange(imin,imax),dtype=np.int32)
			#natarray = i_to_global(iarray,nx)
			##print natarray
			#globalarray = global_to_natural.app2petsc(natarray)
			
		#if pde.ndims == 2:
			#nx,ny = da.getSizes()
			#imin,imax = da.getRanges()[0]
			#jmin,jmax = da.getRanges()[1]
			#print pde.petsc.COMM_WORLD.getRank(),daname,'i',imin,imax,nx,'j',jmin,jmax,ny
			#ilist = np.array(xrange(imin,imax),dtype=np.int32)
			#jlist = np.array(xrange(jmin,jmax),dtype=np.int32)
			##print ilist.dtype
			##print jlist.dtype
			#iarray,jarray = np.meshgrid(ilist,jlist)
			#iarray = iarray.astype(np.int32) #this is a weird hack, but meshgrid is returning int64 arrays
			#jarray = jarray.astype(np.int32) #this is a weird hack, but meshgrid is returning int64 arrays
			#natarray = ij_to_global(iarray,nx,jarray,ny)
			#globalarray = global_to_natural.app2petsc(natarray)
			
		##set arrays into vecs
		#ivec.setArray(iarray)
		#natvec.setArray(natarray)
		#globalvec.setArray(globalarray)
		#if pde.ndims == 2:
			#jvec.setArray(jarray)
		
		##set rank array
		#rankarr = np.ones((iarray.shape),dtype=np.int32) * pde.petsc.COMM_WORLD.getRank()
		#rankvec.setArray(rankarr)
		
		##output them
		#vector_output(ivec,daname + '_i',ViewHDF5)
		#if pde.ndims == 2:
			#vector_output(jvec,daname + '_j',ViewHDF5)
		#vector_output(natvec,daname + '_nat',ViewHDF5)
		#vector_output(globalvec,daname + '_global',ViewHDF5)
		#vector_output(rankvec,daname + '_rank',ViewHDF5)

	#ViewHDF5.destroy()


def vector_output(vector,name,viewer):
	vector.setName(name)
	viewer.view(obj=vector)

#BELOW IS BASIC PARALLEL HDF5 STUFF IN PYTHON- MIGHT BE USEFUL TO OUTPUT 
#METADATA INTO HDF5 FILE IN A PARALLEL MANNER
#SHOULD PROBABLY BE PART OF CONFIGURATION STEP

#rank = MPI.COMM_WORLD.rank  # The process ID (integer 0-3 for 4-process run)

#f = h5py.File('parallel_test.hdf5', 'w', driver='mpio', comm=MPI.COMM_WORLD)

#dset = f.create_dataset('test', (4,), dtype='i')
#dset[rank] = rank

#f.close()
