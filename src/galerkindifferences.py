import numpy as np
import sympy

def LagrangePoly(x,order,i,xi=None):
	if xi==None:
		xi=symbols('x:%d'%(order+1))
	index = range(order+1)
	index.pop(i)
	return sympy.prod([(x-xi[j])/(xi[i]-xi[j]) for j in index])
    
def lagrange_poly_support(i,p,x):
  '''Give a set of N support points, an index i, and a variable x, return Langrange polynomial P_i(x)
  which interpolates the values (0,0,..1,..,0) where 1 is at the ith support point
  :param i: non-zero location
  :param p: set of N support points
  :param x: symbolic variable
  '''
  n = len(p)
  return LagrangePoly(x,n-1,i,xi=p)



#def f1func(x):
	#return  .5*( x[:,0]**3/3. + x[:,0]**2/2. - x[:,0]/12.  - 1./8.) 			  # 0 [0 0] 1
#def f2func(x):
	#return -.5*( x[:,0]**3   + x[:,0]**2/2. - 9./4*x[:,0] - 9./8.)               # 0 [0 1] 0
#def f3func(x):
	#return -.5*(-x[:,0]**3   + x[:,0]**2/2. + 9./4*x[:,0] - 9./8.)               # 0 [1 0] 0
#def f4func(x):
	#return  .5*(-x[:,0]**3/3. + x[:,0]**2/2. + x[:,0]/12.  - 1./8.)              # 1 [0 0] 0

#def f1func(var):
	#return  .5*( var**3/3. + var**2/2. - var/12.  - 1./8.) 			  # 0 [0 0] 1
#def f2func(var):
	#return -.5*( var**3   + var**2/2. - 9./4*var - 9./8.)               # 0 [0 1] 0
#def f3func(var):
	#return -.5*(-var**3   + var**2/2. + 9./4*var - 9./8.)               # 0 [1 0] 0
#def f4func(var):
	#return  .5*(-var**3/3. + var**2/2. + var/12.  - 1./8.)              # 1 [0 0] 0
	
#def f1mfunc(x):
	#return  .5*( (x[:,0]-1.)**3/3. + (x[:,0]-1.)**2/2. - (x[:,0]-1.)/12.  - 1./8.)    # [0 0] 0 1
#def f2mfunc(x):
	#return -.5*( (x[:,0]-1.)**3   + (x[:,0]-1.)**2/2. - 9./4.*(x[:,0]-1.) - 9./8.)   # [0 0] 1 0
#def f3mfunc(x):
	#return -.5*(-(x[:,0]-1.)**3   + (x[:,0]-1.)**2/2. + 9./4.*(x[:,0]-1.) - 9./8.)   # [0 1] 0 0
#def f4mfunc(x):
	#return  .5*(-(x[:,0]-1.)**3/3. + (x[:,0]-1.)**2/2. + (x[:,0]-1.)/12.  - 1./8.)   # [1 0] 0 0
#def f1pfunc(x):
	#return  .5*( (x[:,0]+1.)**3/3. + (x[:,0]+1.)**2/2. - (x[:,0]+1.)/12.  - 1./8.)   # 0 0 [0 1]
#def f2pfunc(x):
	#return -.5*( (x[:,0]+1.)**3   + (x[:,0]+1.)**2/2. - 9./4.*(x[:,0]+1.) - 9./8.)   # 0 0 [1 0]
#def f3pfunc(x):
	#return -.5*(-(x[:,0]+1)**3   + (x[:,0]+1.)**2/2. + 9./4.*(x[:,0]+1.) - 9./8.)   # 0 1 [0 0]
#def f4pfunc(x):
	#return  .5*(-(x[:,0]+1)**3/3. + (x[:,0]+1.)**2/2. + (x[:,0]+1.)/12.  - 1./8.)   # 1 0 [0 0]

#def df1func(x):
	#return  .5*(   x[:,0]**2 + x[:,0] - 1./12) # 0 [0 0] 1
#def df2func(x):
	#return -.5*( 3.*x[:,0]**2 + x[:,0] - 9./4.)  # 0 [0 1] 0
#def df3func(x):
	#return -.5*(-3.*x[:,0]**2 + x[:,0] + 9./4.)  # 0 [1 0] 0
#def df4func(x):
	#return  .5*(  -x[:,0]**2 + x[:,0] + 1./12.) # 1 [0 0] 0
#def df1mfunc(x):
	#return  .5*(   (x[:,0]-1.)**2 + (x[:,0]-1.) - 1./12.)
#def df2mfunc(x):
	#return -.5*( 3.*(x[:,0]-1.)**2 + (x[:,0]-1.) - 9./4.)
#def df3mfunc(x):
	#return -.5*(-3.*(x[:,0]-1.)**2 + (x[:,0]-1.) + 9./4.)
#def df4mfunc(x):
	#return  .5*(  -(x[:,0]-1.)**2 + (x[:,0]-1.) + 1./12.)
#def df1pfunc(x):
	#return  .5*(   (x[:,0]+1.)**2 + (x[:,0]+1.) - 1./12.)
#def df2pfunc(x):
	#return -.5*( 3.*(x[:,0]+1.)**2 + (x[:,0]+1.) - 9./4.)
#def df3pfunc(x):
	#return -.5*(-3.*(x[:,0]+1.)**2 + (x[:,0]+1.) + 9./4.)
#def df4pfunc(x):
	#return  .5*(  -(x[:,0]+1.)**2 + (x[:,0]+1.) + 1./12.)

#def delta1_func(var):
	#return -.5*var + .5*var**2 - 1./24.  #  1 [0] 0
#def delta0_func(var):
	#return 13./12 - var**2            	   #  0 [1] 0
#def deltam1_func(var):
	#return .5*var  + .5*var**2 - 1./24.  #  0 [0] 1


##def delta1_func(x):
	##return -.5*x[:,0] + .5*x[:,0]**2 - 1./24.  #  1 [0] 0
##def delta0_func(x):
	##return 13./12 - x[:,0]**2            	   #  0 [1] 0
##def deltam1_func(x):
	##return .5*x[:,0]  + .5*x[:,0]**2 - 1./24.  #  0 [0] 1

#def alpha1_func(x):
	#return 13./12. - (x[:,0]-1)**2        # [0] 1  0
#def beta1_func(x):
	#return .5*x[:,0]**2-1.5*x[:,0] + 23./24.    # [1] 0  0
#def gamma1_func(x):
	#return 1. - beta1_func(x) - alpha1_func(x)      # [0] 0  1
#def alphaN_func(x):
	#return 13./12. - (x[:,0]+1)**2        #  0  1 [0]
#def betaN_func(x):
	#return .5*x[:,0]**2+1.5*x[:,0] + 23./24.    # 0  0 [1]
#def gammaN_func(x):
	#return 1. - betaN_func(x) - alphaN_func(x)      #  1  0 [0]

#def ddelta1_func(x):
	#return -.5 + x[:,0]  #  1 [0] 0
#def ddelta0_func(x):
	#return -2.*x[:,0]    #  0 [1] 0
#def ddeltam1_func(x):
	#return .5  + x[:,0]  #  0 [0] 1

#def dalpha1_func(x):
	#return -2.*(x[:,0]-1)        # [0] 1  0
#def dbeta1_func(x):
	#return x[:,0] - 1.5    # [1] 0  0
#def dgamma1_func(x):
	#return - dbeta1_func(x) - dalpha1_func(x)      # [0] 0  1
#def dalphaN_func(x):
	#return -2.*(x[:,0]+1.)        #  0  1 [0]
#def dbetaN_func(x):
	#return x[:,0] + 1.5    #  0  0 [1]
#def dgammaN_func(x):
	#return -dbetaN_func(x) - dalphaN_func(x)      #  1  0 [0]
