
from mesh import *
from quad import *
from basis import *
from functionspace import *
import time
from common import *
		
class TwoForm():
	def __init__(self,mesh,names,functionals,target,source,quadrature,params):
		
		self.names = names[1]
		self.name = names[0]
		self.functionals = functionals
		self.target = target
		self.source = source
		self.mesh = mesh
		self.params = params
		#create quadrature
		self.quad = quadrature

		#create matrices
		self.target_size = target.nelems
		self.source_size = source.nelems
		i = 0
		M = 0
		self.rowstarts = [None for _ in xrange(self.target_size)]
		self.colstarts = [None for _ in xrange(self.source_size)]
		for t in xrange(target.nspaces):
			tspace = target.get_space(t)
			for k in xrange(tspace.nelems):
				m = tspace.get_ndofs(k)
				self.rowstarts[i] = M
				M = M + m
				j = 0
				N = 0
				for s in xrange(source.nspaces):
					sspace = source.get_space(s)
					for l in xrange(sspace.nelems):						
						n = sspace.get_ndofs(l)
						self.colstarts[j] = N
						N = N + n
						j = j + 1
				i = i + 1
		self.matrix = sympy.zeros(M,N)
		
	def assemble(self,show_matrix=True):	
		start = time.time()
		for t in xrange(self.target.nspaces):
			self.target.get_space(t).fill_space(self.quad)

		for s in xrange(self.source.nspaces):
			self.source.get_space(s).fill_space(self.quad)
		
		for t in xrange(self.target.nspaces):
			tspace = self.target.get_space(t)
			toff = self.target.get_space_offset(t)
			for s in xrange(self.source.nspaces):
				sspace = self.source.get_space(s)
				soff = self.source.get_space_offset(s)
				for i in xrange(self.target.get_space(t).nelems):
					for j in xrange(self.source.get_space(s).nelems):
						print "assembling", i+toff,j+soff,self.names[i+toff][j+soff]
						two_form_fill(self.mesh, self.matrix, self.functionals[t][s],tspace,sspace,i,j,self.rowstarts[i+toff],self.colstarts[j+soff],self.quad,self.params)

		if show_matrix:
			print self.matrix
		end = time.time()
		print 'filled symbolic mat',self.name,end-start
							
def two_form_fill(mesh,matrix,matrixfunc,space1,space2,ci1,ci2,rowstart,colstart,quad,params):

	pts_list = quad.pts_list
	wts_list = quad.wts_list
	
	#Get reference element basis functions and derivatives
	#Get reference element offsets
	xsymb = sympy.var('x')
	ysymb = sympy.var('y')
	zsymb = sympy.var('z')
	
	space1size = space1.nelems
	
	elem1 = space1.get_elem(ci1)

	offsets1_list_x = elem1.get_sub_elem(0).get_offsets(0)
	offset_mult1_list_x = elem1.get_sub_elem(0).get_offsetmult(0)
	symb_basis_func1_list_x = elem1.get_sub_elem(0).symbolic_basis_functions(xsymb)
	symb_basis_derivs1_list_x = elem1.get_sub_elem(0).symbolic_basis_derivatives(xsymb)
	basisvals1_x = elem1.get_sub_elem(0).eval_basis_functions(pts_list[0],0)
	derivvals1_x = elem1.get_sub_elem(0).eval_basis_derivatives(pts_list[0],0)

	offsets1_list_y = elem1.get_sub_elem(1).get_offsets(0)
	offset_mult1_list_y = elem1.get_sub_elem(1).get_offsetmult(0)
	symb_basis_func1_list_y = elem1.get_sub_elem(1).symbolic_basis_functions(ysymb)
	symb_basis_derivs1_list_y = elem1.get_sub_elem(1).symbolic_basis_derivatives(ysymb)
	basisvals1_y = elem1.get_sub_elem(1).eval_basis_functions(pts_list[1],0)
	derivvals1_y = elem1.get_sub_elem(1).eval_basis_derivatives(pts_list[1],0)
	
	offsets1_list_z = elem1.get_sub_elem(2).get_offsets(0)
	offset_mult1_list_z = elem1.get_sub_elem(2).get_offsetmult(0)
	symb_basis_func1_list_z = elem1.get_sub_elem(2).symbolic_basis_functions(zsymb)
	symb_basis_derivs1_list_z = elem1.get_sub_elem(2).symbolic_basis_derivatives(zsymb)
	basisvals1_z = elem1.get_sub_elem(2).eval_basis_functions(pts_list[2],0)
	derivvals1_z = elem1.get_sub_elem(2).eval_basis_derivatives(pts_list[2],0)
		
	elem2 = space2.get_elem(ci2)

	offsets2_list_x = elem2.get_sub_elem(0).get_offsets(0)
	offset_mult2_list_x = elem2.get_sub_elem(0).get_offsetmult(0)
	symb_basis_func2_list_x = elem2.get_sub_elem(0).symbolic_basis_functions(xsymb)
	symb_basis_derivs2_list_x = elem2.get_sub_elem(0).symbolic_basis_derivatives(xsymb)
	basisvals2_x = elem2.get_sub_elem(0).eval_basis_functions(pts_list[0],0)
	derivvals2_x = elem2.get_sub_elem(0).eval_basis_derivatives(pts_list[0],0)
	
	offsets2_list_y = elem2.get_sub_elem(1).get_offsets(0)
	offset_mult2_list_y = elem2.get_sub_elem(1).get_offsetmult(0)
	symb_basis_func2_list_y = elem2.get_sub_elem(1).symbolic_basis_functions(ysymb)
	symb_basis_derivs2_list_y = elem2.get_sub_elem(1).symbolic_basis_derivatives(ysymb)
	basisvals2_y = elem2.get_sub_elem(1).eval_basis_functions(pts_list[1],0)
	derivvals2_y = elem2.get_sub_elem(1).eval_basis_derivatives(pts_list[1],0)
	
	offsets2_list_z = elem2.get_sub_elem(2).get_offsets(0)
	offset_mult2_list_z = elem2.get_sub_elem(2).get_offsetmult(0)
	symb_basis_func2_list_z = elem2.get_sub_elem(2).symbolic_basis_functions(zsymb)
	symb_basis_derivs2_list_z = elem2.get_sub_elem(2).symbolic_basis_derivatives(zsymb)
	basisvals2_z = elem2.get_sub_elem(2).eval_basis_functions(pts_list[2],0)
	derivvals2_z = elem2.get_sub_elem(2).eval_basis_derivatives(pts_list[2],0)

# {% set gradtensor1 = [['d1x','b1x','b1x'],['b1y','d1y','b1y'],['b1z','b1z','d1z']] %}

	#grad1(X,Y) where X is the component (ie x,y,z) and Y is the direction (ie xth deriv, yth deriv, zth deriv) 
	grad1 = [[symb_basis_derivs1_list_x,symb_basis_func1_list_x,symb_basis_func1_list_x],
	[symb_basis_func1_list_y,symb_basis_derivs1_list_y,symb_basis_func1_list_y],
	[symb_basis_func1_list_z,symb_basis_func1_list_z,symb_basis_derivs1_list_z]]
	grad2 = [[symb_basis_derivs2_list_x,symb_basis_func2_list_x,symb_basis_func2_list_x],
	[symb_basis_func2_list_y,symb_basis_derivs2_list_y,symb_basis_func2_list_y],
	[symb_basis_func2_list_z,symb_basis_func2_list_z,symb_basis_derivs2_list_z]]
	gradvals1 = [[derivvals1_x,basisvals1_x,basisvals1_x],[basisvals1_y,derivvals1_y,basisvals1_y],[basisvals1_z,basisvals1_z,derivvals1_z]]
	gradvals2 = [[derivvals2_x,basisvals2_x,basisvals2_x],[basisvals2_y,derivvals2_y,basisvals2_y],[basisvals2_z,basisvals2_z,derivvals2_z]]

	nNx = len(symb_basis_func1_list_x)
	nMx = len(symb_basis_func2_list_x)
	nNy = len(symb_basis_func1_list_y)
	nMy = len(symb_basis_func2_list_y)
	nNz = len(symb_basis_func1_list_z)
	nMz = len(symb_basis_func2_list_z)
	
	nQx = pts_list[0].shape[0]
	nQy = pts_list[1].shape[0]
	nQz = pts_list[2].shape[0]
	
	detJ = mesh.geometry.detJ
	detJinv = mesh.geometry.detJinv
	J = mesh.geometry.J
	Jinv = mesh.geometry.Jinv
	JT = mesh.geometry.JT
	JTinv = mesh.geometry.JTinv
	alpha = mesh.geometry.alpha
	beta = mesh.geometry.beta

	#useful combinations
	if mesh.ndims == 3:
		rot = sympy.Matrix([[0,-1,0],[1,0,0],[0,0,1]])
	if mesh.ndims == 2:
		rot = sympy.Matrix([[0,-1],[1,0]])
	if mesh.ndims == 1:
		rot = sympy.Matrix([[1],])
	JTJ = JT * J
	JinvJTinv =  Jinv * JTinv
	JTrotJ = JT * rot * J
	JinvrotJTinv = Jinv * rot * JTinv
	rotJ = rot * J
	rotJTinv = rot * JTinv
	
	#compute element tensor
	valsx = sympy.zeros(nNx,nMx)
	valsy = sympy.zeros(nNy,nMy)
	valsz = sympy.zeros(nNz,nMz)
	geom = 1
	
	fragment = compile(matrixfunc,'<string>','exec')
	exec(fragment)

	starts,sizes = mesh.cell_da.getCorners()

	junk,nxm1 = space1.get_da(ci1).getCorners()
	junk,nxm2 = space2.get_da(ci2).getCorners()
	starts = list(starts)
	sizes = list(sizes)
	nxm1 = list(nxm1)
	nxm2 = list(nxm2)
		
	
	if mesh.ndims == 1:
		starts.append(0)
		sizes.append(1)
		starts.append(0)
		sizes.append(1)
		nxm1.append(1)
		nxm1.append(1)
		nxm2.append(1)
		nxm2.append(1)
		valsy= sympy.ones(nNy,nMy)
		valsz= sympy.ones(nNz,nMz)
		
	if mesh.ndims == 2:
		starts.append(0)
		sizes.append(1)
		nxm1.append(1)
		nxm2.append(1)
		valsz= sympy.ones(nNz,nMz)
	
	for i in xrange(starts[0],starts[0] + sizes[0]):
		for j in xrange(starts[1],starts[1] + sizes[1]):
			for k in xrange(starts[2],starts[2] + sizes[2]):
				for nx in range(nNx):
					for ny in range(nNy):
						for nz in range(nNz):
							rowi = i_to_global(i*offset_mult1_list_x[nx] +offsets1_list_x[nx],nxm1[0]) 
							rowj = i_to_global(j*offset_mult1_list_y[ny] +offsets1_list_y[ny],nxm1[1]) 
							rowk = i_to_global(k*offset_mult1_list_z[nz] +offsets1_list_z[nz],nxm1[2]) 
							row = rowi + nxm1[0]*(rowj + nxm1[1]*rowk)
							for mx in range(nMx):
								for my in range(nMy):
									for mz in range(nMz):
										coli = i_to_global(i*offset_mult2_list_x[mx] +offsets2_list_x[mx],nxm2[0]) 
										colj = i_to_global(j*offset_mult2_list_y[my] +offsets2_list_y[my],nxm2[1]) 
										colk = i_to_global(k*offset_mult2_list_z[mz] +offsets2_list_z[mz],nxm2[2]) 
										col = coli + nxm2[0]*(colj + nxm2[1]*colk)
										matrix[row+rowstart,col+colstart] = matrix[row+rowstart,col+colstart] + valsx[nx,mx] * valsy[ny,my] * valsz[nz,mz] * geom


