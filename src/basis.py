import numpy as np
from galerkindifferences import *
from quad import *
from lagrange import *
from sympy import diff
from bernstein import *
from quad import *
from sympy.physics.quantum import TensorProduct

class FiniteElement():
	def __init__(self):
		pass
	
	def get_dof(self):
		return self.dof
		
	def get_dimension(self):
		return self.dimension
		
	def get_support_size(self):
		return self.support_size
		
	def get_offsets(self,i):
		return self.offsets[0,:]
		
	def get_offsetmult(self,i):
		return (np.ones(self.offsets.shape,dtype=np.int32) * self.offset_multiplier)[0,:]
		
	def get_ref_cell(self):
		return self.ref_cell
		
	def eval_basis_functions(self,x,i):
		xsymb = sympy.var('x')
		symbas = self.symbolic_basis_functions(xsymb)
		basis_funcs = np.zeros((len(symbas),x.shape[0]))
		for i in range(len(symbas)):
			if symbas[i] == 0.5: #check for the constant basis function
				basis_funcs[i,:] = 0.5
			else:
				basis = sympy.lambdify(xsymb, symbas[i], "numpy")
				basis_funcs[i,:] = np.squeeze(basis(x))
		return basis_funcs

	def eval_basis_derivatives(self,x,i):
		xsymb = sympy.var('x')
		symbas = self.symbolic_basis_functions(xsymb)
		basis_derivs = np.zeros((len(symbas),x.shape[0]))
		for i in range(len(symbas)):
			basis = symbas[i]
			if basis == 0.5: #check for the constant basis function
				basis_derivs[i,:] = 0.0
			else:
				derivsymb = diff(basis)
				deriv = sympy.lambdify(xsymb, derivsymb, "numpy")
				basis_derivs[i,:] = np.squeeze(deriv(x))
		return basis_derivs
		
	def symbolic_basis_derivatives(self,var):
		symbas = self.symbolic_basis_functions(var)
		dsymbas = []
		for i in range(len(symbas)):
			basis = symbas[i]
			if basis == sympy.Rational(1,2): #check for the constant basis function
				dsymbas.append(0)
			else:
				derivsymb = diff(basis)
				dsymbas.append(derivsymb)
		return dsymbas
		
	def fill_offsets(self):
		pass
		
	def get_sub_elem(self,i):
		return self
		
class TensorProductElement(FiniteElement):
	def __init__(self,elemlist):
		if len(elemlist) == 1:
			elemlist.append(EmptyElement(1))
			elemlist.append(EmptyElement(1))
		if len(elemlist) == 2:
			elemlist.append(EmptyElement(1))
			
		self.nelements = len(elemlist)
		
		self.dof = 1
		self.support_size = 0
		self.ref_cell = []
		self.elemlist = elemlist
		for i in range(self.nelements):
			self.support_size = max(self.support_size,self.elemlist[i].support_size)
			self.ref_cell.append(self.elemlist[i].ref_cell[0])
			self.dof = self.dof * self.elemlist[i].get_dof()
		
	def get_sub_elem(self,i):
		return self.elemlist[i]
		
	def get_nxny(self,*args):
		nxlist = []
		for k in range(self.nelements):
			nxlist.append(self.elemlist[k].get_nxny(args[k]))
		return nxlist

	def get_offsetmult(self,*args):						
		#compute offset multiplier using tensor product structure, same as basis and derivs
		offsetmultlist = []
		noffsets = []
		for k in range(self.nelements):
			offsetmultlist.append(self.elemlist[k].get_offsetmult(args[k]))
			noffsets.append(offsetmultlist[k].shape[0])

		#THIS NEEDS OPTIMIZING FOR HIGH DEGREE
		#IS THERE A CLEVER NUMPY WAY OF DOING THINGS?
		#maybe just do it in C?- would be an easy/simple little function to write...
		offsetmults = np.zeros((noffsets + [self.nelements,]))
		for nx in range(noffsets[0]):
			for ny in range(noffsets[1]):
				for nz in range(noffsets[2]):					
					offsetmults[nx][ny][nz][0] = offsetmultlist[0][nx]
					offsetmults[nx][ny][nz][1] = offsetmultlist[1][ny]
					offsetmults[nx][ny][nz][2] = offsetmultlist[2][nz]
					
		return offsetmults
		
	def get_offsets(self,*args):						
		#compute offsets using tensor product structure, same as basis and derivs
		offsetslist = []
		noffsets = []
		for k in range(self.nelements):
			offsetslist.append(self.elemlist[k].get_offsets(args[k]))
			noffsets.append(offsetslist[k].shape[0])

		#THIS NEEDS OPTIMIZING FOR HIGH DEGREE
		#IS THERE A CLEVER NUMPY WAY OF DOING THINGS?
		#maybe just do it in C?- would be an easy/simple little function to write...
		offsets = np.zeros((noffsets + [self.nelements,]))
		for nx in range(noffsets[0]):
			for ny in range(noffsets[1]):
				for nz in range(noffsets[2]):					
					offsets[nx][ny][nz][0] = offsetslist[0][nx]
					offsets[nx][ny][nz][1] = offsetslist[1][ny]
					offsets[nx][ny][nz][2] = offsetslist[2][nz]
								
		return offsets
	
	def eval_basis_functions(self,*args):
		basis_list = []
		nbasis = []
		nquad = []
		for i in range(self.nelements):
			pts = args[i]
			blockindex = args[i + self.nelements]
			basis_list.append(self.elemlist[i].eval_basis_functions(pts,blockindex))
			nquad.append(pts.shape[0])
			nbasis.append(self.elemlist[i].eval_basis_functions(pts,blockindex).shape[0])
		basis = np.zeros((nbasis + nquad))
		
		#THIS NEEDS OPTIMIZING FOR HIGH DEGREE
		#IS THERE A CLEVER NUMPY WAY OF DOING THINGS?
		#maybe just do it in C?- would be an easy/simple little function to write...
		for nx in range(nbasis[0]):
			for ny in range(nbasis[1]):
				for nz in range(nbasis[2]):
					for qx in range(nquad[0]):
						for qy in range(nquad[1]):
							for qz in range(nquad[2]):
								basis[nx][ny][nz][qx][qy][qz] = basis_list[0][nx][qx] * basis_list[1][ny][qy] * basis_list[2][nz][qz]
		
		return basis
		
	def eval_basis_derivatives(self,*args):	
		basis_list = []
		deriv_list = []
		nbasis = []
		nquad = []
		for i in range(self.nelements):
			pts = args[i]
			blockindex = args[i + self.nelements]
			basis_list.append(self.elemlist[i].eval_basis_functions(pts,blockindex))
			deriv_list.append(self.elemlist[i].eval_basis_derivatives(pts,blockindex))
			nquad.append(pts.shape[0])
			nbasis.append(self.elemlist[i].eval_basis_functions(pts,blockindex).shape[0])

		derivs = np.zeros((nbasis + nquad + [self.nelements,]))

		#THIS NEEDS OPTIMIZING FOR HIGH DEGREE
		#IS THERE A CLEVER NUMPY WAY OF DOING THINGS?
		#maybe just do it in C?- would be an easy/simple little function to write...
		for nx in range(nbasis[0]):
			for ny in range(nbasis[1]):
				for nz in range(nbasis[2]):
					for qx in range(nquad[0]):
						for qy in range(nquad[1]):
							for qz in range(nquad[2]):									
								derivs[nx][ny][nz][qx][qy][qz][0] = deriv_list[0][nx][qx] * basis_list[1][ny][qy] * basis_list[2][nz][qz]
								derivs[nx][ny][nz][qx][qy][qz][1] = basis_list[0][nx][qx] * deriv_list[1][ny][qy] * basis_list[2][nz][qz]
								derivs[nx][ny][nz][qx][qy][qz][2] = basis_list[0][nx][qx] * basis_list[1][ny][qy] * deriv_list[2][nz][qz]
								
		
		return derivs 
		
		
	def fill_offsets(self):
		for k in range(self.nelements):
			self.elemlist[k].fill_offsets()
		
class EmptyElement(FiniteElement):
	def __init__(self,n,support_points=None):
		self.continuity = 'H1'
		self.dimension = 1
		self.support_size = 1
		self.ref_cell = [[-1.,1.],]
		self.dof = 1
		self.offset_multiplier = 1
		self.order = 1
		self.ndofs_per_cell = 1

	def get_offsets(self,i):
		return np.array([0,],dtype=np.int32)
		
	def get_offsetmult(self,i):
		return np.array([1,],dtype=np.int32)
		
	def eval_basis_functions(self,x,i):
		return np.array([[1.,],],dtype=np.float64)

	def eval_basis_derivatives(self,x,i):
		return np.array([[1.,],],dtype=np.float64)
		
	def get_nxny(self,nxcell):
		return [nxcell,]

	def get_interaction_cells(self,nxcell,bc):
		interaction_cells = np.zeros((nxcell,2),dtype=np.int32) 
		ilist = np.arange(0,interaction_cells.shape[0])
		rightmost_bound = np.floor_divide(ilist,1) #floor(n/p)
		interaction_cells[:,0] = rightmost_bound 
		interaction_cells[:,1] = rightmost_bound
		return interaction_cells
	
	def symbolic_basis_functions(self,var):
		return [sympy.Rational(1,1),]

	def symbolic_basis_derivatives(self,var):
		return [sympy.Rational(1,1),]
							
class GD(FiniteElement):
	def __init__(self,n,support_points=None):
		self.continuity = 'H1'
		self.dimension = 1
		self.support_size = max(1,n-1)
		self.ref_cell = [[-1.,1.],]
		self.dof = 1
		self.offset_multiplier = 1
		self.order = n
		self.ndofs_per_cell = 1
		
	def get_nxny(self,nxcell):
		return [nxcell +1,]
		
	def fill_offsets(self):
		offsets = np.arange(-(self.order-1)/2,(self.order-1)/2+2,1,dtype=np.int32)
		self.offsets = np.zeros((1,self.order+1),dtype=np.int32)
		self.offsets[:,:] = np.expand_dims(offsets,axis=0)

	def symbolic_basis_functions(self,var):
		symbas = []
		x = var
		a = 2
		b = -self.order
		p = a * np.arange(self.order+1) +b
		#print a,b,p,np.arange(self.order+1)
		for i in range(0,self.order+1):
			symbas.append(lagrange_poly_support(i,p,x))
		return symbas
		
	def get_interaction_cells(self,nxcell,bc):
		if bc == 'periodic':
			off = 0
		else:
			off = 1		
		interaction_offsets = np.array([-(self.order-1)/2-1,(self.order-1)/2],dtype=np.int32)
#		interaction_offsets = np.array([-2,1],dtype=np.int32)
		interaction_cells = np.zeros((nxcell+off,2),dtype=np.int32) 
		ilist = np.arange(0,interaction_cells.shape[0])
		interaction_cells[:,:] = np.expand_dims(ilist,axis=1) + np.expand_dims(interaction_offsets,axis=0)
		#print interaction_cells
		return interaction_cells

class DGD(FiniteElement):
	def __init__(self,n,support_points=None):
		self.continuity = 'L2'
		self.dimension = 1
		self.support_size = max(1,n-2)
		self.ref_cell = [[-1.,1.],]
		self.dof = 1
		self.offset_multiplier = 1
		self.order = n
		self.ndofs_per_cell = 1
		
	def symbolic_basis_functions(self,var):
		cg_symbas = []
		x = var
		a = 2
		b = -self.order
		p = a * np.arange(self.order+1) +b
		for i in range(0,self.order+1):
			cg_symbas.append(lagrange_poly_support(i,p,x))
		
		symbas = []
		symbas.append(diff(-cg_symbas[0]))
		for i in range(1,self.order):
			dN = diff(cg_symbas[i])
			symbas.append(symbas[i-1] - dN)
		return symbas
		
	def get_nxny(self,nxcell):
		return [nxcell,]
		
	def fill_offsets(self):
		offsets = np.arange(-(self.order-1)/2,(self.order-1)/2+1,1,dtype=np.int32)
		self.offsets = np.zeros((1,self.order),dtype=np.int32)
		self.offsets[:,:] = np.expand_dims(offsets,axis=0)

	def get_interaction_cells(self,nxcell,bc):	
		interaction_offsets = np.array([-(self.order-1)/2,(self.order-1)/2],dtype=np.int32)
		interaction_cells = np.zeros((nxcell,2),dtype=np.int32)
		ilist = np.arange(0,interaction_cells.shape[0])
		interaction_cells[:,:] = np.expand_dims(ilist,axis=1) + np.expand_dims(interaction_offsets,axis=0)
		return interaction_cells
		
#class FVFE_CG_Boundary_1D(FiniteElement):
	#def __init__(self,n):
		#self.continuity = 'H1'
		#self.dimension = 1
		#self.support_size = 2
		#self.ref_cell = [[-1./2.,1/2.],]
		#self.dof = 1
		#self.offset_multiplier = 1

	#def symbolic_basis_functions(self,var):
		#pass
		
	#def get_nxny(self,nxcell):
		#return [nxcell +1,]
		
	#def fill_offsets(self): #FIX THIS UP- SHOULD BE FILLING BLOCKS!
		##THINK A LITTLE ABOUT HOW OFFSETS AND IOFFSETS SHOULD BE SIZED HERE?
		##IE I THINK I HAVE IOFFSET BLOCKS AND OFFSET BLOCKS IE CELL BLOCKS AND DOF BLOCKS?
		##THIS NEEDS TO WORK WITH THE CODE TO GET OFFSETS AND IOFFSETS
		##JUST PREALLOC REALLY WHERE THIS IS A LITTLE TRICKY!
		#self.interaction_offsets = np.zeros((5,2),dtype=np.int32)
		#self.offsets = np.zeros((5,4),dtype=np.int32)

		#self.offsets[0,:] = np.array([0,1,2,3],dtype=np.int32)
		#self.offsets[1:4,:] = np.expand_dims(np.array([-1,0,1,2],dtype=np.int32),axis=0)
		#self.offsets[4,:] = np.array([-2,-1,0,1],dtype=np.int32)
		
		#self.interaction_offsets[0,:] = np.array([0,1],dtype=np.int32) 
		#self.interaction_offsets[1,:] = np.array([-1,1],dtype=np.int32) 
		#self.interaction_offsets[2,:] = np.expand_dims(np.array([-2,1],dtype=np.int32),axis=0)
		#self.interaction_offsets[3,:] = np.array([-2,0],dtype=np.int32)
		#self.interaction_offsets[4,:] = np.array([-2,-1],dtype=np.int32)
			
	#def fill_basis_functions(self,x): #FIX THIS UP- SHOULD BE FILLING BLOCKS!
		#f1 = f1func(x)
		#f2 = f2func(x)
		#f3 = f3func(x)
		#f4 = f4func(x)
		#f1p = f1pfunc(x)
		#f2p = f2pfunc(x)
		#f3p = f3pfunc(x)
		#f4p = f4pfunc(x)
		#f1m = f1mfunc(x)
		#f2m = f2mfunc(x)
		#f3m = f3mfunc(x)
		#f4m = f4mfunc(x)
		#self.basis_funcs = np.zeros((ncell,4,x.shape[0]))
		#self.basis_funcs[0,:,:] = np.array([f4m,f3m,f2m,f1m])
		#self.basis_funcs[1:ncell-1,:,:] = np.expand_dims(np.array([f4,f3,f2,f1]),axis=0)
		#self.basis_funcs[ncell-1,:,:] = np.array([f4p,f3p,f2p,f1p])
			
	#def fill_basis_derivatives(self,x): #FIX THIS UP- SHOULD BE FILLING BLOCKS!
		#df1 = df1func(x)
		#df2 = df2func(x)
		#df3 = df3func(x)
		#df4 = df4func(x)
		#df1p = df1pfunc(x)
		#df2p = df2pfunc(x)
		#df3p = df3pfunc(x)
		#df4p = df4pfunc(x)
		#df1m = df1mfunc(x)
		#df2m = df2mfunc(x)
		#df3m = df3mfunc(x)
		#df4m = df4mfunc(x)
		#self.basis_derivs = np.zeros((ncell,4,x.shape[0]))
		#self.basis_derivs[0,:,:] = np.array([df4m,df3m,df2m,df1m])
		#self.basis_derivs[1:ncell-1,:,:] = np.expand_dims(np.array([df4,df3,df2,df1]),axis=0)
		#self.basis_derivs[ncell-1,:,:] = np.array([df4p,df3p,df2p,df1p])

	#def eval_basis_functions(self,i):
		#return np.copy(np.expand_dims(self.basis_funcs[i,:,:],axis=0))
	#def eval_basis_derivatives(self,i):
		#return np.copy(np.expand_dims(self.basis_derivs[i,:,:],axis=0))
	#def get_offsets(self,i):
		#return np.expand_dims(self.offsets[i,:],axis=1)
	#def get_offsets1d(self,i):
		#return self.offsets[i,:]
	#def get_interaction_cells(self,nxcell,bc):
		#pass
		
#class FVFE_DG_Boundary_1D(FiniteElement):
	#def __init__(self,n):
		#self.continuity = 'L2'
		#self.dimension = 1
		#self.support_size = 1
		#self.ref_cell = [[-1./2.,1/2.],]
		#self.dof = 1
		#self.offset_multiplier = 1

	#def symbolic_basis_functions(self,var):
		#pass
		
	#def get_nxny(self,nxcell):
		#return [nxcell,]
		
	#def fill_offsets(self): #FIX THIS UP- SHOULD BE FILLING BLOCKS!
		#self.interaction_offsets = np.zeros((nbasis,2),dtype=np.int32)
		#self.offsets = np.zeros((ncell,3),dtype=np.int32)
		
		#self.offsets[0,:] = np.array([0,1,2],dtype=np.int32)
		#self.offsets[1:ncell-1,:] = np.expand_dims(np.array([-1,0,1],dtype=np.int32),axis=0)
		#self.offsets[ncell-1,:] = np.array([-2,-1,0],dtype=np.int32)
		
		#self.interaction_offsets[0,:] = np.array([0,1],dtype=np.int32) 
		#self.interaction_offsets[1,:] = np.array([-1,1],dtype=np.int32) 
		#self.interaction_offsets[2,:] = np.array([-2,1],dtype=np.int32) 
		#self.interaction_offsets[3:nbasis-3,:] = np.expand_dims(np.array([-1,1],dtype=np.int32),axis=0)
		#self.interaction_offsets[nbasis-3,:] = np.array([-1,2],dtype=np.int32)
		#self.interaction_offsets[nbasis-2,:] = np.array([-1,1],dtype=np.int32)
		#self.interaction_offsets[nbasis-1,:] = np.array([-1,0],dtype=np.int32)
				
	#def fill_basis_functions(self,x): #FIX THIS UP- SHOULD BE FILLING BLOCKS!
		#delta1 = delta1_func(x)
		#delta0 = delta0_func(x)
		#deltam1 = deltam1_func(x)
		#alpha1 = alpha1_func(x)
		#beta1 = beta1_func(x)
		#gamma1 = gamma1_func(x)
		#alphaN = alphaN_func(x)
		#betaN = betaN_func(x)
		#gammaN = gammaN_func(x)
		#self.basis_funcs = np.zeros((ncell,3,x.shape[0]))
		#self.basis_funcs[0,:,:] = np.array([beta1,alpha1,gamma1])
		#self.basis_funcs[1:ncell-1,:,:] = np.expand_dims(np.array([delta1,delta0,deltam1]),axis=0)
		#self.basis_funcs[ncell-1,:,:] = np.array([gammaN,alphaN,betaN]) 
						
	#def fill_basis_derivatives(self,x): #FIX THIS UP- SHOULD BE FILLING BLOCKS!
		#ddelta1 = ddelta1_func(x)
		#ddelta0 = ddelta0_func(x)
		#ddeltam1 = ddeltam1_func(x)
		#dalpha1 = dalpha1_func(x)
		#dbeta1 = dbeta1_func(x)
		#dgamma1 = dgamma1_func(x)
		#dalphaN = dalphaN_func(x)
		#dbetaN = dbetaN_func(x)
		#dgammaN = dgammaN_func(x)
		#self.basis_derivs = np.zeros((ncell,3,x.shape[0]))
		#self.basis_derivs[0,:,:] = np.array([dbeta1,dalpha1,dgamma1])
		#self.basis_derivs[1:ncell-1,:,:] = np.expand_dims(np.array([dgammaN,dalphaN,dbetaN]),axis=0)
		#self.basis_derivs[ncell-1,:,:] = np.array([ddelta1,ddelta0,ddeltam1])

	#def eval_basis_functions(self,i):
		#return np.copy(np.expand_dims(self.basis_funcs[i,:,:],axis=0))
	#def eval_basis_derivatives(self,i):
		#return np.copy(np.expand_dims(self.basis_derivs[i,:,:],axis=0))
	#def get_offsets(self,i):
		#return np.expand_dims(self.offsets[i,:],axis=1)
	#def get_offsets1d(self,i):
		#return self.offsets[i,:]
	#def get_interaction_cells(self,nxcell,bc):
		#pass

class QL(FiniteElement):
	def __init__(self,n,support_points=None):
		self.continuity = 'H1'
		self.dimension = 1
		self.support_size = n
		self.ref_cell = [[-1.,1.],]
		self.dof = 1
		self.order = n
		self.ndofs_per_cell = n
		self.spts = support_points
		if self.spts == None:
			self.spts = gauss_lobatto(n+1)
		
	def symbolic_basis_functions(self,var):
		symbas = []
		for i in range(self.order+1):
			symbas.append(lagrange_poly_support(i,self.spts,var))
		return symbas

	def get_nxny(self,nxcell):
		return [nxcell*self.order + 1,]

	def fill_offsets(self):
		offsets = np.arange(0,self.order+1,dtype=np.int32)
		self.offsets = np.zeros((1,self.order+1),dtype=np.int32)
		self.offsets[:,:] = np.expand_dims(offsets,axis=0)
		self.offset_multiplier = self.order
		#'p*i','p*i+1'...'p+*i + p'
		
	def get_interaction_cells(self,nxcell,bc):
		if bc == 'periodic':
			off = 0
		else:
			off = 1
		interaction_cells = np.zeros((nxcell*self.order + off,2),dtype=np.int32)
		ilist = np.arange(0,interaction_cells.shape[0])
		#build a block of interaction offsets for a cell
		rightmost_bound = np.floor_divide(ilist,self.order) #floor(n/p)
		leftmost_bound = np.floor_divide(ilist-1,self.order) #floor(n-1/p)
		#no circular wrapping since we need the actual value in order to get differences in pre-alloc correct ie 2 - (-1) = 3
		#print rightmost_bound
		#print leftmost_bound
		interaction_cells[:,0] = leftmost_bound 
		interaction_cells[:,1] = rightmost_bound 
		if bc == 'dirichlet' or bc =='neumann':
			interaction_cells[0,0] = interaction_cells[0,1]
			interaction_cells[-1,1] = interaction_cells[-1,0]
		return interaction_cells
		
class DQL(FiniteElement):
	def __init__(self,p,support_points=None):
		self.continuity = 'L2'
		self.dimension = 1
		self.support_size = 1
		self.ref_cell = [[-1.,1.],]
		self.dof = 1
		self.order = p
		self.ndofs_per_cell = p
		self.spts = support_points
		if self.spts == None:
			if self.order >1:
				self.spts = gauss_lobatto(self.order)
			
	def symbolic_basis_functions(self,var):
		symbas = []
		if self.order > 1:
			for i in range(self.order):
				symbas.append(lagrange_poly_support(i,self.spts,var))
		else:
			symbas.append(sympy.Rational(1,2))		
		return symbas
	
		
	def get_nxny(self,nxcell): 
		return [nxcell*self.order,]
		
	def fill_offsets(self): 
		offsets = np.arange(0,self.order,dtype=np.int32)
		self.offsets = np.zeros((1,self.order),dtype=np.int32)
		self.offsets[:,:] = np.expand_dims(offsets,axis=0)
		self.offset_multiplier = self.order
		#'p*i','p*i+1'...'p+*i + p'
		
	def get_interaction_cells(self,nxcell,bc):
		interaction_cells = np.zeros((nxcell*self.order,2),dtype=np.int32) 
		ilist = np.arange(0,interaction_cells.shape[0])
		rightmost_bound = np.floor_divide(ilist,self.order) #floor(n/p)
		interaction_cells[:,0] = rightmost_bound 
		interaction_cells[:,1] = rightmost_bound
		return interaction_cells
		
class DQMSE(FiniteElement):
	def __init__(self,p,support_points=None):
		self.continuity = 'L2'
		self.dimension = 1
		self.support_size = 1
		self.ref_cell = [[-1.,1.],]
		self.dof = 1
		self.order = p
		self.ndofs_per_cell = p
		self.spts = support_points
				
	def symbolic_basis_functions(self,var):
		#define through the mimetic relationship
	
		cont_parent = QL(self.order,support_points=self.spts)
		cg_symbas = cont_parent.symbolic_basis_functions(var)
		
		symbas = []
		symbas.append(diff(-cg_symbas[0]))
		for i in range(1,self.order):
			dN = diff(cg_symbas[i])
			symbas.append(symbas[i-1] - dN)		
		return symbas
	
	def get_nxny(self,nxcell): 
		return [nxcell*self.order,]
		
	def fill_offsets(self): 
		offsets = np.arange(0,self.order,dtype=np.int32)
		self.offsets = np.zeros((1,self.order),dtype=np.int32)
		self.offsets[:,:] = np.expand_dims(offsets,axis=0)
		self.offset_multiplier = self.order
		#'p*i','p*i+1'...'p+*i + p'
		
	def get_interaction_cells(self,nxcell,bc):
		interaction_cells = np.zeros((nxcell*self.order,2),dtype=np.int32) 
		ilist = np.arange(0,interaction_cells.shape[0])
		rightmost_bound = np.floor_divide(ilist,self.order) #floor(n/p)
		interaction_cells[:,0] = rightmost_bound 
		interaction_cells[:,1] = rightmost_bound
		return interaction_cells

class QB(FiniteElement):
	def __init__(self,n,support_points=None):
		self.continuity = 'H1'
		self.dimension = 1
		self.support_size = n
		self.ref_cell = [[-1.,1.],]
		self.dof = 1
		self.order = n
		self.ndofs_per_cell = n
			
	def symbolic_basis_functions(self,var):
		symbas = bernstein_polys(self.order,var)
		return symbas

	def get_nxny(self,nxcell):
		return [nxcell*self.order + 1,]

	def fill_offsets(self):
		offsets = np.arange(0,self.order+1,dtype=np.int32)
		self.offsets = np.zeros((1,self.order+1),dtype=np.int32)
		self.offsets[:,:] = np.expand_dims(offsets,axis=0)
		self.offset_multiplier = self.order
		#'p*i','p*i+1'...'p+*i + p'
		
	def get_interaction_cells(self,nxcell,bc):
		if bc == 'periodic':
			off = 0
		else:
			off = 1
		interaction_cells = np.zeros((nxcell*self.order + off,2),dtype=np.int32)
		ilist = np.arange(0,interaction_cells.shape[0])
		#build a block of interaction offsets for a cell
		rightmost_bound = np.floor_divide(ilist,self.order) #floor(n/p)
		leftmost_bound = np.floor_divide(ilist-1,self.order) #floor(n-1/p)
		#no circular wrapping since we need the actual value in order to get differences in pre-alloc correct ie 2 - (-1) = 3
		#print rightmost_bound
		#print leftmost_bound
		interaction_cells[:,0] = leftmost_bound 
		interaction_cells[:,1] = rightmost_bound 
		if bc == 'dirichlet' or bc =='neumann':
			interaction_cells[0,0] = interaction_cells[0,1]
			interaction_cells[-1,1] = interaction_cells[-1,0]
		return interaction_cells

class DQB(FiniteElement):
	def __init__(self,p,support_points=None):
		self.continuity = 'L2'
		self.dimension = 1
		self.support_size = 1
		self.ref_cell = [[-1.,1.],]
		self.dof = 1
		self.order = p
		self.ndofs_per_cell = p

	def symbolic_basis_functions(self,var):
		#define through the mimetic relationship
		#cg_symbas  = bernstein_polys(self.order,var)
			
		#these are just scaled, discontinuous versions of QB(n-1)!
		#this is EQUIVALENT to the mimetic relationship definition!
		symbas  = bernstein_polys(self.order-1,var)
		for i,basisfunc in enumerate(symbas):
			if basisfunc == 1:
				symbas[i] = sympy.Rational(1,2) #basisfunc / 2
				continue
			symbas[i] = symbas[i] * self.order/2.
		return symbas
	
	def get_nxny(self,nxcell): 
		return [nxcell*self.order,]
		
	def fill_offsets(self): 
		offsets = np.arange(0,self.order,dtype=np.int32)
		self.offsets = np.zeros((1,self.order),dtype=np.int32)
		self.offsets[:,:] = np.expand_dims(offsets,axis=0)
		self.offset_multiplier = self.order
		#'p*i','p*i+1'...'p+*i + p'
		
	def get_interaction_cells(self,nxcell,bc):
		interaction_cells = np.zeros((nxcell*self.order,2),dtype=np.int32) 
		ilist = np.arange(0,interaction_cells.shape[0])
		rightmost_bound = np.floor_divide(ilist,self.order) #floor(n/p)
		interaction_cells[:,0] = rightmost_bound 
		interaction_cells[:,1] = rightmost_bound
		return interaction_cells
