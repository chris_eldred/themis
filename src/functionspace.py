from mesh import *
from basis import *
from geometry import *
from output import *
import instant

#THIS IS AN UGLY HACK NEEDED BECAUSE OWNERSHIP RANGES ARGUMENT TO petc4py DMDA_CREATE is BROKEN
decompfunction_code = r"""
#ifdef SWIG
%include "petsc4py/petsc4py.i"
#endif

#include <petsc.h>

PetscErrorCode decompfunction(DM cellda, DM sda, PetscInt dim, PetscInt cellndofsx, PetscInt cellndofsy, PetscInt cellndofsz, PetscInt bdx, PetscInt bdy, PetscInt bdz) { //,const PetscInt ly[], const PetscInt lz[]

PetscErrorCode ierr;
const PetscInt *lxcell,*lycell,*lzcell;
PetscInt *lx,*ly,*lz;
PetscInt pM,pN,pP;
PetscInt i;

ierr = DMDAGetInfo(cellda,0,0,0,0,&pM,&pN,&pP,0,0,0,0,0,0);
PetscMalloc1(pM,&lx);
PetscMalloc1(pN,&ly);
PetscMalloc1(pP,&lz);
ierr = DMDAGetOwnershipRanges(cellda,&lxcell,&lycell,&lzcell); CHKERRQ(ierr);
for (i=0;i<pM;i++)
{
lx[i] = lxcell[i] * cellndofsx;
}
lx[pM-1] = lx[pM-1] + bdx;
if (dim >= 2) {
for (i=0;i<pN;i++)
{
ly[i] = lycell[i] * cellndofsy;
}
ly[pN-1] = ly[pN-1] + bdy;
}
if (dim >= 3) {
for (i=0;i<pP;i++)
{
lz[i] = lzcell[i] * cellndofsz;
}
lz[pP-1] = lz[pP-1] + bdz;
}
//printf("%i %i %i %i %i\n",bdx,bdy,bdz,lxcell[0],lx[0]);
if (dim==1) {ierr = DMDASetOwnershipRanges(sda, lx, NULL, NULL); CHKERRQ(ierr);}
if (dim==2) {ierr = DMDASetOwnershipRanges(sda, lx, ly, NULL); CHKERRQ(ierr);}
if (dim==3) {ierr = DMDASetOwnershipRanges(sda, lx, ly, lz); CHKERRQ(ierr);}
PetscFree(lx);
PetscFree(ly);
PetscFree(lz);
return 0;
}
"""

decompfunction = instant.build_module(code=decompfunction_code,
					  include_dirs=include_dirs,
					  library_dirs=library_dirs,
					  libraries=libraries,
					  swig_include_dirs=swig_include_dirs).decompfunction 

class FunctionSpace():
	def __init__(self,mesh,element,continuity,mixed=False):
		self.elements = [element,]
		self.nelems = 1	
		self.space_offsets = [0,]
		self.mesh = mesh
		self.continuity = continuity
		
		#fill offsets
		self.elements[0].fill_offsets()
			
		#create dof map
		self.da = [create_dof_map(mesh,element),]

		self.spaces = [self,]
		self.nspaces = 1
		
		#create indices,lgmap and scatter
		#these map from split global to split local, so they don't care if they are part of a mixed space or not
		ind = self.da[0].getLGMap().getIndices() #these are the global indices for each local dof, as a numpy array
		lvec =  self.da[0].createLocalVector() #this is a seq vec with enough padding for ghost cells FOR SPLIT LOCAL SPACE
		gvec =  self.da[0].createGlobalVector() #this is an mpi vec for SPLIT GLOBAL SPACE
		localsize = lvec.getSize() #this is the localndofs PLUS ghost values
		
		#lvec.view()
		#gvec.view()		
		self.indices = [PETSc.IS().createGeneral(ind, comm=PETSc.COMM_WORLD),] #these are global indices for each local dof
		self.lgmaps = [self.da[0].getLGMap(),] 
		localis = PETSc.IS().createStride(localsize, first=0, step=1, comm=PETSc.COMM_WORLD) #this is just arange(localsize)
		
		self.scatters = [PETSc.Scatter().create(gvec, self.indices[0], lvec, localis),]  #this scatter maps from SPLIT GLOBAL vec to SPLIT LOCAL vec
		lvec.destroy()
		gvec.destroy()
		
		#create compindices that map from monolithic global to split global
		#this is where being part of a mixed space matters...
		if not mixed:
			mpicomm = PETSc.COMM_WORLD.tompi4py()
			compoffset = mpicomm.scan(self.get_localndofs(0))
			compoffset = compoffset - self.get_localndofs(0)
			self.compindices = [PETSc.IS().createStride(self.get_localndofs(0), first=compoffset, step=1, comm=PETSc.COMM_WORLD),] #these are components indices- they go from monolithic global to split global

		
	def get_space_offset(self,j):
		return self.space_offsets[j]
	
	def get_indices(self,j):
		return self.indices[j]

	def get_compindices(self,j):
		return self.compindices[j]
				
	def get_space(self,j):
		return self.spaces[j]
		
	def get_elem(self,i):
		return self.elements[i]
		
	def get_da(self,i):
		return self.da[i]

	def get_scatter(self,i):
		return self.scatters[i]

	def get_lgmap(self,i):
		return self.lgmaps[i]
			
	def get_nxny(self,i):
		sizes = self.da[i].getSizes()
		return sizes

	def get_local_nxny(self,i):
		ranges = self.da[i].getRanges()
		nxs = []
		for xy in ranges:
			nxs.append((xy[1] - xy[0]))
		return nxs
				
	def get_xy(self,i):
		ranges = self.da[i].getRanges()
		return ranges
	
	def get_localxy(self,i):
		ranges = self.da[i].getGhostRanges()
		return ranges
		
	def get_localndofs(self,j):
		ranges = self.da[j].getRanges()
		nelem = 1
		for xy in ranges:
			nelem = (xy[1] - xy[0]) * nelem
		return nelem * self.da[j].getDof()	
			
	def get_ndofs(self,j):
		sizes = self.da[j].getSizes()
		nelem = 1
		for size in sizes:
			nelem = size * nelem
		return nelem * self.da[j].getDof()
					
	def destroy(self):
		for space in self.spaces:
			for i in xrange(space.nelems):
				space.da[i].destroy()
				space.indices[i].destroy()
				space.lgmaps[i].destroy()
				space.scatters[i].destroy()
				space.compindices[i].destroy()

	def output(self):
		for space in self.spaces:
			spacename = space.spacename
			for i in xrange(space.nelems):
				is_output(space.get_indices(i),spacename + '.is.' + str(i))
				da_output(space.get_da(i),spacename + '.da.' + str(i))
				is_output(space.get_compindices(i),spacename + '.compis.' + str(i))
				scatter_output(space.get_scatter(i),spacename + '.scatter.' + str(i))
				lgmap_output(space.get_lgmap(i),spacename + '.lgmap.' + str(i))
				
class VectorFunctionSpace(FunctionSpace):
	def __init__(self,mesh,elements,continuity,mixed=False):

		#self.continuity = continuity
		self.elements = elements
		self.nelems = len(elements)
		self.spaces = [self,]
		self.nspaces = 1
		self.space_offsets = [0,]
		self.mesh = mesh
		self.continuity = continuity

		#fill offsets
		for i in xrange(self.nelems):
			self.elements[i].fill_offsets()
				
		#create dof maps and indices
		#create indices and scatter contexts, in parallel

		self.da = []
		self.indices = []
		self.lgmaps = []
		self.scatters = []
		
		#create dof maps 
		for i in xrange(self.nelems):
			self.da.append(create_dof_map(mesh,elements[i]))
		
		if not mixed:
			self.compindices = []
			#create comp indices
			#these operate from monolithic global to split global, so they take into account mixed space
			lndofs_total = 0
			for i in xrange(self.nelems): 	#determine offset into global vector
				lndofs_total = lndofs_total + self.get_localndofs(i)
			mpicomm = PETSc.COMM_WORLD.tompi4py()
			compoffset = mpicomm.scan(lndofs_total)
			compoffset = compoffset -lndofs_total
			
			localoffset = 0 #this is the offset for a given COMPONENT!
			for i in xrange(self.nelems):
				self.compindices.append(PETSc.IS().createStride(self.get_localndofs(i), first=compoffset + localoffset, step=1, comm=PETSc.COMM_WORLD)) #these are components indices- they get the split global space from the monolithic global space
				localoffset = localoffset + self.get_localndofs(i)

		#create scatters, lgmaps and indices
		#these operate in split global space so it doesn't matter if they are part of mixed space
		for i in xrange(self.nelems):
			#create indices and scatter contexts, in parallel
			ind = self.da[i].getLGMap().getIndices() #these are the global indices for each local dof, as a numpy array
			lvec =  self.da[i].createLocalVector() #this is a seq vec with enough padding for ghost cells FOR SPLIT LOCAL SPACE
			gvec =  self.da[i].createGlobalVector() #this is an mpi vec for SPLIT GLOBAL SPACE
			localsize = lvec.getSize() #this is the localndofs PLUS ghost values
			
			self.indices.append(PETSc.IS().createGeneral(ind, comm=PETSc.COMM_WORLD)) #these are global indices for each local dof
			
			self.lgmaps.append(self.da[i].getLGMap())
			localis = PETSc.IS().createStride(localsize, first=0, step=1, comm=PETSc.COMM_WORLD) #this is just arange(localsize)
			
			self.scatters.append(PETSc.Scatter().create(gvec, self.indices[i], lvec, localis))  #this scatter maps from split global vec to split local vec
			lvec.destroy()
			gvec.destroy()
		
class MixedFunctionSpace(FunctionSpace):
	def __init__(self,mesh,spacelist,elemlist,continuitylist):
		self.spaces = []
		self.nspaces = len(spacelist)
		self.mesh = mesh
		
		#create spaces
		for i in xrange(len(spacelist)):
			self.spaces.append(spacelist[i](mesh,elemlist[i],continuitylist[i],mixed=True))

		#create comp indices
		#these operate from monolithic global to split global, so they take into account that they are part of a mixed space
		lndofs_total = 0
		for t in xrange(self.nspaces): 	#determine offset into global vector
			for s in xrange(self.get_space(t).nelems):
				lndofs_total = lndofs_total + self.get_space(t).get_localndofs(s)
		mpicomm = PETSc.COMM_WORLD.tompi4py()
		compoffset = mpicomm.scan(lndofs_total)
		compoffset = compoffset -lndofs_total
			
		localoffset = 0 #this is the offset for a given COMPONENT!
		for t in xrange(self.nspaces): 	
			self.get_space(t).compindices = []
			for s in xrange(self.get_space(t).nelems):
				self.get_space(t).compindices.append(PETSc.IS().createStride(self.get_space(t).get_localndofs(s), first=compoffset + localoffset, step=1, comm=PETSc.COMM_WORLD)) #these are components indices- they get the split global space from the monolithic global space
				localoffset = localoffset + self.get_space(t).get_localndofs(s)
				

		s = 0
		self.space_offsets = []
		for i in xrange(self.nspaces):
			self.space_offsets.append(s)
			s = s + self.spaces[i].nelems
		self.nelems = s

#This is a useful helper function that returns a set of 1D spaces that can be used to create a 
#compatible discretization.
def get_cgdg(spacetype,xbc='periodic',ybc='periodic',zbc='periodic',xorder=1,yorder=1,zorder=1):
	
	#pick periodic and boundary spaces
	if spacetype == 'FEEC' or spacetype == 'FEEC-Gauss' or spacetype == 'FEEC-UniformClosed' or spacetype == 'FEEC-UniformOpen' or spacetype == 'FEEC-UniformRational': 
		#THIS SHOULD REALLY BE Q- INSTEAD OF FEEC
		cg_periodic = QL
		cg_boundary = QL
		dg_periodic = DQL
		dg_boundary = DQL

	if spacetype == 'FEEC-B':
		cg_periodic = QB
		cg_boundary = QB
		dg_periodic = DQB
		dg_boundary = DQB
		
	if spacetype == 'MSE' or spacetype == 'MSE-Uniform': #QMSE = QL
		cg_periodic = QL
		cg_boundary = QL
		dg_periodic = DQMSE
		dg_boundary = DQMSE
				
	if spacetype == 'MGD':
		cg_periodic = GD
		cg_boundary = GD #FVFE_CG_Boundary_1D
		dg_periodic = DGD
		dg_boundary = DGD #FVFE_DG_Boundary_1D
	
	xcpts = None
	ycpts = None
	zcpts = None
	xdpts = None
	ydpts = None
	zdpts = None
	if spacetype == 'FEEC-Gauss':
		xdpts = gauss_legendre(xorder)
		ydpts = gauss_legendre(yorder)
		zdpts = gauss_legendre(zorder)
	if spacetype == 'FEEC-UniformOpen':
		xcpts = NewtonCotesClosed1D(1+xorder).pts
		ycpts = NewtonCotesClosed1D(1+yorder).pts
		zcpts = NewtonCotesClosed1D(1+zorder).pts
		xdpts = NewtonCotesOpen1D(xorder).pts
		ydpts = NewtonCotesOpen1D(yorder).pts
		zdpts = NewtonCotesOpen1D(zorder).pts
	if spacetype == 'FEEC-UniformClosed':
		xcpts = NewtonCotesClosed1D(1+xorder).pts
		ycpts = NewtonCotesClosed1D(1+yorder).pts
		zcpts = NewtonCotesClosed1D(1+zorder).pts
		xdpts = NewtonCotesClosed1D(xorder).pts
		ydpts = NewtonCotesClosed1D(yorder).pts
		zdpts = NewtonCotesClosed1D(zorder).pts
	if spacetype == 'MSE-Uniform':
		xcpts = NewtonCotesClosed1D(1+xorder).pts
		ycpts = NewtonCotesClosed1D(1+yorder).pts
		zcpts = NewtonCotesClosed1D(1+zorder).pts
		xdpts = NewtonCotesClosed1D(1+xorder).pts
		ydpts = NewtonCotesClosed1D(1+yorder).pts
		zdpts = NewtonCotesClosed1D(1+zorder).pts
	if spacetype == 'FEEC-UniformRational':
		xcpts = np.arange(0,1+xorder) * sympy.Rational(2,xorder) + -1
		ycpts = np.arange(0,1+yorder) * sympy.Rational(2,yorder) + -1
		zcpts = np.arange(0,1+zorder) * sympy.Rational(2,zorder) + -1
		xdpts = np.arange(0,xorder) * sympy.Rational(2,xorder-1) + -1
		ydpts = np.arange(0,yorder) * sympy.Rational(2,yorder-1) + -1
		zdpts = np.arange(0,zorder) * sympy.Rational(2,zorder-1) + -1
		
	#properly pick cg/dg
	if xbc == 'dirichlet' or xbc == 'neumann':
		cgx = cg_boundary(xorder,support_points=xcpts)
		dgx = dg_boundary(xorder,support_points=xdpts)
	if xbc == 'periodic':
		cgx = cg_periodic(xorder,support_points=xcpts)
		dgx = dg_periodic(xorder,support_points=xdpts)
	if ybc == 'dirichlet' or ybc == 'neumann':
		cgy = cg_boundary(yorder,support_points=ycpts)
		dgy = dg_boundary(yorder,support_points=ydpts)
	if ybc == 'periodic':
		cgy = cg_periodic(yorder,support_points=ycpts)
		dgy = dg_periodic(yorder,support_points=ydpts)
	if zbc == 'dirichlet' or zbc == 'neumann':
		cgz = cg_boundary(zorder,support_points=zcpts)
		dgz = dg_boundary(zorder,support_points=zdpts)
	if zbc == 'periodic':
		cgz = cg_periodic(zorder,support_points=zcpts)
		dgz = dg_periodic(zorder,support_points=zdpts)
	return cgx,cgy,cgz,dgx,dgy,dgz
	
def create_dof_map(mesh,elem):
	swidth = elem.get_support_size()
	ndof = elem.get_dof()
	
	sizes = []
	ndofs_per_cell = []
	for i in xrange(mesh.ndims):
		nx = elem.get_sub_elem(i).get_nxny(mesh.nxs[i])[0]
		if mesh.bcs[i] == 'periodic' and elem.get_sub_elem(i).continuity == 'H1':
			nx = nx - 1
		sizes.append(nx)
		ndofs_per_cell.append(elem.get_sub_elem(i).ndofs_per_cell)
	
	da = PETSc.DMDA().create(dim=mesh.ndims,dof=ndof,proc_sizes=mesh.cell_da.getProcSizes(),sizes=sizes,boundary_type=mesh.blist, stencil_type= PETSc.DMDA.StencilType.BOX, stencil_width=swidth, setup=False)
	
	#THIS IS AN UGLY HACK NEEDED BECAUSE OWNERSHIP RANGES ARGUMENT TO petc4py DMDA_CREATE is BROKEN
	bdx = list(np.array(sizes) - np.array(mesh.cell_da.getSizes(),dtype=np.int32) * np.array(ndofs_per_cell,dtype=np.int32))
	if mesh.ndims == 1:
		ndofs_per_cell.append(1)
		ndofs_per_cell.append(1)
		bdx.append(0)
		bdx.append(0)
	if mesh.ndims == 2:
		ndofs_per_cell.append(1)
		bdx.append(0)
	decompfunction(mesh.cell_da,da,mesh.ndims,ndofs_per_cell[0],ndofs_per_cell[1],ndofs_per_cell[2],int(bdx[0]),int(bdx[1]),int(bdx[2]))
	da.setUp()
	
	return da
