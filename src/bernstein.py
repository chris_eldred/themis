
import numpy as np
import sympy
import mpmath

def bernstein_polys(n,var):
	'''Returns the n+1 bernstein basis polynomials of degree n (n>=1) on [-1,1]'''
	#if n==0:
	#	return [1/2.,]
	polys = []
	b = 1
	a = -1
	t = sympy.var('t')
	for v in range(0,n+1):
		coeff = sympy.binomial(n,v)
		basisfunc = coeff * (1 - t)**(n-v) * t**v
		basisfunc = basisfunc.subs(t,(var-a)/(b-a))
		polys.append(basisfunc)
	return polys

if __name__ == "__main__":
	x = sympy.var('x')
	print(bernstein_polys(1,x))
	print(bernstein_polys(2,x))
	print(bernstein_polys(3,x))
	print(bernstein_polys(4,x))
	#print(ortho_bernstein_polys(5,x))
