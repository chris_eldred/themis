from mesh import *
from functionspace import *
from output import *
from codegenerator import *
from revisedform import *
from assemble import *

class Coefficient():
	def __init__(self,name,ctype,data,mesh):
		self.name = name
		self.ctype = ctype
		self.data = data
		self.mesh = mesh
		
class Field():
	def __init__(self,names,space,atype='standard',func=None):
		with PETSc.Log.Stage(names[0] + '_init'):

			self.name = names[0]
			self.names = names[1]
			self.space = space
			if func == None:
				func = atype
				
			with PETSc.Log.Event('create_global'):
				#create vector
				globalsize = 0
				localsize = 0
				for t in xrange(self.space.nspaces):
					for k in xrange(self.space.get_space(t).nelems):
						globalsize = globalsize + self.space.get_space(t).get_ndofs(k)
						localsize = localsize + self.space.get_space(t).get_localndofs(k)
				
				self.vector = PETSc.Vec().create(comm=PETSc.COMM_WORLD)
				self.vector.setSizes((localsize,globalsize))
				self.vector.setUp()

			with PETSc.Log.Event('create_local'):
				#create local vectors- one for EACH component
				self.lvectors = []
				for t in xrange(self.space.nspaces):
					for k in xrange(self.space.get_space(t).nelems):
						self.lvectors.append(self.space.get_space(t).get_da(k).createLocalVector())

			with PETSc.Log.Event('create_coefficients'):
				#create coefficients and functionals for evaluating the field at quadrature points
				self.evalcoeffs = []
				self.evalfuncs = []
				for t in xrange(self.space.nspaces):
					subspace = self.space.get_space(t)
					if subspace.continuity == 'H1':
						evalcoeff = Coefficient(self.name,'scalar',None,self.space.mesh)
						evalfunc = Functional(func+'.functional','h1_eval',-1,geometrylist=['detJ',],evaluate=True,valscoeff=evalcoeff,fields=(self,),fsi=(t,),assemblytype=atype)

					if subspace.continuity == 'L2':
						evalcoeff = Coefficient(self.name,'scalar',None,self.space.mesh)
						evalfunc = Functional(func+'.functional','l2_eval',-1,geometrylist=['detJ',],evaluate=True,valscoeff=evalcoeff,fields=(self,),fsi=(t,),assemblytype=atype)

					if subspace.continuity == 'Hdiv':
						evalcoeff = Coefficient(self.name,'vector',None,self.space.mesh)
						evalfunc = Functional(func+'.functional','hdiv_eval',-1,geometrylist=['detJ','J'],evaluate=True,valscoeff=evalcoeff,fields=(self,),fsi=(t,),assemblytype=atype)

					if subspace.continuity == 'Hcurl':
						evalcoeff = Coefficient('ueval','vector',None,self.space.mesh)
						evalfunc = Functional(func+'.functional','hcurl_eval',-1,geometrylist=['detJ','Jinv'],evaluate=True,valscoeff=evalcoeff,fields=(self,),fsi=(t,),assemblytype=atype)
					self.evalcoeffs.append(evalcoeff)
					self.evalfuncs.append(evalfunc)
			
	def eval_at_quad_pts(self,si,evalquadrature):
		subspace = self.space.get_space(si)
		evalfunc = self.evalfuncs[si]
		evalcoeff = self.evalcoeffs[si]
		if subspace.continuity == 'H1' or subspace.continuity == 'L2':
			evalcoeff.data = np.zeros(subspace.mesh.geometry.detJ.shape)
		if subspace.continuity == 'Hdiv' or subspace.continuity == 'Hcurl':
			evalcoeff.data = np.zeros(subspace.mesh.geometry.quadcoords.shape)
		EvaluateCoefficient(evalcoeff,evalfunc,evalquadrature)
		return evalcoeff
		
	def destroy(self):
		self.vector.destroy()
		for lvec in self.lvectors:
			lvec.destroy()

	def output(self,view,ts=None):
		k = 0
		for t in xrange(self.space.nspaces):
			for i in xrange(self.space.get_space(t).nelems):
				if not (ts == None):
					tname = self.names[k] + str(ts)
				else:
					tname = self.names[k]
				indices = self.space.get_space(t).get_compindices(i)
				#indices = self.space.get_space(t).get_indices(i)
				natvec = self.space.get_space(t).get_da(i).createNaturalVec()
				temp = self.vector.getSubVector(indices)
				self.space.get_space(t).get_da(i).globalToNatural(temp,natvec)
				vector_output(natvec,tname,view)
				self.vector.restoreSubVector(indices,temp)
				natvec.destroy()
				k = k+1
		#vector_output(self.vector,self.name,view)
