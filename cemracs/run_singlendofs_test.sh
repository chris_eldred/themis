#!/bin/bash

for i in $3;
do
mkdir -p $2/$1
python assembly_driver.py -log_view -spacetype $2 -elemorder $i -varform $1 > $1_$i.log
grep $1_$i.log -e $1_ > $1_$i.out
python matrixfree_driver.py -log_view -spacetype $2 -elemorder $i -varform $1 > $1_$i.mf.log
grep $1_$i.mf.log -e $1_ > $1_$i.mf.out
done

mv *.log *.out $2/$1
