#!/bin/bash

export PYTHONPATH=/home/aha/themis2/themis/src/
module load aha.profile
module load libs/openblas/0.2.18

mkdir -p strong/$3/$2

mpirun -n 1 python assembly_driver.py -log_view -spacetype $3 -elemorder $1 -varform $2 -nx 64 -ny 64 -nz 64 > $2_$1_1.log
mpirun -n 8 python assembly_driver.py -log_view -spacetype $3 -elemorder $1 -varform $2 -nx 64 -ny 64 -nz 64 > $2_$1_8.log
mpirun -n 16 python assembly_driver.py -log_view -spacetype $3 -elemorder $1 -varform $2 -nx 64 -ny 64 -nz 64 > $2_$1_16.log
mpirun -n 24 python assembly_driver.py -log_view -spacetype $3 -elemorder $1 -varform $2 -nx 64 -ny 64 -nz 64 > $2_$1_24.log
mpirun -n 32 python assembly_driver.py -log_view -spacetype $3 -elemorder $1 -varform $2 -nx 64 -ny 64 -nz 64 > $2_$1_32.log
mpirun -n 40 python assembly_driver.py -log_view -spacetype $3 -elemorder $1 -varform $2 -nx 64 -ny 64 -nz 64 > $2_$1_40.log
mpirun -n 48 python assembly_driver.py -log_view -spacetype $3 -elemorder $1 -varform $2 -nx 64 -ny 64 -nz 64 > $2_$1_48.log

grep $2_$1_1.log -e $2_ > $2_$1_1.out
grep $2_$1_8.log -e $2_ > $2_$1_8.out
grep $2_$1_16.log -e $2_ > $2_$1_16.out
grep $2_$1_24.log -e $2_ > $2_$1_24.out
grep $2_$1_32.log -e $2_ > $2_$1_32.out
grep $2_$1_40.log -e $2_ > $2_$1_40.out
grep $2_$1_48.log -e $2_ > $2_$1_48.out

mpirun -n 1 python matrixfree_driver.py -log_view -spacetype $3 -elemorder $1 -varform $2 -nx 64 -ny 64 -nz 64 > $2_$1_1.mf.log
mpirun -n 8 python matrixfree_driver.py -log_view -spacetype $3 -elemorder $1 -varform $2 -nx 64 -ny 64 -nz 64 > $2_$1_8.mf.log
mpirun -n 16 python matrixfree_driver.py -log_view -spacetype $3 -elemorder $1 -varform $2 -nx 64 -ny 64 -nz 64 > $2_$1_16.mf.log
mpirun -n 24 python matrixfree_driver.py -log_view -spacetype $3 -elemorder $1 -varform $2 -nx 64 -ny 64 -nz 64 > $2_$1_24.mf.log
mpirun -n 32 python matrixfree_driver.py -log_view -spacetype $3 -elemorder $1 -varform $2 -nx 64 -ny 64 -nz 64 > $2_$1_32.mf.log
mpirun -n 40 python matrixfree_driver.py -log_view -spacetype $3 -elemorder $1 -varform $2 -nx 64 -ny 64 -nz 64 > $2_$1_40.mf.log
mpirun -n 48 python matrixfree_driver.py -log_view -spacetype $3 -elemorder $1 -varform $2 -nx 64 -ny 64 -nz 64 > $2_$1_48.mf.log

grep $2_$1_1.mf.log -e $2_ > $2_$1_1.mf.out
grep $2_$1_8.mf.log -e $2_ > $2_$1_8.mf.out
grep $2_$1_16.mf.log -e $2_ > $2_$1_16.mf.out
grep $2_$1_24.mf.log -e $2_ > $2_$1_24.mf.out
grep $2_$1_32.mf.log -e $2_ > $2_$1_32.mf.out
grep $2_$1_40.mf.log -e $2_ > $2_$1_40.mf.out
grep $2_$1_48.mf.log -e $2_ > $2_$1_48.mf.out

mv *.log *.out strong/$3/$2
