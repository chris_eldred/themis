#!/bin/bash

rm *.log *.out

./strong_scaling_single.sh 1 h1mass_weighted FEEC
./strong_scaling_single.sh 2 h1mass_weighted FEEC
./strong_scaling_single.sh 3 h1mass_weighted FEEC
./strong_scaling_single.sh 4 h1mass_weighted FEEC
./clear_cache.sh

./strong_scaling_single.sh 1 hdivmass FEEC
./strong_scaling_single.sh 2 hdivmass FEEC
./strong_scaling_single.sh 3 hdivmass FEEC
./strong_scaling_single.sh 4 hdivmass FEEC
./clear_cache.sh

./strong_scaling_single.sh 1 h1laplacian_weighted FEEC
./strong_scaling_single.sh 2 h1laplacian_weighted FEEC
./strong_scaling_single.sh 3 h1laplacian_weighted FEEC
./strong_scaling_single.sh 4 h1laplacian_weighted FEEC
./clear_cache.sh

./strong_scaling_single.sh 1 h1mass_weighted MGD
./strong_scaling_single.sh 3 h1mass_weighted MGD
./clear_cache.sh

./strong_scaling_single.sh 1 hdivmass MGD
./strong_scaling_single.sh 3 hdivmass MGD
./clear_cache.sh

./strong_scaling_single.sh 1 h1laplacian_weighted MGD
./strong_scaling_single.sh 3 h1laplacian_weighted MGD
./clear_cache.sh
