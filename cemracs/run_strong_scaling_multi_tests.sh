#!/bin/bash

rm *.log *.out

./strong_scaling_multi.sh 1 h1mass_weighted FEEC
./strong_scaling_multi.sh 2 h1mass_weighted FEEC
./strong_scaling_multi.sh 3 h1mass_weighted FEEC
./strong_scaling_multi.sh 4 h1mass_weighted FEEC
./clear_cache.sh

./strong_scaling_multi.sh 1 h1laplacian_weighted FEEC
./strong_scaling_multi.sh 2 h1laplacian_weighted FEEC
./strong_scaling_multi.sh 3 h1laplacian_weighted FEEC
./strong_scaling_multi.sh 4 h1laplacian_weighted FEEC
./clear_cache.sh

./strong_scaling_multi.sh 1 h1mass_weighted MGD
./strong_scaling_multi.sh 3 h1mass_weighted MGD
./clear_cache.sh

./strong_scaling_multi.sh 1 h1laplacian_weighted MGD
./strong_scaling_multi.sh 3 h1laplacian_weighted MGD
./clear_cache.sh
