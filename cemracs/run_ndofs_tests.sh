#!/bin/bash

rm *.log *.out

./run_singlendofs_test.sh h1mass FEEC "1 2 3 4"
./clear_cache.sh
./run_singlendofs_test.sh l2mass FEEC "1 2 3 4"
./clear_cache.sh
./run_singlendofs_test.sh hdivmass FEEC "1 2 3 4"
./clear_cache.sh
./run_singlendofs_test.sh h1laplacian FEEC "1 2 3 4"
./clear_cache.sh
./run_singlendofs_test.sh h1helmholtz FEEC "1 2 3 4"
./clear_cache.sh
./run_singlendofs_test.sh h1mass_weighted FEEC "1 2 3 4"
./clear_cache.sh
./run_singlendofs_test.sh h1laplacian_weighted FEEC "1 2 3 4"
./clear_cache.sh
./run_singlendofs_test.sh h1helmholtz_weighted FEEC "1 2 3 4"
./clear_cache.sh
./run_singlendofs_test.sh zero_h1 FEEC "1 2 3 4"
./clear_cache.sh
./run_singlendofs_test.sh zero_l2 FEEC "1 2 3 4"
./clear_cache.sh
./run_singlendofs_test.sh zero_hdiv FEEC "1 2 3 4"
./clear_cache.sh

./run_singlendofs_test.sh h1mass MGD "1 3"
./clear_cache.sh
./run_singlendofs_test.sh l2mass MGD "1 3"
./clear_cache.sh
./run_singlendofs_test.sh hdivmass MGD "1 3"
./clear_cache.sh
./run_singlendofs_test.sh h1laplacian MGD "1 3"
./clear_cache.sh
./run_singlendofs_test.sh h1helmholtz MGD "1 3"
./clear_cache.sh
./run_singlendofs_test.sh h1mass_weighted MGD "1 3"
./clear_cache.sh
./run_singlendofs_test.sh h1laplacian_weighted MGD "1 3"
./clear_cache.sh
./run_singlendofs_test.sh h1helmholtz_weighted MGD "1 3"
./clear_cache.sh
./run_singlendofs_test.sh zero_h1 MGD "1 3"
./clear_cache.sh
./run_singlendofs_test.sh zero_l2 MGD "1 3"
./clear_cache.sh
./run_singlendofs_test.sh zero_hdiv MGD "1 3"
./clear_cache.sh
