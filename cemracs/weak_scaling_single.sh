#!/bin/bash

export PYTHONPATH=/home/aha/themis2/themis/src/
module load aha.profile
module load libs/openblas/0.2.18

mkdir -p weak/$3/$2

mpirun -n 1 python assembly_driver.py -log_view -spacetype $3 -elemorder $1 -varform $2 -nx 36 -ny 36 -nz 36 > $2_$1_1.log
mpirun -n 2 python assembly_driver.py -log_view -spacetype $3 -elemorder $1 -varform $2 -nx 72 -ny 36 -nz 36 > $2_$1_2.log
mpirun -n 4 python assembly_driver.py -log_view -spacetype $3 -elemorder $1 -varform $2 -nx 72 -ny 72 -nz 36 > $2_$1_4.log
mpirun -n 6 python assembly_driver.py -log_view -spacetype $3 -elemorder $1 -varform $2 -nx 72 -ny 72 -nz 54 > $2_$1_6.log
mpirun -n 8 python assembly_driver.py -log_view -spacetype $3 -elemorder $1 -varform $2 -nx 72 -ny 72 -nz 72 > $2_$1_8.log
mpirun -n 12 python assembly_driver.py -log_view -spacetype $3 -elemorder $1 -varform $2 -nx 108 -ny 72 -nz 72 > $2_$1_12.log

grep $2_$1_1.log -e $2_ > $2_$1_1.out
grep $2_$1_2.log -e $2_ > $2_$1_2.out
grep $2_$1_4.log -e $2_ > $2_$1_4.out
grep $2_$1_6.log -e $2_ > $2_$1_6.out
grep $2_$1_8.log -e $2_ > $2_$1_8.out
grep $2_$1_12.log -e $2_ > $2_$1_12.out

mpirun -n 1 python matrixfree_driver.py -log_view -spacetype $3 -elemorder $1 -varform $2 -nx 36 -ny 36 -nz 36 > $2_$1_1.mf.log
mpirun -n 2 python matrixfree_driver.py -log_view -spacetype $3 -elemorder $1 -varform $2 -nx 72 -ny 36 -nz 36 > $2_$1_2.mf.log
mpirun -n 4 python matrixfree_driver.py -log_view -spacetype $3 -elemorder $1 -varform $2 -nx 72 -ny 72 -nz 36 > $2_$1_4.mf.log
mpirun -n 6 python matrixfree_driver.py -log_view -spacetype $3 -elemorder $1 -varform $2 -nx 72 -ny 72 -nz 54 > $2_$1_6.mf.log
mpirun -n 8 python matrixfree_driver.py -log_view -spacetype $3 -elemorder $1 -varform $2 -nx 72 -ny 72 -nz 72 > $2_$1_8.mf.log
mpirun -n 12 python matrixfree_driver.py -log_view -spacetype $3 -elemorder $1 -varform $2 -nx 108 -ny 72 -nz 72 > $2_$1_12.mf.log

grep $2_$1_1.mf.log -e $2_ > $2_$1_1.mf.out
grep $2_$1_2.mf.log -e $2_ > $2_$1_2.mf.out
grep $2_$1_4.mf.log -e $2_ > $2_$1_4.mf.out
grep $2_$1_6.mf.log -e $2_ > $2_$1_6.mf.out
grep $2_$1_8.mf.log -e $2_ > $2_$1_8.mf.out
grep $2_$1_12.mf.log -e $2_ > $2_$1_12.mf.out

mv *.log *.out weak/$3/$2
