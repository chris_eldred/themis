#import sys, petsc4py
#initialize petsc
#petsc4py.init(sys.argv)
#from petsc4py import PETSc
from common import *
#from configuration import *
from mesh import *
from solver import *
from output import *
#from plotting import *
from basis import *
from revisedform import *
#from functionals import *
from field import *
import h5py
from functionspace import *
from norms import *
from projector import *

#pde = pde(PETSc)
OptDB = PETSc.Options()
#Load Configuration
#load_configuration(pde)
spacetype = OptDB.getString('spacetype', 'FEEC')
elem_order = OptDB.getInt('elemorder', 3)
nx = OptDB.getInt('nx', 24)
ny = OptDB.getInt('ny', 24)
nz = OptDB.getInt('nz', 24)

### fixed options ###
dx = OptDB.getScalar('dx',1.0/nx)
dy = OptDB.getScalar('dy',1.0/ny)
dz = OptDB.getScalar('dz',1.0/nz)
ndims = OptDB.getInt('ndims', 3)
niters = OptDB.getInt('niters', 5)
computenorms = OptDB.getBool('computenorms', True)
scale_dofs = OptDB.getBool('scale_dofs', True)
solver = OptDB.getString('iterative','fgmres')
pc = OptDB.getString('precon','jacobi')
xbc = OptDB.getString('xbc', 'periodic') #periodic neumann
ybc = OptDB.getString('ybc', 'periodic') #periodic neumann
zbc = OptDB.getString('zbc', 'periodic') #periodic neumann
quadchoice = OptDB.getString('quadchoice', 'gauss')
varform = OptDB.getString('varform', 'h1mass')
quadorder = 1 + elem_order

variational_forms_list = [varform,]
#'zero_h1','zero_l2','zero_hdiv',]
#'hdivmass','h1laplacian',]
#'h1mass_weighted','h1laplacian_weighted','h1helmholtz_weighted',]
#['h1mass','l2mass','hdivmass','h1laplacian','h1helmholtz','h1mass_weighted','h1laplacian_weighted','h1helmholtz_weighted','zero_h1','zero_l2','zero_hdiv',]
#['h1mass','h1laplacian','zero_h1']
mf_types_list = ['tensor-product','tensor-product-matrixfree','standard-matrixfree','duality-matrixfree','duality-BLAS-matrixfree',
'duality-tensor-product-matrixfree','duality-tempfree-tensor-product-matrixfree'] 
check_differences = True

print 'nx ny nz',nx,ny,nz
print 'scale dofs',scale_dofs
print 'niters',niters
print 'spacetype order',spacetype,elem_order
print 'varform',varform
print 'matrixfree types',mf_types_list

if spacetype == 'FEEC' and scale_dofs:
	nx = nx / elem_order
	ny = ny / elem_order
	nz = nz / elem_order
#	print 'nx ny nz',nx,ny,nz
#	print 'ndofs',nx*ny*nz*elem_order*elem_order*elem_order
	print 'scaled nx ny nz',nx,ny,nz


#create function spaces
cgx,cgy,cgz,dgx,dgy,dgz = get_cgdg(spacetype,ndims,xbc=xbc,ybc=ybc,zbc=zbc,xorder=elem_order,yorder=elem_order,zorder=elem_order)
l2elem = TensorProductElement([dgx,dgy,dgz])
vecxelem = TensorProductElement([cgx,dgy,dgz])
vecyelem = TensorProductElement([dgx,cgy,dgz])
veczelem = TensorProductElement([dgx,dgy,cgz])
h1elem = TensorProductElement([cgx,cgy,cgz])

quad = get_quadrature(quadchoice)
quadrature = TensorProductQuadrature([quad,quad,quad],[quadorder,quadorder,quadorder])

#Create mesh 
geom = UniformVariableBox(l2elem.get_ref_cell(),quadrature,dx,dy,dz) #UniformVariableBox UniformBox
mesh = Mesh(ndims,geom,[nx,ny,nz],[xbc,ybc,zbc])
l2space = FunctionSpace(mesh,l2elem,'L2')
hdivspace = VectorFunctionSpace(mesh,[vecxelem,vecyelem,veczelem],'Hdiv')
h1space = FunctionSpace(mesh,h1elem,'H1')

#create random fields
randomfield_l2 = Field(('randomfield_l2',['randomfield_l2',]),l2space)
randomfield_l2.vector.setRandom()
randomfield_l2.vector.set(1.0)
randomfield_l2.vector.assemble()

randomfield_h1 = Field(('randomfield_h1',['randomfield_h1',]),h1space)
randomfield_h1.vector.setRandom()
randomfield_h1.vector.set(1.0)
randomfield_h1.vector.assemble()

randomfield_hdiv = Field(('randomfield_hdiv',['randomfield_hdiv',]),hdivspace)
randomfield_hdiv.vector.setRandom()
randomfield_hdiv.vector.assemble()

#create random vecs to multiply by
randomvec_l2 = Field(('randomvec_l2',['randomvec_l2',]),l2space)
randomvec_l2.vector.setRandom()
randomvec_l2.vector.assemble()

randomvec_h1 = Field(('randomvec_h1',['randomvec_h1',]),h1space)
randomvec_h1.vector.setRandom()
randomvec_h1.vector.assemble()

randomvec_hdiv = Field(('randomvec_hdiv',['randomvec_hdiv',]),hdivspace)
randomvec_hdiv.vector.setRandom()
randomvec_hdiv.vector.assemble()

#set up functionals dict
functionaldict = {}
functionaldict['h1mass'] = Functional('standard.functional','h1mass',2,geometrylist=['detJ'],assemblytype='standard')
functionaldict['hdivmass'] = Functional('standard.functional','hdivmass',2,geometrylist=['JTJdetJinv'],assemblytype='standard')
functionaldict['l2mass'] = Functional('standard.functional','l2mass',2,geometrylist=['detJinv'],assemblytype='standard')
functionaldict['h1laplacian'] = Functional('standard.functional','h1laplacian',2,geometrylist=['JinvJTinvdetJ'],assemblytype='standard')
functionaldict['h1helmholtz'] = Functional('standard.functional','h1helmholtz',2,geometrylist=['detJ','JinvJTinv','JinvJTinvdetJ'],assemblytype='standard')
functionaldict['h1mass_weighted'] = Functional('standard.functional','h1mass_weighted',2,geometrylist=['detJ'],assemblytype='standard',fields=(randomfield_l2,),fsi=(0,)) 
functionaldict['h1laplacian_weighted'] = Functional('standard.functional','h1laplacian_weighted',2,geometrylist=['JinvJTinvdetJ'],assemblytype='standard',fields=(randomfield_h1,),fsi=(0,)) 
functionaldict['h1helmholtz_weighted'] = Functional('standard.functional','h1helmholtz_weighted',2,geometrylist=['detJ','JinvJTinv','JinvJTinvdetJ'],assemblytype='standard',fields=(randomfield_h1,),fsi=(0,))
functionaldict['zero_h1'] = Functional('standard.functional','zero',2,geometrylist=[],assemblytype='standard')
functionaldict['zero_l2'] = Functional('standard.functional','zero',2,geometrylist=[],assemblytype='standard')
functionaldict['zero_hdiv'] = Functional('standard.functional','zero',2,geometrylist=[],assemblytype='standard')

spacedict = {}
spacedict['h1mass'] = h1space
spacedict['l2mass'] = l2space
spacedict['hdivmass'] = hdivspace
spacedict['h1laplacian'] = h1space
spacedict['h1helmholtz'] = h1space
spacedict['h1mass_weighted'] = h1space
spacedict['h1laplacian_weighted'] = h1space
spacedict['h1helmholtz_weighted'] = h1space
spacedict['zero_h1'] = h1space
spacedict['zero_l2'] = l2space
spacedict['zero_hdiv'] = hdivspace

vectordict = {}
vectordict['h1mass'] = randomvec_h1
vectordict['l2mass'] = randomvec_l2
vectordict['hdivmass'] = randomvec_hdiv
vectordict['h1laplacian'] = randomvec_h1
vectordict['h1helmholtz'] = randomvec_h1
vectordict['h1mass_weighted'] = randomvec_h1
vectordict['h1laplacian_weighted'] = randomvec_h1
vectordict['h1helmholtz_weighted'] = randomvec_h1
vectordict['zero_h1'] = randomvec_h1
vectordict['zero_l2'] = randomvec_l2
vectordict['zero_hdiv'] = randomvec_hdiv

postfixes = {}
postfixes['standard'] = ''
postfixes['tensor-product'] = ''
postfixes['standard-matrixfree'] = '_mf'
postfixes['tensor-product-matrixfree'] = '_tp-mf'
postfixes['duality-matrixfree'] = '_duality-mf'
postfixes['duality-tensor-product-matrixfree'] = '_duality-tp-mf'
postfixes['duality-BLAS-matrixfree'] = '_duality-BLAS-mf'
postfixes['duality-BLAS-tensor-product-matrixfree'] = '_duality-BLAS-tp-mf'
postfixes['duality-tempfree-tensor-product-matrixfree'] = '_duality-tf-tp-mf'

def vectordiffs(form1,form2,name):
		data1 = form1.vector.getArray()
		data2 = form2.vector.getArray()
		diff = (data1 - data2)
		print name+'abs',np.nanmax(diff),np.nanmin(diff),np.nanmean(diff)
		diff = (data1 - data2)/data1
		print name,np.nanmax(diff),np.nanmin(diff),np.nanmean(diff)
		
#### Actual Assembly Loop ####

for form in variational_forms_list:
	functional = functionaldict[form]
	formlist = []
	resultlist = []
	for atype in mf_types_list:
		functional.functionalfile = atype + '.functional'
		functional.assemblytype = atype
		if form == 'hdivmass' or  form == 'zero_hdiv':
			indivname = [[form + '00',form + '01', form + '02'],[form + '10',form + '11', form + '12'],[form + '20',form + '21', form + '22']]
			resultname = [['result_' +form + '00','result_' +form + '01','result_' +form + '02'],['result_' +form + '10','result_' +form + '11','result_' +form + '12'],['result_' +form + '20','result_' +form + '21','result_' +form + '22']]
			for i in range(len(indivname)):
				for j in range(len(indivname[i])):
					indivname[i][j] = indivname[i][j] + postfixes[atype]
					resultname[i][j] = resultname[i][j] + postfixes[atype]
			formname =  (form + postfixes[atype],indivname)
			result = Field(('result_' + form,resultname),spacedict[form])
		else:
			formname = (form + postfixes[atype],[[form + postfixes[atype],],])
			result = Field(('result_' + form,['result_' + form,]),spacedict[form])
		print form,atype
		if atype == 'standard' or atype == 'tensor-product':
			twoform = TwoForm(mesh,formname,[[functional,],],spacedict[form],spacedict[form],quadrature)
			twoform.assemble()
			#THIS LOGIC CAN GO AWAY ONCE BCS ARE FIXED TO USE SUBMATRIX
			#THEN I just have a twoform.matrix, which is matrices[0][0] OR a MatNest, depending on the space!
			if form == 'hdivmass' or  form == 'zero_hdiv': 
				A = PETSc.Mat().createNest(twoform.matrices,comm=PETSc.COMM_WORLD)		
			for i in range(niters):
				result.vector.zeroEntries()
				with PETSc.Log.Stage(form + '_mvmult'):
					if form == 'hdivmass' or  form == 'zero_hdiv': 
						A.mult(vectordict[form].vector,result.vector) 
					else:
						twoform.matrices[0][0].mult(vectordict[form].vector,result.vector) 
					#twoform.matrix.mult(vectordict[form].vector,result.vector) 
			
		else:
			twoform = TwoFormMatrixFree(mesh,formname,[[functional,],],spacedict[form],spacedict[form],quadrature)
			#if form == 'hdivmass' or  form == 'zero_hdiv': 
			#	A = PETSc.Mat().createNest(twoform.matrices,comm=PETSc.COMM_WORLD) 
			for i in range(niters):
				result.vector.zeroEntries()

				#if form == 'hdivmass' or  form == 'zero_hdiv': 
				#	A.mult(vectordict[form].vector,result.vector) 
				#else:
				#	twoform.matrices[0][0].mult(vectordict[form].vector,result.vector) 
				twoform.matrix.mult(vectordict[form].vector,result.vector) 
		formlist.append(twoform)
		resultlist.append(result)
		
	if check_differences:
		for result,atype in zip(resultlist[1:],mf_types_list[1:]):
			vectordiffs(resultlist[0],result,form + '-' + atype)

#Add Output to console or file
#Petsc logging output?

#MAYBE ADD SOME SOLVING HERE AT SOME POINT?
