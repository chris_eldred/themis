#!/bin/bash


mkdir -p dynamic/$2/$1
python assembly_driver.py -log_view -spacetype $2 -elemorder $3 -varform $1 -nx 12 -ny 12 -nz 12 > $1_$3_12.log
python assembly_driver.py -log_view -spacetype $2 -elemorder $3 -varform $1 -nx 24 -ny 24 -nz 24 > $1_$3_24.log
python assembly_driver.py -log_view -spacetype $2 -elemorder $3 -varform $1 -nx 36 -ny 36 -nz 36 > $1_$3_36.log
python assembly_driver.py -log_view -spacetype $2 -elemorder $3 -varform $1 -nx 48 -ny 48 -nz 48 > $1_$3_48.log
python assembly_driver.py -log_view -spacetype $2 -elemorder $3 -varform $1 -nx 60 -ny 60 -nz 60 > $1_$3_60.log

grep $1_$3_12.log -e $1_ > $1_$3_12.out
grep $1_$3_24.log -e $1_ > $1_$3_24.out
grep $1_$3_36.log -e $1_ > $1_$3_36.out
grep $1_$3_48.log -e $1_ > $1_$3_48.out
grep $1_$3_60.log -e $1_ > $1_$3_60.out

python matrixfree_driver.py -log_view -spacetype $2 -elemorder $3 -varform $1 -nx 12 -ny 12 -nz 12 > $1_$3_12.mf.log
python matrixfree_driver.py -log_view -spacetype $2 -elemorder $3 -varform $1 -nx 24 -ny 24 -nz 24 > $1_$3_24.mf.log
python matrixfree_driver.py -log_view -spacetype $2 -elemorder $3 -varform $1 -nx 36 -ny 36 -nz 36 > $1_$3_36.mf.log
python matrixfree_driver.py -log_view -spacetype $2 -elemorder $3 -varform $1 -nx 48 -ny 48 -nz 48 > $1_$3_48.mf.log
python matrixfree_driver.py -log_view -spacetype $2 -elemorder $3 -varform $1 -nx 60 -ny 60 -nz 60 > $1_$3_60.mf.log

grep $1_$3_12.mf.log -e $1_ > $1_$3_12.mf.out
grep $1_$3_24.mf.log -e $1_ > $1_$3_24.mf.out
grep $1_$3_36.mf.log -e $1_ > $1_$3_36.mf.out
grep $1_$3_48.mf.log -e $1_ > $1_$3_48.mf.out
grep $1_$3_60.mf.log -e $1_ > $1_$3_60.mf.out

mv *.log *.out dynamic/$2/$1
