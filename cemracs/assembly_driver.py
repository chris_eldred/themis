#import sys, petsc4py
#initialize petsc
#petsc4py.init(sys.argv)
#from petsc4py import PETSc
from common import *
#from configuration import *
from mesh import *
from solver import *
from output import *
#from plotting import *
from basis import *
from revisedform import *
#from functionals import *
from field import *
import h5py
from functionspace import *
from norms import *
from projector import *

#pde = pde(PETSc)
OptDB = PETSc.Options()
#Load Configuration
#load_configuration(pde)
spacetype = OptDB.getString('spacetype', 'FEEC')
elem_order = OptDB.getInt('elemorder', 3)
nx = OptDB.getInt('nx', 24)
ny = OptDB.getInt('ny', 24)
nz = OptDB.getInt('nz', 24)

### fixed options ###
dx = OptDB.getScalar('dx',1.0/nx)
dy = OptDB.getScalar('dy',1.0/ny)
dz = OptDB.getScalar('dz',1.0/nz)
ndims = OptDB.getInt('ndims', 3)
computenorms = OptDB.getBool('computenorms', True)
scale_dofs = OptDB.getBool('scale_dofs', True)
niters = OptDB.getInt('niters', 5)
solver = OptDB.getString('iterative','fgmres')
pc = OptDB.getString('precon','jacobi')
xbc = OptDB.getString('xbc', 'periodic') #periodic neumann
ybc = OptDB.getString('ybc', 'periodic') #periodic neumann
zbc = OptDB.getString('zbc', 'periodic') #periodic neumann
quadchoice = OptDB.getString('quadchoice', 'gauss')
varform = OptDB.getString('varform', 'h1mass')
quadorder = 1 + elem_order

variational_forms_list = [varform,]
#['h1mass','l2mass','hdivmass','h1laplacian','h1helmholtz']
#['h1mass','hdivmass','h1laplacian'] 'zero_h1','zero_l2','zero_hdiv',
#'h1mass','l2mass','hdivmass','h1laplacian',
assembly_types_list = ['standard','tensor-product','duality','duality-BLAS','duality-tensor-product',] #
niters = 5
check_differences = True
scale_dofs = True

print 'nx ny nz',nx,ny,nz
print 'scale dofs',scale_dofs
print 'niters',niters
print 'spacetype order',spacetype,elem_order
print 'varform',varform
print 'assembly types',assembly_types_list

if spacetype == 'FEEC' and scale_dofs:
	nx = nx / elem_order
	ny = ny / elem_order
	nz = nz / elem_order
	print 'scaled nx ny nz',nx,ny,nz
	
#create function spaces
cgx,cgy,cgz,dgx,dgy,dgz = get_cgdg(spacetype,ndims,xbc=xbc,ybc=ybc,zbc=zbc,xorder=elem_order,yorder=elem_order,zorder=elem_order)
l2elem = TensorProductElement([dgx,dgy,dgz])
vecxelem = TensorProductElement([cgx,dgy,dgz])
vecyelem = TensorProductElement([dgx,cgy,dgz])
veczelem = TensorProductElement([dgx,dgy,cgz])
h1elem = TensorProductElement([cgx,cgy,cgz])

quad = get_quadrature(quadchoice)
quadrature = TensorProductQuadrature([quad,quad,quad],[quadorder,quadorder,quadorder])

#Create mesh 
geom = UniformVariableBox(l2elem.get_ref_cell(),quadrature,dx,dy,dz) #UniformVariableBox UniformBox
mesh = Mesh(ndims,geom,[nx,ny,nz],[xbc,ybc,zbc])
l2space = FunctionSpace(mesh,l2elem,'L2')
hdivspace = VectorFunctionSpace(mesh,[vecxelem,vecyelem,veczelem],'Hdiv')
h1space = FunctionSpace(mesh,h1elem,'H1')

#create random fields
randomfield_l2 = Field(('randomfield_l2',['randomfield_l2',]),l2space)
randomfield_l2.vector.setRandom()
randomfield_l2.vector.assemble()

randomfield_h1 = Field(('randomfield_h1',['randomfield_h1',]),h1space)
randomfield_h1.vector.setRandom()
randomfield_h1.vector.assemble()

randomfield_hdiv = Field(('randomfield_hdiv',['randomfield_hdiv',]),hdivspace)
randomfield_hdiv.vector.setRandom()
randomfield_hdiv.vector.assemble()

#set up functionals dict
functionaldict = {}
functionaldict['h1mass'] = Functional('standard.functional','h1mass',2,geometrylist=['detJ'],assemblytype='standard')
functionaldict['hdivmass'] = Functional('standard.functional','hdivmass',2,geometrylist=['JTJdetJinv'],assemblytype='standard')
functionaldict['l2mass'] = Functional('standard.functional','l2mass',2,geometrylist=['detJinv'],assemblytype='standard')
functionaldict['h1laplacian'] = Functional('standard.functional','h1laplacian',2,geometrylist=['JinvJTinvdetJ'],assemblytype='standard')
functionaldict['h1helmholtz'] = Functional('standard.functional','h1helmholtz',2,geometrylist=['detJ','JinvJTinv','JinvJTinvdetJ'],assemblytype='standard')
functionaldict['h1mass_weighted'] = Functional('standard.functional','h1mass_weighted',2,geometrylist=['detJ'],assemblytype='standard',fields=(randomfield_l2,),fsi=(0,)) 
functionaldict['h1laplacian_weighted'] = Functional('standard.functional','h1laplacian_weighted',2,geometrylist=['JinvJTinvdetJ'],assemblytype='standard',fields=(randomfield_h1,),fsi=(0,)) 
functionaldict['h1helmholtz_weighted'] = Functional('standard.functional','h1helmholtz_weighted',2,geometrylist=['detJ','JinvJTinv','JinvJTinvdetJ'],assemblytype='standard',fields=(randomfield_h1,),fsi=(0,))
functionaldict['zero_h1'] = Functional('standard.functional','zero',2,geometrylist=None,assemblytype='standard')
functionaldict['zero_l2'] = Functional('standard.functional','zero',2,geometrylist=None,assemblytype='standard')
functionaldict['zero_hdiv'] = Functional('standard.functional','zero',2,geometrylist=None,assemblytype='standard')
spacedict = {}
spacedict['h1mass'] = h1space
spacedict['l2mass'] = l2space
spacedict['hdivmass'] = hdivspace
spacedict['h1laplacian'] = h1space
spacedict['h1helmholtz'] = h1space
spacedict['h1mass_weighted'] = h1space
spacedict['h1laplacian_weighted'] = h1space
spacedict['h1helmholtz_weighted'] = h1space
spacedict['zero_h1'] = h1space
spacedict['zero_l2'] = l2space
spacedict['zero_hdiv'] = hdivspace

postfixes = {}
postfixes['standard'] = ''
postfixes['tensor-product'] = '_tp'
postfixes['duality'] = '_duality'
postfixes['duality-tensor-product'] = '_duality-tp'
postfixes['duality-BLAS'] = '_duality-BLAS'
postfixes['duality-BLAS-tensor-product'] = '_duality-BLAS-tp'
postfixes['duality-tempfree-tensor-product'] = '_duality-tf-tp'

def matrixdiffs(form1,form2,name):
	for i in xrange(len(form1.matrices)):
		for j in xrange(len(form1.matrices[i])):
			junk,junk,data1 = form1.matrices[i][j].getValuesCSR()
			junk,junk,data2 = form2.matrices[i][j].getValuesCSR()
	#junk,junk,data1 = form1.matrix.getValuesCSR()
	#junk,junk,data2 = form2.matrix.getValuesCSR()
			diff = (data1 - data2)
			print name+' abs',i,j,np.nanmax(diff),np.nanmin(diff),np.nanmean(diff)
	#print name+' abs',np.nanmax(diff),np.nanmin(diff),np.nanmean(diff)
			diff = (data1 - data2)/data1
			print name,i,j,np.nanmax(diff),np.nanmin(diff),np.nanmean(diff)
	#print name,np.nanmax(diff),np.nanmin(diff),np.nanmean(diff)


#### Actual Assembly Loop ####

for form in variational_forms_list:
	functional = functionaldict[form]
	formlist = []
	for atype in assembly_types_list:
		print form,atype
		functional.functionalfile = atype + '.functional'
		functional.assemblytype = atype
		formname = (form + postfixes[atype],[[form + postfixes[atype],],])
		if form == 'hdivmass':
			indivname = [['uumass','uvmass','uwmass'],['vumass','vvmass','vwmass'],['wumass','wvmass','wwmass']]
			for i in range(len(indivname)):
				for j in range(len(indivname[i])):
					indivname[i][j] = indivname[i][j] + postfixes[atype]
			formname =  (form + postfixes[atype],indivname)
		twoform = TwoForm(mesh,formname,[[functional,],],spacedict[form],spacedict[form],quadrature)
		for i in range(niters):
			twoform.assemble()
		formlist.append(twoform)
	if check_differences:
		for twoform,atype in zip(formlist[1:],assembly_types_list[1:]):	
			matrixdiffs(formlist[0],twoform,form + '-' + atype)

#Add Output to console or file
#Petsc logging output?

#MAYBE ADD SOME SOLVING HERE AT SOME POINT?
