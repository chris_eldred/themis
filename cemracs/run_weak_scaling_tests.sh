#!/bin/bash

rm *.log *.out

./weak_scaling_single.sh 1 h1mass_weighted FEEC
./weak_scaling_single.sh 2 h1mass_weighted FEEC
./weak_scaling_single.sh 3 h1mass_weighted FEEC
./weak_scaling_single.sh 4 h1mass_weighted FEEC
./clear_cache.sh

./weak_scaling_single.sh 1 hdivmass FEEC
./weak_scaling_single.sh 2 hdivmass FEEC
./weak_scaling_single.sh 3 hdivmass FEEC
./weak_scaling_single.sh 4 hdivmass FEEC
./clear_cache.sh

./weak_scaling_single.sh 1 h1laplacian_weighted FEEC
./weak_scaling_single.sh 2 h1laplacian_weighted FEEC
./weak_scaling_single.sh 3 h1laplacian_weighted FEEC
./weak_scaling_single.sh 4 h1laplacian_weighted FEEC
./clear_cache.sh

./weak_scaling_single.sh 1 h1mass_weighted MGD
./weak_scaling_single.sh 3 h1mass_weighted MGD
./clear_cache.sh

./weak_scaling_single.sh 1 hdivmass MGD
./weak_scaling_single.sh 3 hdivmass MGD
./clear_cache.sh

./weak_scaling_single.sh 1 h1laplacian_weighted MGD
./weak_scaling_single.sh 3 h1laplacian_weighted MGD
./clear_cache.sh
