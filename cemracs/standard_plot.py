import matplotlib.pyplot as plt
import csv
import sys
import numpy as np

varform = sys.argv[1]
plot_assembly = True
plot_mf = True
initialskip = 0

orders = sys.argv[2:]

assembly_types_list = ['standard','tensor-product','duality','duality-BLAS','duality-tensor-product'] #
colors_list = ['b','g','r','m','y']
if plot_assembly:
	A = np.zeros((len(assembly_types_list),len(orders)),dtype=np.float64)
	F = np.zeros((len(assembly_types_list),len(orders)),dtype=np.float64)
	for j,order in enumerate(orders):
		with open(varform + '_' + order +'.log', 'rb') as csvfile:
			logreader = csv.reader(csvfile, delimiter=' ', quotechar='|',skipinitialspace=True)
			for l in range(initialskip):
				logreader.next()
			row0 = logreader.next()
			row1 = logreader.next()
			row2 = logreader.next()
			nx = int(row0[3])
			ny = int(row0[3])
			nz = int(row0[3])
			scale_dofs = bool(row1[2])
			niters = int(row2[1])
		if scale_dofs:
			ndofs = nx*ny*nz
		else:
			ndofs = nx*ny*nz*order*order*order
		with open(varform + '_' + order +'.out', 'rb') as csvfile:
			outreader = csv.reader(csvfile, delimiter=' ', quotechar='|',skipinitialspace=True)
			for i,row in enumerate(outreader):
				if row[0] == '---':
					continue
				A[i,j] = row[2]
				F[i,j] = row[4]
		F[:,j] = F[:,j] / A[:,j] * 10e-10
		A[:,j] = A[:,j] / ndofs / niters
	ax = plt.figure(figsize=(12,12))
	for i in range(len(assembly_types_list)):
		plt.semilogy(orders,A[i,:],marker='o',c=colors_list[i], label=assembly_types_list[i])
	plt.xlabel('element order')
	plt.ylabel('time')
	plt.legend()
	plt.savefig(varform + '.png')
	plt.close()
	ax = plt.figure(figsize=(12,12))
	for i in range(len(assembly_types_list)):
		plt.plot(orders,F[i,:],marker='o',c=colors_list[i], label=assembly_types_list[i])
	plt.xlabel('element order')
	plt.ylabel('gflops/sec')
	plt.legend()
	plt.savefig(varform + '.flops.png')
	plt.close()

mf_types_list = ['PETSc','tensor-product-matrixfree','standard-matrixfree','duality-matrixfree','duality-BLAS-matrixfree','duality-tensor-product-matrixfree','duality-tempfree-tensor-product-matrixfree'] 
colors_list = ['k','b','g','r','m','y','c']

if plot_mf:
	Amf = np.zeros((len(mf_types_list)+1,len(orders)),dtype=np.float64)
	Fmf = np.zeros((len(mf_types_list)+1,len(orders)),dtype=np.float64)
	for j,order in enumerate(orders):
		with open(varform + '_' + order +'.mf.log', 'rb') as csvfile:
			logreader = csv.reader(csvfile, delimiter=' ', quotechar='|',skipinitialspace=True)
			for l in range(initialskip):
				logreader.next()
			row0 = logreader.next()
			row1 = logreader.next()
			row2 = logreader.next()
			nx = int(row0[3])
			ny = int(row0[3])
			nz = int(row0[3])
			scale_dofs = bool(row1[2])
			niters = int(row2[1])
		if scale_dofs:
			ndofs = nx*ny*nz
		else:
			ndofs = nx*ny*nz*order*order*order
		with open(varform + '_' + order +'.mf.out', 'rb') as csvfile:
			outreader = csv.reader(csvfile, delimiter=' ', quotechar='|',skipinitialspace=True)
			for i,row in enumerate(outreader):
				if row[0] == '---':
					continue
				Amf[i,j] = row[2]
				Fmf[i,j] = row[4]
		Fmf[:,j] = Fmf[:,j] / Amf[:,j] * 10e-10
		Amf[:,j] = Amf[:,j] / ndofs / niters
	ax = plt.figure(figsize=(12,12))
	for i in range(len(mf_types_list)):
		plt.semilogy(orders,Amf[i+1,:],marker='o',c=colors_list[i], label=mf_types_list[i])
	plt.xlabel('element order')
	plt.ylabel('time')
	plt.legend()
	plt.savefig(varform + '.mf.png')
	plt.close()
	ax = plt.figure(figsize=(12,12))
	for i in range(len(mf_types_list)):
		plt.plot(orders,Fmf[i,:],marker='o',c=colors_list[i], label=mf_types_list[i])
	plt.xlabel('element order')
	plt.ylabel('gflops/sec')
	plt.legend()
	plt.savefig(varform + '.mf.flops.png')
	plt.close()
