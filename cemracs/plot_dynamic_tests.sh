#!/bin/bash

./plot_singledynamic.sh h1mass FEEC 1
./plot_singledynamic.sh h1mass FEEC 2
./plot_singledynamic.sh h1mass FEEC 3
./plot_singledynamic.sh h1mass FEEC 4

./plot_singledynamic.sh l2mass FEEC 1
./plot_singledynamic.sh l2mass FEEC 2
./plot_singledynamic.sh l2mass FEEC 3
./plot_singledynamic.sh l2mass FEEC 4

./plot_singledynamic.sh hdivmass FEEC 1
./plot_singledynamic.sh hdivmass FEEC 2
./plot_singledynamic.sh hdivmass FEEC 3
./plot_singledynamic.sh hdivmass FEEC 4

./plot_singledynamic.sh h1laplacian FEEC 1
./plot_singledynamic.sh h1laplacian FEEC 2
./plot_singledynamic.sh h1laplacian FEEC 3
./plot_singledynamic.sh h1laplacian FEEC 4

./plot_singledynamic.sh h1helmholtz FEEC 1
./plot_singledynamic.sh h1helmholtz FEEC 2
./plot_singledynamic.sh h1helmholtz FEEC 3
./plot_singledynamic.sh h1helmholtz FEEC 4

./plot_singledynamic.sh h1mass_weighted FEEC 1
./plot_singledynamic.sh h1mass_weighted FEEC 2
./plot_singledynamic.sh h1mass_weighted FEEC 3
./plot_singledynamic.sh h1mass_weighted FEEC 4

./plot_singledynamic.sh zero_h1 FEEC 1
./plot_singledynamic.sh zero_h1 FEEC 2
./plot_singledynamic.sh zero_h1 FEEC 3
./plot_singledynamic.sh zero_h1 FEEC 4

./plot_singledynamic.sh zero_l2 FEEC 1
./plot_singledynamic.sh zero_l2 FEEC 2
./plot_singledynamic.sh zero_l2 FEEC 3
./plot_singledynamic.sh zero_l2 FEEC 4

./plot_singledynamic.sh zero_hdiv FEEC 1
./plot_singledynamic.sh zero_hdiv FEEC 2
./plot_singledynamic.sh zero_hdiv FEEC 3
./plot_singledynamic.sh zero_hdiv FEEC 4


./plot_singledynamic.sh h1mass MGD 1
./plot_singledynamic.sh h1mass MGD 3

./plot_singledynamic.sh l2mass MGD 1
./plot_singledynamic.sh l2mass MGD 3

./plot_singledynamic.sh hdivmass MGD 1
./plot_singledynamic.sh hdivmass MGD 3

./plot_singledynamic.sh h1laplacian MGD 1
./plot_singledynamic.sh h1laplacian MGD 3

./plot_singledynamic.sh h1helmholtz MGD 1
./plot_singledynamic.sh h1helmholtz MGD 3

./plot_singledynamic.sh h1mass_weighted MGD 1
./plot_singledynamic.sh h1mass_weighted MGD 3

./plot_singledynamic.sh h1laplacian_weighted MGD 1
./plot_singledynamic.sh h1laplacian_weighted MGD 3

./plot_singledynamic.sh h1helmholtz_weighted MGD 1
./plot_singledynamic.sh h1helmholtz_weighted MGD 3

./plot_singledynamic.sh zero_h1 MGD 1
./plot_singledynamic.sh zero_h1 MGD 3

./plot_singledynamic.sh zero_l2 MGD 1
./plot_singledynamic.sh zero_l2 MGD 3

./plot_singledynamic.sh zero_hdiv MGD 1
./plot_singledynamic.sh zero_hdiv MGD 3
