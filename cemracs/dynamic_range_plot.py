import matplotlib.pyplot as plt
import csv
import sys
import numpy as np

varform = sys.argv[1]
plot_assembly = True
plot_mf = True
initialskip = 17

sizes = [12,24,36,48,60]
order = int(sys.argv[2])

assembly_types_list = ['standard','tensor-product','duality','duality-BLAS','duality-tensor-product'] #
colors_list = ['b','g','r','m','y']
ndofs_list = []

if plot_assembly:
	A = np.zeros((len(assembly_types_list),len(sizes)),dtype=np.float64)
	F = np.zeros((len(assembly_types_list),len(sizes)),dtype=np.float64)
	for j,size in enumerate(sizes):
		with open(varform + '_' + str(order) + '_' + str(size) + '.log', 'rb') as csvfile:
			logreader = csv.reader(csvfile, delimiter=' ', quotechar='|',skipinitialspace=True)
			for l in range(initialskip):
				logreader.next()
			row0 = logreader.next()
			row1 = logreader.next()
			row2 = logreader.next()
			scale_dofs = bool(row1[2])
			niters = int(row2[1])
		if scale_dofs:
			ndofs = size*size*size
		else:
			ndofs = size*size*size*order*order*order
		with open(varform + '_' + str(order) + '_' + str(size) + '.out', 'rb') as csvfile:
			outreader = csv.reader(csvfile, delimiter=' ', quotechar='|',skipinitialspace=True)
			for i,row in enumerate(outreader):
				if row[0] == '---':
					continue
				A[i,j] = row[2]
				F[i,j] = row[4]
		ndofs_list.append(ndofs) 
		F[:,j] = F[:,j] / A[:,j] * 10e-10
		A[:,j] = A[:,j] / ndofs / niters
	ax = plt.figure(figsize=(12,12))
	for i in range(len(assembly_types_list)):
		plt.semilogy(ndofs_list,A[i,:],marker='o',c=colors_list[i], label=assembly_types_list[i])
	plt.xlabel('ndofs')
	plt.ylabel('time')
	plt.legend()
	plt.savefig(varform + '.' + str(order) + '.dynamic.png')
	plt.close()
	ax = plt.figure(figsize=(12,12))
	for i in range(len(assembly_types_list)):
		plt.plot(ndofs_list,F[i,:],marker='o',c=colors_list[i], label=assembly_types_list[i])
	plt.xlabel('element order')
	plt.ylabel('gflops/sec')
	plt.legend()
	plt.savefig(varform + 'dynamic.flops.png')
	plt.close()
	
mf_types_list = ['PETSc','tensor-product-matrixfree','standard-matrixfree','duality-matrixfree','duality-BLAS-matrixfree','duality-tensor-product-matrixfree','duality-tempfree-tensor-product-matrixfree'] 
colors_list = ['k','b','g','r','m','y','c']
ndofs_list = []

if plot_mf:
	Amf = np.zeros((len(mf_types_list)+1,len(sizes)),dtype=np.float64)
	Fmf = np.zeros((len(mf_types_list)+1,len(sizes)),dtype=np.float64)
	for j,size in enumerate(sizes):
		with open(varform + '_' + str(order) + '_' + str(size) + '.mf.log', 'rb') as csvfile:
			logreader = csv.reader(csvfile, delimiter=' ', quotechar='|',skipinitialspace=True)
			for l in range(initialskip):
				logreader.next()
			row0 = logreader.next()
			row1 = logreader.next()
			row2 = logreader.next()
			scale_dofs = bool(row1[2])
			niters = int(row2[1])
		if scale_dofs:
			ndofs = size*size*size
		else:
			ndofs = size*size*size*order*order*order
		with open(varform + '_' + str(order) + '_' + str(size) + '.mf.out', 'rb') as csvfile:
			outreader = csv.reader(csvfile, delimiter=' ', quotechar='|',skipinitialspace=True)
			for i,row in enumerate(outreader):
				if row[0] == '---':
					continue
				Amf[i,j] = row[2]
				Fmf[i,j] = row[4]
		ndofs_list.append(ndofs)	
		Fmf[:,j] = Fmf[:,j] / Amf[:,j] * 10e-10
		Amf[:,j] = Amf[:,j] / ndofs / niters
	ax = plt.figure(figsize=(12,12))
	for i in range(len(mf_types_list)):
		plt.semilogy(ndofs_list,Amf[i+1,:],marker='o',c=colors_list[i], label=mf_types_list[i])
	plt.xlabel('ndofs')
	plt.ylabel('time')
	plt.legend()
	plt.savefig(varform + '.' + str(order) + '.dynamic.mf.png')
	plt.close()
	ax = plt.figure(figsize=(12,12))
	for i in range(len(mf_types_list)):
		plt.plot(ndofs_list,Fmf[i,:],marker='o',c=colors_list[i], label=mf_types_list[i])
	plt.xlabel('element order')
	plt.ylabel('gflops/sec')
	plt.legend()
	plt.savefig(varform + 'dynamic.mf.flops.png')
	plt.close()
