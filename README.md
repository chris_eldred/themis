# README #

Themis is a PETSc-based software framework for parallel, high-performance discretization of variational forms (and solution of systems of equations involving them) using mimetic, tensor-product Galerkin methods on structured grids. It is intended to enable a rapid cycle of prototyping and experimentation, accelerating both the development of new numerical methods and scientific models that incorporate them. It does this through leveraging a range of existing software packages (PETSc, petsc4py, Numpy, Sympy, Instant) and restricting the focus to a useful subset of methods (mimetic, tensor-product Galerkin). Themis shares high-level design and goals with the FEniCS and Firedrake projects, and the ultimate goal is to enable the same high-level description of variational forms in a mathematical language that is automatically discretized in a high-peformance manner. The majority of the code is written in Python, with performance critical parts either occuring natively in C through the petsc4py interface to PETSc (or in Numpy) or automatically generated in C using jinja2 templates and compiled at run-time using Instant.

As it currently stands, the code is in ALPHA and there is little documentation available. However, many examples can be found under the test directory, including 1D/2D/3D mixed Helmholtz problems and a linear shallow water solver using implicit time stepping. More sophisticated applications (non-linear shallow water equations, Ripa equations, hydrostatic primitive equations) are currently under active development.  Our primary scientific focus is atmospheric dynamical cores, and our choice of applications reflects this. We would welcome contributions from other fields!

### Existing Capabilities ###
    
* Support for single-block, structured grids in 1, 2 and 3 dimensions
* Parallelism through MPI
* Automated generation and compilation of assembly code or matrix-free operator evaluation (through Instant and jijna2, with user supplied assembly kernels), along with support for sum-factorization
* Arbitrary mappings between physical and reference space
* Support for mixed, vector and standard tensor-product Galerkin function spaces using arbitrary order mimetic Galerkin difference elements (see [here](http://www.sciencedirect.com/science/article/pii/S0021999116001121) and [here](http://www.lmd.polytechnique.fr/~dubos/Talks/2014KritsikisPDEs.pdf)), a single grid version of the mimetic spectral element method (see [here](http://arxiv.org/abs/1111.4304)) and Q- elements (from FEEC, with both a Lagrange and a Bernstein basis)
* Variational forms (of degree 0, 1 and 2; only volume integrals for now) and fields defined on those function spaces
* Solution of linear variational problems involving forms and fields on those function spaces
* Support for essential and periodic boundary conditions
* Access to symbolic representation of basis functions (useful for determining properties such as dispersion relationships)

### Planned/Future Capabilities ###

* New discretizations: primal grid mimetic isogeometric analysis
* Weighted row based (as opposed to element-based) assembly and matrix-free operator evaluation for mimetic Galerkin differences and isogeometric analysis
* Optimal assembly and matrix-free operator evaluation using Bernstein polynomials
* Integration of a subset of UFL, targeting automatic compilation to sum-factorization based assembly or matrix-free operator evaluation
* Facet integrals (will enable natural boundary conditions)
* Support for loop optimization packages such as COFFEE or SLOPE
* Multi-block domains
* Better support for many-core architectures through MPI-3 routines such as neighborhood collectives and windows
* Improved support for matrix-free operator evaluation, including integration into PETSc's geometric multigrid
* Solution of nonlinear variational problems
* Vectorization within and across elements
* Optimized tensor contraction routines
* Custom DM specialized towards tensor-product Galerkin methods on structured grids with support for multigrid heirarchies (p and h refinement) and optimized extraction, restriction, prolongation and insertion routines
* Support for automated determination and solution of adjoint and tangeant linear models (like dolfin-adjoint and firedrake-adjoint)
* Additional examples (such as Stokes, Bratu, Heat equation, Vector Lapacian, 3D Gravity Wave)
* Automated regression testing framework
* Automated build system

### How do I install Themis? ###

To obtain the code, simply grab a copy of the repository! Everything is written in Python or compiled at run-time, so there is no need to compile anything. Just make sure the src directory is in your PYTHONPATH. Themis requires petsc4py, mpi4py, Instant, Sympy, jinja2, Numpy, Matplotlib (only for running the examples) and h5py, so make sure those are installed and in your PYTHONPATH as well.

### I want to work on Themis.../I have this great contribution... ###

For contributions, please submit a pull request, and if it look good we will add it in!

### Help! Themis is not working... ###

Please open a bug report with any issues you encounter. Unforunately, we can only offer very limited support at this time. Every effort will be made to reply in a timely manner, but don't be alarmed if it takes a few days to receive a response! 

### Known Issues ###
* Essential boundary conditions in parallel are not terribly scalable- the matrix rows are zeroed for every process, rather than just on the owning process. It also uses the non-scalable natural to global transform. Both of these issues will be fixed soon.
* Only homogenous essential boundaries are currently supported. This is being worked on.

### License ###
Themis is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
Themis is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public License along with Themis. If not, see <http://www.gnu.org/licenses/>.
