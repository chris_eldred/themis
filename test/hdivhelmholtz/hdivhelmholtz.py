from common import *
from mesh import *
from solver import *
from output import *
from plotting import *
from basis import *
from revisedform import *
from field import *
import h5py
from functionspace import *
from assemble import *

OptDB = PETSc.Options()
spacetype = OptDB.getString('spacetype', 'MGD')
elem_order = OptDB.getInt('elemorder', 3)
geomtype = OptDB.getString('geomtype', 'uniform')
nx = OptDB.getInt('nx', 16)
ny = OptDB.getInt('ny', 16)
nz = OptDB.getInt('nz', 16)
dx = OptDB.getScalar('dx',1.0/nx)
dy = OptDB.getScalar('dy',1.0/ny)
dz = OptDB.getScalar('dz',1.0/nz)
ndims = OptDB.getInt('ndims', 2)
simname = OptDB.getString('simname', 'hdivhelmholtz')
plotsoln = OptDB.getBool('plot', True)
computenorms = OptDB.getBool('computenorms', True)
kspinfo = OptDB.getBool('kspinfo', True)
output_matrices = OptDB.getBool('outputMatrix', False)
output_fspaces = OptDB.getBool('outputSpaces', False)
output_ksp = OptDB.getBool('outputKSP', False)
output_mesh = OptDB.getBool('outputMesh', True)
show_matrix = OptDB.getBool('showMatrix', False)
show_vector = OptDB.getBool('showVector', False)
solver = OptDB.getString('iterative','fgmres') #fgmres lgmres
pc = OptDB.getString('precon','ilu') #asm jacobi ilu sor gasm gamg
xbc = OptDB.getString('xbc', 'periodic') #periodic neumann
ybc = OptDB.getString('ybc', 'periodic') #periodic neumann
zbc = OptDB.getString('zbc', 'neumann') #periodic neumann
quadchoice = OptDB.getString('quadchoice', 'gauss')
quadorder = 1 + elem_order
outdir = OptDB.getString('outdir', '')
plot_mesh = OptDB.getBool('plotMesh', False)
assemblytype = OptDB.getString('assemblytype','standard') #tensor-product tensor-product-matrixfree
mattype = OptDB.getString('mattype','block') #block monolithic shell

#create quadrature, elements and mesh
quad = get_quadrature(quadchoice)
cgx,cgy,cgz,dgx,dgy,dgz = get_cgdg(spacetype,xbc=xbc,ybc=ybc,zbc=zbc,xorder=elem_order,yorder=elem_order,zorder=elem_order)
dxs = []
nxs = []
xbcs = []
if geomtype == 'uniform':	
	a = 1. / np.pi
if geomtype == 'distorted':
	a = 6273000.
	dx = np.pi * a / (nx / 2.) 
	dy = np.pi * a / (ny / 2.)
	dz = np.pi * a / (nz / 2.)
if ndims == 1:
	quadrature = TensorProductQuadrature([quad,],[quadorder,])
	l2elem = TensorProductElement([dgx,])
	h1elem = TensorProductElement([cgx,])
	dxs.append(dx)
	nxs.append(nx)
	xbcs.append(xbc)
	rhsname = ('rhs',['hrhs','urhs',])
	solnname = ('solnform',['hsolnform','usolnform',]) 
	mixedname = ('A',[['hmass','hustiff'],['uhstiff','umass']])
	massname = ('mass',[['hmass','humass'],['uhmass','umass']])
	xname = ('x',['h','u'])
	xsolnname = ('xsoln',['hsoln','usoln']) 
	xrhsname = ('xrhs',['hrhs','urhs']) 

if ndims == 2:
	quadrature = TensorProductQuadrature([quad,quad],[quadorder,quadorder])
	l2elem = TensorProductElement([dgx,dgy])
	vecxelem = TensorProductElement([cgx,dgy])
	vecyelem = TensorProductElement([dgx,cgy])
	dxs.append(dx)
	dxs.append(dy)
	nxs.append(nx)
	nxs.append(ny)
	xbcs.append(xbc)
	xbcs.append(ybc)
	rhsname = ('rhs',['hrhs','urhs','vrhs'])
	solnname = ('solnform',['hsolnform','usolnform','vsolnform']) 
	mixedname = ('A',[['hmass','hustiff','hvstiff'],['uhstiff','uumass','uvmass'],['vhstiff','vumass','vvmass']])
	massname = ('mass',[['hmass','humass','hvmass'],['uhmass','uumass','uvmass'],['vhmass','vumass','vvmass']])
	xname = ('x',['h','u','v'])
	xsolnname = ('xsoln',['hsoln','usoln','vsoln']) 
	xrhsname = ('xrhs',['hrhs','urhs','vrhs']) 

if ndims == 3:
	quadrature = TensorProductQuadrature([quad,quad,quad],[quadorder,quadorder,quadorder])
	l2elem = TensorProductElement([dgx,dgy,dgz])
	vecxelem = TensorProductElement([cgx,dgy,dgz])
	vecyelem = TensorProductElement([dgx,cgy,dgz])
	veczelem = TensorProductElement([dgx,dgy,cgz])
	dxs.append(dx)
	dxs.append(dy)
	dxs.append(dz)
	nxs.append(nx)
	nxs.append(ny)
	nxs.append(nz)
	xbcs.append(xbc)
	xbcs.append(ybc)
	xbcs.append(zbc)
	rhsname = ('rhs',['hrhs','urhs','vrhs','wrhs'])
	solnname = ('solnform',['hsolnform','usolnform','vsolnform','wsolnform']) 
	mixedname = ('A',[['hmass','divu','divv','divw'],['gradu','uumass','uvmass','uwmass'],['gradv','vumass','vvmass','vwmass'],['gradw','wumass','wvmass','wwmass']])
	massname = ('mass',[['hmass','humass','hvmass','hwmass'],['uhmass','uumass','uvmass','uwmass'],['vhmass','vumass','vvmass','vwmass'],['whmass','wumass','wvmass','wwmass']])
	xname = ('x',['h','u','v','w'])
	xsolnname = ('xsoln',['hsoln','usoln','vsoln','wsoln']) 
	xrhsname = ('zrhs',['hrhs','urhs','vrhs','wrhs']) 

if geomtype == 'uniform':	
	geom = UniformVariableBox(l2elem.get_ref_cell(),quadrature,*dxs) 
if geomtype == 'distorted':
	geom = DistortedPeriodic2DBox(l2elem.get_ref_cell(),a,quadrature,*dxs) 
mesh = Mesh(ndims,geom,nxs,xbcs)

#create function space
if ndims == 1:
	mixed_space = MixedFunctionSpace(mesh,[FunctionSpace,FunctionSpace],[l2elem,h1elem],['L2','H1'])
if ndims == 2:
	mixed_space = MixedFunctionSpace(mesh,[FunctionSpace,VectorFunctionSpace],[l2elem,[vecxelem,vecyelem]],['L2','Hdiv'])
if ndims == 3:
	mixed_space = MixedFunctionSpace(mesh,[FunctionSpace,VectorFunctionSpace],[l2elem,[vecxelem,vecyelem,veczelem]],['L2','Hdiv'])

#create matrices and vectors
a = 1. / np.pi
hmass = Functional(assemblytype + '.functional','l2mass',2,geometrylist=['detJinv'],assemblytype=assemblytype)
umass = Functional(assemblytype + '.functional','hdivmass',2,geometrylist=['JTJdetJinv'],assemblytype=assemblytype)
divfunc = Functional(assemblytype + '.functional','div',2,geometrylist=['detJinv'],assemblytype=assemblytype)
gradfunc = Functional(assemblytype + '.functional','grad',2,geometrylist=['detJinv'],assemblytype=assemblytype)

if mattype == 'shell':
	mixed_form = TwoForm(mesh,mixedname,mixed_space,mixed_space,mattype=mattype,multfuncs=[[hmass,divfunc],[gradfunc,umass]],multquad=quadrature)
	mass = TwoForm(mesh,massname,mixed_space,mixed_space,mattype=mattype,multfuncs=[[hmass,None],[None,umass]],multquad=quadrature)
else:
	mixed_form = TwoForm(mesh,mixedname,mixed_space,mixed_space)
	mass = TwoForm(mesh,massname,mixed_space,mixed_space)

if assemblytype == 'tensor-product-matrixfree':
	assemblytype = 'tensor-product'
	
hrhscoeff = Coefficient('coeff','scalar',np.zeros(mesh.geometry.detJ.shape),mesh)
hsolncoeff = Coefficient('coeff','scalar',np.zeros(mesh.geometry.detJ.shape),mesh)
urhscoeff = Coefficient('coeff','vector',np.zeros(mesh.geometry.quadcoords.shape),mesh)
usolncoeff = Coefficient('coeff','vector',np.zeros(mesh.geometry.quadcoords.shape),mesh)
hrhs = Functional(assemblytype + '.functional','l2proj_coeff',1,geometrylist=['detJ'],coefficientlist=[hrhscoeff,],assemblytype=assemblytype)
hsoln = Functional(assemblytype + '.functional','l2proj_coeff',1,geometrylist=['detJ',],coefficientlist=[hsolncoeff,],assemblytype=assemblytype)
urhs = Functional(assemblytype + '.functional','hdivproj_coeff',1,geometrylist=['detJ','J'],coefficientlist=[urhscoeff,],assemblytype=assemblytype)
usoln = Functional(assemblytype + '.functional','hdivproj_coeff',1,geometrylist=['detJ','J'],coefficientlist=[usolncoeff,],assemblytype=assemblytype)

if ndims == 1:
	xcoord = mesh.geometry.quadcoords[:,:,:,:,:,:,0]
	if xbc == 'neumann':
		hrhscoeff.data = (-144. /  a  / a + 4.) * np.cos(6. * xcoord / a)
		hsolncoeff.data =  4. * np.cos(6. * xcoord / a)
		usolncoeff.data[:,:,:,:,:,:,0] =  -24. /  a  * np.sin(6. * xcoord / a)
	if xbc == 'periodic':
		hrhscoeff.data = (-144. /  a  / a + 4.) * np.sin(6. * xcoord / a)
		hsolncoeff.data =  4. * np.sin(6. * xcoord / a)
		usolncoeff.data[:,:,:,:,:,:,0] =  24. /  a  * np.cos(6. * xcoord / a)
if ndims == 2:
	xcoord = mesh.geometry.quadcoords[:,:,:,:,:,:,0]
	ycoord = mesh.geometry.quadcoords[:,:,:,:,:,:,1]
	if xbc == 'neumann' and ybc == 'periodic':
		hrhscoeff.data = (-80. /  a  / a + 4.) * np.cos(2. * xcoord / a) * np.sin(4. * ycoord / a )
		hsolncoeff.data =  4. * np.cos(2. * xcoord / a) * np.sin(4. * ycoord / a )
		usolncoeff.data[:,:,:,:,:,:,0] =  -8. /  a  * np.sin(2. * xcoord / a) * np.sin(4. * ycoord / a )
		usolncoeff.data[:,:,:,:,:,:,1] = 16. /  a  * np.cos(2. * xcoord / a) * np.cos(4. * ycoord / a )
	if xbc == 'periodic' and ybc == 'neumann':
		hrhscoeff.data = (-80. /  a  / a + 4.) * np.sin(2. * xcoord / a) * np.cos(4. * ycoord / a )
		hsolncoeff.data =  4. * np.sin(2. * xcoord / a) * np.cos(4. * ycoord / a )
		usolncoeff.data[:,:,:,:,:,:,0] =  8. /  a  * np.cos(2. * xcoord / a) * np.cos(4. * ycoord / a )
		usolncoeff.data[:,:,:,:,:,:,1] = -16. /  a  * np.sin(2. * xcoord / a) * np.sin(4. * ycoord / a )
	if xbc == 'neumann' and ybc == 'neumann':
		hrhscoeff.data = (-80. /  a  / a + 4.) * np.cos(2. * xcoord / a) * np.cos(4. * ycoord / a )
		hsolncoeff.data =  4. * np.cos(2. * xcoord / a) * np.cos(4. * ycoord / a )
		usolncoeff.data[:,:,:,:,:,:,0] =  -8. /  a  * np.sin(2. * xcoord / a) * np.cos(4. * ycoord / a )
		usolncoeff.data[:,:,:,:,:,:,1] = -16. /  a  * np.cos(2. * xcoord / a) * np.sin(4. * ycoord / a )
	if xbc == 'periodic' and ybc == 'periodic':
		hrhscoeff.data = (-80. /  a  / a + 4.) * np.sin(2. * xcoord / a) * np.sin(4. * ycoord / a )
		hsolncoeff.data =  4. * np.sin(2. * xcoord / a) * np.sin(4. * ycoord / a )
		usolncoeff.data[:,:,:,:,:,:,0] =  8. /  a  * np.cos(2. * xcoord / a) * np.sin(4. * ycoord / a )
		usolncoeff.data[:,:,:,:,:,:,1] = 16. /  a  * np.sin(2. * xcoord / a) * np.cos(4. * ycoord / a )

if ndims == 3:
	xcoord = mesh.geometry.quadcoords[:,:,:,:,:,:,0]
	ycoord = mesh.geometry.quadcoords[:,:,:,:,:,:,1]
	zcoord = mesh.geometry.quadcoords[:,:,:,:,:,:,2]
	c = 4.
	sc = -224. /  a  / a + 4.
	if xbc == 'periodic':
		fx = np.sin(2. * xcoord / a) 
		dfx = 2. / a * np.cos(2. * xcoord / a) 
	if xbc == 'neumann':
		fx = np.cos(2. * xcoord / a) 
		dfx = -2. / a * np.sin(2. * xcoord / a) 
	if ybc == 'periodic':
		fy = np.sin(4. * ycoord / a) 
		dfy = 4. / a * np.cos(4. * ycoord / a) 
	if ybc == 'neumann':
		fy = np.cos(4. * ycoord / a) 
		dfy = -4. / a * np.sin(4. * ycoord / a) 
	if zbc == 'periodic':
		fz = np.sin(6. * zcoord / a) 
		dfz = 6. / a * np.cos(6. * zcoord / a) 
	if zbc == 'neumann':
		fz = np.cos(6. * zcoord / a) 
		dfz = -6. / a * np.sin(6. * zcoord / a) 
	hrhscoeff.data = sc * fx * fy * fz
	hsolncoeff.data = c * fx * fy * fz
	usolncoeff.data[:,:,:,:,:,:,0] = c * dfx *  fy *  fz
	usolncoeff.data[:,:,:,:,:,:,1] = c *  fx * dfy *  fz
	usolncoeff.data[:,:,:,:,:,:,2] = c *  fx *  fy * dfz
		
rhs = OneForm(mesh,rhsname,mixed_space)
soln = OneForm(mesh,solnname,mixed_space)

#fill matrices and vectors
if not mattype == 'shell':
	AssembleForm(mixed_form,hmass,quadrature,0,0)
	AssembleForm(mixed_form,divfunc,quadrature,0,1)
	AssembleForm(mixed_form,gradfunc,quadrature,1,0)
	AssembleForm(mixed_form,umass,quadrature,1,1)
	
	AssembleForm(mass,hmass,quadrature,0,0)
	AssembleForm(mass,umass,quadrature,1,1)

AssembleForm(rhs,hrhs,quadrature,0)
AssembleForm(soln,hsoln,quadrature,0)
AssembleForm(soln,usoln,quadrature,1)

#create fields to hold numerical and actual solution
x = Field(xname,mixed_space)
xsoln = Field(xsolnname,mixed_space)
xrhs = Field(xrhsname,mixed_space)

if output_matrices:
	mixed_form.output()
	mass.output()

if output_fspaces:
	mixed_space.output()

#create solver and boundary conditions
bcs = []
if xbc == 'neumann':
	bcs.append(EssentialBC(mesh,x,'x',0.0,1,0))
if ybc == 'neumann' and ndims >1:
	bcs.append(EssentialBC(mesh,x,'y',0.0,1,1))
if zbc == 'neumann' and ndims >2:
	bcs.append(EssentialBC(mesh,x,'z',0.0,1,2))
if len(bcs) == 0:
	bcs = None
linsys = LinearSystem(mixed_form,solver,pc,bcs=bcs,rtol=1e-15,fieldsplit=True)

#solve system
linsys.solve(rhs,x,kspinfo=kspinfo)

#get the exact solution
masssys = LinearSystem(mass,solver,pc,bcs=bcs,rtol=1e-15,fieldsplit=True)
masssys.solve(soln,xsoln,kspinfo=kspinfo)
masssys.solve(rhs,xrhs,kspinfo=kspinfo)
	
if output_ksp:
	linsys.output()
	masssys.output()

#output results
ViewHDF5 = PETSc.Viewer()  
ViewHDF5.createHDF5(outdir + 'results.' + simname + '.h5', mode=PETSc.Viewer.Mode.WRITE,comm= PETSc.COMM_WORLD)
	
#output rhs and soln
x.output(ViewHDF5)
xsoln.output(ViewHDF5)
xrhs.output(ViewHDF5)
rhs.output(ViewHDF5)
soln.output(ViewHDF5)

ViewHDF5.destroy()

if computenorms:
	npts = np.prod(mesh.geometry.detJ.shape)
	hnormfunc = Functional(assemblytype + '.functional','l2norm',0,geometrylist=['detJ'],fields=(x,),fsi=(0,),coefficientlist=[hsolncoeff,],assemblytype=assemblytype)
	unormfunc = Functional(assemblytype + '.functional','hdivnorm',0,geometrylist=['detJ','J'],fields=(x,),fsi=(1,),coefficientlist=[usolncoeff,],assemblytype=assemblytype)
	hl2norm = ZeroForm(mesh,'hl2norm')
	ul2norm = ZeroForm(mesh,'ul2norm')
	AssembleForm(hl2norm,hnormfunc,quadrature)
	AssembleForm(ul2norm,unormfunc,quadrature)
	l2h = sqrt(hl2norm.value / npts)
	l2u = sqrt(ul2norm.value / npts)
	print l2h,l2u

	rank = PETSc.COMM_WORLD.Get_rank() 
	if rank == 0:
		f = h5py.File("results." + simname + ".h5", "r+")
		f['h'].attrs['norm'] = l2h
		f['u'].attrs['norm'] = l2u
		f.close()
		
#plot stuff
rank = PETSc.COMM_WORLD.Get_rank() 
if rank == 0 and plotsoln:

	h = x.eval_at_quad_pts(0,quadrature).data
	hsoln = xsoln.eval_at_quad_pts(0,quadrature).data
	uvec = x.eval_at_quad_pts(1,quadrature)
	uvecsoln = xsoln.eval_at_quad_pts(1,quadrature)
	
	quads = mesh.get_plotting_quad_coords(quadrature)
	
	if ndims == 1:
		quadx = np.ravel(quads[:,:,:,:,:,:,0])
		u = uvec.data[:,:,:,:,:,:,0]
		usoln = uvecsoln.data[:,:,:,:,:,:,0]
		
		plotheader = spacetype + '.' + simname
		spatialplot_1d(quadx,np.ravel(h),plotheader + '.h.' + str(nx))
		spatialplot_1d(quadx,np.ravel(hsoln),plotheader + '.hsoln.' + str(nx))	
		spatialplot_1d(quadx,np.ravel(hsoln - h),plotheader + '.hdiff.' + str(nx))

		spatialplot_1d(quadx,quady,np.ravel(u),plotheader + '.u.' + str(nx))
		spatialplot_1d(quadx,quady,np.ravel(usoln),plotheader + '.usoln.' + str(nx))	
		spatialplot_1d(quadx,quady,np.ravel(usoln - u),plotheader + '.udiff.' + str(nx))
		
	if ndims == 2:
		quadx = np.ravel(quads[:,:,:,:,:,:,0])
		quady = np.ravel(quads[:,:,:,:,:,:,1])

		u = uvec.data[:,:,:,:,:,:,0]
		v = uvec.data[:,:,:,:,:,:,1]
		usoln = uvecsoln.data[:,:,:,:,:,:,0]
		vsoln = uvecsoln.data[:,:,:,:,:,:,1]
		
		plotheader = spacetype + '.' + simname
		spatialplot_2d(quadx,quady,np.ravel(h),plotheader + '.h.' + str(nx))
		spatialplot_2d(quadx,quady,np.ravel(hsoln),plotheader + '.hsoln.' + str(nx))	
		spatialplot_2d(quadx,quady,np.ravel(hsoln - h),plotheader + '.hdiff.' + str(nx))
		
		spatialplot_2d(quadx,quady,np.ravel(u),plotheader + '.u.' + str(nx))
		spatialplot_2d(quadx,quady,np.ravel(usoln),plotheader + '.usoln.' + str(nx))	
		spatialplot_2d(quadx,quady,np.ravel(usoln - u),plotheader + '.udiff.' + str(nx))
		
		spatialplot_2d(quadx,quady,np.ravel(v),plotheader + '.v.' + str(nx))
		spatialplot_2d(quadx,quady,np.ravel(vsoln),plotheader + '.vsoln.' + str(nx))	
		spatialplot_2d(quadx,quady,np.ravel(vsoln - v),plotheader + '.vdiff.' + str(nx))

	if ndims == 3:
		quadx = quads[:,:,:,:,:,:,0]
		quady = quads[:,:,:,:,:,:,1]
		quadz = quads[:,:,:,:,:,:,2]
		
		u = uvec.data[:,:,:,:,:,:,0]
		v = uvec.data[:,:,:,:,:,:,1]
		w = uvec.data[:,:,:,:,:,:,2] 
		usoln = uvecsoln.data[:,:,:,:,:,:,0]
		vsoln = uvecsoln.data[:,:,:,:,:,:,1]
		wsoln = uvecsoln.data[:,:,:,:,:,:,2] 
		
		#newshape = (nz,ny,nx,quadrature.pts.shape[0],quadrature.pts.shape[1],quadrature.pts.shape[2])
		#h.data = np.reshape(h.data,newshape)
		#hsoln.data = np.reshape(hsoln.data,newshape)
	
		plotheader = spacetype + '.' + simname

		spatialplot_3d_slices(quadx,quady,h,'h',plotheader,nz)
		spatialplot_3d_slices(quadx,quady,hsoln,'hsoln',plotheader,nz)
		spatialplot_3d_slices(quadx,quady,hsoln - h,'hdiff',plotheader,nz)

		spatialplot_3d_slices(quadx,quady,u,'u',plotheader,nz)
		spatialplot_3d_slices(quadx,quady,usoln,'usoln',plotheader,nz)
		spatialplot_3d_slices(quadx,quady,usoln - u,'udiff',plotheader,nz)

		spatialplot_3d_slices(quadx,quady,v,'v',plotheader,nz)
		spatialplot_3d_slices(quadx,quady,vsoln,'vsoln',plotheader,nz)
		spatialplot_3d_slices(quadx,quady,vsoln - v,'vdiff',plotheader,nz)

		spatialplot_3d_slices(quadx,quady,w,'w',plotheader,nz)
		spatialplot_3d_slices(quadx,quady,wsoln,'wsoln',plotheader,nz)
		spatialplot_3d_slices(quadx,quady,wsoln - w,'wdiff',plotheader,nz)


linsys.destroy()
masssys.destroy()
mixed_space.destroy()

x.destroy()
xsoln.destroy()

mixed_form.destroy()
mass.destroy()
rhs.destroy()
soln.destroy()

mesh.destroy()
	
