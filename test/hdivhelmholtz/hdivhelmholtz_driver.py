import numpy as np
import matplotlib.pyplot as plt
from plotting import *
import h5py
import subprocess

grid_sizes = [16,32,48,64,96]
discrets = ['FEEC-B',]#'FEEC','FEEC-Gauss','FEEC-UniformClosed','FEEC-UniformOpen','MSE','MSE-Uniform','MGD'] 
resolution = 1.0/np.array(grid_sizes)
orderlist = [(5,4,3,2),(5,4,3,2),(5,4,3,2),(5,4,3,2),(5,4,3,2),(5,4,3,2),(5,4,3,2),(5,4,3,2),]
names = ['QB5','QB4','QB3','QB2',]
#'Q5','Q4','Q3','Q2',
#'QG5','QG4','QG3','QG2',
#'QUC5','QUC4','QUC3','QUC2',
#'QUO5','QUO4','QUO3','QUO2',
#'MSE5','MSE4','MSE3','MSE2',
#'MSEU5','MSEU4','MSEU3','MSEU2',
#'MGD5','MGD4','MGD3','MGD2']
bcs = ['neumann',] #'periodic'] # 'periodic'

for xbc in bcs:
	for ybc in bcs:
	
		l2_h_errors = np.zeros((len(names),len(grid_sizes)))
		#linf_h_errors = np.zeros((len(names),len(grid_sizes)))
		l2_u_errors = np.zeros((len(names),len(grid_sizes)))
		#l2_v_errors = np.zeros((len(names),len(grid_sizes)))
		#linf_u_errors = np.zeros((len(names),len(grid_sizes)))
		ndofs_h = np.zeros((len(names),len(grid_sizes)))
		ndofs_u = np.zeros((len(names),len(grid_sizes)))
		#ndofs_v = np.zeros((len(names),len(grid_sizes)))
	
		j = -1
		for spacetype,orders in zip(discrets,orderlist):
			if spacetype == 'MGD':
				precon = 'jacobi'
			else:
				precon = 'asm'
			for order in orders:
				j = j +1
				for i,grid_size in enumerate(grid_sizes):
					dx = 1.0/grid_size
					plotsoln = "False"
					if grid_size == grid_sizes[-1]:
						plotsoln = "True"
		
					#REMOVE THIS ONCE BOUNDARY ELEMENTS FOR MGD ARE IMPLEMENTED!
					if (xbc == 'neumann' or ybc == 'neumann') and spacetype == 'MGD':
						continue
					
					if spacetype == 'MGD' and order%2 == 0:
						continue
		
					print '################### running ############## '
					print spacetype,grid_size
	
					simname = 'helmholtz.' + spacetype + '.' + xbc + '.' + ybc + '.' + str(order) + '.' + str(grid_size)
					subprocess.call(["mpirun","-n",str(1),"python", "hdiv_2d.py","-nx",str(grid_size), "-ny",str(grid_size), 
					"-spacetype" ,spacetype,"-simname",simname,
					"-plot",plotsoln,"-elemorder",str(order),"-precon",precon,"-xbc",xbc,"-ybc",ybc,
					"-computenorms","True","-kspinfo","True"])
					#ADD IN MPIRUN MULTI PROCESS OPTION!
					
					##Load results
					f = h5py.File("results." + simname + ".h5", "r")
					l2_h = f['h'].attrs['norm']
					l2_u = f['u'].attrs['norm']
					#l2_v = f['v'].attrs['norm']
					ndofsh = np.array(f['h']).shape[0]
					ndofsu = np.array(f['u']).shape[0]
					#ndofsv = np.array(f['v']).shape[0]
					f.close()
					
					#l2_h = np.sqrt(np.sum(np.square(hdiff)))
					#linf_h = np.amax(np.abs(hdiff))
					#l2_u = np.sqrt(np.sum(np.square(udiff)))
					#linf_u = np.amax(np.abs(udiff))
					
					l2_h_errors[j,i] = l2_h
					l2_u_errors[j,i] = l2_u
					#l2_v_errors[j,i] = l2_v
					ndofs_h[j,i] = ndofsh
					ndofs_u[j,i] = ndofsu
					#ndofs_v[j,i] = ndofsv
					print spacetype,order,grid_size,ndofsh,ndofsu,l2_h,l2_u #,l2_v
					print '################### completed ############## '
		
					#print numerical_soln/actual_soln
					#print diff
					
		#REMOVE THIS ONCE BOUNDARY ELEMENTS FOR MGD ARE IMPLEMENTED!
		#if (xbc == 'neumann' or ybc == 'neumann'):
			#l2_h_errors = l2_h_errors[:-4,:]
			#l2_u_errors = l2_u_errors[:-4,:]
			##l2_v_errors = l2_v_errors[:-4,:]
			#ndofs_h = ndofs_h[:-4,:]
			#ndofs_u = ndofs_u[:-4,:]
			##ndofs_v = ndofs_v[:-4,:]

		
		orderlist1 = [5,4,3,2]
		for i in range(len(orderlist1)):
			revised_herrors =  l2_h_errors[i::len(orderlist1),:]
			revised_uerrors =  l2_u_errors[i::len(orderlist1),:]
			#revised_verrors =  l2_v_errors[i::len(orderlist1),:]
			revised_ndofs_h =  ndofs_h[i::len(orderlist1),:]
			revised_ndofs_u =  ndofs_u[i::len(orderlist1),:]
			#revised_ndofs_v =  ndofs_v[i::len(orderlist1),:]
			revised_names = names[i::len(orderlist1)]
			#This gets rid of even order elements for MGD, which don't exist!
			#Can get rid of xbc and ybc checks once boundary elements for MGD are implemented
			#if (orderlist1[i]%2 == 0) and xbc == 'periodic' and ybc == 'periodic':
				#revised_herrors = revised_herrors[:-1,:]
				#revised_uerrors = revised_uerrors[:-1,:]
				##revised_verrors = revised_verrors[:-1,:]
				#revised_ndofs_h = revised_ndofs_h[:-1,:]
				#revised_ndofs_u = revised_ndofs_u[:-1,:]
				##revised_ndofs_v = revised_ndofs_v[:-1,:]
				#revised_names = revised_names[:-1]
			print i,orderlist1[i],revised_names
			convergence_plot(resolution,revised_herrors,revised_names,'l2.h.' + xbc + '.' + ybc + '.' + str(orderlist1[i]) + '.2D')
			convergence_plot(resolution,revised_uerrors,revised_names,'l2.u.' + xbc + '.' + ybc + '.' + str(orderlist1[i]) + '.2D')
			#convergence_plot(resolution,revised_verrors,revised_names,'l2.v.' + xbc + '.' + ybc + '.' + str(orderlist1[i]) + '.2D')
			convergence_plot_ndofs(revised_ndofs_h,revised_herrors,revised_names,'l2.h.ndofs.' + xbc + '.' + ybc + '.' + str(orderlist1[i]) + '.2D')
			convergence_plot_ndofs(revised_ndofs_u,revised_uerrors,revised_names,'l2.u.ndofs.' + xbc + '.' + ybc + '.' + str(orderlist1[i]) + '.2D')
			#convergence_plot_ndofs(revised_ndofs_v,revised_verrors,revised_names,'l2.v.ndofs.' + xbc + '.' + ybc + '.' + str(orderlist1[i]) + '.2D')
	
