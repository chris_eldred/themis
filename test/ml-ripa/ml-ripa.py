#import sys, petsc4py
#initialize petsc
#petsc4py.init(sys.argv)
#from petsc4py import PETSc
from common import *
#from configuration import *
from mesh import *
from solver import *
from output import *
from plotting import *
from basis import *
#from form import *
from field import *
from functionspace import *
import h5py
from math import sqrt
from norms import *
#from swefuncs import *
from revisedform import *
from projector import *

class State():
	def __init__(self):
		pass

def compute_statistics(state,i):

	#calculate 0-forms
	state.peform.assemble()
	state.massform.assemble()
	state.keform.assemble()
	state.pvform.assemble()
	state.entropyform.assemble()
	state.entropyvarianceform1.assemble()
	state.entropyvarianceform2.assemble()
	state.entropyvarianceform3.assemble()

	if (i == 0):
		state.volumeform.assemble()
		parameters = []
		layerwiseh = state.massform.value / state.volumeform.value 
		layerwiseS = state.entropyform.value / state.volumeform.value 
		parameters.append(('layerwiseh',layerwiseh))
		parameters.append(('layerwiseS',layerwiseS))
		print layerwiseh
		print layerwiseS
		print layerwiseS/layerwiseh
		if state.coupled:
			bpefunc = Functional('ml-ripa.functional','zeroform_backgroundpe_coupled',0,geometrylist=['detJ','J'],parameters=parameters)
		else:
			bpefunc = Functional('ml-ripa.functional','zeroform_backgroundpe',0,geometrylist=['detJ','J'],layered=True,parameters=parameters)
		state.bpeform = ZeroForm(state.mesh,'bpe',bpefunc,state.quadrature)	
		state.bpeform.assemble()
		
	state.massstat[i,:] = state.massform.value
	state.pvstat[i,:] = state.pvform.value
	state.entropystat[i,:] = state.entropyform.value
	state.entropyvariancestat1[i,:] = state.entropyvarianceform1.value
	state.entropyvariancestat2[i,:] = state.entropyvarianceform2.value
	state.entropyvariancestat3[i,:] = state.entropyvarianceform3.value

	if state.coupled:
		state.kestat[i] = state.keform.value
		state.pestat[i] = state.peform.value
		state.apestat[i] = state.peform.value - state.bpeform.value
		state.energystat[i] = state.kestat[i] + state.pestat[i]
		state.availenergystat[i] = state.kestat[i] + state.apestat[i]
	else:
		state.kestat[i,:] = state.keform.value
		state.pestat[i,:] = state.peform.value
		state.apestat[i,:] = state.peform.value - state.bpeform.value
		state.energystat[i,:] = state.kestat[i] + state.pestat[i]
		state.availenergystat[i,:] = state.kestat[i] + state.apestat[i]

def plot_rhs(state,i):
	f = h5py.File("results.ml-ripa.h5", "r")

	divF = np.array(f['divF'+str(i)]) / state.dy /state.dx
	divFS = np.array(f['divFS'+str(i)]) / state.dy /state.dx
	curlu = np.array(f['curlu'+str(i)])
	Sproj = np.array(f['Sproj'+str(i)])
	qFtu = np.array(f['qFTu'+str(i)]) / state.dy
	qFtv = np.array(f['qFTv'+str(i)]) / state.dx
	gradBu = np.array(f['gradBu'+str(i)]) / state.dy
	gradBv = np.array(f['gradBv'+str(i)]) / state.dx
	gradSu = np.array(f['gradSu'+str(i)]) / state.dy
	gradSv = np.array(f['gradSv'+str(i)]) / state.dx
	hu = np.array(f['hu'+str(i)]) / state.dy
	hv = np.array(f['hv'+str(i)]) / state.dx

	xcell,ycell,zcell,xuedge,yuedge,zuedge,xvedge,yvedge,zvedge,xwedge,ywedge,zwedge,xvertex,yvertex,zvertex = state.mesh.get_plotting_coords()
	newshape = (state.nz,state.ny,state.nx)
	divF = np.reshape(divF,newshape)
	divFS = np.reshape(divFS,newshape)
	qFtu = np.reshape(qFtu,newshape)
	qFtv = np.reshape(qFtv,newshape)
	gradBu = np.reshape(gradBu,newshape)
	gradBv = np.reshape(gradBv,newshape)
	gradSu = np.reshape(gradSu,newshape)
	gradSv = np.reshape(gradSv,newshape)
	curlu = np.reshape(curlu,newshape)
	hu = np.reshape(hu,newshape)
	hv = np.reshape(hv,newshape)
	Sproj = np.reshape(Sproj,newshape)
	
	plotheader = str(i)

	
	dSfactors = np.linspace(1.0,0.7,state.nz)
	divFSadj = np.zeros(newshape)
	gradBuadj = np.zeros(newshape)
	gradBvadj = np.zeros(newshape)
	for k in range(state.nz):
		divFSadj[k,:,:] = divFS[k,:,:] / dSfactors[k] / state.g
		gradBuadj[k,:,:] = (gradBu[k,:,:] + gradSu[k,:,:]) / dSfactors[k] 
		gradBvadj[k,:,:] = (gradBv[k,:,:] + gradSv[k,:,:]) / dSfactors[k] 
		
	spatialplot_3d_slices_square(xcell,ycell,divF,'divF',plotheader,state.nz)
	spatialplot_3d_slices_square(xcell,ycell,divFS,'divFS',plotheader,state.nz)
	spatialplot_3d_slices_square(xcell,ycell,divFSadj,'divFSadj',plotheader,state.nz)
	spatialplot_3d_slices_square(xuedge,yuedge,qFtu,'qFTu',plotheader,state.nz)
	spatialplot_3d_slices_square(xvedge,yvedge,qFtv,'qFTv',plotheader,state.nz)
	spatialplot_3d_slices_square(xuedge,yuedge,gradBu,'gradBu',plotheader,state.nz)
	spatialplot_3d_slices_square(xvedge,yvedge,gradBv,'gradBv',plotheader,state.nz)
	spatialplot_3d_slices_square(xuedge,yuedge,gradBuadj,'gradBuadj',plotheader,state.nz)
	spatialplot_3d_slices_square(xvedge,yvedge,gradBvadj,'gradBvadj',plotheader,state.nz)
	spatialplot_3d_slices_square(xuedge,yuedge,gradSu,'gradSu',plotheader,state.nz)
	spatialplot_3d_slices_square(xvedge,yvedge,gradSv,'gradSv',plotheader,state.nz)
	spatialplot_3d_slices_square(xuedge,yuedge,hu,'hu',plotheader,state.nz)
	spatialplot_3d_slices_square(xvedge,yvedge,hv,'hv',plotheader,state.nz)
	spatialplot_3d_slices_square(xvertex,yvertex,curlu,'curlu',plotheader,state.nz)
	spatialplot_3d_slices_square(xvertex,yvertex,Sproj,'Sproj',plotheader,state.nz)
		
def plot_fields(state,i):
	
	#Load results
	f = h5py.File("results.ml-ripa.h5", "r")
	hvals = np.array(f['h'+str(i)]) 
	qvals = np.array(f['q'+str(i)])
	Svals = np.array(f['S'+str(i)])
	svals = np.array(f['s'+str(i)])
	uvals = np.array(f['u'+str(i)]) 
	vvals = np.array(f['v'+str(i)]) 
	Fuvals = np.array(f['Fu'+str(i)]) 
	Fvvals = np.array(f['Fv'+str(i)])
	f.close()

	state.h.vector.setArray(hvals)
	state.q.vector.setArray(qvals)
	state.S.vector.setArray(Svals)
	state.s.vector.setArray(svals)
	
	uindices = state.u.space.get_space(0).get_compindices(0)
	vindices = state.u.space.get_space(0).get_compindices(1)
	tempu = state.u.vector.getSubVector(uindices)
	tempv = state.u.vector.getSubVector(vindices)
	tempu.setArray(uvals)
	tempv.setArray(vvals)
	state.u.vector.restoreSubVector(uindices,tempu)
	state.u.vector.restoreSubVector(vindices,tempv)

	uindices = state.F.space.get_space(0).get_compindices(0)
	vindices = state.F.space.get_space(0).get_compindices(1)
	tempu = state.F.vector.getSubVector(uindices)
	tempv = state.F.vector.getSubVector(vindices)
	tempu.setArray(Fuvals)
	tempv.setArray(Fvvals)
	state.F.vector.restoreSubVector(uindices,tempu)
	state.F.vector.restoreSubVector(vindices,tempv)
	
	h = state.h.eval_at_quad_pts(0,state.quadrature).data
	q = state.q.eval_at_quad_pts(0,state.quadrature).data
	S = state.S.eval_at_quad_pts(0,state.quadrature).data
	s = state.s.eval_at_quad_pts(0,state.quadrature).data
	uvec = state.u.eval_at_quad_pts(0,state.quadrature).data
	u = uvec[:,:,:,:,:,:,0]
	v = uvec[:,:,:,:,:,:,1]
	Fuvec = state.F.eval_at_quad_pts(0,state.quadrature).data
	Fu = Fuvec[:,:,:,:,:,:,0]
	Fv = Fuvec[:,:,:,:,:,:,1]

	quads = state.mesh.get_plotting_quad_coords(state.quadrature)
		
	x = quads[:,:,:,:,:,:,0]
	y = quads[:,:,:,:,:,:,1]
	#data is stored as nz,ny,nx,qx,qy,qz
	
	newshape = (state.nz,state.ny,state.nx,state.quadrature.pts.shape[0],state.quadrature.pts.shape[1],state.quadrature.pts.shape[2])
	h = np.reshape(h,newshape)
	u = np.reshape(u,newshape)
	v = np.reshape(v,newshape)
	Fu = np.reshape(Fu,newshape)
	Fv = np.reshape(Fv,newshape)
	q = np.reshape(q,newshape)
	S = np.reshape(S,newshape)
	s = np.reshape(s,newshape)
	plotheader = str(i)

	#dSfactors = np.linspace(1.0,0.7,state.nz)
	#Sadj = S / state.g
	#for k in range(state.nz):
	#	Sadj[k,:,:] = Sadj[k,:,:] / dSfactors[k]
	spatialplot_3d_slices(x,y,h,'h',plotheader,state.nz)
	spatialplot_3d_slices(x,y,S,'S',plotheader,state.nz)
	#spatialplot_3d_slices_square(xcell,ycell,Sadj,'Sadj',plotheader,state.nz)
	spatialplot_3d_slices(x,y,u,'u',plotheader,state.nz)
	spatialplot_3d_slices(x,y,v,'v',plotheader,state.nz)
	spatialplot_3d_slices(x,y,Fu,'Fu',plotheader,state.nz)
	spatialplot_3d_slices(x,y,Fv,'Fv',plotheader,state.nz)
	spatialplot_3d_slices(x,y,q,'q',plotheader,state.nz)
	spatialplot_3d_slices(x,y,s,'sl',plotheader,state.nz)


def plot_stats(state):
	x = xrange(state.massstat.shape[0])
	for k in range(state.nz):
		spatialplot_1d(x,(state.massstat[:,k]-state.massstat[0,k])/state.massstat[0,k]*100.0,'mass.' + str(k))
		spatialplot_1d(x,(state.pvstat[:,k]-state.pvstat[0,k])/state.pvstat[0,k]*100.0,'pv.' + str(k))
		spatialplot_1d(x,(state.entropystat[:,k]-state.entropystat[0,k])/state.entropystat[0,k]*100.0,'entropy.' + str(k))
		spatialplot_1d(x,(state.entropyvariancestat1[:,k]-state.entropyvariancestat1[0,k])/state.entropyvariancestat1[0,k]*100.0,'entropyvariance1.' + str(k))
		spatialplot_1d(x,(state.entropyvariancestat2[:,k]-state.entropyvariancestat2[0,k])/state.entropyvariancestat2[0,k]*100.0,'entropyvariance2.' + str(k))
		spatialplot_1d(x,(state.entropyvariancestat3[:,k]-state.entropyvariancestat3[0,k])/state.entropyvariancestat3[0,k]*100.0,'entropyvariance3.' + str(k))
		spatialplot_1d(x,state.entropystat[:,k],'entropy_time.' + str(k))
		spatialplot_1d(x,state.entropyvariancestat1[:,k],'entropyvariance1_time.' + str(k))
		spatialplot_1d(x,state.entropyvariancestat2[:,k],'entropyvariance2_time.' + str(k))
		spatialplot_1d(x,state.entropyvariancestat3[:,k],'entropyvariance3_time.' + str(k))
		spatialplot_1d(x,state.massstat[:,k],'mass_time.' + str(k))
		spatialplot_1d(x,state.pvstat[:,k],'pv_time.' + str(k))

	if state.coupled:
		spatialplot_1d(x,(state.kestat-state.kestat[0])/state.kestat[0]*100.0,'ke')
		spatialplot_1d(x,(state.pestat-state.pestat[0])/state.pestat[0]*100.0,'pe')
		spatialplot_1d(x,(state.apestat-state.apestat[0])/state.apestat[0]*100.0,'ape')
		spatialplot_1d(x,(state.energystat-state.energystat[0])/state.energystat[0]*100.0,'te')
		spatialplot_1d(x,(state.availenergystat-state.availenergystat[0])/state.availenergystat[0]*100.0,'ate')
		spatialplot_1d(x,state.energystat,'te_time')
		spatialplot_1d(x,state.availenergystat,'te_time')
		spatialplot_1d(x,state.pestat,'pe_time')
		spatialplot_1d(x,state.apestat,'ape_time')
		spatialplot_1d(x,state.kestat,'ke_time')
		spatialplot_1d_multi(x,[state.energystat,state.kestat,state.pestat],['te','ke','pe'],'energy')		
		spatialplot_1d_multi(x,[state.availenergystat,state.kestat,state.apestat],['ate','ke','ape'],'availenergy')		
	else:
		for k in range(state.nz):
			spatialplot_1d(x,(state.kestat[:,k]-state.kestat[0,k])/state.kestat[0,k]*100.0,'ke.' + str(k))
			spatialplot_1d(x,(state.pestat[:,k]-state.pestat[0,k])/state.pestat[0,k]*100.0,'pe.' + str(k))
			spatialplot_1d(x,(state.apestat[:,k]-state.apestat[0,k])/state.apestat[0,k]*100.0,'ape.' + str(k))
			spatialplot_1d(x,(state.energystat[:,k]-state.energystat[0,k])/state.energystat[0,k]*100.0,'te.' + str(k))
			spatialplot_1d(x,(state.availenergystat[:,k]-state.availenergystat[0,k])/state.availenergystat[0,k]*100.0,'ate.' + str(k))
			spatialplot_1d(x,state.energystat[:,k],'te_time.' + str(k))
			spatialplot_1d(x,state.availenergystat[:,k],'ate_time.' + str(k))
			spatialplot_1d(x,state.pestat[:,k],'pe_time.' + str(k))
			spatialplot_1d(x,state.apestat[:,k],'ape_time.' + str(k))
			spatialplot_1d(x,state.kestat[:,k],'ke_time.' + str(k))
			spatialplot_1d_multi(x,[state.energystat[:,k],state.kestat[:,k],state.pestat[:,k]],['te','ke','pe'],'energy.' + str(k))
			spatialplot_1d_multi(x,[state.availenergystat[:,k],state.kestat[:,k],state.apestat[:,k]],['ate','ke','ape'],'availenergy.' + str(k))
			
def create_projectors(state):
	if not ((state.spacetype == 'MGD') or (state.elem_order == 1)):
		state.h1_proj = Projector(state.h1,solver=state.solver,pc=state.pc)
		state.l2_proj = Projector(state.l2,solver=state.solver,pc=state.pc)
		state.hdiv_proj = Projector(state.hdiv,solver=state.solver,pc=state.pc)

def output_fields(state,i,view):
	#EVENTUALLY THIS NEEDS TO SET HDF5 TIMESTEP...
	#for field in [state.h,state.q,state.F,state.u]:
	#	field.name = field.name + str(i)  #this is a hack
	#	for j in xrange(len(field.names)):
	#		field.names[j] = field.names[j] + str(i) #this is a hack
	state.h.output(view,ts=i)
	state.u.output(view,ts=i)
	state.q.output(view,ts=i)
	state.S.output(view,ts=i)
	state.s.output(view,ts=i)
	state.F.output(view,ts=i)
	
	#state.htemp.output(view,ts=i)
	#state.utemp.output(view,ts=i)

	#if not ((state.spacetype == 'MGD') or (state.elem_order == 1)):
		
		##project fields into plotting spaces
		#proj_h = state.l2_proj.project(state.h,kspinfo=state.kspinfo)
		#proj_u = state.hdiv_proj.project(state.u,kspinfo=state.kspinfo)
		#proj_F = state.hdiv_proj.project(state.F,kspinfo=state.kspinfo)
		#proj_q = state.h1_proj.project(state.q,kspinfo=state.kspinfo)
		#proj_s = state.h1_proj.project(state.s,kspinfo=state.kspinfo)
		#proj_S = state.l2_proj.project(state.S,kspinfo=state.kspinfo)
		
		#proj_q.output(view,ts=i)
		#proj_s.output(view,ts=i)
		#proj_S.output(view,ts=i)
		#proj_h.output(view,ts=i)
		#proj_F.output(view,ts=i)
		#proj_u.output(view,ts=i)

	#ViewHDF5.incrementTimestep(1)	

def diagnose(state):

	state.qmat.assemble(show_matrix=state.show_matrix)
	state.hu.assemble(show_vector=state.show_vector)
	state.curlu.assemble(show_vector=state.show_vector)
	state.Sproj.assemble(show_vector=state.show_vector)
	
	state.qsys = LinearSystem(state.qmat,state.h1,state.solver,state.pc,bcs=None,rtol=1e-20)
	state.qsys.solve(state.curlu,state.q,kspinfo=state.kspinfo)
	state.qsys.solve(state.Sproj,state.s,kspinfo=state.kspinfo)
	state.hdivmass_sys.solve(state.hu,state.F,kspinfo=state.kspinfo)

def output_rhs(state,i,view):
	state.divF.output(view,ts=i)
	state.divFS.output(view,ts=i)
	state.qFt.output(view,ts=i)
	state.gradB.output(view,ts=i)
	state.gradS.output(view,ts=i)
	state.hu.output(view,ts=i)
	state.curlu.output(view,ts=i)
	state.Sproj.output(view,ts=i)

def rhs(state,i):
	
	state.qFt.assemble(show_vector=state.show_vector)
	state.gradB.assemble(show_vector=state.show_vector)
	state.gradS.assemble(show_vector=state.show_vector)
	state.urhs.vector = -state.qFt.vector + state.gradB.vector + state.gradS.vector 
	state.hdivmass_sys.solve(state.urhs,state.ku_is[i],kspinfo=state.kspinfo)

	state.divF.assemble(show_vector=state.show_vector)
	state.hrhs.vector = -state.divF.vector
	state.l2mass_sys.solve(state.hrhs,state.kh_is[i],kspinfo=state.kspinfo)

	state.divFS.assemble(show_vector=state.show_vector)
	state.Srhs.vector = -state.divFS.vector
	state.l2mass_sys.solve(state.Srhs,state.kS_is[i],kspinfo=state.kspinfo)
	
def create_timestep_aux_rk(state):
	state.htemp = Field(('htemp',['htemp',]),state.l2)
	state.Stemp = Field(('Stemp',['Stemp',]),state.l2)
	state.utemp = Field(('utemp',['utemp','vtemp']),state.hdiv)
	urhsfunc = Functional('ml-ripa.functional','zeroproj',1,geometrylist=['detJ','J']) #uinit zeroproj hinit
	state.urhs = OneForm(state.mesh,['urhs',['urhs','vrhs']],[urhsfunc,],state.hdiv,state.quadrature)
	hrhsfunc = Functional('ml-ripa.functional','zeroproj',1,geometrylist=['detJ','J']) #uinit zeroproj hinit
	state.hrhs = OneForm(state.mesh,['hrhs',['hrhs',]],[hrhsfunc,],state.l2,state.quadrature)
	Srhsfunc = Functional('ml-ripa.functional','zeroproj',1,geometrylist=['detJ','J']) #uinit zeroproj hinit
	state.Srhs = OneForm(state.mesh,['Srhs',['Srhs',]],[Srhsfunc,],state.l2,state.quadrature)
	
	state.ku_is = []
	state.kh_is = []
	state.kS_is = []
	
	for i in xrange(4): #THIS SHOULD REALLY CHANGE DEPENDING ON TABLEAU
		state.kh_is.append(Field(('kh_'+str(i),['kh_'+str(i),]),state.l2))
		state.kS_is.append(Field(('kS_'+str(i),['kS_'+str(i),]),state.l2))
		state.ku_is.append(Field(('ku_'+str(i),['ku_'+str(i),'kv_'+str(i)]),state.hdiv))

	#create ku_i's, kh_i's and htemp/utemp, as needed by the provided tableau

def take_explicit_rk_step(state):
	khvecs = []
	kSvecs = []
	kuvecs = []
	rhs(state,0)
	khvecs.append(state.kh_is[0].vector)
	kSvecs.append(state.kS_is[0].vector)
	kuvecs.append(state.ku_is[0].vector)
	beta = np.array([0.5,0.5,1.]) * state.dt #THIS SHOULD REALLY CHANGE DEPENDING ON TABLEAU
	for i in xrange(3):
		state.htemp.vector = state.h.vector + state.kh_is[i].vector * beta[i]
		state.Stemp.vector = state.S.vector + state.kS_is[i].vector * beta[i]
		state.utemp.vector = state.u.vector + state.ku_is[i].vector * beta[i]
		diagnose(state)
		rhs(state,i+1)
		khvecs.append(state.kh_is[i+1].vector)
		kSvecs.append(state.kS_is[i+1].vector)
		kuvecs.append(state.ku_is[i+1].vector)
	alphas = np.array([1./6.,1./3.,1./3.,1./6.]) * state.dt #THIS SHOULD REALLY CHANGE DEPENDING ON TABLEAU
	state.h.vector.maxpy(alphas,khvecs)
	state.S.vector.maxpy(alphas,kSvecs)
	state.u.vector.maxpy(alphas,kuvecs)
	state.htemp.vector = state.h.vector.copy()
	state.Stemp.vector = state.S.vector.copy()
	state.utemp.vector = state.u.vector.copy()
	diagnose(state)

	#using rhs and diagnose, take an arbitrary explicit rk step using the provided tableau
	
def initial_condition(state):

	hinit_rhs = Coefficient('hinit_rhs','scalar',np.zeros(state.mesh.geometry.detJ.shape))
	Sinit_rhs = Coefficient('Sinit_rhs','scalar',np.zeros(state.mesh.geometry.detJ.shape))
	uinit_rhs = Coefficient('uinit_rhs','vector',np.zeros(state.mesh.geometry.quadcoords.shape))
	
	hfunc = Functional('ml-ripa.functional','hinitcoeff',1,geometrylist=['detJ',],coefficientlist=[hinit_rhs,])
	Sfunc = Functional('ml-ripa.functional','Sinitcoeff',1,geometrylist=['detJ',],coefficientlist=[Sinit_rhs,])
	ufunc = Functional('ml-ripa.functional','uinitcoeff',1,geometrylist=['detJ','J'],coefficientlist=[uinit_rhs,])
	
	x = state.mesh.geometry.quadcoords[:,:,:,:,:,:,0]
	y = state.mesh.geometry.quadcoords[:,:,:,:,:,:,1]
	z = state.mesh.geometry.quadcoords[:,:,:,:,:,:,2]
	
	####### This is single vortex #####
	if state.initcond == 'singlevortex':
		dh = 75.0 
		sigmax = 3./40.*state.Lx
		sigmay = 3./40.*state.Ly
		xc = state.Lx/2.
		yc = state.Ly/2.
		
		xprime = state.Lx / (np.pi * sigmax) * np.sin( np.pi / state.Lx * (x- xc))
		yprime = state.Ly / (np.pi * sigmay) * np.sin( np.pi / state.Ly * (y- yc))
		xprimeprime = state.Lx / (2.0 * np.pi * sigmax) * np.sin( 2 * np.pi / state.Lx * (x - xc))
		yprimeprime = state.Ly / (2.0 * np.pi * sigmay) * np.sin( 2 * np.pi / state.Ly * (y - yc))
		s = 1.0 + 0.1 * np.sin(2. * np.pi / state.Lx * (x - xc))
		h = state.H0 - dh * np.exp(-0.5 * (xprime * xprime + yprime * yprime))
		hinit_rhs.data = h
		Sinit_rhs.data = state.g * s * h
		uinit_rhs.data[:,:,:,:,:,:,0] = - state.g * dh / state.f / sigmay * 2. * yprimeprime * np.exp(-0.5*(xprime * xprime + yprime * yprime))
		uinit_rhs.data[:,:,:,:,:,:,1] =   state.g * dh / state.f / sigmax * 2. * xprimeprime * np.exp(-0.5*(xprime * xprime + yprime * yprime))

	##########

	####### This is double vortex #####
	if state.initcond == 'doublevortex':
		dh = 75.0
		o = 0.1 
		sigmax = 3./40.*state.Lx
		sigmay = 3./40.*state.Ly
		xc1 = (0.5-o) * state.Lx
		yc1 = (0.5-o) * state.Ly
		xc2 = (0.5+o) * state.Lx
		yc2 = (0.5+o) * state.Ly		
		xc = state.Lx/2.
		yc = state.Ly/2.
		
		xprime1 = state.Lx / (np.pi * sigmax) * np.sin( np.pi / state.Lx * (x- xc1))
		yprime1 = state.Ly / (np.pi * sigmay) * np.sin( np.pi / state.Ly * (y- yc1))
		xprime2 = state.Lx / (np.pi * sigmax) * np.sin( np.pi / state.Lx * (x- xc2))
		yprime2 = state.Ly / (np.pi * sigmay) * np.sin( np.pi / state.Ly * (y- yc2))
		xprimeprime1 = state.Lx / (2.0 * np.pi * sigmax) * np.sin( 2 * np.pi / state.Lx * (x - xc1))
		yprimeprime1 = state.Ly / (2.0 * np.pi * sigmay) * np.sin( 2 * np.pi / state.Ly * (y - yc1))
		xprimeprime2 = state.Lx / (2.0 * np.pi * sigmax) * np.sin( 2 * np.pi / state.Lx * (x - xc2))
		yprimeprime2 = state.Ly / (2.0 * np.pi * sigmay) * np.sin( 2 * np.pi / state.Ly * (y - yc2))
		h = state.H0 - dh * (np.exp(-0.5 * (xprime1 * xprime1 + yprime1 * yprime1)) + np.exp(-0.5 * (xprime2 * xprime2 + yprime2 * yprime2)) - 4. * np.pi * sigmax * sigmay / state.Lx / state.Ly)
		s = 1.0 + 0.1 * np.sin(2. * np.pi / state.Lx * (x - xc))

		hinit_rhs.data = h		
		Sinit_rhs.data = state.g * s * h
		uinit_rhs.data[:,:,:,:,:,:,0] = - state.g * dh / state.f / sigmay * (yprimeprime1 * np.exp(-0.5*(xprime1 * xprime1 + yprime1 * yprime1)) + yprimeprime2 * np.exp(-0.5*(xprime2 * xprime2 + yprime2 * yprime2)))
		uinit_rhs.data[:,:,:,:,:,:,1] =   state.g * dh / state.f / sigmax * (xprimeprime1 * np.exp(-0.5*(xprime1 * xprime1 + yprime1 * yprime1)) + xprimeprime2 * np.exp(-0.5*(xprime2 * xprime2 + yprime2 * yprime2)))

	##########
	
	####### This is gravity wave coupled #####
	if state.initcond == 'gravitywave_coupled':
		dh = 50.0 
		sigmax = 3./40.*state.Lx
		sigmay = 3./40.*state.Ly
		xc = state.Lx/2.
		yc = state.Ly/2.
		dhfactors = np.zeros((state.nz,))
		dhfactors[0] = -1.0
		dhfactors[1] = 1.0
		dSfactors = np.linspace(1.0,0.7,state.nz)
			
		for k in range(state.nz):
			xadj = x[k,:,:,:,:,:]
			yadj = y[k,:,:,:,:,:]
			xprime = state.Lx / (np.pi * sigmax) * np.sin( np.pi / state.Lx * (xadj- xc))
			yprime = state.Ly / (np.pi * sigmay) * np.sin( np.pi / state.Ly * (yadj- yc))
			xprimeprime = state.Lx / (2.0 * np.pi * sigmax) * np.sin( 2 * np.pi / state.Lx * (xadj - xc))
			yprimeprime = state.Ly / (2.0 * np.pi * sigmay) * np.sin( 2 * np.pi / state.Ly * (yadj - yc))
			h = state.H0 - dhfactors[k] * dh * np.exp(-0.5 * (xprime * xprime + yprime * yprime))
			s = dSfactors[k] * (1.0 + 0.1 * np.sin(2. * np.pi / state.Lx * (xadj - xc)))
			hinit_rhs.data[k,:,:,:,:,:] = h
			Sinit_rhs.data[k,:,:,:,:,:] = state.g * s * h
			

	##########
	
	state.hinit = OneForm(state.mesh,['hinit',['hinit',]],[hfunc,],state.l2,state.quadrature)
	state.Sinit = OneForm(state.mesh,['Sinit',['Sinit',]],[Sfunc,],state.l2,state.quadrature)
	state.uinit = OneForm(state.mesh,['uinit',['uinit',]],[ufunc,],state.hdiv,state.quadrature)

	state.hinit.assemble(show_vector=state.show_vector)
	state.Sinit.assemble(show_vector=state.show_vector)
	state.uinit.assemble(show_vector=state.show_vector)
	state.hdivmass_sys.solve(state.uinit,state.u,kspinfo=state.kspinfo)
	state.l2mass_sys.solve(state.hinit,state.h,kspinfo=state.kspinfo)
	state.l2mass_sys.solve(state.Sinit,state.S,kspinfo=state.kspinfo)
	#set initial values for u and h
	#do this by assembling 1-forms for u and h, and then solving mass matrix system
	#ie projection
	#for an interpolatory basis, could just interpolate to nodal values
	#but this procedure is more general
	state.htemp.vector = state.h.vector.copy()
	state.Stemp.vector = state.S.vector.copy()
	state.utemp.vector = state.u.vector.copy()

def create_mesh_and_spaces(state):
	#create elements
	cgx,cgy,cgz,dgx,dgy,dgz = get_cgdg(state.spacetype,2,xbc=state.xbc,ybc=state.ybc,xorder=state.elem_order,yorder=state.elem_order)
	l2elem = TensorProductElement([dgx,dgy])
	vecxelem = TensorProductElement([cgx,dgy]) 
	vecyelem = TensorProductElement([dgx,cgy])
	h1elem = TensorProductElement([cgx,cgy])

	#create mesh
	geom = UniformVariableBox(l2elem.get_ref_cell(),state.quadrature,state.dx,state.dy,state.dz)
	state.mesh = Mesh(3,geom,[state.nx,state.ny,state.nz],[state.xbc,state.ybc,state.zbc])
	
	#Create  spaces
	state.h1 = FunctionSpace(state.mesh,h1elem,'H1')
	state.hdiv = VectorFunctionSpace(state.mesh,[vecxelem,vecyelem],'Hdiv')
	state.l2 = FunctionSpace(state.mesh,l2elem,'L2')

def destroy(state):
	#fields
	state.h.destroy()
	state.S.destroy()
	state.u.destroy()
	state.q.destroy()
	state.s.destroy()
	state.F.destroy()
	state.htemp.destroy()
	state.Stemp.destroy()
	state.utemp.destroy()	
	for temp in state.ku_is:
		temp.destroy()
	for temp in state.kh_is:
		temp.destroy()
	for temp in state.kS_is:
		temp.destroy()
	state.urhs.destroy()
	state.hrhs.destroy()
	state.Srhs.destroy()
	
	#forms
	state.l2mass.destroy()
	state.hdivmass.destroy()
	state.qmat.destroy()
	
	state.hu.destroy()
	state.curlu.destroy()
	state.Sproj.destroy()
	state.qFt.destroy()
	state.gradB.destroy()
	state.gradS.destroy()
	state.divF.destroy()
	state.divFS.destroy()
	state.hinit.destroy()
	state.Sinit.destroy()
	state.uinit.destroy()
	
	state.peform.destroy()
	state.massform.destroy()
	state.keform.destroy()
	state.pvform.destroy()
	state.entropyform.destroy()
	state.volumeform.destroy()
	state.bpeform.destroy()
	state.entropyvarianceform1.destroy()
	state.entropyvarianceform2.destroy()
	state.entropyvarianceform3.destroy()
	
	#linear systems
	state.l2mass_sys.destroy()
	state.hdivmass_sys.destroy()
	state.qsys.destroy()

	#function spaces
	state.h1.destroy()
	state.l2.destroy()
	state.hdiv.destroy()
	
	#mesh
	state.mesh.destroy()
	
def create_2_forms(state):
	l2massfunc = Functional('ml-ripa.functional','l2mass',2,geometrylist=['detJ'])
	hdivmassfunc = Functional('ml-ripa.functional','hdivmass',2,geometrylist=['detJ','JTJ'])
	qfunc = Functional('ml-ripa.functional','qfunc',2,geometrylist=['detJ'],fields=(state.htemp,),fsi=(0,))
	state.l2mass = TwoForm(state.mesh,('l2mass',('l2mass',)),[[l2massfunc,],],state.l2,state.l2,state.quadrature)
	state.hdivmass = TwoForm(state.mesh,('hdivmass',('uumass','uvmass','vumass','vvmass')),[[hdivmassfunc,],],state.hdiv,state.hdiv,state.quadrature)
	state.qmat = TwoForm(state.mesh,('qmat',('qmat',)),[[qfunc,],],state.h1,state.h1,state.quadrature)
	
	#assemble mass matrices
	state.l2mass.assemble(show_matrix=state.show_matrix)
	state.hdivmass.assemble(show_matrix=state.show_matrix)

def create_1_forms(state):
	if state.coupled:
		gradBfunc = Functional('ml-ripa.functional','gradB_coupled',1,geometrylist=['detJ','J'],fields=(state.utemp,state.Stemp),fsi=(0,0)) #uinit zeroproj hinit
		gradSfunc = Functional('ml-ripa.functional','gradS_coupled',1,geometrylist=['detJ','J'],fields=(state.htemp,state.s),fsi=(0,0)) #uinit zeroproj hinit
	else:
		gradBfunc = Functional('ml-ripa.functional','gradB',1,geometrylist=['detJ','J'],fields=(state.utemp,state.Stemp),fsi=(0,0)) #uinit zeroproj hinit
		gradSfunc = Functional('ml-ripa.functional','gradS',1,geometrylist=['detJ','J'],fields=(state.htemp,state.s),fsi=(0,0)) #uinit zeroproj hinit
	
	hufunc = Functional('ml-ripa.functional','hu',1,geometrylist=['detJ','J'],fields=(state.htemp,state.utemp),fsi=(0,0)) #uinit zeroproj hinit
	curlufunc = Functional('ml-ripa.functional','curlu',1,geometrylist=['detJ','J','JTinv'],parameters=[('f',state.f)],fields=(state.utemp,),fsi=(0,)) #uinit zeroproj hinit
	Sprojfunc = Functional('ml-ripa.functional','Sproj',1,geometrylist=['detJ',],fields=(state.Stemp,),fsi=(0,)) #uinit zeroproj hinit
	qFTfunc = Functional('ml-ripa.functional','qFT',1,geometrylist=['detJ','J','rotJ'],fields=(state.q,state.F),fsi=(0,0)) #uinit zeroproj hinit
	divFfunc = Functional('ml-ripa.functional','divF',1,geometrylist=['detJ','J'],fields=(state.F,),fsi=(0,)) #uinit zeroproj hinit
	divFSfunc = Functional('ml-ripa.functional','divFS',1,geometrylist=['detJ','J'],fields=(state.F,state.s),fsi=(0,0)) #uinit zeroproj hinit

	state.hu = OneForm(state.mesh,['hu',['hu','hv']],[hufunc,],state.hdiv,state.quadrature)
	state.curlu = OneForm(state.mesh,['curlu',['curlu',]],[curlufunc,],state.h1,state.quadrature)
	state.qFt = OneForm(state.mesh,['qFT',['qFTu','qFTv']],[qFTfunc,],state.hdiv,state.quadrature)
	state.gradB = OneForm(state.mesh,['gradB',['gradBu','gradBv']],[gradBfunc,],state.hdiv,state.quadrature)
	state.gradS = OneForm(state.mesh,['gradS',['gradSu','gradSv']],[gradSfunc,],state.hdiv,state.quadrature)
	state.divF = OneForm(state.mesh,['divF',['divF',]],[divFfunc,],state.l2,state.quadrature)
	state.divFS = OneForm(state.mesh,['divFS',['divFS',]],[divFSfunc,],state.l2,state.quadrature)
	state.Sproj = OneForm(state.mesh,['Sproj',['Sproj',]],[Sprojfunc,],state.h1,state.quadrature)

def create_0_forms(state):
	if state.coupled:
		pefunc = Functional('ml-ripa.functional','zeroform_pe_coupled',0,geometrylist=['detJ',],fields=(state.htemp,state.Stemp),fsi=(0,0))
		kefunc = Functional('ml-ripa.functional','zeroform_ke',0,geometrylist=['detJ','J'],fields=(state.utemp,state.htemp),fsi=(0,0)) #note field order- u, h!
	else:
		pefunc = Functional('ml-ripa.functional','zeroform_pe',0,geometrylist=['detJ',],fields=(state.htemp,state.Stemp),fsi=(0,0),layered=True)
		kefunc = Functional('ml-ripa.functional','zeroform_ke',0,geometrylist=['detJ','J'],fields=(state.utemp,state.htemp),fsi=(0,0),layered=True) #note field order- u, h!
		
	volumefunc = Functional('ml-ripa.functional','zeroform_volume',0,geometrylist=['detJ','J'],layered=True)		
	massfunc = Functional('ml-ripa.functional','zeroform_mass',0,geometrylist=['detJ',],fields=(state.htemp,),fsi=(0,),layered=True)
	pvfunc = Functional('ml-ripa.functional','zeroform_pv',0,geometrylist=['detJ',],fields=(state.htemp,state.q),fsi=(0,0),layered=True)
	entropyfunc = Functional('ml-ripa.functional','zeroform_entropy',0,geometrylist=['detJ',],fields=(state.Stemp,),fsi=(0,),layered=True)
	entropyvariancefunc1 = Functional('ml-ripa.functional','zeroform_entropyvariance1',0,geometrylist=['detJ',],fields=(state.Stemp,state.htemp),fsi=(0,0),layered=True)
	entropyvariancefunc2 = Functional('ml-ripa.functional','zeroform_entropyvariance2',0,geometrylist=['detJ',],fields=(state.s,state.htemp),fsi=(0,0),layered=True)
	entropyvariancefunc3 = Functional('ml-ripa.functional','zeroform_entropyvariance3',0,geometrylist=['detJ',],fields=(state.Stemp,state.s),fsi=(0,0),layered=True)

	state.volumeform = ZeroForm(state.mesh,'volume',volumefunc,state.quadrature)	
	state.peform = ZeroForm(state.mesh,'pe',pefunc,state.quadrature)
	state.massform = ZeroForm(state.mesh,'mass',massfunc,state.quadrature)
	state.keform = ZeroForm(state.mesh,'ke',kefunc,state.quadrature)
	state.pvform = ZeroForm(state.mesh,'pv',pvfunc,state.quadrature)
	state.entropyform = ZeroForm(state.mesh,'entropy',entropyfunc,state.quadrature)
	state.entropyvarianceform1 = ZeroForm(state.mesh,'entropyvariance1',entropyvariancefunc1,state.quadrature)
	state.entropyvarianceform2 = ZeroForm(state.mesh,'entropyvariance2',entropyvariancefunc2,state.quadrature)
	state.entropyvarianceform3 = ZeroForm(state.mesh,'entropyvariance3',entropyvariancefunc3,state.quadrature)
		
	state.massstat = np.zeros((N/nstat + 1,state.nz))
	if state.coupled:
		state.kestat = np.zeros((N/nstat + 1))
		state.pestat = np.zeros((N/nstat + 1))
		state.apestat = np.zeros((N/nstat + 1))
		state.energystat = np.zeros((N/nstat + 1))
		state.availenergystat = np.zeros((N/nstat + 1))
	else:
		state.kestat = np.zeros((N/nstat + 1,state.nz))
		state.pestat = np.zeros((N/nstat + 1,state.nz))
		state.apestat = np.zeros((N/nstat + 1,state.nz))
		state.energystat = np.zeros((N/nstat + 1,state.nz))
		state.availenergystat = np.zeros((N/nstat + 1,state.nz))
	state.pvstat = np.zeros((N/nstat + 1,state.nz))
	state.entropystat = np.zeros((N/nstat + 1,state.nz))
	state.entropyvariancestat1 = np.zeros((N/nstat + 1,state.nz))
	state.entropyvariancestat2 = np.zeros((N/nstat + 1,state.nz))
	state.entropyvariancestat3 = np.zeros((N/nstat + 1,state.nz))

def create_fields(state):
	state.h = Field(('h',['h',]),state.l2)
	state.S = Field(('S',['S',]),state.l2)
	state.s = Field(('s',['s',]),state.h1)
	state.u = Field(('u',['u','v']),state.hdiv)
	state.q = Field(('q',['q',]),state.h1)
	state.F = Field(('F',['Fu','Fv']),state.hdiv)

def create_solvers(state):
	state.l2mass_sys = LinearSystem(state.l2mass,state.l2,state.solver,state.pc,bcs=None,rtol=1e-20)
	state.hdivmass_sys = LinearSystem(state.hdivmass,state.hdiv,state.solver,state.pc,bcs=None,rtol=1e-20)
	#state.qsys = LinearSystem(state.qmat,state.h1,solver,pc,bcs=None)

###############################################################################################
#Load Configuration
state = State()
rank = PETSc.COMM_WORLD.Get_rank() 

OptDB = PETSc.Options()
state.spacetype = OptDB.getString('spacetype', 'FEEC')
state.elem_order = OptDB.getInt('elemorder', 1)
state.nx = OptDB.getInt('nx', 32)
state.ny = OptDB.getInt('ny', 32)
state.nz = OptDB.getInt('nz', 2)
state.simname = OptDB.getString('simname', 'test')
state.initcond = OptDB.getString('initcond', 'doublevortex') #singlevortex doublevortex gravitywave_coupled
state.coupled = OptDB.getBool('coupled', False)

state.diagnostics = OptDB.getBool('diagnostics', False)
state.kspinfo = OptDB.getBool('kspinfo', False)
state.show_matrix = OptDB.getBool('showMatrix', False)
state.show_vector = OptDB.getBool('showVector', False)

state.solver = OptDB.getString('iterative','cg') #fgmres
if state.spacetype == 'MGD':
	state.pc = OptDB.getString('precon','jacobi')
else:
	state.pc = OptDB.getString('precon','ilu')
	
quadchoice = 'gauss'
quadorder = 1 + state.elem_order
quad = get_quadrature(quadchoice)
#state.quadrature = TensorProductQuadrature([quad,quad,EmptyQuad],[quadorder,quadorder,1])
state.quadrature = TensorProductQuadrature([quad,quad],[quadorder,quadorder])

output_matrices = OptDB.getBool('outputMatrix', False)
output_fspaces = OptDB.getBool('outputSpaces', False)
output_ksp = OptDB.getBool('outputKSP', False)
output_mesh = OptDB.getBool('outputMesh', False)

state.f = 0.00006147 #0.00006147 #0.00006147 0.0001
state.g = 9.81
state.Ly = 5000.0 * 1000.0
state.Lx = 5000.0 * 1000.0
state.C = 0.1 / state.elem_order  #ADJUST THIS BASED ON ELEM ORDER
state.H0 = 750.0

#if state.coupled:
	#rholist = OptDB.getString('rholist','1.0,0.9,0.8,0.7')
	#rholist = rholist.split(',')
	#state.rholist = np.array([float(x) for x in rholist])
	#state.hcoeff = np.ones((state.nz,state.nz))
	#for i in range(state.nz):
		#for j in range(i+1,state.nz):
			#state.hcoeff[i,j] = state.rholist[j]/state.rholist[i]
			##print i,j,state.rholist[i],state.rholist[j],state.hcoeff[i,j]
	#print state.hcoeff
	
N = 150
nout = 25
nstat = 1
state.dx = OptDB.getScalar('dx',state.Lx/state.nx)
state.dy = OptDB.getScalar('dy',state.Ly/state.ny) #100000
state.dz = 2. #OptDB.getScalar('dz',20000./state.nz)
state.dt = min(state.dx,state.dy) * state.C / sqrt(state.g * state.H0)
print state.dx/1000.0,state.dy/1000.0,state.dz/1000.0,state.dt
state.xbc = 'periodic'
state.ybc = 'periodic'
state.zbc = 'dirichlet'

#create mesh and function spaces (l2,hdiv,h1)
create_mesh_and_spaces(state)
if output_fspaces:
	state.h1.output()
	state.l2.output()
	state.hdiv.output()

#create fields to hold solutions at various time steps
create_fields(state)

#create auxiliary fields for time stepping auxiliary variables
create_timestep_aux_rk(state)

#create forms
create_2_forms(state)
if output_matrices:
	state.l2mass.output()
	state.hdivmass.output()
create_1_forms(state)
create_0_forms(state)

#create solvers
create_solvers(state)
if output_ksp:
	state.l2mass_sys.output()
	state.hdivmass_sys.output()
	state.qsys.output()
create_projectors(state)

#initial condition
initial_condition(state)
diagnose(state)
	
#create output and output initial condition
ViewHDF5 = PETSc.ViewerHDF5().create('results.ml-ripa.h5', mode=PETSc.Viewer.Mode.WRITE,comm= PETSc.COMM_WORLD)
#ViewHDF5.setTimestep(0)
if state.diagnostics:
	output_rhs(state,0,ViewHDF5)
state.mesh.output(ViewHDF5)
output_fields(state,0,ViewHDF5)
plot_fields(state,0)

#compute initial condition statistics
compute_statistics(state,0)

for i in xrange(1,N+1):
	print 'step',i
	#take an explicit rk step
	take_explicit_rk_step(state)

	if state.diagnostics:
		output_rhs(state,i,ViewHDF5)
		
	if (i%nout == 0):
		output_fields(state,i/nout,ViewHDF5)
		
	if (i%nstat == 0):
		compute_statistics(state,i/nstat)
		
#close output
ViewHDF5.destroy()

#do plotting if needed
if (rank == 0):
	##print massstat
	##print energystat
	plot_stats(state)
	for i in xrange(0,N+1):
		if state.diagnostics:
			plot_rhs(state,i)
		if (i%nout == 0):
			plot_fields(state,i/nout)

#destroy stuff
destroy(state)
