from common import *
from mesh import *
from solver import *
from output import *
from plotting import *
from basis import *
from field import *
from functionspace import *
import h5py
from math import sqrt
from revisedform import *
from assemble import *

def compute_statistics(i):
	#calculate 0-forms
	AssembleForm(peform,pefunc,quadrature)
	AssembleForm(massform,massfunc,quadrature)
	AssembleForm(keform,kefunc,quadrature)
	
	massstat[i/nstat] = massform.value
	kestat[i/nstat] = 0.5 * H0 * keform.value
	pestat[i/nstat] = 0.5 * g * peform.value
	energystat[i/nstat] = kestat[i/nstat] + pestat[i/nstat]
	#print massstat[i/nstat],kestat[i/nstat],pestat[i/nstat],energystat[i/nstat]

def plot_linearswe(i):

	#Load results
	f = h5py.File("results.linearswe.h5", "r")
	hvals = np.array(f['h'+str(i)]) 
	uvals = np.array(f['u'+str(i)]) 
	vvals = np.array(f['v'+str(i)])
	f.close()
		
	hindices = xn.space.get_space(0).get_compindices(0)
	uindices = xn.space.get_space(1).get_compindices(0)
	vindices = xn.space.get_space(1).get_compindices(1)
	temph = xn.vector.getSubVector(hindices)
	tempu = xn.vector.getSubVector(uindices)
	tempv = xn.vector.getSubVector(vindices)
	temph.setArray(hvals)
	tempu.setArray(uvals)
	tempv.setArray(vvals)
	xn.vector.restoreSubVector(hindices,temph)
	xn.vector.restoreSubVector(uindices,tempu)
	xn.vector.restoreSubVector(vindices,tempv)

	h = xn.eval_at_quad_pts(0,quadrature).data
	uvec = xn.eval_at_quad_pts(1,quadrature).data
	u = uvec[:,:,:,:,:,:,0]
	v = uvec[:,:,:,:,:,:,1]
	
	quads = mesh.get_plotting_quad_coords(quadrature)
		
	x = np.ravel(quads[:,:,:,:,:,:,0])
	y = np.ravel(quads[:,:,:,:,:,:,1])
	#data is stored as nz,ny,nx,qx,qy,qz
	
	#newshape = (ny,nx,quadrature.pts.shape[0],quadrature.pts.shape[1],quadrature.pts.shape[2])
	#h = np.reshape(h,newshape)
	#u = np.reshape(u,newshape)
	#v = np.reshape(v,newshape)
	plotheader = str(i)
	
	spatialplot_2d(x,y,np.ravel(h),'h.'+plotheader)
	spatialplot_2d(x,y,np.ravel(u),'u.'+plotheader)
	spatialplot_2d(x,y,np.ravel(v),'v.'+plotheader)
	
def plot_stats():
	x = xrange(N/nstat + 1)
	spatialplot_1d(x,(massstat-massstat[0])/massstat[0],'mass')
	spatialplot_1d(x,(kestat-kestat[0])/kestat[0],'ke')
	spatialplot_1d(x,(pestat-pestat[0])/pestat[0],'pe')
	spatialplot_1d(x,(energystat-energystat[0])/energystat[0],'te')

	spatialplot_1d_multi(x,[energystat,kestat,pestat],['te','ke','pe'],'energy')
	print energystat[-1],pestat[-1],kestat[-1]

OptDB = PETSc.Options()
spacetype = OptDB.getString('spacetype', 'MGD')
#FEEC FEEC-Gauss FEEC-UniformOpen FEEC-UniformClosed MSE FEEC-B MSE-Uniform MGD
elem_order = OptDB.getInt('elemorder', 1)
nx = OptDB.getInt('nx', 32)
ny = OptDB.getInt('ny', 32)
ndims = OptDB.getInt('dims', 2)

simname = OptDB.getString('simname', 'linearswe')
quadchoice = OptDB.getString('quadchoice', 'gauss') #only gauss supported for now
quadorder = OptDB.getInt('quadorder', 1 + elem_order) #only gauss supported for now
solver = OptDB.getString('iterative','fgmres')#fgmres
pc = OptDB.getString('precon','ilu')
if spacetype == 'MGD':
	pc = 'jacobi'
kspinfo = OptDB.getBool('kspinfo', False)
output_matrices = OptDB.getBool('outputMatrix', True)
output_fspaces = OptDB.getBool('outputSpaces', False)
output_ksp = OptDB.getBool('outputKSP', False)
output_mesh = OptDB.getBool('outputMesh', True)
outdir = OptDB.getString('outdir', '')

#NEED TO FIX UP STUFF HERE BETWEEN F-PLANE AND VARIABLE F-CYLINDERS...
f = 0.00006147 #0.00006147 #0.00006147 0.0001
g = 9.81
H0 = 750.0
dh = 75.0 
Ly = 5000.0 * 1000.0 
Lx = 5000.0 * 1000.0
L = 5000.0 * 1000.0

#NEED TO FIX UP STUFF HERE BETWEEN F-PLANE AND VARIABLE F-CYLINDERS...
#a = 6273000.
#dx = np.pi * a / (nx / 2.) 
#dy = np.pi * a / (ny / 2.)
#L = 2. * np.pi * a
#Lx = 2. * np.pi * a
#Ly = 2. * np.pi * a
#xc = 0. #Lx/2.
#yc = 0. #Ly/2.

C = 0.25
dx = OptDB.getScalar('dx',Lx/nx)
dy = OptDB.getScalar('dy',Ly/ny) #100000
dt = float(min(dx,dy) * C / sqrt(g * H0)) #should probably use some average of dx and dy? or maybe minimum?

sigmax = 3./40.*L
sigmay = 3./40.*L
xc = Lx/2.
yc = Ly/2.
N = 50
nout = 10
nstat = 1
print dx/1000.0,dy/1000.0,dt
xbc = 'periodic'
ybc = 'periodic'

rank = PETSc.COMM_WORLD.Get_rank() 

#create function spaces
cgx,cgy,cgz,dgx,dgy,dgz = get_cgdg(spacetype,xbc=xbc,ybc=ybc,xorder=elem_order,yorder=elem_order)
l2elem = TensorProductElement([dgx,dgy])
vecxelem = TensorProductElement([cgx,dgy]) 
vecyelem = TensorProductElement([dgx,cgy])

quad = get_quadrature(quadchoice)
quadrature = TensorProductQuadrature([quad,quad],[quadorder,quadorder])

#Create mesh and mixed space
#geom = UniformBox(l2elem.get_ref_cell(),quadrature,dx,dy)
geom = UniformVariableBox(l2elem.get_ref_cell(),quadrature,dx,dy) 
mesh = Mesh(ndims,geom,[nx,ny],[xbc,ybc])
mixed_space = MixedFunctionSpace(mesh,[FunctionSpace,VectorFunctionSpace],[l2elem,[vecxelem,vecyelem]],['L2','Hdiv'])

#create forms
#NEED TO FIX UP STUFF HERE BETWEEN F-PLANE AND VARIABLE F-CYLINDERS...
hmassfunc = Functional('standard.functional','l2mass',2,geometrylist=['detJinv'])
umassfunc = Functional('standard.functional','hdivmass',2,geometrylist=['JTJdetJinv'])
divfunc = Functional('linearswe.functional','div',2,geometrylist=['detJinv'],parameters=[('divscale',H0 * dt / 2.)])
gradfunc = Functional('linearswe.functional','grad',2,geometrylist=['detJinv'],parameters=[('gradscale',-g * dt / 2.)])
perpfunc = Functional('linearswe.functional','perp',2,geometrylist=['JTrotJdetJinv'],parameters=[('perpscale',f * dt / 2.)])
massperpfunc = Functional('linearswe.functional','massperp',2,geometrylist=['JTrotJdetJinv','JTJdetJinv'],parameters=[('perpscale',f * dt / 2.)])
divfuncB = Functional('linearswe.functional','div',2,geometrylist=['detJinv'],parameters=[('divscale',-H0 * dt / 2.)])
gradfuncB = Functional('linearswe.functional','grad',2,geometrylist=['detJinv'],parameters=[('gradscale',g * dt / 2.)])
perpfuncB = Functional('linearswe.functional','perp',2,geometrylist=['JTrotJdetJinv'],parameters=[('perpscale',-f * dt / 2.)])
massperpfuncB = Functional('linearswe.functional','massperp',2,geometrylist=['JTrotJdetJinv','JTJdetJinv'],parameters=[('perpscale',-f * dt / 2.)])

mixednameA = ('A',[['hmassA','divuA','divvA'],['graduA','massuA','perpuA'],['gradvA','perpvA','massuA']])
mixednameB = ('B',[['hmassB','divuB','divvB'],['graduB','massuB','perpuB'],['gradvB','perpvB','massuB']])
massname = ('mass',[['hmass','humass','hvmass'],['uhmass','umass','uvmass'],['vhmass','vumass','vmass']])
A = TwoForm(mesh,mixednameA,mixed_space,mixed_space)
B = TwoForm(mesh,mixednameB,mixed_space,mixed_space)
mass = TwoForm(mesh,massname,mixed_space,mixed_space)

#HOW SHOULD INITIAL CONDITIONS BE HANDLED FOR VARIABLE F CASE?

hinit_rhs = Coefficient('coeff','scalar',np.zeros(mesh.geometry.detJ.shape),mesh)
uinit_rhs = Coefficient('coeff','vector',np.zeros(mesh.geometry.quadcoords.shape),mesh)
	
hfunc = Functional('standard.functional','l2proj_coeff',1,geometrylist=['detJ',],coefficientlist=[hinit_rhs,])
ufunc = Functional('standard.functional','hdivproj_coeff',1,geometrylist=['detJ','J'],coefficientlist=[uinit_rhs,])
	
x = mesh.geometry.quadcoords[:,:,:,:,:,:,0]
y = mesh.geometry.quadcoords[:,:,:,:,:,:,1]
xprime = Lx / (np.pi * sigmax) * np.sin( np.pi / Lx * (x- xc))
yprime = Ly / (np.pi * sigmay) * np.sin( np.pi / Ly * (y- yc))
xprimeprime = Lx / (2.0 * np.pi * sigmax) * np.sin( 2 * np.pi / Lx * (x - xc))
yprimeprime = Ly / (2.0 * np.pi * sigmay) * np.sin( 2 * np.pi / Ly * (y - yc))
		
hinit_rhs.data =  dh * np.exp(-0.5 * (xprime * xprime + yprime * yprime))
uinit_rhs.data[:,:,:,:,:,:,0] = - g * dh / f / sigmay * 2. * yprimeprime * np.exp(-0.5*(xprime * xprime + yprime * yprime))
uinit_rhs.data[:,:,:,:,:,:,1] =   g * dh / f / sigmax * 2. * xprimeprime * np.exp(-0.5*(xprime * xprime + yprime * yprime))

initname = ('init',['hinit','uinit','vinit'])
init = OneForm(mesh,initname,mixed_space)
bname = ('rhs',['hrhs','urhs','vrhs'])
b = OneForm(mesh,bname,mixed_space)

#assemble forms
AssembleForm(mass,hmassfunc,quadrature,0,0)
AssembleForm(mass,umassfunc,quadrature,1,1)

AssembleForm(A,hmassfunc,quadrature,0,0)
AssembleForm(A,divfunc,quadrature,0,1)
AssembleForm(A,gradfunc,quadrature,1,0)
#AssembleForm(A,umassfunc,quadrature,1,1)
#AssembleForm(A,perpfunc,quadrature,1,1)
AssembleForm(A,massperpfunc,quadrature,1,1)

AssembleForm(B,hmassfunc,quadrature,0,0)
AssembleForm(B,divfuncB,quadrature,0,1)
AssembleForm(B,gradfuncB,quadrature,1,0)
#AssembleForm(B,umassfunc,quadrature,1,1)
#AssembleForm(B,perpfuncB,quadrature,1,1)
AssembleForm(B,massperpfuncB,quadrature,1,1)

AssembleForm(init,hfunc,quadrature,0)
if not (f == 0):
	AssembleForm(init,ufunc,quadrature,1)

#output matrices if needed
if output_matrices:
	A.output()
	B.output()
	mass.output()
	
#output spaces
if output_fspaces:
	mixed_space.output()
	
#create fields to hold solutions at various time steps
xnname = ('x0',['h0','u0','v0'])
xn = Field(xnname,mixed_space)
xnp1name = ('x1',['h1','u1','v1'])
xnp1 = Field(xnp1name,mixed_space)

#create 0 forms
pefunc = Functional('linearswe.functional','zeroform_pe_take2',0,geometrylist=['detJ'],fields=(xn,),fsi=(0,))
peform = ZeroForm(mesh,'pe')
massfunc = Functional('linearswe.functional','zeroform_mass',0,geometrylist=['detJ'],fields=(xn,),fsi=(0,))
massform = ZeroForm(mesh,'mass')
kefunc = Functional('linearswe.functional','zeroform_ke_take2',0,geometrylist=['detJ','J'],fields=(xn,),fsi=(1,))
keform = ZeroForm(mesh,'ke')

massstat = np.zeros((N/nstat + 1))
kestat = np.zeros((N/nstat + 1))
pestat = np.zeros((N/nstat + 1))
energystat = np.zeros((N/nstat + 1))

#create initial condition
masssys = LinearSystem(mass,solver,pc,bcs=None,rtol=1e-20,fieldsplit=True)
masssys.solve(init,xn,kspinfo=kspinfo)

#create solvers
linsys = LinearSystem(A,solver,pc,bcs=None,rtol=1e-20,fieldsplit=True)
#output ksp
if output_ksp:
	linsys.output()
	masssys.output()

def output_linearswe_vectors(i,view):

	#EVENTUALLY THIS NEEDS TO SET VIA HDF5 TIMESTEP...
	xnname = ('x'+str(i),['h'+str(i),'u'+str(i),'v'+str(i)])
	xn.name = xnname[0]
	xn.names = xnname[1]
	xn.output(ViewHDF5)
	
	#ViewHDF5.incrementTimestep(1)

#create output and output initial condition
ViewHDF5 = PETSc.ViewerHDF5().create(outdir + 'results.' + simname + '.h5', mode=PETSc.Viewer.Mode.WRITE,comm= PETSc.COMM_WORLD)
#ViewHDF5.setTimestep(0)
mesh.output(ViewHDF5)
output_linearswe_vectors(0,ViewHDF5)
plot_linearswe(0)
#PROPERLY SET TIMESTEP HERE!!
#TIMESTEP SETTING OUTPUT IS NOT WORKING....WHY?
#MIGHT BE SOME WEIRD INTERNAL PETSC THING...I SEEM TO REMEMBER HAVING A SIMILAR ISSUE MUCH EARLIER

#compute initial condition statistics
compute_statistics(0)

for i in xrange(1,N+1):
	print 'step ',i
	B.A.mult(xn.vector,b.vector) #x,y for Ax = y 
	#solve system
	linsys.solve(b,xnp1,kspinfo=kspinfo)
	#print 'solved'
	xn.vector = xnp1.vector.copy()

	if (i%nout == 0):
		output_linearswe_vectors(i,ViewHDF5)
		
	if (i%nstat == 0):
		compute_statistics(i)

#close output
ViewHDF5.destroy()

#do plotting if needed
if (rank == 0):
	plot_stats()
	for i in xrange(0,N+1):
		if (i%nout == 0):
			plot_linearswe(i)

A.destroy()
B.destroy()
mass.destroy()
init.destroy()

linsys.destroy()
masssys.destroy()

mixed_space.destroy()

xn.destroy()
xnp1.destroy()

mesh.destroy()
