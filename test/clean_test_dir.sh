
cd h1_1d
rm *.png *.h5 *.out
cd ..

cd h1_2d
rm *.png *.h5 *.out
cd ..

cd mixed_1d
rm *.png *.h5 *.out
cd ..

cd hdiv_2d
rm *.png *.h5 *.out
cd ..

cd hcurl_2d
rm *.png *.h5 *.out
cd ..

cd linearswe
rm *.png *.h5 *.out
cd ..
