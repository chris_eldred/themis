from common import *
from mesh import *
from solver import *
from output import *
from plotting import *
from basis import *
from field import *
from functionspace import *
import h5py
from math import sqrt
from revisedform import *
from assemble import *

class State():
	def __init__(self):
		pass

def compute_statistics(state,i):

	#calculate 0-forms
	AssembleForm(state.peform,state.pefunc,state.quadrature)
	AssembleForm(state.massform,state.massfunc,state.quadrature)
	AssembleForm(state.keform,state.kefunc,state.quadrature)
	AssembleForm(state.pvform,state.pvfunc,state.quadrature)
	AssembleForm(state.enstrophyform,state.enstrophyfunc,state.quadrature)

	if (i == 0):
		#state.volumeform.assemble()
		AssembleForm(state.volumeform,state.volumefunc,state.quadrature)

		parameters = []
		layerwiseh = state.massform.value / state.volumeform.value 
		print layerwiseh
		print state.volumeform.value
		print state.massform.value
		parameters.append(('layerwiseh',layerwiseh))
		if state.coupled:
			parameters.append(('rholist',state.rholist))
			bpefunc = Functional('ml-nlswe.functional','zeroform_backgroundpe_coupled',0,geometrylist=['detJ','J'],parameters=parameters)
			state.bpeform = ZeroForm(state.mesh,'bpe')	
		else:
			bpefunc = Functional('ml-nlswe.functional','zeroform_backgroundpe',0,geometrylist=['detJ','J'],parameters=parameters)
			state.bpeform = ZeroForm(state.mesh,'bpe',layered=True)	
		#state.bpeform.assemble()
		AssembleForm(state.bpeform,bpefunc,state.quadrature)

	state.massstat[i,:] = state.massform.value
	state.pvstat[i,:] = state.pvform.value
	state.enstrophystat[i,:] = state.enstrophyform.value
	if state.coupled:
		state.kestat[i] = 0.5 * state.keform.value
		state.pestat[i] = 0.5 * state.g * state.peform.value
		state.apestat[i] = 0.5 * state.g * (state.peform.value - state.bpeform.value)
		state.energystat[i] = state.kestat[i] + state.pestat[i]
		state.availenergystat[i] = state.kestat[i] + state.apestat[i]
	else:
		state.kestat[i,:] = 0.5 * state.keform.value
		state.pestat[i,:] = 0.5 * state.g * state.peform.value
		state.apestat[i,:] = 0.5 * state.g * (state.peform.value - state.bpeform.value)
		state.energystat[i,:] = state.kestat[i] + state.pestat[i]
		state.availenergystat[i,:] = state.kestat[i] + state.apestat[i]

#SUPER BROKEN
def plot_rhs(state,i):
	f = h5py.File("results.ml-nlswe.h5", "r")

	divF = np.array(f['divF'+str(i)]) / state.dy /state.dx
	qFtu = np.array(f['qFTu'+str(i)]) / state.dy
	qFtv = np.array(f['qFTv'+str(i)]) / state.dx
	curlu = np.array(f['curlu'+str(i)])
	gradBu = np.array(f['gradBu'+str(i)]) / state.dy
	gradBv = np.array(f['gradBv'+str(i)]) / state.dx
	hu = np.array(f['hu'+str(i)]) / state.dy
	hv = np.array(f['hv'+str(i)]) / state.dx

	xcell,ycell,zcell,xuedge,yuedge,zuedge,xvedge,yvedge,zvedge,xwedge,ywedge,zwedge,xvertex,yvertex,zvertex = state.mesh.get_plotting_coords()
	newshape = (state.nz,state.ny,state.nx)
	divF = np.reshape(divF,newshape)
	curlu = np.reshape(curlu,newshape)
	qFtu = np.reshape(qFtu,newshape)
	qFtv = np.reshape(qFtv,newshape)
	gradBu = np.reshape(gradBu,newshape)
	gradBv = np.reshape(gradBv,newshape)
	hu = np.reshape(hu,newshape)
	hv = np.reshape(hv,newshape)
	plotheader = str(i)

	spatialplot_3d_slices_square(xcell,ycell,divF,'divF',plotheader,state.nz)
	spatialplot_3d_slices_square(xvertex,yvertex,curlu,'curlu',plotheader,state.nz)
	spatialplot_3d_slices_square(xuedge,yuedge,qFtu,'qFTu',plotheader,state.nz)
	spatialplot_3d_slices_square(xvedge,yvedge,qFtv,'qFTv',plotheader,state.nz)
	spatialplot_3d_slices_square(xuedge,yuedge,gradBu,'gradBu',plotheader,state.nz)
	spatialplot_3d_slices_square(xvedge,yvedge,gradBv,'gradBv',plotheader,state.nz)
	spatialplot_3d_slices_square(xuedge,yuedge,hu,'hu',plotheader,state.nz)
	spatialplot_3d_slices_square(xvedge,yvedge,hv,'hv',plotheader,state.nz)

def plot_fields(state,i):
	
	#Load results
	f = h5py.File("results.ml-nlswe.h5", "r")
	hvals = np.array(f['h'+str(i)]) 
	qvals = np.array(f['q'+str(i)])
	uvals = np.array(f['u'+str(i)]) 
	vvals = np.array(f['v'+str(i)])
	Fuvals = np.array(f['Fu'+str(i)]) 
	Fvvals = np.array(f['Fv'+str(i)])
	f.close()

	state.h.vector.setArray(hvals)
	state.q.vector.setArray(qvals)
	
	uindices = state.u.space.get_space(0).get_compindices(0)
	vindices = state.u.space.get_space(0).get_compindices(1)
	tempu = state.u.vector.getSubVector(uindices)
	tempv = state.u.vector.getSubVector(vindices)
	tempu.setArray(uvals)
	tempv.setArray(vvals)
	state.u.vector.restoreSubVector(uindices,tempu)
	state.u.vector.restoreSubVector(vindices,tempv)

	uindices = state.F.space.get_space(0).get_compindices(0)
	vindices = state.F.space.get_space(0).get_compindices(1)
	tempu = state.F.vector.getSubVector(uindices)
	tempv = state.F.vector.getSubVector(vindices)
	tempu.setArray(Fuvals)
	tempv.setArray(Fvvals)
	state.F.vector.restoreSubVector(uindices,tempu)
	state.F.vector.restoreSubVector(vindices,tempv)
	
	h = state.h.eval_at_quad_pts(0,state.quadrature).data
	q = state.q.eval_at_quad_pts(0,state.quadrature).data
	uvec = state.u.eval_at_quad_pts(0,state.quadrature).data
	u = uvec[:,:,:,:,:,:,0]
	v = uvec[:,:,:,:,:,:,1]
	Fuvec = state.F.eval_at_quad_pts(0,state.quadrature).data
	Fu = Fuvec[:,:,:,:,:,:,0]
	Fv = Fuvec[:,:,:,:,:,:,1]


	quads = state.mesh.get_plotting_quad_coords(state.quadrature)
		
	x = quads[:,:,:,:,:,:,0]
	y = quads[:,:,:,:,:,:,1]
	#data is stored as nz,ny,nx,qx,qy,qz
	
	#newshape = (state.nz,state.ny,state.nx,state.quadrature.pts.shape[0],state.quadrature.pts.shape[1],state.quadrature.pts.shape[2])
	#h = np.reshape(h,newshape)
	#u = np.reshape(u,newshape)
	#v = np.reshape(v,newshape)
	#Fu = np.reshape(Fu,newshape)
	#Fv = np.reshape(Fv,newshape)
	#q = np.reshape(q,newshape)
	plotheader = str(i)
	
	spatialplot_3d_slices(x,y,h,'h',plotheader,state.nz)
	spatialplot_3d_slices(x,y,u,'u',plotheader,state.nz)
	spatialplot_3d_slices(x,y,v,'v',plotheader,state.nz)
	spatialplot_3d_slices(x,y,Fu,'Fu',plotheader,state.nz)
	spatialplot_3d_slices(x,y,Fv,'Fv',plotheader,state.nz)
	spatialplot_3d_slices(x,y,q,'q',plotheader,state.nz)

def plot_stats(state):
	x = xrange(state.massstat.shape[0])
	for k in range(state.nz):
		spatialplot_1d(x,(state.massstat[:,k]-state.massstat[0,k])/state.massstat[0,k]*100.0,'mass.' + str(k))
		spatialplot_1d(x,(state.pvstat[:,k]-state.pvstat[0,k])/state.pvstat[0,k]*100.0,'pv.' + str(k))
		spatialplot_1d(x,(state.enstrophystat[:,k]-state.enstrophystat[0,k])/state.enstrophystat[0,k]*100.0,'ens.' + str(k))
		spatialplot_1d(x,state.enstrophystat[:,k],'ens_time.' + str(k))
		spatialplot_1d(x,state.massstat[:,k],'mass_time.' + str(k))
		spatialplot_1d(x,state.pvstat[:,k],'pv_time.' + str(k))

	if state.coupled:
		spatialplot_1d(x,(state.kestat-state.kestat[0])/state.kestat[0]*100.0,'ke')
		spatialplot_1d(x,(state.pestat-state.pestat[0])/state.pestat[0]*100.0,'pe')
		spatialplot_1d(x,(state.apestat-state.apestat[0])/state.apestat[0]*100.0,'ape')
		spatialplot_1d(x,(state.energystat-state.energystat[0])/state.energystat[0]*100.0,'te')
		spatialplot_1d(x,(state.availenergystat-state.availenergystat[0])/state.availenergystat[0]*100.0,'ate')
		spatialplot_1d(x,state.energystat,'te_time')
		spatialplot_1d(x,state.availenergystat,'ate_time')
		spatialplot_1d(x,state.pestat,'pe_time')
		spatialplot_1d(x,state.apestat,'ape_time')
		spatialplot_1d(x,state.kestat,'ke_time')
		spatialplot_1d_multi(x,[state.energystat,state.kestat,state.pestat],['te','ke','pe'],'energy')		
		spatialplot_1d_multi(x,[state.availenergystat,state.kestat,state.apestat],['ate','ke','ape'],'availenergy')		
	else:
		for k in range(state.nz):
			spatialplot_1d(x,(state.kestat[:,k]-state.kestat[0,k])/state.kestat[0,k]*100.0,'ke.' + str(k))
			spatialplot_1d(x,(state.pestat[:,k]-state.pestat[0,k])/state.pestat[0,k]*100.0,'pe.' + str(k))
			spatialplot_1d(x,(state.apestat[:,k]-state.apestat[0,k])/state.apestat[0,k]*100.0,'ape.' + str(k))
			spatialplot_1d(x,(state.energystat[:,k]-state.energystat[0,k])/state.energystat[0,k]*100.0,'te.' + str(k))
			spatialplot_1d(x,(state.availenergystat[:,k]-state.availenergystat[0,k])/state.availenergystat[0,k]*100.0,'ate.' + str(k))
			spatialplot_1d(x,state.energystat[:,k],'te_time.' + str(k))
			spatialplot_1d(x,state.availenergystat[:,k],'ate_time.' + str(k))
			spatialplot_1d(x,state.pestat[:,k],'pe_time.' + str(k))
			spatialplot_1d(x,state.apestat[:,k],'ape_time.' + str(k))
			spatialplot_1d(x,state.kestat[:,k],'ke_time.' + str(k))
			spatialplot_1d_multi(x,[state.energystat[:,k],state.kestat[:,k],state.pestat[:,k]],['te','ke','pe'],'energy.' + str(k))
			spatialplot_1d_multi(x,[state.availenergystat[:,k],state.kestat[:,k],state.apestat[:,k]],['ate','ke','ape'],'availenergy.' + str(k))
			
def output_rhs(state,i,view):
	state.divF.output(view,ts=i)
	state.qFt.output(view,ts=i)
	state.gradB.output(view,ts=i)
	state.curlu.output(view,ts=i)
	state.hu.output(view,ts=i)
	
def output_fields(state,i,view):
	#EVENTUALLY THIS NEEDS TO SET HDF5 TIMESTEP...
	#for field in [state.h,state.q,state.F,state.u]:
	#	field.name = field.name + str(i)  #this is a hack
	#	for j in xrange(len(field.names)):
	#		field.names[j] = field.names[j] + str(i) #this is a hack
	#create 0-forms for evaluation
	#quad = get_quadrature('gauss') #newton-cotes-closed
	
	state.h.output(view,ts=i)
	state.u.output(view,ts=i)
	state.q.output(view,ts=i)
	state.F.output(view,ts=i)
	
	#state.htemp.output(view,ts=i)
	#state.utemp.output(view,ts=i)

	#if not ((state.spacetype == 'MGD') or (state.elem_order == 1)):
		
		##project fields into plotting spaces
		#proj_h = state.l2_proj.project(state.h,kspinfo=state.kspinfo)
		#proj_u = state.hdiv_proj.project(state.u,kspinfo=state.kspinfo)
		#proj_F = state.hdiv_proj.project(state.F,kspinfo=state.kspinfo)
		#proj_q = state.h1_proj.project(state.q,kspinfo=state.kspinfo)
		
		#proj_q.output(view,ts=i)
		#proj_h.output(view,ts=i)
		#proj_F.output(view,ts=i)
		#proj_u.output(view,ts=i)

	#ViewHDF5.incrementTimestep(1)	
	
def diagnose(state):
	#print 'diagnose'
	AssembleForm(state.qmat,state.qfunc,state.quadrature,0,0,zeroform=True)	
	AssembleForm(state.hu,state.hufunc,state.quadrature,0,zeroform=True)
	AssembleForm(state.curlu,state.curlufunc,state.quadrature,0,zeroform=True)

	state.qsys = LinearSystem(state.qmat,state.solver,state.pc,bcs=None,rtol=1e-20)
	state.qsys.solve(state.curlu,state.q,kspinfo=state.kspinfo)
	state.hdivmass_sys.solve(state.hu,state.F,kspinfo=state.kspinfo)

def rhs(state,i):
	#print 'rhs'
	AssembleForm(state.qFt,state.qFtfunc,state.quadrature,0,zeroform=True)
	AssembleForm(state.gradB,state.gradBfunc,state.quadrature,0,zeroform=True)
	AssembleForm(state.divF,state.divFfunc,state.quadrature,0,zeroform=True)

	state.urhs.vector = -state.qFt.vector + state.gradB.vector
	state.hdivmass_sys.solve(state.urhs,state.ku_is[i],kspinfo=state.kspinfo)

	state.hrhs.vector = -state.divF.vector
	state.l2mass_sys.solve(state.hrhs,state.kh_is[i],kspinfo=state.kspinfo)
	
def create_timestep_aux_rk(state):
	state.htemp = Field(('htemp',['htemp',]),state.l2)
	state.utemp = Field(('utemp',['utemp','vtemp','wtemp']),state.hdiv)
	state.urhs = OneForm(state.mesh,['urhs',['urhs','vrhs','wrhs']],state.hdiv)
	state.hrhs = OneForm(state.mesh,['hrhs',['hrhs',]],state.l2)
	
	state.ku_is = []
	state.kh_is = []
	
	for i in xrange(4): #THIS SHOULD REALLY CHANGE DEPENDING ON TABLEAU
		state.kh_is.append(Field(('kh_'+str(i),['kh_'+str(i),]),state.l2))
		state.ku_is.append(Field(('ku_'+str(i),['ku_'+str(i),'kv_'+str(i)]),state.hdiv))

	#create ku_i's, kh_i's and htemp/utemp, as needed by the provided tableau

def take_explicit_rk_step(state):
	khvecs = []
	kuvecs = []
	rhs(state,0)
	khvecs.append(state.kh_is[0].vector)
	kuvecs.append(state.ku_is[0].vector)
	beta = np.array([0.5,0.5,1.]) * state.dt #THIS SHOULD REALLY CHANGE DEPENDING ON TABLEAU
	for i in xrange(3):
		state.htemp.vector = state.h.vector + state.kh_is[i].vector * beta[i]
		state.utemp.vector = state.u.vector + state.ku_is[i].vector * beta[i]
		diagnose(state)
		rhs(state,i+1)
		khvecs.append(state.kh_is[i+1].vector)
		kuvecs.append(state.ku_is[i+1].vector)
	alphas = np.array([1./6.,1./3.,1./3.,1./6.]) * state.dt #THIS SHOULD REALLY CHANGE DEPENDING ON TABLEAU
	state.h.vector.maxpy(alphas,khvecs)
	state.u.vector.maxpy(alphas,kuvecs)
	state.htemp.vector = state.h.vector.copy()
	state.utemp.vector = state.u.vector.copy()
	diagnose(state)

	#using rhs and diagnose, take an arbitrary explicit rk step using the provided tableau
	
def initial_condition(state):

	hinit_rhs = Coefficient('coeff','scalar',np.zeros(state.mesh.geometry.detJ.shape),state.mesh)
	uinit_rhs = Coefficient('coeff','vector',np.zeros(state.mesh.geometry.quadcoords.shape),state.mesh)
	
	hfunc = Functional('standard.functional','l2proj_coeff',1,geometrylist=['detJ',],coefficientlist=[hinit_rhs,],assemblytype='standard')
	ufunc = Functional('standard.functional','hdivproj_coeff',1,geometrylist=['detJ','J'],coefficientlist=[uinit_rhs,],assemblytype='standard')
	
	x = state.mesh.geometry.quadcoords[:,:,:,:,:,:,0]
	y = state.mesh.geometry.quadcoords[:,:,:,:,:,:,1]
	z = state.mesh.geometry.quadcoords[:,:,:,:,:,:,2]
	
	####### This is single vortex #####
	if state.initcond == 'singlevortex':
		dh = 75.0 
		sigmax = 3./40.*state.Lx
		sigmay = 3./40.*state.Ly
		xc = state.Lx/2.
		yc = state.Ly/2.
		
		xprime = state.Lx / (np.pi * sigmax) * np.sin( np.pi / state.Lx * (x- xc))
		yprime = state.Ly / (np.pi * sigmay) * np.sin( np.pi / state.Ly * (y- yc))
		xprimeprime = state.Lx / (2.0 * np.pi * sigmax) * np.sin( 2 * np.pi / state.Lx * (x - xc))
		yprimeprime = state.Ly / (2.0 * np.pi * sigmay) * np.sin( 2 * np.pi / state.Ly * (y - yc))
		
		hinit_rhs.data = state.H0 - dh * np.exp(-0.5 * (xprime * xprime + yprime * yprime))
		uinit_rhs.data[:,:,:,:,:,:,0] = - state.g * dh / state.f / sigmay * 2. * yprimeprime * np.exp(-0.5*(xprime * xprime + yprime * yprime))
		uinit_rhs.data[:,:,:,:,:,:,1] =   state.g * dh / state.f / sigmax * 2. * xprimeprime * np.exp(-0.5*(xprime * xprime + yprime * yprime))
	
		#parameters = []
		#parameters.append(('Lx',state.Lx))
		#parameters.append(('Ly',state.Ly))
		#parameters.append(('sigmax',sigmax))
		#parameters.append(('sigmay',sigmay))
		#parameters.append(('xc',xc))
		#parameters.append(('yc',yc))
		#parameters.append(('g',state.g))
		#parameters.append(('f',state.f))
		#parameters.append(('dh',dh))
		#parameters.append(('H0',state.H0))
		#ufunc = Functional('ml-nlswe.functional','uinitsingle',1,geometrylist=['detJ','coords','J'],parameters=parameters) #uinit zeroproj
		#hfunc = Functional('ml-nlswe.functional','hinitsingle',1,geometrylist=['detJ','coords'],parameters=parameters) #zeroproj hinit
	##########

	####### This is double vortex #####
	if state.initcond == 'doublevortex':
		dh = 75.0
		o = 0.1 
		sigmax = 3./40.*state.Lx
		sigmay = 3./40.*state.Ly
		xc1 = (0.5-o) * state.Lx
		yc1 = (0.5-o) * state.Ly
		xc2 = (0.5+o) * state.Lx
		yc2 = (0.5+o) * state.Ly		
		
		xprime1 = state.Lx / (np.pi * sigmax) * np.sin( np.pi / state.Lx * (x- xc1))
		yprime1 = state.Ly / (np.pi * sigmay) * np.sin( np.pi / state.Ly * (y- yc1))
		xprime2 = state.Lx / (np.pi * sigmax) * np.sin( np.pi / state.Lx * (x- xc2))
		yprime2 = state.Ly / (np.pi * sigmay) * np.sin( np.pi / state.Ly * (y- yc2))
		xprimeprime1 = state.Lx / (2.0 * np.pi * sigmax) * np.sin( 2 * np.pi / state.Lx * (x - xc1))
		yprimeprime1 = state.Ly / (2.0 * np.pi * sigmay) * np.sin( 2 * np.pi / state.Ly * (y - yc1))
		xprimeprime2 = state.Lx / (2.0 * np.pi * sigmax) * np.sin( 2 * np.pi / state.Lx * (x - xc2))
		yprimeprime2 = state.Ly / (2.0 * np.pi * sigmay) * np.sin( 2 * np.pi / state.Ly * (y - yc2))
		
		hinit_rhs.data = state.H0 - dh * (np.exp(-0.5 * (xprime1 * xprime1 + yprime1 * yprime1)) + np.exp(-0.5 * (xprime2 * xprime2 + yprime2 * yprime2)) - 4. * np.pi * sigmax * sigmay / state.Lx / state.Ly)
		uinit_rhs.data[:,:,:,:,:,:,0] = - state.g * dh / state.f / sigmay * (yprimeprime1 * np.exp(-0.5*(xprime1 * xprime1 + yprime1 * yprime1)) + yprimeprime2 * np.exp(-0.5*(xprime2 * xprime2 + yprime2 * yprime2)))
		uinit_rhs.data[:,:,:,:,:,:,1] =   state.g * dh / state.f / sigmax * (xprimeprime1 * np.exp(-0.5*(xprime1 * xprime1 + yprime1 * yprime1)) + xprimeprime2 * np.exp(-0.5*(xprime2 * xprime2 + yprime2 * yprime2)))

		#parameters = []
		#parameters.append(('Lx',state.Lx))
		#parameters.append(('Ly',state.Ly))
		#parameters.append(('sigmax',sigmax))
		#parameters.append(('sigmay',sigmay))
		#parameters.append(('xc1',xc1))
		#parameters.append(('yc1',yc1))
		#parameters.append(('xc2',xc2))
		#parameters.append(('yc2',yc2))
		#parameters.append(('g',state.g))
		#parameters.append(('f',state.f))
		#parameters.append(('dh',dh))
		#parameters.append(('H0',state.H0))
		#ufunc = Functional('ml-nlswe.functional','uinitdouble',1,geometrylist=['detJ','coords','J'],parameters=parameters) #uinit zeroproj
		#hfunc = Functional('ml-nlswe.functional','hinitdouble',1,geometrylist=['detJ','coords'],parameters=parameters) #zeroproj hinit
	##########

	####### This is gravity wave- coupled #####
	if state.initcond == 'gravitywave_coupled':
		dh = 50.0 
		sigmax = 3./40.*state.Lx
		sigmay = 3./40.*state.Ly
		xc = state.Lx/2.
		yc = state.Ly/2.
		dhfactors = np.zeros((state.nz,))
		dhfactors[0] = -1.0
		dhfactors[1] = 1.0
			
		for k in range(state.nz):
			xadj = x[k,:,:,:,:,:]
			yadj = y[k,:,:,:,:,:]
			xprime = state.Lx / (np.pi * sigmax) * np.sin( np.pi / state.Lx * (xadj- xc))
			yprime = state.Ly / (np.pi * sigmay) * np.sin( np.pi / state.Ly * (yadj- yc))
			xprimeprime = state.Lx / (2.0 * np.pi * sigmax) * np.sin( 2 * np.pi / state.Lx * (xadj - xc))
			yprimeprime = state.Ly / (2.0 * np.pi * sigmay) * np.sin( 2 * np.pi / state.Ly * (yadj - yc))
			hinit_rhs.data[k,:,:,:,:,:] = state.H0 - dhfactors[k] * dh * np.exp(-0.5 * (xprime * xprime + yprime * yprime))
		
		#parameters = []
		#parameters.append(('Lx',state.Lx))
		#parameters.append(('Ly',state.Ly))
		#parameters.append(('sigmax',sigmax))
		#parameters.append(('sigmay',sigmay))
		#parameters.append(('xc',xc))
		#parameters.append(('yc',yc))
		#parameters.append(('g',state.g))
		#parameters.append(('f',state.f))
		#parameters.append(('dh',dh))
		#parameters.append(('H0',state.H0))
		#parameters.append(('dhfactors',dhfactors))
		#ufunc = Functional('ml-nlswe.functional','zeroproj',1,geometrylist=['detJ','coords','J'],parameters=parameters) #uinit zeroproj
		#hfunc = Functional('ml-nlswe.functional','hinitsingle_coupled',1,geometrylist=['detJ','coords'],parameters=parameters) #zeroproj hinit
	##########
	
	state.hinit = OneForm(state.mesh,['hinit',['hinit',]],state.l2)
	state.uinit = OneForm(state.mesh,['uinit',['uinit',]],state.hdiv)

	AssembleForm(state.hinit,hfunc,state.quadrature,0)
	AssembleForm(state.uinit,ufunc,state.quadrature,0)

	state.hdivmass_sys.solve(state.uinit,state.u,kspinfo=state.kspinfo)
	state.l2mass_sys.solve(state.hinit,state.h,kspinfo=state.kspinfo)
	#set initial values for u and h
	#do this by assembling 1-forms for u and h, and then solving mass matrix system
	#ie projection
	#for an interpolatory basis, could just interpolate to nodal values
	#but this procedure is more general
	state.htemp.vector = state.h.vector.copy()
	state.utemp.vector = state.u.vector.copy()

def create_mesh_and_spaces(state):
	#create elements
	cgx,cgy,cgz,dgx,dgy,dgz = get_cgdg(state.spacetype,xbc=state.xbc,ybc=state.ybc,xorder=state.elem_order,yorder=state.elem_order)
	l2elem = TensorProductElement([dgx,dgy])
	vecxelem = TensorProductElement([cgx,dgy]) 
	vecyelem = TensorProductElement([dgx,cgy])
	veczelem = TensorProductElement([dgx,dgy,EmptyElement(1)])
	h1elem = TensorProductElement([cgx,cgy])
	
	#create mesh
	geom = UniformVariableBox(l2elem.get_ref_cell(),state.quadrature,state.dx,state.dy,state.dz)
	state.mesh = Mesh(3,geom,[state.nx,state.ny,state.nz],[state.xbc,state.ybc,state.zbc])
	
	#Create  spaces
	state.h1 = FunctionSpace(state.mesh,h1elem,'H1')
	#state.hdiv = VectorFunctionSpace(state.mesh,[vecxelem,vecyelem,veczelem],'Hdiv')
	state.hdiv = VectorFunctionSpace(state.mesh,[vecxelem,vecyelem,veczelem],'Hdiv')
	state.l2 = FunctionSpace(state.mesh,l2elem,'L2')

def destroy(state):
	#fields
	state.h.destroy()
	state.u.destroy()
	state.q.destroy()
	state.F.destroy()
	state.htemp.destroy()
	state.utemp.destroy()	
	for temp in state.ku_is:
		temp.destroy()
	for temp in state.kh_is:
		temp.destroy()
	state.urhs.destroy()
	state.hrhs.destroy()
	
	#forms
	state.l2mass.destroy()
	state.hdivmass.destroy()
	state.qmat.destroy()
	
	state.hu.destroy()
	state.curlu.destroy()
	state.qFt.destroy()
	state.gradB.destroy()
	state.divF.destroy()
	state.hinit.destroy()
	state.uinit.destroy()
	
	state.peform.destroy()
	state.massform.destroy()
	state.keform.destroy()
	state.pvform.destroy()
	state.enstrophyform.destroy()
	state.volumeform.destroy()
	state.bpeform.destroy()
	
	#linear systems
	state.l2mass_sys.destroy()
	state.hdivmass_sys.destroy()
	state.qsys.destroy()

	#function spaces
	state.h1.destroy()
	state.l2.destroy()
	state.hdiv.destroy()
	
	#mesh
	state.mesh.destroy()
	
def create_2_forms(state):
	if not state.mf:
		l2massfunc = Functional('standard.functional','l2mass',2,geometrylist=['detJinv'],assemblytype='standard')
		hdivmassfunc = Functional('standard.functional','hdivmass',2,geometrylist=['JTJdetJinv'],assemblytype='standard')
		state.l2mass = TwoForm(state.mesh,('l2mass',('l2mass',)),state.l2,state.l2)
		state.hdivmass = TwoForm(state.mesh,('hdivmass',('uumass','uvmass','vumass','vvmass')),state.hdiv,state.hdiv) #,mattype='monolithic'
		#FIX NAMES FOR HDIV MASS...
		AssembleForm(state.l2mass,l2massfunc,state.quadrature,0,0)
		AssembleForm(state.hdivmass,hdivmassfunc,state.quadrature,0,0)
	else:
		l2massfunc = Functional('tensor-product-matrixfree.functional','l2mass',2,geometrylist=['detJinv'],assemblytype='tensor-product-matrixfree')
		hdivmassfunc = Functional('tensor-product-matrixfree','hdivmass',2,geometrylist=['JTJdetJinv'],assemblytype='tensor-product-matrixfree')
		state.l2mass = TwoForm(state.mesh,('l2mass',('l2mass',)),state.l2,state.l2,mattype='shell',multfuncs=[[l2massfunc,],],multquad=state.quadrature)
		state.hdivmass = TwoForm(state.mesh,('hdivmass',('uumass','uvmass','vumass','vvmass')),state.hdiv,state.hdiv,mattype='shell',multfuncs=[[hdivmassfunc,],],multquad=state.quadrature)
		#FIX NAMES FOR HDIV MASS...

	state.qfunc = Functional('ml-nlswe.functional','qfunc',2,fields=(state.htemp,),fsi=(0,))
	state.qmat = TwoForm(state.mesh,('qmat',('qmat',)),state.h1,state.h1) #,mattype='monolithic'
	

def create_1_forms(state):
	if state.coupled:
		state.gradBfunc = Functional('ml-nlswe.functional','gradB_coupled',1,geometrylist=['detJ','J'],parameters=[('g',state.g),('hcoeff',state.hcoeff)],fields=(state.utemp,state.htemp),fsi=(0,0)) #uinit zeroproj hinit
	else:
		state.gradBfunc = Functional('ml-nlswe.functional','gradB',1,geometrylist=['detJ','J'],parameters=[('g',state.g)],fields=(state.utemp,state.htemp),fsi=(0,0)) #uinit zeroproj hinit
	state.hufunc = Functional('ml-nlswe.functional','hu',1,geometrylist=['detJ','J'],fields=(state.htemp,state.utemp),fsi=(0,0)) #uinit zeroproj hinit
	state.curlufunc = Functional('ml-nlswe.functional','curlu',1,geometrylist=['detJ','J','JTinv'],parameters=[('f',state.f)],fields=(state.utemp,),fsi=(0,)) #uinit zeroproj hinit
	state.qFtfunc = Functional('ml-nlswe.functional','qFT',1,geometrylist=['detJ','J','rotJ'],fields=(state.q,state.F),fsi=(0,0)) #uinit zeroproj hinit
	state.divFfunc = Functional('ml-nlswe.functional','divF',1,geometrylist=['detJ','J'],fields=(state.F,),fsi=(0,)) #uinit zeroproj hinit

	state.hu = OneForm(state.mesh,['hu',['hu','hv']],state.hdiv)
	state.curlu = OneForm(state.mesh,['curlu',['curlu',]],state.h1)
	state.qFt = OneForm(state.mesh,['qFT',['qFTu','qFTv']],state.hdiv)
	state.gradB = OneForm(state.mesh,['gradB',['gradBu','gradBv']],state.hdiv)
	state.divF = OneForm(state.mesh,['divF',['divF',]],state.l2)

def create_0_forms(state):
	if state.coupled:
		state.pefunc = Functional('ml-nlswe.functional','zeroform_pe_coupled',0,parameters=[('rholist',state.rholist),],geometrylist=['detJ','J'],fields=(state.htemp,),fsi=(0,))
		state.kefunc = Functional('ml-nlswe.functional','zeroform_ke_coupled',0,parameters=[('rholist',state.rholist),],geometrylist=['detJ','J'],fields=(state.utemp,state.htemp),fsi=(0,0)) #note field order- u, h!
		state.enstrophyfunc = Functional('ml-nlswe.functional','zeroform_enstrophy_coupled',0,parameters=[('rholist',state.rholist),],geometrylist=['detJ','J'],fields=(state.htemp,state.q),fsi=(0,0))
		state.peform = ZeroForm(state.mesh,'pe')	
		state.keform = ZeroForm(state.mesh,'ke')
	else:
		state.pefunc = Functional('ml-nlswe.functional','zeroform_pe',0,geometrylist=['detJ','J'],fields=(state.htemp,),fsi=(0,))
		state.kefunc = Functional('ml-nlswe.functional','zeroform_ke',0,geometrylist=['detJ','J'],fields=(state.utemp,state.htemp),fsi=(0,0)) #note field order- u, h!
		state.enstrophyfunc = Functional('ml-nlswe.functional','zeroform_enstrophy',0,geometrylist=['detJ','J'],fields=(state.htemp,state.q),fsi=(0,0))
		state.peform = ZeroForm(state.mesh,'pe',layered=True)	
		state.keform = ZeroForm(state.mesh,'ke',layered=True)
	state.pvfunc = Functional('ml-nlswe.functional','zeroform_pv',0,geometrylist=['detJ','J'],fields=(state.htemp,state.q),fsi=(0,0))
	state.massfunc = Functional('ml-nlswe.functional','zeroform_mass',0,geometrylist=['detJ','J'],fields=(state.htemp,),fsi=(0,))
	state.volumefunc = Functional('ml-nlswe.functional','zeroform_volume',0,geometrylist=['detJ','J'])
		
	state.peform = ZeroForm(state.mesh,'pe')	
	state.keform = ZeroForm(state.mesh,'ke')
	
	state.enstrophyform = ZeroForm(state.mesh,'enstrophy',layered=True)
	state.volumeform = ZeroForm(state.mesh,'volume',layered=True)	
	state.massform = ZeroForm(state.mesh,'mass',layered=True)
	state.pvform = ZeroForm(state.mesh,'pv',layered=True)
	
	state.massstat = np.zeros((N/nstat + 1,state.nz))
	if state.coupled:
		state.kestat = np.zeros((N/nstat + 1))
		state.pestat = np.zeros((N/nstat + 1))
		state.apestat = np.zeros((N/nstat + 1))
		state.energystat = np.zeros((N/nstat + 1))
		state.availenergystat = np.zeros((N/nstat + 1))
	else:
		state.kestat = np.zeros((N/nstat + 1,state.nz))
		state.pestat = np.zeros((N/nstat + 1,state.nz))
		state.apestat = np.zeros((N/nstat + 1,state.nz))
		state.energystat = np.zeros((N/nstat + 1,state.nz))
		state.availenergystat = np.zeros((N/nstat + 1,state.nz))
	state.pvstat = np.zeros((N/nstat + 1,state.nz))
	state.enstrophystat = np.zeros((N/nstat + 1,state.nz))

def create_fields(state):
	state.h = Field(('h',['h',]),state.l2)
	state.u = Field(('u',['u','v','w']),state.hdiv)
	state.q = Field(('q',['q',]),state.h1)
	state.F = Field(('F',['Fu','Fv','Fw']),state.hdiv)


def create_solvers(state):
	state.l2mass_sys = LinearSystem(state.l2mass,state.solver,state.pc,bcs=None,rtol=1e-20)
	if not state.mf:
		state.hdivmass_sys = LinearSystem(state.hdivmass,state.solver,state.pc,bcs=None,rtol=1e-20,fieldsplit=True)
	else:
		state.hdivmass_sys = LinearSystem(state.hdivmass,state.solver,state.pc,bcs=None,rtol=1e-20,fieldsplit=False)
	#state.qsys = LinearSystem(state.qmat,state.h1,solver,pc,bcs=None)

###############################################################################################
#Load Configuration
state = State()
rank = PETSc.COMM_WORLD.Get_rank() 

OptDB = PETSc.Options()
state.spacetype = OptDB.getString('spacetype', 'FEEC')
state.elem_order = OptDB.getInt('elemorder', 2)
state.nx = OptDB.getInt('nx', 32)
state.ny = OptDB.getInt('ny', 32)
state.nz = OptDB.getInt('nz', 1)
state.simname = OptDB.getString('simname', 'test')
state.initcond = OptDB.getString('initcond', 'doublevortex') #singlevortex doublevortex gravitywave_coupled
state.coupled = OptDB.getBool('coupled', False)
state.mf = OptDB.getBool('matrixfree', False)

state.diagnostics = OptDB.getBool('diagnostics', False)

state.kspinfo = OptDB.getBool('kspinfo', False)

state.solver = OptDB.getString('iterative','cg') #fgmres
if state.spacetype == 'MGD' or state.elem_order == 1:
	state.pc = OptDB.getString('precon','jacobi')
else:
	state.pc = OptDB.getString('precon','ilu')
if state.mf:
	state.pc = OptDB.getString('precon','none')
	
quadchoice = 'gauss'
quadorder = 1 + state.elem_order
quad = get_quadrature(quadchoice)
#state.quadrature = TensorProductQuadrature([quad,quad,EmptyQuad],[quadorder,quadorder,1])
state.quadrature = TensorProductQuadrature([quad,quad],[quadorder,quadorder])

output_matrices = OptDB.getBool('outputMatrix', False)
output_fspaces = OptDB.getBool('outputSpaces', False)
output_ksp = OptDB.getBool('outputKSP', False)
output_mesh = OptDB.getBool('outputMesh', True)

state.f = 0.00006147 #0.00006147 #0.00006147 0.0001
state.g = 9.81
state.Ly = 5000.0 * 1000.0
state.Lx = 5000.0 * 1000.0
state.C = 0.1 / state.elem_order #ADJUST THIS BASED ON ELEM ORDER
state.H0 = 750.0

if state.coupled:
	#rholist = OptDB.getString('rholist','1.0,0.9,0.8,0.7')
	#rholist = rholist.split(',')
	state.rholist = np.linspace(1.0,0.7,state.nz) #np.array([float(x) for x in rholist])
	state.hcoeff = np.ones((state.nz,state.nz))
	for i in range(state.nz):
		for j in range(i+1,state.nz):
			state.hcoeff[i,j] = state.rholist[j]/state.rholist[i]
			#print i,j,state.rholist[i],state.rholist[j],state.hcoeff[i,j]
	print state.hcoeff

N = 50
nout = 10
nstat = 1
state.dx = OptDB.getScalar('dx',state.Lx/state.nx)
state.dy = OptDB.getScalar('dy',state.Ly/state.ny) #100000
state.dz = 2. #OptDB.getScalar('dz',20000./state.nz)
state.dt = min(state.dx,state.dy) * state.C / sqrt(state.g * state.H0)
print state.dx/1000.0,state.dy/1000.0,state.dz/1000.0,state.dt
state.xbc = 'periodic'
state.ybc = 'periodic'
state.zbc = 'dirichlet'

#create mesh and function spaces (l2,hdiv,h1)
create_mesh_and_spaces(state)
if output_fspaces:
	state.h1.output()
	state.l2.output()
	state.hdiv.output()

#create fields to hold solutions at various time steps
create_fields(state)

#create auxiliary fields for time stepping auxiliary variables
create_timestep_aux_rk(state)

#create forms
create_2_forms(state)
if output_matrices:
	state.l2mass.output()
	state.hdivmass.output()
create_1_forms(state)
create_0_forms(state)

#create solvers
create_solvers(state)
if output_ksp:
	state.l2mass_sys.output()
	state.hdivmass_sys.output()
	state.qsys.output()

#initial condition
initial_condition(state)
diagnose(state)
		
#create output and output initial condition
ViewHDF5 = PETSc.ViewerHDF5().create('results.ml-nlswe.h5', mode=PETSc.Viewer.Mode.WRITE,comm= PETSc.COMM_WORLD)
#ViewHDF5.setTimestep(0)
if state.diagnostics:
	output_rhs(state,0,ViewHDF5)
state.mesh.output(ViewHDF5)
output_fields(state,0,ViewHDF5)
plot_fields(state,0)

#compute initial condition statistics
compute_statistics(state,0)

for i in xrange(1,N+1):
	print 'step',i
	#take an explicit rk step
	take_explicit_rk_step(state)
	
	if state.diagnostics:
		output_rhs(state,i,ViewHDF5)
		
	if (i%nout == 0):
		output_fields(state,i/nout,ViewHDF5)
		
	if (i%nstat == 0):
		compute_statistics(state,i/nstat)
		
#close output
ViewHDF5.destroy()

#do plotting if needed
if (rank == 0):
	plot_stats(state)
	for i in xrange(0,N+1):
		if state.diagnostics:
			plot_rhs(state,i)
		if (i%nout == 0):
			plot_fields(state,i/nout)

destroy(state)
