#import sys, petsc4py
#initialize petsc
#petsc4py.init(sys.argv)
#from petsc4py import PETSc
from common import *
#from configuration import *
from mesh import *
from solver import *
from output import *
from plotting import *
from basis import *
from revisedform import *
#from functionals import *
from field import *
import h5py
from functionspace import *

#pde = pde(PETSc)
OptDB = PETSc.Options()
#Load Configuration
#load_configuration(pde)
spacetype = OptDB.getString('spacetype', 'FEEC')
elem_order = OptDB.getInt('elemorder', 1)
geomtype = OptDB.getString('geomtype', 'uniform')
nx = OptDB.getInt('nx', 64)
ny = OptDB.getInt('ny', 64)
nz = OptDB.getInt('nz', 8)
dx = OptDB.getScalar('dx',1.0/nx)
dy = OptDB.getScalar('dy',1.0/ny)
dz = OptDB.getScalar('dz',1.0/nz)
ndims = OptDB.getInt('ndims', 2)
simname = OptDB.getString('simname', 'h1helmholtz')
plotsoln = OptDB.getBool('plot', True)
computenorms = OptDB.getBool('computenorms', True)
kspinfo = OptDB.getBool('kspinfo', True)
output_matrices = OptDB.getBool('outputMatrix', False)
output_fspaces = OptDB.getBool('outputSpaces', False)
output_ksp = OptDB.getBool('outputKSP', False)
output_mesh = OptDB.getBool('outputMesh', False)
solver = OptDB.getString('iterative','preonly') #preonly lgmres
pc = OptDB.getString('precon','lu') #asm ilu gamg- much better sor gasm
xbc = OptDB.getString('xbc', 'periodic') #periodic dirichlet 
ybc = OptDB.getString('ybc', 'periodic') #periodic dirichlet
zbc = OptDB.getString('zbc', 'periodic') #periodic dirichlet
quadchoice = OptDB.getString('quadchoice', 'gauss')
quadorder = 1 + elem_order
outdir = OptDB.getString('outdir', '')
plot_mesh = OptDB.getBool('plotMesh', False)
assemblytype = OptDB.getString('assemblytype','standard') #tensor-product tensor-product-matrixfree
mattype = OptDB.getString('mattype','block') #block monolithic shell

#create quadrature, elements and mesh
quad = get_quadrature(quadchoice)
cgx,cgy,cgz,dgx,dgy,dgz = get_cgdg(spacetype,xbc=xbc,ybc=ybc,zbc=zbc,xorder=elem_order,yorder=elem_order,zorder=elem_order)
dxs = []
nxs = []
xbcs = []
if geomtype == 'uniform':	
	a = 1. / np.pi
if geomtype == 'distorted':
	a = 6273000.
	dx = np.pi * a / (nx / 2.) 
	dy = np.pi * a / (ny / 2.)
	dz = np.pi * a / (nz / 2.)
if ndims == 1:
	quadrature = TensorProductQuadrature([quad,],[quadorder,])
	elem = TensorProductElement([cgx,])
	dxs.append(dx)
	nxs.append(nx)
	xbcs.append(xbc)
if ndims == 2:
	quadrature = TensorProductQuadrature([quad,quad],[quadorder,quadorder])
	elem = TensorProductElement([cgx,cgy])
	dxs.append(dx)
	dxs.append(dy)
	nxs.append(nx)
	nxs.append(ny)
	xbcs.append(xbc)
	xbcs.append(ybc)
if ndims == 3:
	quadrature = TensorProductQuadrature([quad,quad,quad],[quadorder,quadorder,quadorder])
	elem = TensorProductElement([cgx,cgy,cgz])
	dxs.append(dx)
	dxs.append(dy)
	dxs.append(dz)
	nxs.append(nx)
	nxs.append(ny)
	nxs.append(nz)
	xbcs.append(xbc)
	xbcs.append(ybc)
	xbcs.append(zbc)
if geomtype == 'uniform':	
	geom = UniformVariableBox(elem.get_ref_cell(),quadrature,*dxs) 
if geomtype == 'distorted':
	geom = DistortedPeriodic2DBox(elem.get_ref_cell(),a,quadrature,*dxs) 
mesh = Mesh(ndims,geom,nxs,xbcs)

#create function space
fspace = FunctionSpace(mesh,elem,'H1')

#create matrices and vectors
Aname = ('A',[['A00',],])
massname = ('mass',[['mass00',],])
bname = ('b',['b',])
solnname = ('solnform',['solnform',]) 

massfunc = Functional(assemblytype + '.functional','h1mass',2,geometrylist=['detJ'],assemblytype=assemblytype)
helmholtzfunc_signflip = Functional(assemblytype + '.functional','h1helmholtz_signflip',2,geometrylist=['JinvJTinvdetJ','detJ'],assemblytype=assemblytype)
helmholtzfunc = Functional(assemblytype + '.functional','h1helmholtz',2,geometrylist=['JinvJTinvdetJ','detJ'],assemblytype=assemblytype)

if assemblytype == 'tensor-product-matrixfree':
	assemblytype = 'tensor-product'
	
rhscoeff = Coefficient('coeff','scalar',np.zeros(mesh.geometry.detJ.shape),mesh)
solncoeff = Coefficient('coeff','scalar',np.zeros(mesh.geometry.detJ.shape),mesh)
helmholtzrhs = Functional(assemblytype + '.functional','h1proj_coeff',1,geometrylist=['detJ'],coefficientlist=[rhscoeff,],assemblytype=assemblytype)
helmholtzsoln = Functional(assemblytype + '.functional','h1proj_coeff',1,geometrylist=['detJ',],coefficientlist=[solncoeff,],assemblytype=assemblytype)


if mattype == 'shell':
	if ndims == 1 and xbc == 'dirichlet':
		twoform = TwoForm(mesh,Aname,fspace,fspace,mattype=mattype,multfuncs=[[helmholtzfunc,],],multquad=quadrature)
	else:
		twoform = TwoForm(mesh,Aname,fspace,fspace,mattype=mattype,multfuncs=[[helmholtzfunc_signflip,],],multquad=quadrature)
	mass = TwoForm(mesh,massname,fspace,fspace,mattype=mattype,multfuncs=[[massfunc,],],multquad=quadrature)
else:
	twoform = TwoForm(mesh,Aname,fspace,fspace,mattype=mattype)
	mass = TwoForm(mesh,massname,fspace,fspace,mattype=mattype)

oneform = OneForm(mesh,bname,fspace)
solnform = OneForm(mesh,solnname,fspace)

#fill matrices and vectors
if ndims == 1:
	x = mesh.geometry.quadcoords[:,:,:,:,:,:,0]
	if xbc == 'dirichlet':
		rhscoeff.data = x
		solncoeff.data = x - np.sinh(x)/np.sinh(1.)
	else:
		rhscoeff.data = (-144. /  a  / a + 4.) * np.sin(6. * x / a)
		solncoeff.data = 4. * np.sin(6. * x / a)
if ndims == 2:
	x = mesh.geometry.quadcoords[:,:,:,:,:,:,0]
	y = mesh.geometry.quadcoords[:,:,:,:,:,:,1]
	rhscoeff.data = (-80. /  a  / a + 4.) * np.sin(2. * x / a) * np.sin(4. * y / a )
	solncoeff.data = 4. * np.sin(2. * x / a) * np.sin(4. * y / a )
if ndims == 3:
	x = mesh.geometry.quadcoords[:,:,:,:,:,:,0]
	y = mesh.geometry.quadcoords[:,:,:,:,:,:,1]
	z = mesh.geometry.quadcoords[:,:,:,:,:,:,2]
	rhscoeff.data = (-224. /  a  / a + 4.) * np.sin(2. * x / a) * np.sin(4. * y / a ) * np.sin(6. * z / a )
	solncoeff.data = 4. * np.sin(2. * x / a) * np.sin(4. * y / a ) * np.sin(6. * z / a )

if not mattype == 'shell':
	if ndims == 1 and xbc == 'dirichlet':
		AssembleForm(twoform,helmholtzfunc,quadrature,0,0)
	else:
		AssembleForm(twoform,helmholtzfunc_signflip,quadrature,0,0)
	AssembleForm(mass,massfunc,quadrature,0,0)
AssembleForm(oneform,helmholtzrhs,quadrature,0)
AssembleForm(solnform,helmholtzsoln,quadrature,0)

#create fields to hold solution, and actual solution
soln = Field(('x',['x',]),fspace)
actualsoln = Field(('xsoln',['xsoln',]),fspace)

#create solver and boundary conditions
bcs = []
if xbc == 'dirichlet':
	bcs.append(EssentialBC(mesh,soln,'x',0.0,0,0))
if ybc == 'dirichlet' and ndims > 1:
	bcs.append(EssentialBC(mesh,soln,'y',0.0,0,0))
if zbc == 'dirichlet' and ndims > 2:
	bcs.append(EssentialBC(mesh,soln,'z',0.0,0,0))
if (len(bcs)==0):
	bcs = None
linsys = LinearSystem(twoform,solver,pc,bcs=bcs,rtol=1e-15)

#solve system
linsys.solve(oneform,soln,kspinfo=kspinfo)

#get the exact solution
masssys = LinearSystem(mass,solver,pc,bcs=bcs,rtol=1e-15)
masssys.solve(solnform,actualsoln,kspinfo=kspinfo)

#output results
ViewHDF5 = PETSc.Viewer()  
ViewHDF5.createHDF5(outdir + 'results.' + simname + '.h5', mode=PETSc.Viewer.Mode.WRITE,comm= PETSc.COMM_WORLD)

#output rhs and soln
soln.output(ViewHDF5)
actualsoln.output(ViewHDF5)
oneform.output(ViewHDF5)
solnform.output(ViewHDF5)

ViewHDF5.destroy()

if computenorms:
	npts = np.prod(mesh.geometry.detJ.shape)
	normfunc = Functional(assemblytype + '.functional','h1norm',0,fields=(soln,),fsi=(0,),coefficientlist=[solncoeff,],assemblytype=assemblytype)
	l2norm = ZeroForm(mesh,'l2norm')
	AssembleForm(l2norm,normfunc,quadrature)
	l2 = sqrt(l2norm.value / npts)
	print l2

	rank = PETSc.COMM_WORLD.Get_rank() 
	if rank == 0:
		f = h5py.File("results." + simname + ".h5", "r+")
		f['x'].attrs['norm'] = l2
		f.close()
		#['x']
		
if output_matrices:
	twoform.output()
	mass.output()

if output_fspaces:
	fspace.output()
	
if output_ksp:
	linsys.output()
	masssys.output()

#plot stuff
rank = PETSc.COMM_WORLD.Get_rank() 
if rank == 0 and plotsoln:

	h = soln.eval_at_quad_pts(0,quadrature)
	hsoln = actualsoln.eval_at_quad_pts(0,quadrature)
	quads = mesh.get_plotting_quad_coords(quadrature)
	
	if ndims == 1:
		quadx = np.ravel(quads[:,:,:,:,:,:,0])
		
		plotheader = spacetype + '.' + simname
		spatialplot_1d(quadx,np.ravel(h.data),plotheader + '.h.' + str(nx))
		spatialplot_1d(quadx,np.ravel(hsoln.data),plotheader + '.hsoln.' + str(nx))	
		spatialplot_1d(quadx,np.ravel(hsoln.data - h.data),plotheader + '.hdiff.' + str(nx))
		
	if ndims == 2:
		quadx = np.ravel(quads[:,:,:,:,:,:,0])
		quady = np.ravel(quads[:,:,:,:,:,:,1])
		
		plotheader = spacetype + '.' + simname
		spatialplot_2d(quadx,quady,np.ravel(h.data),plotheader + '.h.' + str(nx))
		spatialplot_2d(quadx,quady,np.ravel(hsoln.data),plotheader + '.hsoln.' + str(nx))	
		spatialplot_2d(quadx,quady,np.ravel(hsoln.data - h.data),plotheader + '.hdiff.' + str(nx))
		
	if ndims == 3:
		quadx = quads[:,:,:,:,:,:,0]
		quady = quads[:,:,:,:,:,:,1]
		quadz = quads[:,:,:,:,:,:,2]

		newshape = (nz,ny,nx,quadrature.pts.shape[0],quadrature.pts.shape[1],quadrature.pts.shape[2])
		h.data = np.reshape(h.data,newshape)
		hsoln.data = np.reshape(hsoln.data,newshape)
	
		plotheader = spacetype + '.' + simname

		spatialplot_3d_slices(quadx,quady,h.data,'h',plotheader,nz)
		spatialplot_3d_slices(quadx,quady,hsoln.data,'hsoln',plotheader,nz)
		spatialplot_3d_slices(quadx,quady,hsoln.data - h.data,'hdiff',plotheader,nz)


linsys.destroy()
masssys.destroy()

twoform.destroy()
mass.destroy()
solnform.destroy()
oneform.destroy()

fspace.destroy()

soln.destroy()
actualsoln.destroy()

mesh.destroy()
