import numpy as np
import matplotlib.pyplot as plt
from plotting import *
import h5py
import subprocess

grid_sizes = [16,24,32,40,48]
discrets = ['FEEC','MGD'] #'FEEC','FEEC-Gauss','FEEC-UniformClosed','FEEC-UniformOpen','MSE','MSE-Uniform','MGD'] 
resolution = 1.0/np.array(grid_sizes)
orderlist = [(5,4,3,2,1),(5,4,3,2,1),(5,4,3,2,1),(5,4,3,2),(5,4,3,2),(5,4,3,2),(5,4,3,2),(5,4,3,2),]
names = [#
#'QB5','QB4','QB3','QB2',
'Q5','Q4','Q3','Q2','Q1',
#'QG5','QG4','QG3','QG2',
#'QUC5','QUC4','QUC3','QUC2',
#'QUO5','QUO4','QUO3','QUO2',
#'MSE5','MSE4','MSE3','MSE2','MSE1',
#'MSEU5','MSEU4','MSEU3','MSEU2',
'MGD5','MGD4','MGD3','MGD2','MGD1'
]
xbcs = ['periodic',] #'periodic'] #'periodic'
ybcs = ['periodic',] #'periodic'] #

run_model = True

for xbc in xbcs:
	for ybc in ybcs:
		l2_h_errors = np.zeros((len(names),len(grid_sizes)))
		#linf_h_errors = np.zeros((len(names),len(grid_sizes)))
		ndofs_h = np.zeros((len(names),len(grid_sizes)))
			
		j = -1
		for spacetype,orders in zip(discrets,orderlist):
			for order in orders:
				j = j +1
				for i,grid_size in enumerate(grid_sizes):
					dx = 1.0/grid_size
					plotsoln = "False"
					if grid_size == grid_sizes[-1]:
						plotsoln = "False"
					
					#REMOVE THIS ONCE BOUNDARY ELEMENTS FOR MGD ARE IMPLEMENTED!
					if (xbc == 'dirichlet' or ybc == 'dirichlet') and spacetype == 'MGD':
						continue
					
					if spacetype == 'MGD' and order%2 == 0:
						continue
						
					print '################### running ############## '
					print spacetype,grid_size
					simname = 'helmholtz.' + spacetype + '.' + xbc + '.' + ybc + '.' + str(order) + '.' + str(grid_size)
					if run_model:
						subprocess.call(["mpirun","-n",str(1),"python","h1helmholtz.py","-nx",str(grid_size), "-ny",str(grid_size),
						"-spacetype" ,spacetype,"-simname", simname ,"-plot",plotsoln,"-elemorder",str(order),"-xbc",xbc,"-ybc",ybc,
						"-computenorms","True","-kspinfo","True"])
					#ADD IN MPIRUN MULTI PROCESS OPTION!
					
					#Load results
					f = h5py.File("results." + simname + ".h5", "r")
					l2 = f['x'].attrs['norm']
					ndofsh = np.array(f['x']).shape[0]
					f.close()
					l2_h_errors[j,i] = l2
					ndofs_h[j,i] = ndofsh
					
					print spacetype,order,grid_size,ndofsh,l2
					print '################### completed ############## '
		
		#REMOVE THIS ONCE BOUNDARY ELEMENTS FOR MGD ARE IMPLEMENTED!
		#if (xbc == 'dirichlet' or ybc == 'dirichlet'):
			#l2_h_errors = l2_h_errors[:-4,:]
			#ndofs_h = ndofs_h[:-4,:]
			
		orderlist1 = [5,4,3,2,1]
		for i in range(len(orderlist1)):
			revised_ndofs_h =  ndofs_h[i::len(orderlist1),:]
			revised_errors =  l2_h_errors[i::len(orderlist1),:]
			revised_names = names[i::len(orderlist1)]
			#This gets rid of even order elements for MGD, which don't exist!
			#Can get rid of xbc and ybc checks once boundary elements for MGD are implemented
			if (orderlist1[i]%2 == 0) and xbc == 'periodic' and ybc == 'periodic':
				revised_ndofs_h = revised_ndofs_h[:-1,:]
				revised_errors = revised_errors[:-1,:]
				revised_names = revised_names[:-1]
			print i,orderlist1[i],revised_names
			convergence_plot(resolution,revised_errors,revised_names,'l2.h.' + xbc + '.' + ybc + '.' + str(orderlist1[i]) + '.2D')
			convergence_plot_ndofs(revised_ndofs_h,revised_errors,revised_names,'l2.h.ndofs.' + xbc + '.' + ybc + '.' + str(orderlist1[i]) + '.2D')
		#convergence_plot(resolution,linf_h_errors,names,'linf.h.helmholtz.1D')
	
